# 0.23.2 iOS Beta (2020-04-28)

### Bug fixes
- Hiding the Tab Bar on the Updated Forgot Password Screen ([fb5c547d](/fb5c547dc644d701981bc355b32f2c6de300a7fc))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([451663ae](/451663aee6bb9d1db5029156209a9576e477cf7f))

# 0.23.1 iOS Beta (2020-04-27)

### Bug fixes
- **ios:** remove pod install on ci ([18029809](/180298098076398b22c0dab6692ff2913db0dde0))

### Other work

# 0.23.0 iOS Beta (2020-04-27)

### Features
- **ci:** enable testflight upload ([7df4b40d](/7df4b40d0396cb0e0c961b36f09f0b5e2cf78173))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([7b7d61d1](/7b7d61d15031f275436fabafe323172941023b16))

### Other work
- enable upload to testflight ([2c37e3f9](/2c37e3f98ffb88febe5b9454499f86fa43570186))

# 0.22.1 iOS Beta (2020-04-24)

### Features
- **ci:** enable app store uploads in ci ([c290acc6](/c290acc600bc1ee560552cc5ba4554ee44e388fb))
- **notifications:** adds default notification channel to support android notification banners ([73d39663](/73d39663d5362524392498f92dad46bce1e4e488))
- **care:** add geolocation data and allow sorting by wait or distance ([a07c399f](/a07c399f62aeaaae8e440bf3fa196200c867763c))
- **care:** allows refresh of user location when selecting sort by location ([16cb5e68](/16cb5e68603eb3f62a9e2444cd2bf31a6d60c7cc))
- **class:** update background image for class confirmation ([61f27509](/61f2750941d971360eb54849f93d30af12f4df50))
- **login:** update color of the create account button ([04d065ab](/04d065ab1072d7873af4b6fb4a44c2bc8a5efbad))
- **biocircuit:** update intro text ([5afee308](/5afee308424a92d48c0a9daf35b259a6fa1b54f2))
- **ui updates:** Updating the Home Screen and Class Reservation Modal Designs ([33a683ce](/33a683ce1679efaaa83ff52a38b1d2f23ce4199f))
- **forgot password:** Setting up Forgot Password as a Standalone Screen ([b1e421bb](/b1e421bb31e0a933a3461da5e9f40d78a1fc04de))
- **care:** add specific location images for urgent care clinics ([b810ac7c](/b810ac7cc86495dbb32275e66e7424f1bc036e7a))
- **biocircuit:** change heading on biocircuit intro ([11e5f685](/11e5f6852f86d66cf1c23a66c5c9c7265ff5b2bd))
- **create account:** show password rules and disable upload to google in CI` ([494f7efa](/494f7efaeadc897367e79048513f780c3eb9159b))
- **CareScreen:** Geolocation and Data Refetching Enhancements ([ed1cc913](/ed1cc913529d0f6803b56cff1e361addf9c0a955))

### Bug fixes
- **care:** fix image for sand canyon ([2f863a1b](/2f863a1b56ac67575b856fb7f9a841950fca5cdd))
- iOS testflight upload ([4c422c25](/4c422c2510875af97b5fcbdaea46ea9e6d811009))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([5837be5d](/5837be5d7f6525f98f9488446e009a0c8b5480af))
- **changelog:** Update Android CHANGELOG [skip ci] ([eafe66a3](/eafe66a3c875422d34fb8fd3bb94d2496b5197bd))

### Other work
- RC 4/24/2020 ([f19e08a3](/f19e08a3e1e1c2b61cea5a6ebe156a7507702b6e))
- RC 4-24-2020 ([9212a29e](/9212a29e49456deeabf796c4d5898348d2c07c01))

# 0.9.1 iOS Beta (2020-04-17)

### Bug fixes
- **notifications:** updates to support ios background notifications and move towards complete RNN removal ([b30df7a1](/b30df7a1edd38852b0d3b37e8e5787e3a2863633))

### Building system
- **changelog:** Update Android CHANGELOG [skip ci] ([89c47a71](/89c47a712effff9774f5a6998cbaeaaffa87d18d))

### Other work

# 0.9.0 iOS Beta (2020-04-14)

### Features
- adds comment ([056009e3](/056009e3b787611f64b2d86ec8c7c1828798c992))

### Building system
- **chat:** remove tab bar visiblity and add consitent padding ([34e7da58](/34e7da58dac9cca783bcd8c3c852156265230e18))
- **changelog:** Update Android CHANGELOG [skip ci] ([12f83053](/12f83053fe29f9b627a921cf59090f43f0ceb0cb))
- **changelog:** Update iOS CHANGELOG [skip ci] ([8d160f56](/8d160f56201dbdb64c9b8476f61fb7a4fe0f1070))
- **changelog:** Update Android CHANGELOG [skip ci] ([1c3970ea](/1c3970eafb70cd123b0dff535553e08e7ca85278))

### Other work
- RC 2 4/14/2020 ([940ad4ed](/940ad4edf7414d5b87795ded5c4d236e03aad6a9))
- RC 2 4/14/2020 ([a2c85bd8](/a2c85bd83c459d41c9623c39281ae4579340b418))

# 0.9.0 iOS Beta (2020-04-14)

### Features
- adds comment ([056009e3](/056009e3b787611f64b2d86ec8c7c1828798c992))

### Building system
- **chat:** remove tab bar visiblity and add consitent padding ([34e7da58](/34e7da58dac9cca783bcd8c3c852156265230e18))
- **changelog:** Update Android CHANGELOG [skip ci] ([12f83053](/12f83053fe29f9b627a921cf59090f43f0ceb0cb))

### Other work
- RC 2 4/14/2020 ([940ad4ed](/940ad4edf7414d5b87795ded5c4d236e03aad6a9))
- RC 2 4/14/2020 ([a2c85bd8](/a2c85bd83c459d41c9623c39281ae4579340b418))

# 0.8.0 iOS Beta (2020-04-14)

### Features
- **HomeScreen:** Adding "Featured" Content to the Home Screen ([0a717ad9](/0a717ad92de0ce6f359eedb0dfc8f1a2b9cc4940))
- **home:** Feature News & Classes ([01fee3cc](/01fee3cca7dcd7189bead91914f0ed2a803db232))
- **care:** implement care options screens ([04f2b608](/04f2b60825ddb65025484e8ee544b23e004a190c))
- **nav:** Navigating Back to the Appropriate ClassDetails Screen After Login ([3fcec7bb](/3fcec7bbbd8a5c123aac10a55a8fe58bcb91f297))

### Building system
- **care:** Telehealth Button changes ([5b331bfc](/5b331bfcb38bfe0aab28df73faffe367a8724a3a))
- **classes:** change class header to live stream ([fef915ab](/fef915abd397e939fa062cb3f1da72ec7af73f1c))
- **care:** Self Assessment ([b04405e5](/b04405e58fd26bc208626a927d0b5e611cea02f9))
- **changelog:** Update Android CHANGELOG [skip ci] ([5d457112](/5d45711234d70c51e78d9f2809c686a6d6b5a909))

### Other work
- Release candidate 4/14/2020 ([ea5d7d9c](/ea5d7d9c344e52d335b9dc254280cf72bff0c7aa))

# 0.4.0 iOS Beta (2020-04-10)

### Features
- **classes:** Navigate back to Class Details after Login ([516c3795](/516c37957fe4ef3c251113a670feebd34818d572))

### Building system
- Small UI Tweaks and Restoring the Old Tab Bar Styles ([ee1738a6](/ee1738a6fb2c8210eee50b1531697683e9f116ba))
- **changelog:** Update Android CHANGELOG [skip ci] ([ca02433e](/ca02433e98e7030aae952dbca5cbcbf5d3d861d9))

### Other work
- Release candidate 2 4/10/2020 ([183450a3](/183450a33e71acdbd78f043ba269374293bcf958))

# 0.3.0 iOS Beta (2020-04-10)

### Features
- **care:** Telehealth & Self Assessment Placeholders ([5121b62b](/5121b62b17f78f481d70e22df18aa85be7b28d02))
- **notifications:** Notifications for Events Should Link to the Event Details ([f059ac51](/f059ac51cc66721efd1a9c3fb1fecff8c216f035))
- **branding:** Update to remove Foothill Ranch branding ([cd2c6243](/cd2c6243b354e5aaebb2c401aae18fd44705b426))

### Building system
- **setup:** Provisioning Profile and Updated Firebase Setup ([7a954811](/7a95481149547447df878936a81bc3df2ab8831d))
- **navigation:** move programs to drawer only nav ([b371d58a](/b371d58a3f549025a65d9ee9f9854c9d517cac06))
- **ci:** update android executor resource size ([2d87711c](/2d87711c48d9dc38eb1d57467aa8912472573b0a))
- **notifications:** Update libs to the latest versions and suppress warning ([ab01dd85](/ab01dd85bf0e09ca95bd3c48267b9ee57470b1d1))
- **login:** Add Keyboard type for email field ([6a625b80](/6a625b806fa1b5784d19e5cdecd14ed2da71fd82))
- **changelog:** Update Android CHANGELOG [skip ci] ([603ce38c](/603ce38c8b9512b1fdba1ca6d32b4104ae4f9a9c))

### Other work
- update provisioning profile and firebase configs ([71740146](/717401464a9d930a3cbb6e111c416c70f7cf1bae))
- add telehealth and self assessment care screens ([d3f1d8d1](/d3f1d8d18d745571e707c56a3f5b8c5f0fdc5348))
- better name env var ([e90653fb](/e90653fbebfa0b5c36866974275775d9a52d298d))
- clean up code ([3faeb69c](/3faeb69c2cbd2b956ef38d8338f1e9bd96a8f6ab))
- remove dead code ([1dcc1274](/1dcc12743e3768687faf3a47bab5b7654866a569))
- comment out credentials for google ([c3fdd6a5](/c3fdd6a56b2d6524e2281ef2dd112bd06124fcce))


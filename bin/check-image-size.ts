import path from 'path';
import fs from 'fs';
import glob from 'glob';

// set this to the max size you'd like to allow
const FILE_SIZE_LIMIT_IN_MB = 1;
const BYTES_PER_MB = 1000000;

/**
 * Checks a directory for large images. This script should be called on commit via lint-staged
 */
export function checkImageSize(): void {
  const root = path.join(__dirname, '../src/assets/**/*');
  const files = glob.sync(root);
  let fileSizeInMegabytes;

  const badFile = files.find(file => {
    const stats = fs.statSync(file);
    const fileSizeInBytes = stats.size;
    fileSizeInMegabytes = fileSizeInBytes / BYTES_PER_MB;

    return fileSizeInMegabytes > FILE_SIZE_LIMIT_IN_MB;
  });

  if (badFile) {
    console.error(`Looks like ${badFile} is a bit large! (${fileSizeInMegabytes}MB)`);
    process.exit(1);
  }
}

checkImageSize();

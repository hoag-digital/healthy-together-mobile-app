import React, { FC } from 'react';
import { Divider } from 'react-native-elements';

import { Button } from '../Button';
import { Container } from '../Container';
import { Modal } from '../Modal';
import { Text } from '../Text';

import { t } from '../../utils';
import { colors } from '../../styles';

interface Props {
  /** whether the modal is visible */
  isVisible: boolean;
  /** callback function to toggle the modal visibility */
  setIsVisible: (value: boolean) => void;
  /** callback function to invoke on class time reservation */
  onCancelClass: (value: string) => void;
  /** whether the class cancellation request is currently in progress */
  isCancelling: boolean;
}

/**
 * This is the modal shown when cancelling a class from the schedule screen
 */
export const CancelClassModal: FC<Props> = ({
  isVisible,
  setIsVisible,
  onCancelClass,
  isCancelling,
}) => {
  return (
    <Modal isVisible={isVisible} setIsVisible={(): void => {}} bottomHalf displayClose={false}>
      <Container fullWidth centerContent pt={5} px={1}>
        <Text py={3}>{t('schedule.cancelClass')}</Text>
        <Container flexDirection="row" fill px={2}>
          <Divider
            style={{
              width: '100%',
              backgroundColor: colors.silver,
            }}
          />
        </Container>
        <Container fullWidth flexDirection="row" py={5}>
          <Container p={2} width="50%">
            <Button
              backgroundColor={colors.transparent}
              label="Cancel"
              color={colors.cinnabar}
              borderRadius={5}
              py={3}
              onPress={(): void => {
                setIsVisible(false);
              }}
              borderColor={colors.cinnabar}
            />
          </Container>
          <Container p={2} width="50%">
            <Button
              loading={isCancelling}
              backgroundColor={colors.malibu}
              label="Confirm"
              color={colors.white}
              borderRadius={5}
              py={3}
              onPress={onCancelClass}
              ignoreDisabledStyles
            />
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export default CancelClassModal;

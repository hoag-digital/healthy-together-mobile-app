import React, { FC } from 'react';
import { ImageStyle, ImageSourcePropType } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image';

import { Container } from '../Container';
import { Text } from '../Text';
import { colors, space } from '../../styles';
import { radii } from '../../styles/margins';
interface Props {
  /** app to connect to */
  app: string;
  /** second line of text to display */
  subtitle: string;
  /** third line of text to display */
  appReview: string;
  /** gradient colors */
  gradient: string[];
  /** logo image to display */
  logo: ImageSourcePropType;
  /** color review text should be */
  reviewColor: string;
  //TODO: will likely add prop to define a click action and wrap this in a touchable
}

/**
 * Component docs: rounded rectangular card that displays with a gradient for connecting to another application
 */
export const AppConnect: FC<Props> = ({
  app,
  subtitle,
  appReview,
  logo,
  gradient,
  reviewColor,
}) => {
  const borderRadius = 10;

  const linearContainer = {
    flex: 1,
    width: '100%',
    borderRadius: radii[3],
    marginVertical: 10,
    justifyContent: 'center',
    bg: colors.transparent,

    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  };

  const linearStyle = {
    borderRadius: radii[3],
    justifyContent: 'center',
    padding: space[2],
  };

  const imageContainer = {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: space[2],
  };

  const image: ImageStyle = {
    width: 75,
    height: 75,
    resizeMode: 'cover',
    borderRadius: borderRadius,
    marginRight: space[4],
  };

  return (
    <Container {...linearContainer} shadowColor={gradient[1]}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={linearStyle}
        colors={gradient}
      >
        <Container {...imageContainer}>
          <Icon
            name="external-link"
            type="font-awesome"
            color={colors.white}
            containerStyle={{ position: 'absolute', top: 0, right: 5 }}
          />
          <FastImage accessibilityLabel={`${app} Icon`} style={image} source={logo} />
          <Container>
            <Text fontSize={28} fontWeight="bold" color={colors.white} pb={space[1]}>
              {app}
            </Text>
            <Text fontSize={20} fontWeigh="medium" color={colors.white} pb={space[1]}>
              {subtitle}
            </Text>
            <Text fontSize={14} color={reviewColor} pb={space[1]}>
              {appReview}
            </Text>
          </Container>
        </Container>
      </LinearGradient>
    </Container>
  );
};

export default AppConnect;

import React, { FC, ReactElement } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import { SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import { Container } from '../Container';
import { Text } from '../Text';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

export interface SubNavLayoutProps extends NavigationStackScreenProps {
  /** title to display */
  title?: string;
  /** hide title defaults to false */
  hideTitle?: boolean;
  /** if Active returns content otherwise returns null */
  isActive?: boolean;
  /** whether to display a centered loading spinner instead of the inner contents */
  isLoading?: boolean;
  /** The refreshControl of the inner Scrollview */
  refreshControl?: ReactElement;
  /** testId value for e2e testing selection */
  testId?: string;
  /**These props are part of NavigationScreen props and added to satisfy typescript linter */
  theme?: any;
  navigation?: any;
  screenProps?: any;
  scrollEnabled?: boolean;
  /** allows passing a state function that can set the navigation from inside a child layout item
   * Noted this should likely be revisted for better solution with navigation (custom tab navigatior?)
   */
  setParentActiveItem?: Function;
}

/**
 * SubNavLayout is a container that will display a title and React children.
 * It will return null if isActive is false, thereby allowing multiple SubNavLayouts
 * to be simultaneously mounted without affecting performance.
 */
export const SubNavLayout: FC<SubNavLayoutProps> = ({
  children,
  hideTitle = false,
  isActive,
  isLoading = false,
  refreshControl,
  title,
  testId = title, //defaults to title
  scrollEnabled = true,
  setParentActiveItem = (): void => {},
}) => {
  if (!isActive) return null;

  return isLoading ? (
    <CenteredLoadingSpinner withScrollviewPadding transparentBackground />
  ) : (
    <ScrollView
      contentContainerStyle={{ minHeight: '100%' }}
      scrollEnabled={scrollEnabled}
      testID={testId}
      scrollIndicatorInsets={{ right: 1 }}
      refreshControl={refreshControl}
    >
      <Container fill p={5} pb={SCROLLVIEW_BOTTOM_PADDING} testID={testId}>
        {!hideTitle ? (
          <Text fontSize={4} fontWeight="bold" pb={5}>
            {title}
          </Text>
        ) : null}
        {children}
      </Container>
    </ScrollView>
  );
};

export default SubNavLayout;

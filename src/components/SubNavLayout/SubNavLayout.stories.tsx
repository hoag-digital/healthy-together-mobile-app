import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { SubNavLayout } from './SubNavLayout';

storiesOf('components/SubNavLayout', module).add('Default', () => (
  <SubNavLayout title="My Cool SubNav Item" isActive />
));

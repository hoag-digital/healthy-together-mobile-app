import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { FloatingButton } from './FloatingButton';

storiesOf('components/FloatingButton', module).add('Default', () => <FloatingButton />);

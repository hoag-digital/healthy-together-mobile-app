import React, { FC, ReactElement } from 'react';
import { ActivityIndicator } from 'react-native';
import { BorderProps, ColorProps, SpaceProps, FlexProps, LayoutProps } from 'styled-system';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';

interface FloatingButtonProps {
  /** accessibility label */
  accessibilityLabel: string;
  /** disabled button state */
  disabled?: boolean;
  /**icon to display on the button */
  icon?: ReactElement;
  /** loading button state */
  loading?: boolean;
  /** should button expand full width? */
  fullWidth?: boolean;
  /** the text label of the button */
  label: string;
  /** the callback to be invoked onPress */
  onPress: () => void;
}

type ComponentProps = FloatingButtonProps &
  BorderProps &
  ColorProps &
  SpaceProps &
  FlexProps &
  LayoutProps;

/**
 * This is a button that remains fixed at the bottom of a page and allows for a full-screen
 * View to be placed beneath it.
 */
export const FloatingButton: FC<ComponentProps> = ({
  accessibilityLabel,
  icon,
  label,
  onPress,
  disabled,
  loading,
  fullWidth,
  ...props
}) => {
  const FloatingButtonContainer = disabled ? Container : Touchable;
  const onPressAction = loading ? null : onPress;

  return (
    <FloatingButtonContainer
      centerContent
      flexDirection="row"
      bg={disabled ? colors.disabled : colors.eclipse}
      borderRadius={8}
      height={50}
      width={fullWidth ? 0.85 : 0.3}
      accessibilityLabel={accessibilityLabel}
      onPress={onPressAction}
      disabled={disabled}
      position="absolute"
      right={30}
      bottom={100}
      {...props}
    >
      {loading ? (
        <ActivityIndicator />
      ) : (
        <>
          <Text color={disabled ? colors.silver : colors.black} fontWeight="bold" fontSize={2}>
            {label}
          </Text>
          {icon ? icon() : null}
        </>
      )}
    </FloatingButtonContainer>
  );
};

FloatingButton.defaultProps = {
  disabled: false,
  borderColor: colors.transparent,
  borderWidth: 1,
  backgroundColor: colors.eclipse,
};

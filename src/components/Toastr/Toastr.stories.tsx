import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { Toastr } from './Toastr';

storiesOf('components/Toast', module).add('Default', () => <Toastr />);

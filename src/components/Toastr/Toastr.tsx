import React, { FC, useState } from 'react';
import Toast from 'react-native-root-toast';
import { colors } from '../../styles';

interface Props {
  /** display toast? */
  isVisible?: boolean;
  /** position of the toast */
  position?: number;
  /** display with shadow? */
  shadow?: boolean;
  /** display with animation */
  animation?: boolean;
  /** hide toast on press? */
  hideOnPress?: boolean;
  /** color of the toast */
  backgroundColor?: string;
  /**opacity of the container */
  opacity?: number;
  /** message to display */
  message: string;
  /**milliseconds for how long message should display, default 5000 */
  duration?: number;
}

/**
 * This component displays a ”toast” message to the user which overlays existing UI elements
 */
export const Toastr: FC<Props> = ({
  isVisible = true,
  position = Toast.positions.CENTER,
  shadow = true,
  animation = true,
  hideOnPress = true,
  backgroundColor = colors.success,
  opacity = 1,
  message = '',
  duration = 5000,
}) => {
  const [visible, setVisible] = useState(isVisible);
  setTimeout(() => {
    setVisible(false);
  }, duration);

  return (
    <Toast
      visible={visible}
      position={position}
      shadow={shadow}
      animation={animation}
      hideOnPress={hideOnPress}
      backgroundColor={backgroundColor}
      opacity={opacity}
      duration={duration}
    >
      {message}
    </Toast>
  );
};

export default Toastr;

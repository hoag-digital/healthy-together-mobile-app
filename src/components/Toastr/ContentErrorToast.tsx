import React, { FC } from 'react';
import { colors } from '../../styles';
import { Toastr } from './Toastr';

/**
 * This component displays a content-based "toast" message
 */
export const ContentErrorToast: FC<{}> = () => {
  return (
    <Toastr backgroundColor={colors.error} message={`⚠️\nError Loading Content`} opacity={0.7} />
  );
};

export default ContentErrorToast;

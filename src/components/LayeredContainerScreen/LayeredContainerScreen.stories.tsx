import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { colors } from '../../styles';
import { Text } from '../Text';
import { LayeredContainerScreen } from './LayeredContainerScreen';

storiesOf('components/LayeredContainer', module).add('Default', () => (
  <LayeredContainerScreen
    screenTitle="My Title"
    headerColor={colors.cornflowerBlue}
    headerFontColor={colors.white}
    containerColor={colors.white}
  >
    <Text>My cool container</Text>
  </LayeredContainerScreen>
));

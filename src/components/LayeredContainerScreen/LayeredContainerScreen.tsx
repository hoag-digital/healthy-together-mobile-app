import React, { FC } from 'react';

import { Container } from '../Container';
import { Screen } from '../Screen';

interface Props {
  /** title to display in the modal header */
  screenTitle: string | null | undefined;
  /**header color */
  headerColor: string;
  //**color for header text */
  headerFontColor: string;
  /**container color */
  containerColor: string;
}

/**
 * Component docs: This component renders a screen with a layered body
 * featuring rounded top corners like a modal
 */
export const LayeredContainerScreen: FC<Props> = ({
  screenTitle,
  headerColor,
  headerFontColor,
  containerColor,
  children,
}) => {
  return (
    <Screen
      modalHeader
      screenTitle={screenTitle}
      headerColor={headerColor}
      backgroundColor={containerColor}
      headerFontColor={headerFontColor}
    >
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        height="100%"
        borderWidth={1}
        borderColor={containerColor}
        bg={containerColor}
        top={-10}
        paddingTop={4}
      >
        {children}
      </Container>
    </Screen>
  );
};

export default LayeredContainerScreen;

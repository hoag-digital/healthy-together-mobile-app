import React, { FC } from 'react';
import { ActivityIndicator } from 'react-native';
import {
  backgroundColor,
  BorderProps,
  ColorProps,
  SpaceProps,
  FlexProps,
  LayoutProps,
} from 'styled-system';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';

interface ButtonProps {
  /** accessibility label */
  accessibilityLabel: string;
  /** disabled button state */
  disabled?: boolean;
  /** loading button state */
  loading?: boolean;
  /** the text label of the button */
  label: string;
  /** the optional text to display below the label of the button */
  subLabel: string;
  /**sublabel color, defaults to component color or color assigned */
  subLabelColor: string;
  /** font size of the label text */
  fontSize?: number;
  /** font weight of the label text*/
  fontWeight?: string;
  /** the callback to be invoked onPress */
  onPress: () => void;
  /**ReactNode function to return an icon */
  renderIcon?: Function;
  /**Should icon render on the left (default) */
  iconLeft?: boolean;
  /** whether to ignore the default disabled color variation */
  ignoreDisabledStyles: boolean;
}

type ComponentProps = ButtonProps & BorderProps & ColorProps & SpaceProps & FlexProps & LayoutProps;

/**
 * notes:
 * - restricting inner text style from being directly configurable to avoid style prop conflicts
 * - if button is disabled it will not render a touchableOpacity at all
 */
export const Button: FC<ComponentProps> = ({
  accessibilityLabel,
  renderIcon,
  iconLeft = true,
  label,
  subLabel,
  subLabelColor,
  onPress,
  disabled,
  loading,
  color: componentColor,
  borderColor,
  ignoreDisabledStyles,
  fontSize = 2,
  fontWeight = '500',
  ...props
}) => {
  const ButtonContainer = disabled ? Container : Touchable;
  const onPressAction = loading ? null : onPress;
  const defaultTextColor = disabled ? colors.silver : componentColor;
  const textColor = ignoreDisabledStyles ? componentColor : defaultTextColor;
  const textSubLabelColor = subLabelColor ? subLabelColor : textColor;

  const Icon = renderIcon ? renderIcon() : null;

  return (
    <ButtonContainer
      centerContent
      bg={disabled ? colors.disabled : backgroundColor}
      borderColor={disabled ? colors.disabled : borderColor}
      borderRadius={40}
      accessibilityLabel={accessibilityLabel}
      flexDirection="column"
      onPress={onPressAction}
      disabled={disabled}
      {...props}
    >
      {loading ? (
        <ActivityIndicator />
      ) : (
        <>
          <Container flexDirection="row" alignItems="center">
            {iconLeft ? Icon : null}

            <Text color={textColor} fontSize={fontSize} fontWeight={fontWeight}>
              {label}
            </Text>
            {!iconLeft ? Icon : null}
          </Container>

          {subLabel ? (
            <Container flexDirection="row">
              <Text color={textSubLabelColor} fontSize={fontSize} fontWeight={fontWeight}>
                {subLabel}
              </Text>
            </Container>
          ) : null}
        </>
      )}
    </ButtonContainer>
  );
};

Button.defaultProps = {
  disabled: false,
  borderColor: colors.transparent,
  borderWidth: 1,
  backgroundColor: colors.terra,
  loading: false,
  icon: null,
};

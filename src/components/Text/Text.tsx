/* eslint-disable @typescript-eslint/explicit-function-return-type */ //This is to resolve lint error on the props functions
import styled from '@emotion/native';
import {
  color,
  space,
  position,
  PositionProps,
  typography,
  textStyle,
  ColorProps,
  SpaceProps,
  TextStyleProps,
  TypographyProps,
} from 'styled-system';

import { colors } from '../../styles';

interface TextProps {
  /** textDecoration style */
  textDecorationLine: 'none' | 'underline' | 'line-through' | 'underline line-through';
  /** reference id for e2e testing framework */
  testID?: string;
}

export type ComponentProps = TextProps &
  ColorProps &
  SpaceProps &
  TextStyleProps &
  TypographyProps &
  PositionProps;

/**
 * This is our primitive Text component with styled-system props applied
 */
export const Text = styled.Text<ComponentProps>`
  ${space};
  ${color};
  ${position};
  ${typography};
  ${textStyle};
  ${props =>
    props.textDecorationLine &&
    `
      textDecorationLine: ${props.textDecorationLine};
      textDecorationStyle: solid;
      textDecorationColor: ${props.color};
    `}
`;

Text.defaultProps = {
  color: colors.black,
  fontSize: 3,
  maxFontSizeMultiplier: 1.15,
};

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { VirtualEventListItem } from './VirtualEventListItem';

storiesOf('components/VirtualEventListItem', module).add('Default', () => <VirtualEventListItem />);

import { storiesOf } from '@storybook/react-native';
import React, { ReactElement, useState } from 'react';

import { colors } from '../../styles';
import { Button } from '../Button';
import { RxTokenModal } from './RxTokenModal';

const ModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = useState(false);
  const [userToken, setUserToken] = useState('');
  const [tokenError] = useState(false);

  return (
    <>
      <Button
        width="100%"
        marginTop={2}
        marginLeft={2}
        marginright={2}
        label="Show Token Modal"
        color={colors.white}
        backgroundColor={colors.aqua}
        borderRadius={5}
        onPress={(): void => {
          setIsVisible(true);
        }}
      />
      <RxTokenModal
        isVisible={isVisible}
        setIsVisible={setIsVisible}
        userTokenValue={userToken}
        setTokenValue={(code: string): void => setUserToken(code)}
        tokenError={tokenError}
        validate={(): void => console.log('validation attempted')}
      />
    </>
  );
};

storiesOf('components/RxTokenModal', module).add('Default', () => <ModalTest />);

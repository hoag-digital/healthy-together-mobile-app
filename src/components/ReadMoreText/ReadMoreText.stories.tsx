import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { colors } from '../../styles';
import { Text } from '../Text';

import { ReadMoreText } from './ReadMoreText';

const catIpsum = `Toy mouse squeak roll over cats are the world and find empty spot in cupboard and sleep all day but kitty scratches couch bad kitty try to jump onto window and fall while scratching at wall. Chew iPad power cord.`;

storiesOf('components/ReadMoreText', module)
  .add('Default', () => (
    <>
      <ReadMoreText text={catIpsum} />
      <ReadMoreText text={catIpsum} seeMoreText="Show" seeLessText="Hide" />
      <ReadMoreText
        text={catIpsum}
        textStyle={{ color: 'purple', fontSize: 25 }}
        seeMoreTextStyle={{ color: 'orange', fontSize: 20 }}
      />
    </>
  ))
  .add('Empty Text', () => (
    <>
      <Text>undefined text</Text>
      <ReadMoreText text={undefined} />
      <Text>Null text</Text>
      <ReadMoreText text={null} />
    </>
  ));

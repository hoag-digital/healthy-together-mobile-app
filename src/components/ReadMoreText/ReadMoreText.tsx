import React, { FC } from 'react';
import ReadMore from '@fawazahmed/react-native-read-more';
import { TextStyle, ViewStyle } from 'react-native';
import { Text } from '../Text';
import { colors } from '../../styles';

interface Props {
  /** Text to display */
  text: string | null | undefined;
  /** number of lines to display, default is 1 */
  numberOfLines?: number;
  /**style for the text to be displayed */
  textStyle?: TextStyle;
  /** style for the "See more" text object */
  seeMoreTextStyle?: TextStyle;
  /** style for the "See less" text object */
  seeLessTextStyle?: TextStyle;
  /** custom text for see more */
  seeMoreText?: string;
  /** custom text for see less */
  seeLessText?: string;
  /** custom view wrapper style */
  wrapperStyle?: ViewStyle;
}

const defaultTextStyle = {
  paddingTop: 16,
  paddingBottom: 1,
  marginHorizontal: 5,
  fontSize: 14,
};

const defaultSeeMoreTextStyle = {
  ...defaultTextStyle,
  color: colors.chino,
};

const defaultSeeLessTextStyle = {
  ...defaultTextStyle,
  color: colors.chino,
  paddingTop: 0,
};

const defaultWrapperStyle = {
  marginVertical: 5,
  marginHorizontal: 5,
};

/**
 * Component docs: This component allows a lengthy bit of text to be limited to a specified number of lines
 * It adds a "see more" / "see less" functionality
 */
export const ReadMoreText: FC<Props> = ({
  text,
  numberOfLines = 1,
  textStyle = defaultTextStyle,
  seeMoreTextStyle = defaultSeeMoreTextStyle,
  seeLessTextStyle = defaultSeeLessTextStyle,
  seeMoreText = 'See more',
  seeLessText = 'See less',
  wrapperStyle = defaultWrapperStyle,
}) => {
  return (
    <ReadMore
      numberOfLines={numberOfLines}
      customTextComponent={Text}
      style={textStyle}
      seeMoreStyle={seeMoreTextStyle}
      seeLessStyle={seeLessTextStyle}
      seeMoreText={seeMoreText}
      seeLessText={seeLessText}
      wrapperStyle={wrapperStyle}
    >
      {text}
    </ReadMore>
  );
};

export default ReadMoreText;

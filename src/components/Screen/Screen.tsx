import React, { ReactNode, FC } from 'react';
import { StatusBar } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { FlexProps, SpaceProps } from 'styled-system';

import { colors } from '../../styles';
import { ScreenHeader } from '../ScreenHeader';
import { ModalHeader } from '../ModalHeader';
import { Container } from '../Container';

interface ScreenProps {
  /*what type of header should we display*/
  headerType?: 'screen' | 'module' | 'modal';
  /* whether to display the screen header component (logo and hamburger menu) */
  screenHeader?: boolean;
  /* whether to display the screen module header (logo and back button instead of hamburger)*/
  drawerEnabled?: boolean;
  loginBanner?: boolean;
  /* whether to display the modal header component (screen title and back button) */
  modalHeader?: boolean;
  /** The screen title to display; no title will be shown unless explicitly provided; only for ModalHeader */
  screenTitle?: string;
  /** Custom background color of the screen */
  backgroundColor?: string;
  /** Custom background color for the modalHeader component */
  headerColor?: string;
  /** Custom font color for the modalHeader component */
  headerFontColor?: string;
  /* whether to override all background styles for a transparent background */
  transparent?: boolean;
  /* content to render on the right side of the modalHeader */
  rightHeaderContent?: ReactNode;
  /** the content to render within the screen */
  children?: ReactNode;
}

type ComponentProps = ScreenProps & FlexProps & SpaceProps;

export const MODULE_COLORS = {
  maternity: {
    header: colors.chino,
    border: colors.gray,
    text: colors.white,
    background: colors.pearlLusta,
  },
  biocircuit: {
    header: colors.tangerineYellow,
    border: colors.gray,
    text: colors.white,
    background: colors.pearlLusta,
  },
};

export const Screen: FC<ComponentProps> = ({
  activeModule,
  backgroundColor,
  headerColor,
  headerFontColor,
  children,
  screenTitle,
  screenHeader,
  modalHeader,
  moduleHeader,
  transparent,
  rightHeaderContent,
  drawerEnabled = true,
  loginBanner = true,
  ...screenProps
}) => {
  const hasTopInset = screenHeader || modalHeader || moduleHeader;

  // the root container should be the header color by default to handle overflowing
  let rootContainerBackgroundColor = headerColor;

  // override the root container color if using the "screenHeader"
  if (screenHeader || moduleHeader) {
    rootContainerBackgroundColor = activeModule ? MODULE_COLORS[activeModule].header : colors.white;
  }

  // override the root container color if specifying "transparent"
  if (transparent) {
    rootContainerBackgroundColor = colors.transparent;
  }

  const renderScreenHeader = (): ReactNode => {
    if (screenHeader) {
      return (
        <ScreenHeader
          drawerEnabled={drawerEnabled}
          loginBanner={loginBanner}
          activeModule={activeModule}
        />
      );
    }

    if (moduleHeader) {
      return <ScreenHeader exitEnabled activeModule={activeModule} />;
    }

    if (modalHeader) {
      return (
        <ModalHeader
          backEnabled
          screenTitle={screenTitle}
          backgroundColor={headerColor}
          fontColor={headerFontColor}
          rightHeaderContent={rightHeaderContent}
        />
      );
    }

    return null;
  };

  return (
    <Container fill fullWidth backgroundColor={rootContainerBackgroundColor}>
      <StatusBar
        translucent
        animated
        backgroundColor={colors.transparent}
        barStyle="dark-content"
      />
      <SafeAreaView
        style={{ flex: 1 }}
        forceInset={{ top: hasTopInset ? 'always' : 'never', bottom: 'never' }}
      >
        {renderScreenHeader()}
        <Container bg={transparent ? colors.transparent : backgroundColor} {...screenProps}>
          {children}
        </Container>
      </SafeAreaView>
    </Container>
  );
};

Screen.defaultProps = {
  headerColor: colors.white,
  screenHeader: false,
  screenTitle: null,
  modalHeader: false,
};

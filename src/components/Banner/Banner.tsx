import React, { FC } from 'react';

import { Container } from '../Container';
import { colors } from '../../styles';

interface Props {
  /** the backgroundColor of the banner */
  backgroundColor?: string;
}

/**
 * This component is a Container/Touchable component with preconfigured "Banner" dimensions
 */
export const Banner: FC<Props> = ({ backgroundColor = colors.white, children }) => {
  return (
    <Container
      width="100%"
      px={2}
      border={1}
      borderRadius={25}
      alignItems="center"
      justifyContent="center"
      bg={backgroundColor}
      height={70}
    >
      {children}
    </Container>
  );
};

export default Banner;

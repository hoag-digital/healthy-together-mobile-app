import React, { ReactElement } from 'react';
import { Icon } from 'react-native-elements';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
// import Swipeable from 'react-native-swipeable';
import dayjs from 'dayjs';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { SMALLEST_ICON_SIZE, t } from '../../utils';
import { colors } from '../../styles';
import { Event } from '../../graphql/types';

interface ScheduleItemProps extends NavigationSwitchScreenProps {
  /** The scheduled event from the api */
  item: Event;
  /** Callback function to invoke the mutation for cancelling an event registration */
  onCancel: () => void;
}

const SCHEDULE_LABEL_COLORS = {
  NUTRITION: colors.violet,
  BIOCIRCUIT: colors.gold,
  URGENT_CARE: colors.crusta,
  CLASS: colors.cornflowerBlue2,
  DEFAULT: colors.cornflowerBlue2,
};

export function getScheduleLabel(eventName: string): string | null {
  switch (eventName) {
    case 'Biocircuit Studio':
      return 'Biocircuit';
    default:
      return eventName;
  }
}

/**
 * The ScheduleListItem component renders an individual appointment on the ScheduleScreen
 */
const ScheduleListItemComponent = ({
  item,
  navigation,
  onCancel,
}: ScheduleItemProps): ReactElement => {
  const formattedTime = dayjs(item.startTime).format('h:mm A'); // '9:51 AM'
  const formattedTimeElements = formattedTime.split(' '); // ['9:51', 'AM']

  const renderLabel = (): ReactElement => {
    const title = getScheduleLabel(item.name) || t('schedule.defaultClassTitle');

    return (
      <Container flexDirection="row" fill>
        <Text fontWeight="400" fontSize={2}>
          {title}
        </Text>
      </Container>
    );
  };

  const getBorderColor = (): string => {
    if (item.name === 'Biocircuit Studio') {
      return SCHEDULE_LABEL_COLORS.BIOCIRCUIT;
    }

    return SCHEDULE_LABEL_COLORS.DEFAULT;
  };

  const navigateToClassDetails = (): void => {
    const eventGroupName = item.name;

    if (!eventGroupName) return;

    const isBiocircuit = eventGroupName === 'Biocircuit Studio';

    if (isBiocircuit) {
      navigation.navigate('BiocircuitDetails');
    } else {
      navigation.navigate('ClassDetails', { eventGroupName });
    }
  };

  // const deleteButton = (
  //   <Touchable
  //     py={3}
  //     pl={7}
  //     onPress={onCancel}
  //     style={{ backgroundColor: colors.cinnabar, alignItems: 'flex-start' }}
  //   >
  //     <Icon size={SMALLEST_ICON_SIZE} name="ios-trash" type="ionicon" color={colors.white} />
  //   </Touchable>
  // );

  // const swipeContainerRightButtons = [deleteButton];

  return (
    // <Swipeable rightButtons={swipeContainerRightButtons}>
    <Touchable onPress={navigateToClassDetails}>
      <Container flexDirection="row">
        <Container
          bg={colors.whiteSmoke}
          flexDirection="row"
          width="25%"
          p={3}
          my={0.5}
          centerContent
        >
          <Text fontWeight="bold">
            {formattedTimeElements[0]} <Text fontSize={1}>{formattedTimeElements[1]}</Text>
          </Text>
        </Container>
        <Container
          borderLeftWidth={3}
          borderColor={getBorderColor()}
          bg={colors.whiteSmokeLight}
          flexDirection="row"
          alignItems="center"
          fill
          p={3}
          my={0.5}
        >
          {renderLabel()}
          <Container ml={3}>
            <Icon
              size={SMALLEST_ICON_SIZE}
              name="eye"
              type="material-community"
              color={colors.balticSea}
            />
          </Container>
        </Container>
        <Container
          bg={colors.whiteSmokeLight}
          flexDirection="row"
          alignItems="center"
          p={2}
          my={0.5}
        >
          <Touchable px={3} onPress={onCancel} alignItems="center">
            <Icon
              size={SMALLEST_ICON_SIZE}
              name="ios-trash"
              type="ionicon"
              color={colors.balticSea}
            />
          </Touchable>
        </Container>
      </Container>
    </Touchable>
    // </Swipeable>
  );
};

export const ScheduleListItem = withNavigation(ScheduleListItemComponent);
export default ScheduleListItem;

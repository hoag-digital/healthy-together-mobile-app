import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { GestationalAge } from '../../graphql/types';
import { colors } from '../../styles';
import { Container } from '../Container';

import { MaternityStatus } from './MaternityStatus';

const gestation: GestationalAge = {
  days: 5,
  weeks: 38,
  trimester: 2,
  currentBabySize: {
    description: 'an apple',
    image: 'https://dev.hoag.io/wp-content/uploads/15-Apple-300x287.jpg',
  },
};

storiesOf('components/MaternityStatus', module).add('Default', () => (
  <Container flex={1} bg={colors.chino} justifyContent="center">
    <MaternityStatus gestationalAge={gestation} />
  </Container>
));

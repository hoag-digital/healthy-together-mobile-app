import React, { FC } from 'react';
import FastImage from 'react-native-fast-image';
import { GestationalAge } from '../../graphql/types';
import { colors, fonts } from '../../styles';
import { CARD_BORDER_RADIUS } from '../../utils/constants';
import stringifyTrimester from '../../utils/stringifyTrimester';

import { Container } from '../Container';
import { Text } from '../Text';
import { GestationalProgress } from '../GestationalProgress';

interface Props {
  /** gestational age object that shows weeks/days/trimester for gestational progress */
  gestationalAge: GestationalAge;
}

const defaultContainerProps = {
  flex: 0.15,
  my: 3,
  minHeight: 150, // default is the smallest of all card variants
  borderRadius: CARD_BORDER_RADIUS,
  borderWidth: 1,
  borderColor: colors.whisper,
  bg: colors.white,
  centerContent: true,
  elevation: 4,
  mx: 5,
  p: 2,
};

const styles = {
  font: {
    font: fonts.regular,
  },
  image: {
    height: 100,
    width: 100,
  },
};

/**
 * Displays gestation progress bar and baby size information based on the user's maternity status
 */
export const MaternityStatus: FC<Props> = ({ gestationalAge }) => {
  return (
    <Container {...defaultContainerProps}>
      <Text fontSize={3} textAlign="center">
        You're in week {gestationalAge.weeks} {'\n'}
        and in your {stringifyTrimester(gestationalAge.trimester)}!
      </Text>
      <GestationalProgress gestationalAge={gestationalAge} />
      <Container flexDirection="row" pt={1} px={7} width="100%">
        <Container flex={0.55} alignItems="center" justifyContent="center">
          <Text fontSize={3} textAlign="center" py={2}>
            Your baby is the {'\n'}size of {'\n'} {gestationalAge.currentBabySize?.description}!
          </Text>
        </Container>
        <Container flex={0.45} alignItems="flex-end" justifyContent="flex-end" px={3}>
          <FastImage
            source={{
              uri: gestationalAge.currentBabySize?.image,
            }}
            style={styles.image}
          />
        </Container>
      </Container>
    </Container>
  );
};

export default MaternityStatus;

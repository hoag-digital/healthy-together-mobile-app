import { storiesOf } from '@storybook/react-native';
import React, { ReactElement, useState } from 'react';

import { colors } from '../../styles';
import { Text } from '../Text';
import { Container } from '../Container';
import { Datepicker } from './Datepicker';

const ModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = useState(false);
  const [dateValue, setDateValue] = useState<Date>();

  const updateDate = (value: Date): void => {
    setDateValue(value);
  };

  return (
    <Container
      width="100%"
      px={1}
      bg={colors.pearlLusta}
      flex={0.3}
      borderRadius={35}
      borderColor={colors.chino}
      borderWidth={2}
      justifyContent="center"
      alignItems="center"
    >
      <Text fontSize={6} color={colors.gray} py={6}>
        Pick a date!
      </Text>
      <Datepicker
        isVisible={isVisible}
        setIsVisible={setIsVisible}
        setSelectedDate={updateDate}
        selectedDate={dateValue}
      />
    </Container>
  );
};

storiesOf('components/Datepicker', module).add('Default', () => <ModalTest />);

import React, { FC } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { gradients } from '../../styles';

interface Props {
  /** image height */
  height: number;
  /** zIndex of container, default to 49 to be just below the fastImage setting */
  zIndex?: number;
}

/**
 * Component docs: Creates a blackTransparent to clear gradient that overlays on top of an image for full bleed
 */
export const TransparencyImageOverlay: FC<Props> = ({ height, zIndex = 49, children }) => {
  return (
    <LinearGradient
      style={{
        alignItems: 'center',
        height: height,
        zIndex: zIndex,
        position: 'absolute',
        top: 0,
        width: '100%',
      }}
      colors={gradients.blackTransparentClear}
    >
      {children}
    </LinearGradient>
  );
};

export default TransparencyImageOverlay;

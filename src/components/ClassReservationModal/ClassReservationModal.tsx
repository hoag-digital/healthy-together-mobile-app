import React, { FC } from 'react';
import dayjs from 'dayjs';

import { Button } from '../Button';
import { Container } from '../Container';
import { Modal } from '../Modal';
import { Text } from '../Text';

import { colors } from '../../styles';
import { t } from '../../utils';

interface Props {
  /** whether the modal is visible */
  isVisible: boolean;
  /** whether to display as program or class, default false */
  isProgram?: boolean;
  /** callback function to toggle the modal visibility */
  setIsVisible: (value: boolean) => void;
  /** the class image resource */
  classImageUrl: string;
  /** id of the class time entity */
  classTimeId: number | null;
  /** date */
  date: string;
  /** start time */
  startTime: string;
  /** end time */
  endTime: string;
  /** the name of the related HoagClass entity */
  className: string;
  /** callback function to invoke on class time reservation */
  onReserveClass: (value: string) => void;
  /** wether the class reservation mutations is in progress */
  isRegistering: boolean;
}

/**
 * This is the modal shown when reserving a class on the class details screen
 */
export const ClassReservationModal: FC<Props> = ({
  isVisible,
  setIsVisible,
  classTimeId,
  className,
  date,
  startTime,
  endTime,
  onReserveClass,
  isRegistering,
  isProgram = false,
}) => {
  const reserveClass = (): void => {
    // do the actual class reservation using classTimeId
    if (classTimeId) {
      onReserveClass(classTimeId);
    }
  };

  const formattedDate = date ? dayjs(date).format('MMMM D') : '';
  const formattedStartTime = startTime ? dayjs(startTime).format('h:mm A') : '';
  const formattedEndTime = endTime ? dayjs(endTime).format('h:mm A') : '';

  return (
    <Modal isVisible={isVisible} setIsVisible={setIsVisible} displayClose={false} bottomHalf>
      <Container fullWidth centerContent pt={5} px={1}>
        <Text py={3}>Please confirm scheduling</Text>
        <Container
          fullWidth
          mt={3}
          p={5}
          borderWidth={1}
          borderRadius={10}
          borderColor={colors.silver}
          alignItems="flex-start"
        >
          <Container
            fullWidth
            px={5}
            py={3}
            justifyContent="space-between"
            flexDirection="row"
            alignItems="center"
          >
            <Container fullWidth>
              <Text fontSize={2} color={colors.balticSea50}>
                {t(`${isProgram ? 'programs' : 'classes'}.title`)}
              </Text>
              <Text fontSize={3} numberOfLines={1}>
                {className}
              </Text>
            </Container>
          </Container>
          <Container mx={5} py={4} flexDirection="row" justifyContent="space-between" width="100%">
            <Container width="50%">
              <Text fontSize={2} color={colors.balticSea50}>
                Date
              </Text>
              <Text fontSize={2}>{formattedDate}</Text>
            </Container>
            <Container width="50%">
              <Text fontSize={2} color={colors.balticSea50}>
                Time
              </Text>
              <Text fontSize={2}>{`${formattedStartTime} - ${formattedEndTime}`}</Text>
            </Container>
          </Container>
        </Container>
        <Container fullWidth flexDirection="row" py={5}>
          <Container p={2} width="50%">
            <Button
              backgroundColor={colors.transparent}
              label="Cancel"
              color={colors.cinnabar}
              borderRadius={5}
              py={3}
              onPress={(): void => {
                setIsVisible(false);
              }}
              borderColor={colors.cinnabar}
            />
          </Container>
          <Container p={2} width="50%">
            <Button
              loading={isRegistering}
              backgroundColor={colors.malibu}
              label="Confirm"
              color={colors.white}
              borderRadius={5}
              py={3}
              onPress={reserveClass}
              ignoreDisabledStyles
            />
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export default ClassReservationModal;

import React, { ReactElement, useState } from 'react';
import { storiesOf } from '@storybook/react-native';

import { colors } from '../../styles';
import { Button } from '../Button';
import { ErrorModal } from './ErrorModal';

const ModalTest = (): ReactElement => {
  const [isVisible, setIsVisible] = useState(false);
  return (
    <>
      <Button
        width="100%"
        marginTop={2}
        marginLeft={2}
        marginright={2}
        label="Show Error Modal"
        color={colors.white}
        backgroundColor={colors.aqua}
        borderRadius={5}
        onPress={(): void => {
          setIsVisible(true);
        }}
      />
      <ErrorModal isVisible={isVisible} setIsVisible={setIsVisible} error="Error message 🚀">
        <></>
      </ErrorModal>
    </>
  );
};

storiesOf('components/LoginModal', module).add('Default', () => <ModalTest />);

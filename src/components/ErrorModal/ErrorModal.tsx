import React, { FC } from 'react';
import { Button } from '../Button';
import { Modal } from '../Modal';
import { Text } from '../Text';
import { colors } from '../../styles';
import { Container } from '../Container';
import { SCREEN_WIDTH, t } from '../../utils';

interface Props {
  /** Whether or not the Modal is visible */
  isVisible: boolean;
  /** The function for setting the visibility */
  setIsVisible: (visible: boolean) => void;
  /**  The error message to display */
  error: string;
  /** value to override default button color of terra */
  buttonColor?: string;
  /** text color to over default white */
  buttonTextColor?: string;
}

/**
 * Component docs: Component of type Modal that displays an error message
 */
export const ErrorModal: FC<Props> = ({
  isVisible,
  setIsVisible,
  error,
  buttonColor = colors.terra,
  buttonTextColor = colors.white,
}) => {
  return (
    <Modal isVisible={isVisible} setIsVisible={setIsVisible} displayClose={false}>
      <Container mt={10} mb={1} alignItems="center">
        <Text fontSize={3} my={2} textAlign="center">
          {error}
        </Text>

        <Button
          py={3}
          label={t('errors.buttons.ok')}
          color={buttonTextColor}
          backgroundColor={buttonColor}
          borderRadius={5}
          mt={5}
          mb={20}
          width={SCREEN_WIDTH * 0.6}
          onPress={(): void => {
            setIsVisible(false);
          }}
        />
      </Container>
    </Modal>
  );
};

export default ErrorModal;

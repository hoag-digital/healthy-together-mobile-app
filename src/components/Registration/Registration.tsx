import React, { FC, useState, useEffect, createRef } from 'react';
import { BorderProps, ColorProps, SpaceProps, FlexProps } from 'styled-system';
import { Icon } from 'react-native-elements';
import { ScrollView } from 'react-native';
import { Button } from '../Button';
import { Text } from '../Text';
import { TextInput } from '../TextInput';
import { Touchable } from '../Touchable';
import { Container } from '../Container';

import { colors } from '../../styles';
import { t } from '../../utils';
import {
  matchingPassword,
  strongPassword,
} from '../../utils/passwordValidation/passwordValidation';

interface ErrorsObject {
  email?: string;
}

export interface RegisterCallbackParams {
  email: string;
  password: string;
  passwordConfirmation: string;
  firstName: string;
  lastName: string;
}

interface RegistrationProps {
  /** Callback that should register the user */
  onRegister: (params: RegisterCallbackParams) => void;
  /** Callback if the user cancels */
  onCancelRegistration: () => void;
  /** Disables buttons when loading */
  registrationLoading: boolean;
  /** Hash of errors from the API */
  errors: ErrorsObject;
}

type ComponentProps = RegistrationProps & FlexProps & SpaceProps & BorderProps & ColorProps;

export const Registration: FC<ComponentProps> = ({
  registrationLoading,
  onRegister,
  onCancelRegistration,
  errors,
  ...props
}) => {
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [passwordMatch, setPasswordMatch] = useState(true);
  const [passwordRules, setPasswordRules] = useState(true);
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    async function validateInputs(): Promise<void> {
      const valid =
        password.length &&
        passwordConfirmation.length &&
        passwordMatch &&
        passwordRules &&
        email.length &&
        firstName.length &&
        lastName.length;

      setIsValid(!!valid);
    }

    validateInputs();
  }, [
    email.length,
    firstName.length,
    lastName.length,
    password.length,
    passwordConfirmation.length,
    passwordMatch,
    passwordRules,
  ]);

  const validatePasswords = (): void => {
    if (!password.length || !passwordConfirmation.length) return;
    setPasswordMatch(matchingPassword(password, passwordConfirmation));

    setPasswordRules(strongPassword(password));
  };

  const emailRef = createRef();
  const passwordRef = createRef();
  const confirmationRef = createRef();
  const lastNameRef = createRef();

  return (
    <Container
      bg={colors.snow}
      borderRadius={20}
      height="85%"
      elevation={4}
      overflow="visible"
      padding={4}
      shadowColor={colors.silver}
      shadowOffset={{ width: 0, height: 0 }}
      shadowOpacity={0.15}
      shadowRadius={5}
      {...props}
    >
      <Container flex={0.1} fullWidth>
        <Touchable onPress={(): void => onCancelRegistration()} fullWidth alignItems="flex-start">
          <Icon name="arrow-back" />
        </Touchable>
      </Container>

      <Container flex={0.9} fullWidth>
        <ScrollView>
          <Text fontSize={32} fontWeight="bold" marginBottom="10%" marginTop="5%">
            {t('registration.welcome')}
          </Text>

          <TextInput
            placeholder={t('registration.firstName')}
            autoCorrect={false}
            autoCompleteType="name"
            autoCapitalize="words"
            onChangeText={(value: string): void => setFirstName(value)}
            onSubmitEditing={(): void => (lastNameRef as any).current.focus()}
            icon={<Icon name="user" type="font-awesome" color={colors.whisper} />}
            marginTop={2}
            borderRadius={5}
            borderColor={colors.whisper}
            returnKeyType="next"
          />

          <TextInput
            ref={lastNameRef}
            autoCorrect={false}
            autoCompleteType="name"
            autoCapitalize="words"
            placeholder={t('registration.lastName')}
            onChangeText={(value: string): void => setLastName(value)}
            onSubmitEditing={(): void => (emailRef as any).current.focus()}
            icon={<Icon name="user" type="font-awesome" color={colors.whisper} />}
            marginTop={2}
            borderRadius={5}
            borderColor={colors.whisper}
            returnKeyType="next"
          />

          <TextInput
            ref={emailRef}
            autoCorrect={false}
            autoCompleteType="email"
            autoCapitalize="none"
            placeholder={t('registration.email')}
            onChangeText={(value: string): void => setEmail(value)}
            onSubmitEditing={(): void => (passwordRef as any).current.focus()}
            icon={<Icon name="envelope" type="font-awesome" color={colors.whisper} />}
            marginTop={2}
            borderRadius={5}
            borderColor={errors.email ? colors.cinnabar : colors.whisper}
            returnKeyType="next"
          />

          {errors.email ? (
            <Text color={colors.cinnabar} fontSize={2} mb={2}>
              {t('registration.errors.email')}
            </Text>
          ) : null}

          <TextInput
            ref={passwordRef}
            autoCorrect={false}
            autoCompleteType="password"
            placeholder={t('registration.password')}
            onChangeText={(value: string): void => setPassword(value)}
            onBlur={(): void => validatePasswords()}
            onSubmitEditing={(): void => (confirmationRef as any).current.focus()}
            // blurOnSubmit is required to remove the iOS 13 strong password overlay
            blurOnSubmit={false}
            secureTextEntry
            icon={<Icon name="key" type="font-awesome" color={colors.whisper} />}
            marginTop={2}
            borderRadius={5}
            borderColor={colors.whisper}
            returnKeyType="next"
          />
          <TextInput
            ref={confirmationRef}
            autoCorrect={false}
            autoCompleteType="password"
            placeholder={t('registration.passwordConfirmation')}
            onChangeText={(value: string): void => setPasswordConfirmation(value)}
            onBlur={(): void => validatePasswords()}
            onSubmitEditing={(): void => validatePasswords()}
            // blurOnSubmit is required to remove the iOS 13 strong password overlay
            blurOnSubmit={false}
            secureTextEntry
            icon={<Icon name="key" type="font-awesome" color={colors.whisper} />}
            marginTop={2}
            borderRadius={5}
            borderColor={colors.whisper}
            returnKeyType="go"
          />

          <Text fontSize={1} color={passwordMatch === false ? colors.cinnabar : colors.transparent}>
            {t('registration.validation.match')}
          </Text>
          <Text fontSize={1} color={passwordRules ? colors.gray : colors.cinnabar}>
            {t('registration.validation.rules')}
          </Text>
          <Button
            py={3}
            width="100%"
            marginTop={12}
            label={t('registration.buttons.createAccount')}
            color={colors.cinnabar}
            backgroundColor={colors.white}
            borderRadius={5}
            borderColor={colors.cinnabar}
            loading={registrationLoading}
            disabled={!isValid}
            onPress={(): void =>
              onRegister({ email, password, passwordConfirmation, firstName, lastName })
            }
          />
        </ScrollView>
      </Container>
    </Container>
  );
};

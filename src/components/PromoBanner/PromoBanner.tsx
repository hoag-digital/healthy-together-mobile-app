import React, { FC } from 'react';
import FastImage from 'react-native-fast-image';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';

import analytics from '@react-native-firebase/analytics';
import { Banner } from '../Banner';
import { Container } from '../Container';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import { Text } from '../Text';
import { Content } from '../../graphql/types';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import { LARGEST_ICON_SIZE } from '../../utils';
type Props = Content & NavigationSwitchScreenProps;

/**
 * Component docs: This component links to the PromoScreen
 * This component was based off of the TrimesterProgress component.
 */
const PromoBannerComponent: FC<Props> = ({ id, title, thumbnailImage, navigation }) => {
  const navigateToPromoDetails = (): void => {
    if (!id) return;
    analytics().logEvent('promo_details_click', { postId: id, title });
    navigation.navigate('PromoDetailsModal', { postId: id });
  };

  return (
    <Banner>
      <Touchable onPress={navigateToPromoDetails}>
        {/* TODO: Going to need to work to make this layout uniform with the TrimsterProgress component */}
        <Container
          flexDirection="row"
          px={1}
          py={2}
          width="100%"
          alignItems="center"
          justifyContent="space-between"
        >
          <Container width="20%" alignItems="flex-start">
            <FastImage
              accessibilityLabel={`${title} class card`}
              style={{
                width: 60,
                height: 50,
                resizeMode: 'cover',
                borderRadius: 15,
              }}
              source={thumbnailImage ? { uri: thumbnailImage } : GenericClassImage}
            />
          </Container>
          <Container flex={1}>
            <Text fontSize={2} fontWeight="bold">
              {title || 'Promotional Banner'}
            </Text>
          </Container>
          <Container width="10%" alignItems="flex-end">
            <Icon
              name="ios-arrow-round-forward"
              type="ionicon"
              size={LARGEST_ICON_SIZE}
              color={colors.gray}
            />
          </Container>
        </Container>
      </Touchable>
    </Banner>
  );
};

export const PromoBanner = withNavigation(PromoBannerComponent);
export default PromoBanner;

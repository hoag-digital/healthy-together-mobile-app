import React, { FC, ReactNode } from 'react';
import { Container } from '../Container';
import { Touchable } from '../Touchable';
import { CARD_BORDER_RADIUS, TOUCHABLE_PRESS_DELAY } from '../../utils';
import { colors } from '../../styles';

export interface CardProps {
  /** Callback function to invoke when the root container is pressable. Renders a Touchable when not null */
  onPress?: () => void;
  /** composition function to render the top portion of the Card */
  renderTop: () => ReactNode;
  /** composition function to render the bottom segment of the Card */
  renderBottom: () => ReactNode;
  /** overridable props for wrapper <Container> / <Touchable>. Will be merged with defaults. */
  containerProps?: object;
}

const defaultContainerProps = {
  flex: 1,
  my: 1,
  minHeight: 150, // default is the smallest of all card variants
  borderRadius: CARD_BORDER_RADIUS,
  borderWidth: 1,
  borderColor: colors.whisper,
  bg: colors.white,
  centerContent: true,
  elevation: 4,
  shadowColor: colors.snow,
  shadowOffset: { width: 5, height: 0 },
  shadowOpacity: 0.9,
  shadowRadius: 5,
};

/**
 * This is the core of the compositional Card component leveraged by most of the other Card-style
 * components of the app.
 */
export const Card: FC<CardProps> = ({ onPress, renderTop, renderBottom, containerProps }) => {
  const mergedContainerProps = {
    ...defaultContainerProps,
    ...containerProps,
  };

  /**
   * Note: delayPressIn is an undocumented TouchableOpacity prop that delays the opacity overlay from
   * triggering. This is added here since the Card component is typically rendered in a listview which
   * the user can scroll. This is an attempt to remove the opacity from the "scroll gesture" but to
   * leave it in place for the "card press gesture".
   */
  return onPress ? (
    <Touchable onPress={onPress} delayPressIn={TOUCHABLE_PRESS_DELAY} {...mergedContainerProps}>
      {renderTop()}
      {renderBottom()}
    </Touchable>
  ) : (
    <Container {...mergedContainerProps}>
      {renderTop()}
      {renderBottom()}
    </Container>
  );
};

export default Card;

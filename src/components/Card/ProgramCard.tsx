import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';
import { Text } from '../Text';
import { space, colors } from '../../styles';
import { CARD_BORDER_RADIUS } from '../../utils';
import { Container } from '../Container';
import { Card } from './Card';

/**
 * ProgramCard is a pressable card with a full image overlay and a program label.
 */

interface ProgramCardProps {
  /** Whether to highlight the card label */
  highlighted?: boolean;
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Label for the cover image */
  label?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

export const ProgramCard: FC<ProgramCardProps> = ({
  label,
  imageAsset,
  imageSrc,
  onPress,
  highlighted,
}) => {
  const fontSize = 30;
  const labelHeight = fontSize + 2 * space[1];

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <FastImage
              accessibilityLabel={`This is an image card for ${label}`}
              style={{
                width: '100%',
                height: 250,
                borderRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
            <Container
              position="absolute"
              zIndex={20}
              bottom={CARD_BORDER_RADIUS}
              left={0}
              flexDirection="row"
            >
              <Container px={2} bg={highlighted ? colors.tangerineYellow : 'transparent'}>
                <Text
                  textDecorationLine="none"
                  color={highlighted ? 'black' : 'white'}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={fontSize}
                  lineHeight={labelHeight}
                >
                  {label}
                </Text>
              </Container>
              {highlighted ? (
                <Container
                  width={0}
                  height={0}
                  bg="transparent"
                  borderStyle="solid"
                  borderRightWidth={labelHeight / 2}
                  borderTopWidth={labelHeight}
                  borderRightColor="transparent"
                  borderTopColor={colors.tangerineYellow}
                />
              ) : null}
            </Container>
            <Container
              position="absolute"
              bottom={CARD_BORDER_RADIUS}
              left={150}
              flexDirection="row"
            >
              <Container px={2} bg={highlighted ? colors.eclipse : 'transparent'}>
                <Text
                  textDecorationLine="none"
                  color={highlighted ? 'white' : 'black'}
                  fontWeight="bold"
                  textAlign="center"
                  fontSize={fontSize - 15}
                  marginLeft={15}
                  marginRight={15}
                  lineHeight={labelHeight}
                >
                  Learn More
                </Text>
              </Container>
              {highlighted ? (
                <Container
                  width={0}
                  height={0}
                  bg="transparent"
                  borderStyle="solid"
                  borderRightColor="transparent"
                  borderTopColor={colors.eclipse}
                />
              ) : null}
            </Container>
          </>
        );
      }}
      renderBottom={(): ReactNode => {
        return null;
      }}
    />
  );
};

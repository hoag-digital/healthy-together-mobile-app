import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';
import { Container } from '../Container';
import { Text } from '../Text';
import { CARD_BORDER_RADIUS } from '../../utils';
import { colors } from '../../styles';
import { Card } from './Card';

/**
 * CategoryCard is a pressable card with a full image overlay and a class category label.
 */

interface ClassCategoryCardProps {
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string | null | undefined;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Label for the cover image */
  label?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

export const ClassCategoryCard: FC<ClassCategoryCardProps> = props => {
  const { label, imageSrc, imageAsset, onPress } = props;

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <FastImage
              accessibilityLabel={`This is an image card for ${label}`}
              style={{
                width: '100%',
                height: 170,
                borderRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
            <Container position="absolute" bottom={CARD_BORDER_RADIUS} left={0} flexDirection="row">
              <Container px={2} bg="transparent">
                <Text
                  textDecorationLine="none"
                  color={colors.white}
                  fontWeight="bold"
                  fontSize={4}
                  textAlign="left"
                >
                  {label}
                </Text>
              </Container>
            </Container>
          </>
        );
      }}
      renderBottom={(): ReactNode => {
        return null;
      }}
    />
  );
};

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { colors } from '../../styles';
import Generic from '../../assets/images/generic_class.jpg';

import {
  ClassCategoryCard,
  ContentCard,
  EventGroupCard,
  ModuleCard,
  OnDemandEventCard,
  ProgramCard,
} from './';

storiesOf('components/Card', module)
  .add('Class Category', () => <ClassCategoryCard />)
  .add('Content', () => <ContentCard heading="My Heading" text="Some text to display" />)
  .add('EventGroup', () => <EventGroupCard id="TEST" />)
  .add('Module', () => (
    <ModuleCard
      label="My Module"
      secondaryLabel="Learn More"
      primaryColor={colors.balticSea}
      secondaryColor={colors.cinnabar}
      imageAsset={Generic}
    />
  ))
  .add('OnDemandEvent', () => <OnDemandEventCard hideMetadata id="test" />)
  .add('Program', () => <ProgramCard />);

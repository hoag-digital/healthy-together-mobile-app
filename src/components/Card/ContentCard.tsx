import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';

import { Container } from '../Container';
import { Text } from '../Text';

import { CARD_BORDER_RADIUS, DEFAULT_CONTENT_DESCRIPTION_LENGTH } from '../../utils';
import { trimText } from '../../utils/trimText';
import { colors } from '../../styles';
import { radii } from '../../styles/margins';
import NewsIcon from '../../assets/images/icons/news.svg';
import { Card } from './Card';

/**
 * This is the image/description combination card for news, classes, and videos displayed on the
 * Home Screen.
 */
interface ContentCardProps {
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string | null | undefined;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Article heading (title) */
  heading: string | null | undefined;
  /** Description text displayed under heading */
  text: string | null | undefined;
  /** Description of cover image (used for accessibility label) */
  label?: string;
  /** What label should be displayed on card */
  cardLabel?: string;
  /** What color should the container be for the card label */
  cardLabelBackgroundColor?: string;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

export const ContentCard: FC<ContentCardProps> = ({
  onPress,
  heading,
  imageSrc,
  imageAsset,
  label,
  cardLabel,
  cardLabelBackgroundColor,
  text,
}) => {
  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <Container
            fullWidth
            borderTopLeftRadius={CARD_BORDER_RADIUS}
            borderTopRightRadius={CARD_BORDER_RADIUS}
            position="relative"
          >
            <FastImage
              accessibilityLabel={label}
              style={{
                borderTopLeftRadius: CARD_BORDER_RADIUS,
                borderTopRightRadius: CARD_BORDER_RADIUS,
                aspectRatio: 16 / 9,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
          </Container>
        );
      }}
      renderBottom={(): React.ReactNode => {
        return (
          <>
            {cardLabel ? (
              <Container
                backgroundColor={cardLabelBackgroundColor}
                borderRadius={radii[1]}
                px={2}
                py={1}
                position="absolute"
                top={10}
                left={10}
                flexDirection="row"
                justifyContent="center"
                alignItems="center"
              >
                <NewsIcon height={10} width={10} />
                <Text fontSize={1} color={colors.white} marginLeft={1}>
                  {cardLabel}
                </Text>
              </Container>
            ) : null}

            <Container px={4} py={5} justifyContent="space-around" fullWidth fill>
              <Text fontWeight="bold" fontSize={3}>
                {heading}
              </Text>
              <Text fontSize={2} pt={1}>
                {text ? trimText(text, DEFAULT_CONTENT_DESCRIPTION_LENGTH) : null}
              </Text>
            </Container>
          </>
        );
      }}
    />
  );
};

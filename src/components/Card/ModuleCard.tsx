import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';
import { Icon } from 'react-native-elements';
import { Container } from '../Container';
import { space } from '../../styles';
import { CARD_BORDER_RADIUS } from '../../utils';
import { Text } from '../Text';

import { Card } from './Card';

interface ModuleCardProps {
  /** The background color of the ModuleCard */
  primaryColor?: string;
  /** The color of the spacing element between hooag logo and module label */
  secondaryColor?: string;
  /** Label for the cover image */
  label?: string;
  /** Secondary label displays in the right side of the label container */
  secondaryLabel?: string;
  /** Link to remote image used as background cover on card (preferred) */
  imageSrc?: string;
  /** Local image asset used as background cover on card (fallback) */
  imageAsset?: number;
  /** Callback function to invoke on press */
  onPress?: () => void;
}

/**
 * ModuleCard is a pressable card with a large hoag icon and module label.
 */
export const ModuleCard: FC<ModuleCardProps> = ({
  primaryColor,
  secondaryColor,
  label,
  secondaryLabel,
  imageSrc,
  imageAsset,
  onPress,
}) => {
  const fontSize = 30;
  const labelHeight = fontSize + 2 * space[1];
  const labelContainerHeight = 45;
  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <FastImage
              accessibilityLabel={`This is a module card for ${label}`}
              style={{
                width: '100%',
                height: 150,
                borderRadius: CARD_BORDER_RADIUS,
              }}
              source={imageSrc ? { uri: imageSrc } : imageAsset}
              resizeMode="cover"
            />
            <Container
              position="absolute"
              zIndex={20}
              bottom={CARD_BORDER_RADIUS}
              left={0}
              flexDirection="row"
              bg={primaryColor}
              width="95%"
              height={labelContainerHeight}
            >
              <Container flexDirection="row" flex={2} justifyContent="space-between" pl={2}>
                <Text
                  textDecorationLine="none"
                  color={secondaryColor}
                  fontWeight="bold"
                  fontSize={fontSize}
                  lineHeight={labelHeight}
                >
                  {label}
                </Text>
                <Container
                  width={0}
                  height={0}
                  bg={secondaryColor}
                  borderStyle="solid"
                  borderRightWidth={labelHeight / 1.75}
                  borderTopWidth={labelContainerHeight}
                  borderRightColor={secondaryColor}
                  borderTopColor={primaryColor}
                  position="absolute"
                  right={-1}
                />
              </Container>
              <Container bg={secondaryColor} flex={1} height={labelContainerHeight}>
                {secondaryLabel ? (
                  <Text
                    textDecorationLine="none"
                    color={primaryColor}
                    fontWeight="bold"
                    textAlign="right"
                    fontSize={fontSize - 15}
                    marginRight={10}
                    lineHeight={labelHeight}
                  >
                    {secondaryLabel}
                  </Text>
                ) : (
                  <Icon
                    name="chevron-right"
                    containerStyle={{
                      height: labelHeight,
                      justifyContent: 'center',
                      alignItems: 'flex-end',
                    }}
                  />
                )}
              </Container>
            </Container>
          </>
        );
      }}
      renderBottom={(): ReactNode => {
        return null;
      }}
    />
  );
};

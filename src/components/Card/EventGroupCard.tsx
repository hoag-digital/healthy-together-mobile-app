import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';
import { Container } from '../Container';
import { Text } from '../Text';
import { CARD_BORDER_RADIUS, DEFAULT_CONTENT_DESCRIPTION_LENGTH } from '../../utils';
import { trimText } from '../../utils/trimText';
import { colors, radii } from '../../styles';
import ClassIcon from '../../assets/images/icons/classes-label.svg';
import { EventGroup } from '../../graphql/types';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import { Card } from './Card';

/**
 * This is the list item to be displayed on the EventGroupsScreen
 */

interface EventGroupCardProps extends EventGroup {
  /** Callback function to invoke on press */
  onPress?: () => void;
  /** Determines if we display a label badge on the card */
  withBadge?: boolean;
}

export const EventGroupCard: FC<EventGroupCardProps> = ({
  name,
  mediumImage,
  description,
  onPress,
  withBadge = false,
}) => {
  const trimmedDescription = description
    ? trimText(description, DEFAULT_CONTENT_DESCRIPTION_LENGTH)
    : null;

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <Container
            fullWidth
            borderTopLeftRadius={CARD_BORDER_RADIUS}
            borderTopRightRadius={CARD_BORDER_RADIUS}
            position="relative"
          >
            <FastImage
              accessibilityLabel={`${name} class card`}
              style={{
                resizeMode: 'cover',
                borderTopLeftRadius: CARD_BORDER_RADIUS,
                borderTopRightRadius: CARD_BORDER_RADIUS,
                aspectRatio: 16 / 9,
              }}
              source={mediumImage ? { uri: mediumImage } : GenericClassImage}
            />
          </Container>
        );
      }}
      renderBottom={(): React.ReactNode => {
        return (
          <>
            {withBadge ? (
              <Container
                backgroundColor={colors.mediumOrchid}
                borderRadius={radii[1]}
                px={2}
                py={1}
                position="absolute"
                top={10}
                left={10}
                flexDirection="row"
                justifyContent="center"
                alignItems="center"
              >
                <ClassIcon height={10} width={10} />
                <Text fontSize={1} color={colors.white} marginLeft={1}>
                  Event
                </Text>
              </Container>
            ) : null}
            <Container padding={4} justifyContent="space-around" fullWidth fill>
              <Container pb={2} flexDirection="row" justifyContent="space-between">
                <Container width="80%">
                  <Text fontWeight="bold">{name}</Text>
                </Container>
              </Container>
              <Text fontSize={2}>{trimmedDescription}</Text>
            </Container>
          </>
        );
      }}
    />
  );
};

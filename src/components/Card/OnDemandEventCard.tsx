import React, { FC, ReactNode } from 'react';
import FastImage from 'react-native-fast-image';
import dayjs from 'dayjs';
import { Text } from '../Text';
import { trimText } from '../../utils/trimText';
import { DEFAULT_CONTENT_DESCRIPTION_LENGTH, t, CARD_BORDER_RADIUS } from '../../utils';
import { Container } from '../Container';
import { colors, radii } from '../../styles';
import { Event } from '../../graphql/types';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import OnDemandIcon from '../../assets/images/icons/on-demand.svg';
import { Card } from './Card';

// eslint-disable-next-line
const relativeTime = require('dayjs/plugin/relativeTime');

// required for semantic phrasing like 'X days ago'
dayjs.extend(relativeTime);

/**
 * This is the list item to be displayed on the OnDemandEventsScreen
 */

interface OnDemandEventCardProps extends Event {
  /** Callback function to invoke on press */
  onPress?: () => void;
  /** optional prop to hide the duration and date posted (used for homescreen uniformity) */
  hideMetadata: boolean;
}

export const OnDemandEventCard: FC<OnDemandEventCardProps> = ({
  name,
  mediumImage,
  description,
  duration,
  onPress,
  startTime,
  hideMetadata = false,
}) => {
  const trimmedDescription = description
    ? trimText(description, DEFAULT_CONTENT_DESCRIPTION_LENGTH)
    : null;

  const renderOriginalDate = !hideMetadata && !!startTime;

  return (
    <Card
      onPress={onPress}
      renderTop={(): ReactNode => {
        return (
          <>
            <Container
              backgroundColor={colors.mediumOrchid}
              borderRadius={radii[1]}
              px={2}
              py={1}
              position="absolute"
              top={10}
              left={10}
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
              zIndex={50}
            >
              <OnDemandIcon height={10} width={10} />
              <Text fontSize={1} color={colors.white} marginLeft={1}>
                {t('classes.onDemandClass.label')}
              </Text>
            </Container>
            <Container
              fullWidth
              borderTopLeftRadius={CARD_BORDER_RADIUS}
              borderTopRightRadius={CARD_BORDER_RADIUS}
              position="relative"
            >
              <FastImage
                accessibilityLabel={`${name} class card`}
                style={{
                  width: '100%',
                  height: 200,
                  resizeMode: 'cover',
                  borderTopLeftRadius: CARD_BORDER_RADIUS,
                  borderTopRightRadius: CARD_BORDER_RADIUS,
                }}
                source={mediumImage ? { uri: mediumImage } : GenericClassImage}
              />
              {!hideMetadata ? (
                <Container
                  backgroundColor={colors.balticSea50}
                  borderRadius={radii[1]}
                  px={2}
                  py={1}
                  position="absolute"
                  bottom={10}
                  right={10}
                  flexDirection="row"
                  justifyContent="center"
                  alignItems="center"
                  zIndex={50}
                >
                  <Text fontSize={1} color={colors.white} marginLeft={1}>
                    {/* TODO: update to convert to hours:min if over 60 */}
                    {`${duration} min`}
                  </Text>
                </Container>
              ) : null}
            </Container>
          </>
        );
      }}
      renderBottom={(): React.ReactNode => {
        return (
          <Container padding={4} justifyContent="space-around" fill fullWidth>
            <Container
              pb={2}
              flexDirection="row"
              justifyContent="space-between"
              alignItems="flex-start"
            >
              <Container width={renderOriginalDate ? '70%' : '100%'}>
                <Text fontWeight="bold">{name}</Text>
              </Container>
              {renderOriginalDate ? (
                <Text fontSize={2} fontWeight="bold">
                  {dayjs(startTime).fromNow()}
                </Text>
              ) : null}
            </Container>
            <Text fontSize={2}>{trimmedDescription}</Text>
          </Container>
        );
      }}
    />
  );
};

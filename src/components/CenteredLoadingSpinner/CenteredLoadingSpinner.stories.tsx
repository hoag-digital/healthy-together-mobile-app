import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { CenteredLoadingSpinner } from './CenteredLoadingSpinner';

storiesOf('components/CenteredLoadingSpinner', module).add('Default', () => (
  <CenteredLoadingSpinner />
));

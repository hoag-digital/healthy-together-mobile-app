import React, { FC } from 'react';
import { ActivityIndicator } from 'react-native';

import { Container } from '../Container';
import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING } from '../../utils';

interface Props {
  /** whether to add additional padding for the tab bar. (when used in full-screen Scrollviews) */
  withScrollviewPadding?: boolean;
  /** whether to make the background transparent, thereby allowing any existing background color to show */
  transparentBackground?: boolean;
}

const DEFAULT_PROPS: Props = {
  withScrollviewPadding: false,
  transparentBackground: false,
};

/**
 * This component renders a loading spinner centered in a flex layout
 */
export const CenteredLoadingSpinner: FC<Props> = (props = DEFAULT_PROPS) => {
  const { withScrollviewPadding, transparentBackground } = props;

  return (
    <Container
      fill
      bg={transparentBackground ? colors.transparent : colors.white}
      pt={withScrollviewPadding ? 5 : 0}
      pb={withScrollviewPadding ? SCROLLVIEW_BOTTOM_PADDING : 0}
    >
      <Container fill fullWidth centerContent>
        <ActivityIndicator />
      </Container>
    </Container>
  );
};

export default CenteredLoadingSpinner;

import React, { useState, FC, ReactElement } from 'react';
import { Icon } from 'react-native-elements';

import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { Modal } from '../Modal';
import { Container } from '../Container';

import { colors } from '../../styles';
import { t, DEFAULT_ICON_SIZE, SMALLEST_ICON_SIZE } from '../../utils';

// controls inner modal content
const MODES = {
  SORT: 'sort',
  FILTER: 'filter',
};

export type SelectableOption = {
  /** the label to display to the user for the selectable option */
  label: string;
  /** the computational value to denote the selectable option */
  value: string;
};

interface SortAndFilterProps {
  /** the avialble options for filtering */
  filterOptions: SelectableOption[];
  /** the callback to invoke when a filter option is selected */
  onFilterSelect: (arg: string) => void;
  /** the ids of the selected filter options: note: multiple can be active at once */
  activeFilterOptionIds: string[];
  /** the available options for sorting */
  sortOptions: SelectableOption[];
  /** the callback to invoke when a sort option is selected */
  onSortSelect: (arg: string) => void;
  /** the id of the active sort option. note: only a single option is expected */
  activeSortOptionId: string | null;
}

/**
 * SortAndFilter - a set of icons that presents a modal to the user with selectable options.
 *
 * Note that the sort and filter were combined into a single component because of a technical
 * limitation of react-native-modal where only one modal can be mounted at a time, and thus the sort
 * and filter options need to share the same modal instance
 *
 * If a third type of list element is added to this component the internal logic would need
 * to be slightly refactored but nothing major.
 */
export const SortAndFilter: FC<SortAndFilterProps> = ({
  filterOptions = [],
  onFilterSelect = (): void => {},
  activeFilterOptionIds = [],
  sortOptions = [],
  onSortSelect = (): void => {},
  activeSortOptionId = null,
}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [mode, setMode] = useState('sort');

  const isFilter = mode === MODES.FILTER;
  const optionsToDisplay = isFilter ? filterOptions : sortOptions;

  const renderOption = ({ label, value }): ReactElement => {
    const isActive = isFilter
      ? activeFilterOptionIds.includes(value)
      : activeSortOptionId === value;

    const onOptionPress = (): void => {
      if (isFilter) {
        onFilterSelect(value);
      } else {
        onSortSelect(value);
      }
    };

    return (
      <Touchable
        key={value}
        onPress={onOptionPress}
        flexDirection="column"
        alignItems="center"
        py={3}
        justifyContent="flex-start"
        borderBottomWidth={1}
        borderBottomColor={colors.whisper}
        width="85%"
      >
        <Container
          py={1}
          flexDirection="row"
          width="70%"
          alignItems="center"
          justifyContent="space-between"
        >
          <Text fontSize={3} height={DEFAULT_ICON_SIZE}>
            {label}
          </Text>
          {isActive ? (
            <Icon name="check" color={colors.terra} size={DEFAULT_ICON_SIZE} />
          ) : (
            // rendering a transparent icon to prevent jumping
            <Icon name="check" color={colors.transparent} size={DEFAULT_ICON_SIZE} />
          )}
        </Container>
      </Touchable>
    );
  };

  const renderSortIcon = (): ReactElement => {
    return (
      <Touchable
        onPress={(): void => {
          // TODO: improve async performance if noticeable in UI
          setMode(MODES.SORT);
          setIsVisible(true);
        }}
      >
        <Container flexDirection="row" alignItems="center">
          <Icon name="sort" type="font-awesome" size={SMALLEST_ICON_SIZE} />
          <Text pl={2} fontSize={1}>
            {t('sort.label')}
          </Text>
        </Container>
      </Touchable>
    );
  };

  const renderFilterIcon = (): ReactElement => {
    return (
      <Touchable
        onPress={(): void => {
          // TODO: improve async performance if noticeable in UI
          setMode(MODES.FILTER);
          setIsVisible(true);
        }}
      >
        <Container flexDirection="row" alignItems="center">
          <Icon name="filter-list" size={SMALLEST_ICON_SIZE} />
          <Text pl={1} fontSize={1}>
            {t('filter.label')}
          </Text>
        </Container>
      </Touchable>
    );
  };

  return (
    <>
      <Container flexDirection="row" justifyContent="flex-end" p={2} alignItems="center">
        {renderSortIcon()}
        <Container pl={4}>{renderFilterIcon()}</Container>
      </Container>
      <Modal isVisible={isVisible} setIsVisible={setIsVisible} bottomHalf>
        <Container fullWidth centerContent pb={5}>
          {/* The "drag" bar for the modal */}
          <Container
            borderBottomWidth={3}
            borderBottomColor={colors.silver}
            width="20%"
            alignContent="center"
            marginBottom={5}
          />
          {optionsToDisplay.map(renderOption)}
        </Container>
      </Modal>
    </>
  );
};

export default SortAndFilter;

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { GestationalAge } from '../../graphql/types';
import { Container } from '../Container';
import { colors } from '../../styles';
import { TrimesterProgress } from './TrimesterProgress';

const gestation: GestationalAge = {
  days: 5,
  weeks: 8,
  trimester: 1,
};

storiesOf('components/TrimesterProgress', module).add('Default', () => (
  <Container flex={1} bg={colors.chino} justifyContent="center">
    <TrimesterProgress gestationalAge={gestation} />
    <TrimesterProgress gestationalAge={{ ...gestation, trimester: 2, weeks: 24 }} />
    <TrimesterProgress gestationalAge={{ ...gestation, trimester: 2, weeks: 13 }} />
    <TrimesterProgress gestationalAge={{ ...gestation, trimester: 2, weeks: 17 }} />
    <TrimesterProgress gestationalAge={{ ...gestation, trimester: 3, weeks: 32 }} />
    <TrimesterProgress gestationalAge={{ ...gestation, trimester: 3, weeks: 42 }} />
  </Container>
));

import React, { FC } from 'react';

import { Banner } from '../Banner';
import { Container } from '../Container';
import { GestationalAge } from '../../graphql/types';
import { Text } from '../Text';
import { colors } from '../../styles';
import stringifyTrimester from '../../utils/stringifyTrimester';
import getTrimesterProgress from '../../utils/getTrimesterProgress';

interface Props {
  /** gestational age object that shows weeks/days/trimester */
  gestationalAge: GestationalAge;
}

/**
 * This component displays a progress of the users current trimester
 * each trimester is 13 weeks
 * 1-13 - First
 * 13-27 - Second
 * 27-40 - Third
 */
export const TrimesterProgress: FC<Props> = ({ gestationalAge }) => {
  const { weeks, trimester } = gestationalAge;

  const trimesterProgress = getTrimesterProgress(gestationalAge);

  return (
    <Banner>
      <Container py={2} px={1}>
        <Container flexDirection="row" px={1} width="100%">
          <Container flex={0.45} justifyContent="flex-end">
            <Text fontSize={4} fontWeight="bold">
              {stringifyTrimester(trimester)}
            </Text>
          </Container>
          <Container flex={0.55} alignItems="flex-end" justifyContent="flex-end">
            <Text fontSize={2}>
              You're
              <Text fontSize={3} fontWeight="bold">
                &nbsp;{weeks}&nbsp;
              </Text>
              weeks pregnant
            </Text>
          </Container>
        </Container>
        <Container flexDirection="row" mt={3}>
          <Container
            width={trimesterProgress.complete}
            height={10}
            backgroundColor={colors.buttermilk}
            borderBottomLeftRadius={25}
            borderTopLeftRadius={25}
            borderBottomRightRadius={trimesterProgress.complete === '100%' ? 25 : 0}
            borderTopRightRadius={trimesterProgress.complete === '100%' ? 25 : 0}
            alignItems="flex-start"
          />
          <Container
            width={trimesterProgress.remaining}
            height={10}
            backgroundColor={colors.gray}
            borderBottomLeftRadius={trimesterProgress.remaining === '100%' ? 25 : 0}
            borderBottomRightRadius={25}
            borderTopRightRadius={25}
            alignItems="flex-start"
          />
        </Container>
      </Container>
    </Banner>
  );
};

export default TrimesterProgress;

import React, { FC } from 'react';
import { Container } from '../Container';
import { GestationalAge } from '../../graphql/types';
import { colors } from '../../styles';
import getGestationalProgress from '../../utils/getGestationalProgress';

interface Props {
  /** gestational age object that shows weeks/days/trimester */
  gestationalAge: GestationalAge;
}

/**
 * This component displays a progress of the users current gestation (out of 40 weeks)
 */
export const GestationalProgress: FC<Props> = ({ gestationalAge }) => {
  const gestationProgress = getGestationalProgress(gestationalAge);

  return (
    <Container>
      <Container flexDirection="row" mt={3} px={4}>
        <Container
          width={gestationProgress.complete}
          height={12}
          backgroundColor={colors.buttermilk}
          borderBottomLeftRadius={25}
          borderTopLeftRadius={25}
          borderBottomRightRadius={gestationProgress.complete === '100%' ? 25 : 0}
          borderTopRightRadius={gestationProgress.complete === '100%' ? 25 : 0}
          alignItems="flex-start"
        />
        <Container
          width={gestationProgress.remaining}
          height={12}
          backgroundColor={colors.gray}
          borderBottomLeftRadius={gestationProgress.remaining === '100%' ? 25 : 0}
          borderBottomRightRadius={25}
          borderTopRightRadius={25}
          alignItems="flex-start"
        />
      </Container>
    </Container>
  );
};

export default GestationalProgress;

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { GestationalProgress } from './GestationalProgress';

storiesOf('components/GestationalProgress', module).add('Default', () => <GestationalProgress />);

import React, { FC, ReactElement } from 'react';
import { FlatList } from 'react-native';

import { Container } from '../Container';
import { Touchable } from '../Touchable';
import { Text } from '../Text';
import { colors } from '../../styles';
import { IS_ANDROID, TOUCHABLE_PRESS_DELAY } from '../../utils';

import NewsIcon from '../../assets/images/icons/news.svg';

interface Props {
  /** the id of the active item */
  activeId: string | null;
  /** the list if items to render in the horizontal picker */
  items: any[];
  /** the callback to invoked when you select an item */
  onPress: (_arg: any) => void;
}

const DEFAULT_PROPS: Props = {
  activeId: null,
  items: [],
  onPress: (_arg: any) => {},
};

/**
 * This is a horizontal picker component for module subsections
 */
export const HorizontalPicker: FC<Props> = (props = DEFAULT_PROPS) => {
  const { items, onPress, activeId } = props;

  const renderItem = ({ item, index }): ReactElement => {
    const isFirst = index === 0;
    const isLast = index === items.length - 1;
    const isActive = item.id === activeId;
    const { Icon } = item;
    return (
      <Touchable
        delayPressIn={TOUCHABLE_PRESS_DELAY}
        key={`${item}-${index}`}
        borderRadius={4}
        minWidth={IS_ANDROID ? 70 : 85}
        minHeight={IS_ANDROID ? 80 : 95}
        py={2}
        ml={isFirst ? 4 : 2}
        mr={isLast ? 4 : 2}
        bg={isActive ? colors.white : colors.silver}
        onPress={(): void => onPress(item.id)}
        centerContent
      >
        <Container fill fullWidth centerContent>
          {Icon ? <Icon /> : <NewsIcon height={40} width={40} />}
        </Container>
        <Text
          fontSize={IS_ANDROID ? 1 : 2}
          fontWeight="bold"
          color={isActive ? colors.black : colors.gray}
        >
          {item.name}
        </Text>
      </Touchable>
    );
  };

  return (
    <Container fullWidth>
      <FlatList style={{ flexGrow: 0 }} data={items} renderItem={renderItem} horizontal />
    </Container>
  );
};

export default HorizontalPicker;

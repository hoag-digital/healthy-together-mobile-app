import React, { FC } from 'react';
import { Text, ComponentProps } from '../Text';

const DEFAULT_PROPS: Partial<ComponentProps> = {};

/**
 * Drop-and-go styling for paragraph text.
 */
export const BodyText: FC<ComponentProps> = (props = DEFAULT_PROPS) => {
  const { children } = props;

  return (
    <Text fontSize={3} lineHeight={3} {...props}>
      {children}
    </Text>
  );
};

export default BodyText;

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { BodyText } from './BodyText';

storiesOf('components/BodyText', module).add('Default', () => (
  <BodyText>
    Omnis similique accusantium ea. Ipsam nulla nisi consequuntur sint recusandae voluptatem et
    laboriosam suscipit. Soluta temporibus odio hic dignissimos. Officiis nemo aliquid excepturi.
  </BodyText>
));

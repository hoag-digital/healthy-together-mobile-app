import React, { ReactElement } from 'react';
import HTML from 'react-native-render-html';

import { colors, fonts, fontSizes, space } from '../../styles';
import { lineHeights } from '../../styles/fonts';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../utils';
import { openInAppBrowser } from '../../utils/openInAppBrowser';

/*
Media is sized to the device width by default.
We must offset the width by accounting for any content 
padding in the parent, grandparent, etc nodes above the
rich text content.
*/
const RICH_TEXT_MEDIA_WIDTH = SCREEN_WIDTH - space[5] * 2;
const RICH_TEXT_MEDIA_HEIGHT = SCREEN_HEIGHT * 0.4;

const baseContainerStyle = {
  paddingTop: space[2],
  padding: space[5],
};

const baseHeadingStyle = {
  paddingVertical: space[2],
};

const baseListStyle = {
  marginBottom: 0,
  paddingTop: space[3],
};

const baseBodyFontStyle = {
  color: colors.black,
  fontSize: fontSizes[3],
  lineHeight: lineHeights[4],
};

const tagsStyles = {
  h1: {
    ...baseHeadingStyle,
  },
  h2: {
    ...baseHeadingStyle,
  },
  h3: {
    ...baseHeadingStyle,
  },
  h4: {
    ...baseHeadingStyle,
  },
  h5: {
    ...baseHeadingStyle,
  },
  h6: {
    ...baseHeadingStyle,
  },
  p: {
    ...baseBodyFontStyle,
    paddingVertical: space[2],
  },
  ul: {
    ...baseBodyFontStyle,
    ...baseListStyle,
  },
  ol: {
    ...baseBodyFontStyle,
    ...baseListStyle,
  },
  li: {
    ...baseBodyFontStyle,
  },
  a: {
    ...baseBodyFontStyle,
    color: colors.aqua,
  },
  figcaption: {
    ...baseBodyFontStyle,
    color: colors.eclipse,
  },
  iframe: {
    width: RICH_TEXT_MEDIA_WIDTH,
    height: RICH_TEXT_MEDIA_HEIGHT,
  },
  img: {
    marginVertical: space[2],
  },
  video: {
    marginVertical: space[2],
  },
};

const baseFontStyle = {
  ...fonts.regular,
};

function alterNode(node): any {
  if (node.name === 'iframe') {
    // unset width & height attrs to allow iframe to receive style from tagsStyles prop
    node.attribs.width = undefined;
    node.attribs.height = undefined;

    return node;
  }
}

async function onLinkPress(_event, url): Promise<void> {
  openInAppBrowser(url);
}

export const PostRichText = ({ richText }): ReactElement => {
  return (
    <HTML
      alterNode={alterNode}
      baseFontStyle={baseFontStyle}
      containerStyle={baseContainerStyle}
      html={richText}
      imagesMaxWidth={RICH_TEXT_MEDIA_WIDTH}
      onLinkPress={onLinkPress}
      tagsStyles={tagsStyles}
    />
  );
};

export default PostRichText;

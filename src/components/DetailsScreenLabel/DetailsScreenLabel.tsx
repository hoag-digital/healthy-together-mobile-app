import React, { FC } from 'react';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import { Container } from '../Container';
import { Text } from '../Text';
import { colors } from '../../styles';
import { SMALLEST_ICON_SIZE, t } from '../../utils';

import ClassesIcon from '../../assets/images/icons/classes-label.svg';
import NewsIcon from '../../assets/images/icons/news.svg';
import OnDemandIcon from '../../assets/images/icons/on-demand.svg';

/**
 * iconType: IconComponent
 */
const ICONS = {
  class: ClassesIcon,
  post: NewsIcon,
  ondemand: OnDemandIcon,
};

interface DetailsScreenLabelProps {
  /** the text value of the label */
  label?: string;
  /** the background color override of the label */
  backgroundColor?: string;
  /** the optional icon component type to render with the label */
  iconType?: 'class' | 'post' | 'ondemand';
}

/**
 * DetailsScreenLabel is the category label displayed on post and event details screens
 * TODO: fix typing top extend Container props? NO... remove style prop pass-through
 */
export const DetailsScreenLabel: FC<DetailsScreenLabelProps> = props => {
  const Icon = props.iconType ? ICONS[props.iconType] : null;

  return (
    <Container
      zIndex={51}
      backgroundColor={props.backgroundColor || colors.mediumOrchid}
      borderTopRightRadius={7}
      borderBottomRightRadius={7}
      position="absolute"
      left={0}
      top={10 + getStatusBarHeight()}
      flexDirection="row"
      overflow="hidden"
      justifyContent="center"
      alignItems="center"
      px={3}
      py={1}
      {...props}
    >
      {Icon ? (
        <Icon width={SMALLEST_ICON_SIZE} height={SMALLEST_ICON_SIZE} marginRight={5} />
      ) : null}
      <Text color={colors.white} fontSize={2} fontWeight="600">
        {props.label || t('classes.details.label')}
      </Text>
    </Container>
  );
};

export default DetailsScreenLabel;

import { storiesOf } from '@storybook/react-native';
import React from 'react';

import { t } from '../../utils';
import { colors } from '../../styles';
import { DetailsScreenLabel } from './DetailsScreenLabel';

storiesOf('components/DetailsScreenLabel', module).add('Default', () => (
  <>
    <DetailsScreenLabel iconType="class" label={t('classes.details.liveStreamLabel')} />
    <DetailsScreenLabel iconType="ondemand" label={t('onDemand.class.label')} />
    <DetailsScreenLabel
      iconType="post"
      backgroundColor={colors.summerSky}
      label={t('news.label')}
    />
    <DetailsScreenLabel
      iconType="post"
      backgroundColor={colors.sunsetOrange}
      label={t('news.featuredLabel')}
    />
  </>
));

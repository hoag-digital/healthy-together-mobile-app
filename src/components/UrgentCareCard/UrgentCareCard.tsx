import React, { FC } from 'react';
import FastImage, { Source } from 'react-native-fast-image';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { colors } from '../../styles';
import Plus from '../../assets/images/urgent-plus.svg';
import { t } from '../../utils';

interface Props {
  /** click action to be fired onPress */
  onPress: () => void;
  /** identifier for test framework */
  testID?: string;
  name: string;
  closes: string;
  wait: number;
  id: number;
  locationImage: Source;
}

/**
 * Component docs: UrgentCareCard is a rounded corner rectangle that displays an image and wait times
 * // TODO: extend for a touchable action defined by usage
 * // TODO: need to figure out better solution for width of the white container and having it align with the right edge
 * // TODO: text from i18n
 */
export const UrgentCareCard: FC<Props> = ({ onPress, closes, name, wait, locationImage }) => {
  return (
    <Touchable onPress={onPress}>
      <Container
        borderRadius={15}
        backgroundColor={colors.skyBlue}
        minHeight={100}
        fullWidth
        overflow="hidden"
        borderRightWidth={1}
        borderRightColor={colors.whisper}
        p={3}
      >
        <Container flexDirection="row">
          <FastImage source={locationImage} borderRadius={15} style={{ width: 120, height: 95 }} />
          <Container paddingLeft={20} flex={1}>
            <Text fontSize={3} fontWeight="bold" color={colors.white}>
              {name}
            </Text>
            <Text fontSize={2} color={colors.white}>
              {closes}
            </Text>
            <Container
              backgroundColor={colors.white}
              borderRightWidth={1}
              borderRightColor={colors.whisper}
              width="200%"
              alignItems="center"
              marginTop={2}
              borderTopStartRadius={50}
              borderBottomStartRadius={50}
              flexDirection="row"
              paddingLeft={1}
              paddingVertical={2}
            >
              <Container mr={4}>
                <Plus />
              </Container>
              <Text fontSize={5} fontWeight="bold">
                {wait ? (
                  <>
                    {`${wait} `}
                    <Text fontSize={1} fontWeight="bold">
                      min
                    </Text>
                  </>
                ) : (
                  t('care.closed').toUpperCase()
                )}
              </Text>
            </Container>
          </Container>
        </Container>
      </Container>
    </Touchable>
  );
};

export default UrgentCareCard;

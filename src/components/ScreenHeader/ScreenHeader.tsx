import React, { FC } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { NavigationSwitchScreenProps, withNavigation } from 'react-navigation';
import { FlexProps, SpaceProps } from 'styled-system';

import { Icon, Avatar } from 'react-native-elements';

import DefaultLogo from '../../assets/images/hoag-logo-solid.svg';
import ModuleLogo from '../../assets/images/hoag-logo-white.svg';

import { Container } from '../Container';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { MODULE_COLORS } from '../Screen';
import { colors } from '../../styles';
import {
  LARGER_ICON_SIZE,
  STATUS_BAR_PADDING_SCALE,
  t,
  DEFAULT_HITSLOP,
  DEFAULT_ICON_SIZE,
} from '../../utils';
import { LoggedOutContent, LoggedInContent } from '../../lib/AuthUtils';

interface ScreenHeaderProps extends NavigationSwitchScreenProps {
  /** Whether to display the drawer menu button */
  drawerEnabled?: boolean;
  loginBanner?: boolean;
  /** Whether to display a back button, should not be used with drawer */
  exitEnabled?: boolean;
}

type ComponentProps = ScreenHeaderProps & FlexProps & SpaceProps;

const ScreenHeaderComponent: FC<ComponentProps> = ({
  activeModule,
  exitEnabled,
  drawerEnabled,
  loginBanner,
  navigation,
}) => {
  const toggleDrawer = (): void => {
    navigation.toggleDrawer();
  };

  const navigateToLogin = (): void => {
    navigation.navigate('Login');
  };

  const resetModule = (): void => {
    navigation.navigate('MyHoag');
  };

  return (
    <Container bg={activeModule ? MODULE_COLORS[activeModule].header : colors.white}>
      <Container
        minHeight={30}
        flexDirection="row"
        justifyContent="space-around"
        alignItems="center"
        fullWidth
        borderBottomWidth={1}
        borderBottomColor={activeModule ? MODULE_COLORS[activeModule].border : colors.whisper}
        pt={STATUS_BAR_PADDING_SCALE}
        pb={2}
      >
        <Container width="30%" flexDirection="row" justifyContent="flex-start">
          {drawerEnabled && (
            <Touchable testID="drawer-icon" onPress={toggleDrawer} px={3} hitSlop={DEFAULT_HITSLOP}>
              <Icon
                name="ios-menu"
                type="ionicon"
                size={LARGER_ICON_SIZE}
                color={activeModule ? MODULE_COLORS[activeModule].text : colors.black}
              />
            </Touchable>
          )}
          {exitEnabled ? (
            <Touchable testID="back-icon" onPress={resetModule} px={3} hitSlop={DEFAULT_HITSLOP}>
              <Icon
                name="close-circle-outline"
                type="material-community"
                size={LARGER_ICON_SIZE}
                color={activeModule ? MODULE_COLORS[activeModule].text : colors.black}
              />
            </Touchable>
          ) : null}
        </Container>
        <Container width="40%" centerContent flexDirection="row">
          {activeModule ? <ModuleLogo height="30" width={50} /> : <DefaultLogo />}
          <Text
            color={activeModule ? MODULE_COLORS[activeModule].text : colors.gray}
            fontSize={2}
            marginTop={2}
          >
            {` | ${t(`${activeModule || 'home'}.name`)}`}
          </Text>
        </Container>
        <Container width="30%" alignItems="flex-end" pr={3}>
          {/* TODO: add navigation to a user profile screen and figure out the default icon to display */}
          <LoggedInContent>
            <Avatar
              rounded
              showEditButton
              onPress={(): boolean => navigation.navigate('Account')}
              icon={{
                name: 'user',
                type: 'font-awesome',
                size: DEFAULT_ICON_SIZE,
                color: colors.aqua,
              }}
            />
          </LoggedInContent>
        </Container>
      </Container>
      {/* Global login banner for all screens leveraging this component */}
      {loginBanner && (
        <LoggedOutContent>
          <TouchableWithoutFeedback onPress={navigateToLogin}>
            <Container py={1} centerContent bg={colors.terra}>
              {/* TODO: i18n */}
              <Text fontSize={1} color={colors.white}>
                Login to gain full access to the app!{' '}
                <Text fontWeight="bold" fontSize={1} color={colors.white}>
                  →
                </Text>
              </Text>
            </Container>
          </TouchableWithoutFeedback>
        </LoggedOutContent>
      )}
      {/* end Login banner */}
    </Container>
  );
};

ScreenHeaderComponent.defaultProps = {
  drawerEnabled: false,
};

export const ScreenHeader = withNavigation(ScreenHeaderComponent);

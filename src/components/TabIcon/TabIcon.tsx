import React, { FC } from 'react';

import { Container } from '../Container';
import { Text } from '../Text';
import { colors } from '../../styles';

interface Props {
  /** whether the orange indicator should be displayed next to the label */
  activeTab?: boolean;
  /** the name of the tab */
  label?: string;
  /** the icon component to render */
  icon?: JSX.Element;
}

const DEFAULT_PROPS: Partial<Props> = {
  activeTab: false,
};

/**
 * TabIcon is a custom component that renders the Icon and Tab Bar Label for the Tab Navigation
 */
export const TabIcon: FC<Props> = (props = DEFAULT_PROPS) => {
  const { icon, label, activeTab } = props;

  return (
    <Container alignItems="center">
      <Container flexDirection="row" paddingLeft={2}>
        {icon ? icon : null}
      </Container>
      <Container pt={1} flexDirection="row" alignItems="center" left={-3}>
        {/* Rendering a transparent indicator prevents text jumping :) */}
        <Text fontSize={6.5} color={activeTab ? colors.crusta : colors.transparent}>
          {'\u2B24'}
        </Text>
        <Text fontSize={1} ml={1}>
          {label}
        </Text>
      </Container>
    </Container>
  );
};

export default TabIcon;

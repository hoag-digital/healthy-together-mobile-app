import React, { FC, ReactNode } from 'react';

import { Container } from '../Container';
import { Text } from '../Text';
import { space } from '../../styles/margins';
import { fontSizes } from '../../styles/fonts';

import AppConnectIcon from '../../assets/images/menu/appconnect.svg';
import CareIcon from '../../assets/images/menu/care.svg';
import ClassIcon from '../../assets/images/menu/classes.svg';
import FAQIcon from '../../assets/images/menu/fact.svg';
import HomeIcon from '../../assets/images/menu/home.svg';
import LoginIcon from '../../assets/images/menu/login.svg';
import LogoutIcon from '../../assets/images/menu/logout.svg';
import MyHoagIcon from '../../assets/images/menu/my-hoag.svg';
import ProgramIcon from '../../assets/images/menu/programs.svg';
import ScheduleIcon from '../../assets/images/menu/my-classes.svg';
import SettingsIcon from '../../assets/images/menu/settings.svg';
import TelehealthIcon from '../../assets/images/menu/telehealth.svg';

import { t } from '../../utils';

const ICON_HEIGHT = 20;
const ICON_WIDTH = 20;

/**
 * Returns an i18n route name for the route of the tab navigator.
 * "routeName" params map to the keys of the navigationOptions object.
 */
export function getLabelForRoute(routeName: string): string {
  let label = '';

  switch (routeName) {
    case 'Home':
      label = t('navigation.home');
      break;
    case 'Classes':
      label = t('navigation.classes');
      break;
    case 'Care':
      label = t('navigation.care');
      break;
    case 'MyHoag':
      label = t('navigation.myHoag');
      break;
    case 'Programs':
      label = t('navigation.programs');
      break;
    case 'Schedule':
      label = t('navigation.schedule');
      break;
    default:
      break;
  }

  return label;
}

interface CustomDrawerItemProps {
  /** child contents to render */
  children: ReactNode;
  /** the name of the route, i.e. Home, Settings, etc. */
  itemKey: string;
  /** boolean which indicates is the active user is "logged-out" */
  isLoggedOut: boolean;
}

function renderIcon(itemKey: string): ReactNode {
  switch (itemKey) {
    case 'AppConnect':
      return <AppConnectIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'FAQ':
      return <FAQIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Home':
      return <HomeIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Telehealth':
      return <TelehealthIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Settings':
      return <SettingsIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Classes':
      return <ClassIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'MyHoag':
      return <MyHoagIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Care':
      return <CareIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Schedule':
      return <ScheduleIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Programs':
      return <ProgramIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Logout':
      return <LogoutIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    case 'Login':
      return <LoginIcon height={ICON_HEIGHT} width={ICON_WIDTH} />;
    default:
      return null;
  }
}

/**
 * CustomDrawerItem: used to render the links in the CustomDrawerNav in the DrawerNav
 */
export const CustomDrawerItem: FC<CustomDrawerItemProps> = ({ children, itemKey, isLoggedOut }) => {
  const isMockTabRoute = itemKey.indexOf('-Tab') > -1;
  let customLabel = children;

  // hide the default Home route, which is the TabNavigator itself
  if (itemKey === 'Home') return null;

  // custom labels and keys for the tab route proxies
  if (isMockTabRoute) {
    itemKey = itemKey.replace('-Tab', '');
    customLabel = getLabelForRoute(itemKey);
  }

  // toggling login/logout label depending on the user's state
  if (isLoggedOut && itemKey === 'Logout') {
    customLabel = 'Login';
    itemKey = 'Login';
  }

  return (
    <Container flexDirection="row" justifyContent="center" alignItems="center" py={1}>
      {renderIcon(itemKey)}
      <Text marginLeft={space[3]} fontSize={fontSizes[4]}>
        {customLabel}
      </Text>
    </Container>
  );
};

export default CustomDrawerItem;

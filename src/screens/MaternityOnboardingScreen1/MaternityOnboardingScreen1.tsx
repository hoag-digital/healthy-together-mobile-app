import React, { FC, ReactNode, useState } from 'react';
import { StatusBar } from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import analytics from '@react-native-firebase/analytics';
import { Button } from '../../components/Button';
import { colors, fonts, fontSizes, space } from '../../styles';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { SCREEN_HEIGHT, SCREEN_WIDTH, t } from '../../utils';
import { setUserStatus, USER_ROLE } from '../../utils/maternityOnboarding';
import { Text } from '../../components/Text';

const checkboxStyle = {
  iconRight: true,
  right: true,
  checkedColor: colors.oxley,
  containerStyle: { backgroundColor: colors.transparent, borderWidth: 0 },
  textStyle: { color: colors.gray, fontSize: 24, fontWeight: '400', paddingRight: 75 },
  checkedIcon: 'check-square',
};

export const MaternityOnboardingScreen1: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const [isPregnant, setIsPregnant] = useState(false);

  const onNext = async (): Promise<void> => {
    //TODO: bring this line back when we allow role selection again.
    //await setUserStatus(isPregnant ? USER_ROLE.PREGNANT : USER_ROLE.NEW_PARENT);
    analytics().logEvent('maternity_onboarding_click_next_1');
    await setUserStatus(USER_ROLE.PREGNANT);
    isPregnant
      ? navigation.navigate('MaternityOnboarding2')
      : navigation.navigate('CustomizeMaternityExperience');
  };

  const onCancel = (): void => {
    analytics().logEvent('maternity_onboarding_cancel_1');
    navigation.navigate('MyHoag');
  };

  return (
    <Screen testID="maternityOnboardingScreen1" backgroundColor={colors.pearlLusta}>
      <StatusBar barStyle="light-content" />
      <Container
        justifyContent="flex-start"
        alignItems="center"
        pt={SCREEN_HEIGHT * 0.2}
        height={SCREEN_HEIGHT}
      >
        <Text fontSize={7} color={colors.chino} {...fonts.light}>
          {t('maternity.onboarding.firstStep.title')}
        </Text>
        <Container
          my={space[8]}
          p={1}
          backgroundColor={colors.white}
          borderRadius={25}
          borderWidth={1}
          borderColor={colors.white}
          minHeight={75} //this was 100 with the second checkbox to give more space
          width={SCREEN_WIDTH * 0.85}
        >
          <CheckBox
            title={t('maternity.onboarding.firstStep.pregnantOption')}
            checked={isPregnant}
            onPress={(): void => setIsPregnant(!isPregnant)}
            {...checkboxStyle}
          />
          {/* //TODO enable this checkbox when new parent content is available in future release
          <CheckBox
            title={t('maternity.onboarding.firstStep.newParentOption')}
            checked={!isPregnant}
            onPress={(): void => setIsPregnant(!isPregnant)}
            {...checkboxStyle}
          /> */}
        </Container>
        <Container fullWidth flexDirection="row" justifyContent="center">
          <Container width="50%" alignItems="flex-end" mx={2}>
            {/** Adding cancel button since we only have one role to choose from and if they aren't pregnant then we want to give an exit option */}
            <Button
              label={t('maternity.onboarding.buttons.cancel')}
              backgroundColor={colors.chino}
              width="75%"
              py={2}
              fontSize={6}
              color={colors.white}
              borderRadius={35}
              onPress={onCancel}
              renderIcon={(): ReactNode => (
                <Container pl={1} height={45} justifyContent="center">
                  {
                    //this keeps the buttons consistent looking
                  }
                </Container>
              )}
            />
          </Container>
          <Container width="50%" alignItems="flex-start" mx={2}>
            <Button
              label={t('maternity.onboarding.buttons.next')}
              disabled={!isPregnant}
              backgroundColor={colors.wewak}
              width="75%"
              py={2}
              fontSize={6}
              color={colors.white}
              borderRadius={35}
              renderIcon={(): ReactNode => (
                <Container pl={1} height={45} justifyContent="center">
                  <Icon
                    name="chevron-right"
                    color={isPregnant ? colors.white : colors.disabled}
                    size={fontSizes[6]}
                  />
                </Container>
              )}
              iconLeft={false}
              onPress={onNext}
            />
          </Container>
        </Container>
      </Container>
    </Screen>
  );
};

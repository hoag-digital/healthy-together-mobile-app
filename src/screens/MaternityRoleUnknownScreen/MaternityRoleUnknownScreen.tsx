import React, { FC } from 'react';
import styled from '@emotion/native';
import { ImageBackground, StyleSheet, StatusBar } from 'react-native';
import { Icon } from 'react-native-elements';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Button } from '../../components/Button';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { LARGER_ICON_SIZE, DEFAULT_HITSLOP, t } from '../../utils';
import { colors } from '../../styles';
import Logo from '../../assets/images/hoag-logo-white.svg';
import welcomeImg from '../../assets/images/welcome-bg.png';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const MaternityRoleUnknownScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const resetModule = (): void => {
    navigation.navigate('MyHoag');
  };

  const completeProfile = (): void => {
    resetModule();
    navigation.navigate('Account');
  };

  return (
    <BackgroundImage source={welcomeImg} resizeMode="stretch">
      <Screen testID="welcomeScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Touchable
          testID="back-icon"
          onPress={resetModule}
          px={3}
          hitSlop={DEFAULT_HITSLOP}
          style={{ position: 'absolute', top: 20, left: 0, zIndex: 100 }}
        >
          <Icon
            name="close-circle-outline"
            type="material-community"
            size={LARGER_ICON_SIZE}
            color={colors.black}
          />
        </Touchable>
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Text fontWeight="bold" fontSize={5} py={2} textAlign="center">
            {t('maternity.unknownRole.title')}
          </Text>
          <Text fontWeight="700" fontSize={4} py={2} textAlign="center">
            {t('maternity.unknownRole.line1')}
          </Text>
          <Text fontWeight="500" fontSize={3} py={2} textAlign="center">
            {t('maternity.unknownRole.line2')}
          </Text>
          <Button
            label="Account Settings"
            backgroundColor={colors.bouquet}
            color={colors.white}
            fontSize={3}
            m={2}
            p={2}
            onPress={completeProfile}
          />
        </Container>
      </Screen>
    </BackgroundImage>
  );
};

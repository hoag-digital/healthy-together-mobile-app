import React, { ReactElement, FC, useEffect, useState } from 'react';
import { ScrollView } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import VideoPlayer from 'react-native-video-controls';

import FastImage from 'react-native-fast-image';
import { NavigationActions } from 'react-navigation';
import { Icon } from 'react-native-elements';
import { FloatingButton } from '../../components/FloatingButton';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING, SCREEN_WIDTH, LARGEST_ICON_SIZE } from '../../utils';
import { AuthConsumer } from '../../lib/AuthConsumer';

import imageUpperLeft from '../../assets/images/biocircuit/1.jpg';
import imageUpperRight from '../../assets/images/biocircuit/2.jpg';
import imageLowerLeft from '../../assets/images/biocircuit/3.jpg';
import imageLowerRight from '../../assets/images/biocircuit/4.jpg';
import { LoginModal } from '../../components/LoginModal';
import { Touchable } from '../../components/Touchable';

// Action that is dispatched from the login screen to navigate back to program details.
const AUTH_TO_PROGRAM_DETAILS_REDIRECT_ACTION = NavigationActions.navigate({
  routeName: 'Home',
  action: NavigationActions.navigate({
    routeName: 'MyHoag',
    action: NavigationActions.navigate({
      routeName: 'BiocircuitOnboarding',
      action: NavigationActions.navigate({
        routeName: 'BiocircuitIntro',
        action: NavigationActions.navigate({
          routeName: 'BiocircuitDetails',
        }),
      }),
    }),
  }),
});

//TODO: replace with the correct URL this is our upload of THEIR video
const VIDEO_LINK =
  'https://player.vimeo.com/external/393813850.hd.mp4?s=01815f287b15c0bb8795f10e86765c6fdd53d47d&profile_id=174';

const VIDEO_THUMBNAIL = 'https://i.vimeocdn.com/video/859456430_640.jpg';
const BOTTOM_SCROLL_POSITION = 165;

export const BiocircuitIntroScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const [isBottom, setIsBottom] = useState(false);
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const [pauseVideo, setPauseVideo] = useState(true);
  const [showThumbnail, setShowThumbnail] = useState(true);

  const resetVideo = (): void => {
    setShowThumbnail(true);
  };

  const endVideo = (): void => {
    setShowThumbnail(true);
  };

  const playVideo = (): void => {
    setShowThumbnail(false);
    setPauseVideo(false);
  };

  useEffect(() => {
    const willFocus = navigation.addListener('willFocus', () => resetVideo());
    const willBlur = navigation.addListener('willBlur', () => endVideo());

    return (): void => {
      willFocus.remove();
      willBlur.remove();
    };
  });

  const proceedToBiocircuitDetailsScreen = (): void => {
    navigation.navigate('BiocircuitDetails');
  };

  const handleScroll = (event): void => {
    if (event.nativeEvent.contentOffset.y < BOTTOM_SCROLL_POSITION) setIsBottom(false);
    else {
      setIsBottom(true);
    }
  };

  const onCreateAccount = (): void => {
    navigation.navigate('Registration', {
      redirect: {
        action: AUTH_TO_PROGRAM_DETAILS_REDIRECT_ACTION,
        callback: proceedToBiocircuitDetailsScreen,
      },
    });
  };

  const onLogin = (): void => {
    navigation.navigate('Login', {
      redirect: {
        action: AUTH_TO_PROGRAM_DETAILS_REDIRECT_ACTION,
        callback: proceedToBiocircuitDetailsScreen,
      },
    });
  };

  return (
    <Screen
      testID="biocircuit-screen"
      moduleHeader
      activeModule="biocircuit"
      backgroundColor={colors.white}
      fill
      height="100%"
      paddingTop={0}
    >
      {/* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */}
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        scrollEventThrottle={16}
        onScroll={(event): void => handleScroll(event)}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pb={SCROLLVIEW_BOTTOM_PADDING * 2}>
          <Container>
            {showThumbnail ? (
              <Touchable onPress={(): void => playVideo()}>
                <FastImage
                  source={{ uri: VIDEO_THUMBNAIL }}
                  style={{ width: SCREEN_WIDTH, height: 250 }}
                />
                <Icon
                  name="play-circle"
                  type="font-awesome"
                  size={LARGEST_ICON_SIZE}
                  color={colors.tangerineYellow}
                  containerStyle={{ top: -145 }}
                />
              </Touchable>
            ) : (
              <VideoPlayer
                source={{
                  uri: VIDEO_LINK,
                }}
                disableBack
                disableVolume
                disableFullscreen
                paused={pauseVideo} //this keeps it from autoplaying when loading
                resizeMode="cover"
                repeat={false}
                onEnd={(): void => resetVideo()}
                style={{
                  width: SCREEN_WIDTH,
                  height: 250,
                  backgroundColor: colors.blackTransparent,
                }}
              />
            )}
            <Container py={4} px={6}>
              <Text fontSize={5} fontWeight="bold" pb={2}>
                {t('programs.introduction.personalizedExperience.title')}
              </Text>
              <Text>{t('programs.introduction.personalizedExperience.copy')}</Text>
            </Container>
            <Container>
              <Container flexDirection="row" justifyContent="center">
                <FastImage
                  source={imageUpperLeft}
                  style={{ width: SCREEN_WIDTH / 2, height: 100 }}
                />
                <FastImage
                  source={imageUpperRight}
                  style={{ width: SCREEN_WIDTH / 2, height: 100 }}
                />
              </Container>
              <Container flexDirection="row" justifyContent="center">
                <FastImage
                  source={imageLowerLeft}
                  style={{ width: SCREEN_WIDTH / 2, height: 100 }}
                />
                <FastImage
                  source={imageLowerRight}
                  style={{ width: SCREEN_WIDTH / 2, height: 100 }}
                />
              </Container>
            </Container>
            <Container pt={4} px={6}>
              <Text fontSize={5} fontWeight="bold" pb={2}>
                {t('programs.introduction.optimizeYourHealth.title')}
              </Text>
              <Text>{t('programs.introduction.optimizeYourHealth.copy')}</Text>
            </Container>
          </Container>
        </Container>
      </ScrollView>
      <AuthConsumer>
        {(auth): ReactElement => {
          const { token: authToken } = auth;

          // Note the "authToken" here is the JWT token for validating api resuests,
          // not the "program-specific" token for program access
          return (
            <>
              <FloatingButton
                onPress={authToken ? proceedToBiocircuitDetailsScreen : setLoginModalVisible}
                label={t('programs.introduction.skip')}
                backgroundColor={colors.tangerineYellow}
                fullWidth={isBottom}
              />
              <LoginModal
                isVisible={loginModalVisible}
                setIsVisible={setLoginModalVisible}
                createAccount={onCreateAccount}
                login={onLogin}
              />
            </>
          );
        }}
      </AuthConsumer>
    </Screen>
  );
};

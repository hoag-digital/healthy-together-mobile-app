import * as Sentry from '@sentry/react-native';
import React, { FC, ReactElement, useState } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Divider } from 'react-native-elements';
import { SectionList } from 'react-native';
import dayjs from 'dayjs';

import { Button } from '../../components/Button';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { ScheduleListItem } from '../../components/ScheduleListItem';
import { CancelClassModal } from '../../components/CancelClassModal';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { LoggedInContent, LoggedOutContent } from '../../lib/AuthUtils';
import { SCROLLVIEW_BOTTOM_PADDING, t } from '../../utils';
import { colors } from '../../styles';
import {
  Event,
  MyScheduledEventsDocument,
  useMyScheduledEventsQuery,
  useEventCancelRegistrationMutation,
} from '../../graphql/types';
import { ContentErrorToast } from '../../components/Toastr';

// sorting the events in order (TODO: move to api)
const sortEvents = (events: Event[]): Event[] => {
  return events.sort((event1: Event, event2: Event): any => {
    const event1Start = parseInt(event1.eventStart, 10);
    const event2Start = parseInt(event2.eventStart, 10);
    return dayjs(event1Start).diff(event2Start);
  });
};

// grouping the events by common days (TODO: move to api)
const groupEvents = (sortedEvents: Event[]): Record<string, any> => {
  return sortedEvents.reduce((acc, currentEvent) => {
    const currentDate = dayjs(currentEvent.date).format('dddd, MMMM D');

    if (acc[currentDate]) {
      return {
        ...acc,
        [currentDate]: [...acc[currentDate], currentEvent],
      };
    } else {
      return {
        ...acc,
        [currentDate]: [currentEvent],
      };
    }
  }, {});
};

export const ScheduleScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const [selectedEventId, selectEventId] = useState(null);
  const [cancelClassModalVisible, setCancelClassModalVisible] = useState(false);

  const navigateToClassesScreen = (): void => {
    navigation.navigate('Classes');
  };

  const navigateToLoginScreen = (): void => {
    navigation.navigate('Login');
  };

  const {
    error: scheduledEventsError,
    data: scheduledEventsData = {},
    loading: scheduledEventsLoading,
    refetch: refetchScheduledEvents,
    networkStatus: scheduledEventsNetworkStatus,
  } = useMyScheduledEventsQuery({
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const [cancelEventRegistration, { loading: isCancelling }] = useEventCancelRegistrationMutation({
    update: cache => {
      const { myEvents: myCachedEvents } = cache.readQuery({
        query: MyScheduledEventsDocument,
      });

      // Filtering out the recently cancelled eventId
      const myUpdatedEvents = myCachedEvents.filter(
        ({ postId }): boolean => postId !== selectedEventId
      );

      cache.writeQuery({
        query: MyScheduledEventsDocument,
        data: { myEvents: myUpdatedEvents },
      });
    },
  });

  // This function opens the class cancellation modal (Step 1)
  // note: we are using the postId returned from the api as the "eventId"
  const initiateClassCancellation = (postId: string): void => {
    selectEventId(postId);
    setCancelClassModalVisible(true);
  };

  // This is the callback function invoked when a class is cancelled from the modal (Step 2)
  const onCancelClass = async (): Promise<void> => {
    if (!selectedEventId) return;

    try {
      await cancelEventRegistration({
        variables: {
          postId: selectedEventId,
        },
      });
    } catch (error) {
      Sentry.captureException(error);
    } finally {
      // NOTE: the selectedEventId is being used in the cancelEventRegistration "update" function.
      // A better solution has it return from the api so that the update function doesnt rely on the component state.
      setCancelClassModalVisible(false);
      selectEventId(null);
    }
  };

  const renderScheduleScreenContents = (): ReactElement | null => {
    if (scheduledEventsLoading && !Object.keys(scheduledEventsData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    const events = scheduledEventsData ? scheduledEventsData.myEvents : [];

    // if there are no scheduled events render a CTA to the class categories screen
    if (!events || !events.length) {
      return (
        <Container fill centerContent bg={colors.blackTransparent} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Text pb={5} color={colors.white}>
            {t('schedule.noScheduledItems')}
          </Text>
          <Button
            py={3}
            px={6}
            color={colors.white}
            backgroundColor={colors.turquoiseBlue}
            borderRadius={2}
            label={t('schedule.noScheduledItemsCTA')}
            onPress={navigateToClassesScreen}
          />
        </Container>
      );
    }

    const eventGroupMap = groupEvents(sortEvents(events));

    const eventGroups = Object.entries(eventGroupMap).map(([date, eventsForDate]) => ({
      title: date,
      data: eventsForDate,
    }));

    // networkStatus 4 indicates a refetch while 1 indicates the initial query fetch...
    // only show the scrollview loading spinner on an explicit user-requested refetch
    const isRefetching = scheduledEventsLoading && scheduledEventsNetworkStatus === 4;

    return (
      <Container fill>
        <SectionList
          refreshing={isRefetching}
          onRefresh={refetchScheduledEvents}
          sections={eventGroups}
          keyExtractor={(item): string => item.postId}
          renderItem={({ item }): ReactElement => {
            return (
              <ScheduleListItem
                item={item}
                onCancel={(): void => initiateClassCancellation(item.postId)}
              />
            );
          }}
          renderSectionHeader={({ section: { title } }): ReactElement => {
            return (
              <Container flexDirection="row" alignItems="center" my={4} px={5}>
                <Divider
                  style={{
                    flexDirection: 'row',
                    backgroundColor: colors.silver,
                    flex: 1,
                  }}
                />
                <Text fontSize={1} mx={3} color={colors.gray}>
                  {title}
                </Text>
                <Divider
                  style={{
                    flexDirection: 'row',
                    backgroundColor: colors.silver,
                    flex: 1,
                  }}
                />
              </Container>
            );
          }}
          ListHeaderComponent={(): ReactElement => (
            <Text fontWeight="bold" fontSize={5} pt={4} px={5}>
              {t('schedule.header')}
            </Text>
          )}
          ListFooterComponent={(): ReactElement => <Container pb={SCROLLVIEW_BOTTOM_PADDING} />}
        />
      </Container>
    );
  };

  if (scheduledEventsError) {
    <ContentErrorToast />;
  }

  const renderLoginCTA = (): ReactElement => {
    return (
      <Container fill centerContent bg={colors.blackTransparent} pb={SCROLLVIEW_BOTTOM_PADDING}>
        <Text pb={5} color={colors.white}>
          {t('schedule.authProtectedMessage')}
        </Text>
        <Button
          py={3}
          px={6}
          color={colors.white}
          backgroundColor={colors.turquoiseBlue}
          borderRadius={2}
          label={t('schedule.loginCTA')}
          onPress={navigateToLoginScreen}
        />
      </Container>
    );
  };

  const renderCancelClassReservationModal = (): ReactElement => {
    return (
      <CancelClassModal
        isVisible={cancelClassModalVisible}
        setIsVisible={setCancelClassModalVisible}
        onCancelClass={onCancelClass}
        isCancelling={isCancelling}
      />
    );
  };

  return (
    <Screen screenHeader paddingTop={0} fill>
      <LoggedOutContent>{renderLoginCTA()}</LoggedOutContent>
      <LoggedInContent>
        {renderCancelClassReservationModal()}
        {renderScheduleScreenContents()}
      </LoggedInContent>
    </Screen>
  );
};

import React, { FC, ReactNode, useState } from 'react';
import { RefreshControl, ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { CheckBox } from 'react-native-elements';

import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import {
  BirthplanCategoriesDocument,
  BirthplanContentCategoryDocument,
  ContentCategory,
  useBirthplanContentCategoryQuery,
  useRecordBirthplanPreferenceMutation,
} from '../../graphql/types';
import { colors } from '../../styles';
import { SCREEN_WIDTH, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import { Button } from '../../components/Button';

export interface BirthplanPost {
  id: string;
  contentCategoryId: string;
  selected: boolean;
  title: string;
}

export const BirthPlanCategoryScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const category: ContentCategory = navigation.getParam('category');
  const currentIndex = navigation.getParam('currentIndex');
  const changeIndex = navigation.getParam('changeIndex');
  const isLastCategory = navigation.getParam('isLastCategory');

  const [selectedPreferenceId, setSelectedPreferenceId] = useState<string | null>(null);

  const { error, data, loading, refetch } = useBirthplanContentCategoryQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      categoryId: category.id,
    },
  });

  const [recordBirthplanPreference] = useRecordBirthplanPreferenceMutation({
    refetchQueries: [
      {
        query: BirthplanCategoriesDocument,
      },
    ],
    update: cache => {
      const { birthplanContentByCategory: myCachedBirthplanPosts } = cache.readQuery({
        query: BirthplanContentCategoryDocument,
        variables: {
          categoryId: category.id,
        },
      });

      const myUpdatedBirthplanPosts = myCachedBirthplanPosts.map(post => {
        if (post.id === selectedPreferenceId) return { ...post, selected: !post.selected };

        return { ...post };
      });

      cache.writeQuery({
        query: BirthplanContentCategoryDocument,
        variables: {
          categoryId: category.id,
        },
        data: { birthplanContentByCategory: myUpdatedBirthplanPosts },
      });
    },
  });

  const togglePreference = async (post: BirthplanPost, selected: boolean): Promise<void> => {
    await recordBirthplanPreference({
      variables: {
        category: { id: Number(category.id), name: category.name },
        preference: { postId: Number(post.id), title: post.title, selected },
      },
    });
  };

  const renderItem = (item: BirthplanPost): ReactNode => {
    const { title } = item;

    const selected = item?.selected || false;

    return (
      <Container
        key={item.id}
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        pl={2}
        pb={3}
      >
        <Container flex={0.8}>
          <Text fontSize={3}>{title}</Text>
        </Container>
        <Container flex={0.2} alignItems="flex-end">
          <CheckBox
            checked={selected}
            onPress={async (): Promise<void> => {
              await setSelectedPreferenceId(item.id);
              togglePreference(item, !item.selected);
            }}
            checkedColor={colors.wisteria}
          />
        </Container>
      </Container>
    );
  };

  const renderContent = (): ReactNode => {
    if (loading) {
      return <CenteredLoadingSpinner />;
    }

    const items = data?.birthplanContentByCategory;

    return items?.map((item: BirthplanPost): ReactNode => renderItem(item));
  };

  if (error) {
    //handle graphql error
  }

  return (
    <LayeredContainerScreen
      screenTitle={category.name}
      headerColor={colors.wisteria}
      headerFontColor={colors.white}
      containerColor={colors.white}
    >
      <ScrollView
        scrollIndicatorInsets={{
          right: 1,
        }}
        style={{
          width: SCREEN_WIDTH,
          backgroundColor: colors.transparent,
          paddingLeft: 10,
        }}
        refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          {renderContent()}
          <Container flexDirection="row">
            {!!currentIndex && (
              <Button
                flex={1}
                py={3}
                px={6}
                marginRight={10}
                color={colors.white}
                backgroundColor={colors.wisteria}
                borderRadius={2}
                label="Previous"
                onPress={(): void => changeIndex(currentIndex - 1)}
              />
            )}
            {!isLastCategory ? (
              <Button
                flex={1}
                py={3}
                px={6}
                marginRight={10}
                color={colors.white}
                backgroundColor={colors.wisteria}
                borderRadius={2}
                label="Next"
                onPress={(): void => changeIndex(currentIndex + 1)}
              />
            ) : (
              <Button
                flex={1}
                py={3}
                px={6}
                marginRight={10}
                color={colors.white}
                backgroundColor={colors.wisteria}
                borderRadius={2}
                label="View My Plan"
                onPress={(): boolean => navigation.navigate('BirthplanDetails')}
              />
            )}
          </Container>
        </Container>
      </ScrollView>
    </LayeredContainerScreen>
  );
};

export default withNavigation(BirthPlanCategoryScreen);

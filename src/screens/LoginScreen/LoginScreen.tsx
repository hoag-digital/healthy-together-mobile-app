import * as Sentry from '@sentry/react-native';
import React, { useState, FC, ReactElement } from 'react';
import {
  ImageBackground,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import { NavigationSwitchScreenProps, NavigationAction } from 'react-navigation';
import styled from '@emotion/native';
import { Icon } from 'react-native-elements';

import { NotificationsConsumer } from '../../services/notifications';
import Logo from '../../assets/images/hoag-logo-white.svg';
import { Login } from '../../components/Login';
import { Screen } from '../../components/Screen';
import bgImage from '../../assets/images/hoag-background.jpg';
import { Container } from '../../components/Container';
import { AuthConsumer } from '../../lib/AuthConsumer';
import { colors } from '../../styles';
import ENV from '../../env';
import { Touchable } from '../../components/Touchable';
import { LARGEST_ICON_SIZE, DEFAULT_HITSLOP } from '../../utils';
import { trackLogin } from '../../services/analytics';
import { useLoginMutation } from '../../graphql/types';

const { USE_SENTRY } = ENV;

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

type NavigationRedirectParam = {
  action: NavigationAction;
  callback?(): void;
};

interface LoginNavigationParams {
  redirect: NavigationRedirectParam;
}

export const LoginScreen: FC<NavigationSwitchScreenProps<LoginNavigationParams>> = ({
  navigation,
}) => {
  const [loading, setLoading] = useState(false);
  const [login] = useLoginMutation();
  const [loginError, setLoginError] = useState(false);

  const loginPress = async (
    auth: any,
    email: string,
    password: string,
    notificationsContext: any
  ): Promise<void> => {
    setLoading(true);
    Keyboard.dismiss();

    try {
      const { data: loginData } = await login({
        variables: {
          email,
          password,
        },
      });

      const token = loginData?.login?.token;
      const user = loginData?.login?.user;

      if (token) {
        await auth.saveToken(token);

        if (user) {
          if (USE_SENTRY) {
            Sentry.setUser({ id: user.id });
          }

          // tracking login event in analytics
          trackLogin(user);
        }

        setLoading(false);

        const redirect = navigation.getParam('redirect');

        if (redirect) {
          navigation.dispatch(redirect.action);
          redirect.callback?.();
        } else {
          navigation.navigate('Home');
        }

        notificationsContext.setNotificationsShouldBeInitialized(true);
      } else {
        setLoginError(true);
      }
    } catch (error) {
      Sentry.captureException(error);
      setLoginError(true);
    } finally {
      setLoading(false);
    }
  };

  const registrationPress = (): void => {
    navigation.navigate('Registration', { redirect: navigation.getParam('redirect') });
  };

  const forgotPwdPress = (): void => {
    navigation.navigate('ForgotPassword');
  };

  return (
    <BackgroundImage source={bgImage} resizeMode="cover">
      <Screen testID="registrationScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <KeyboardAvoidingView behavior="position" enabled>
          <Container
            flexDirection="row"
            justifyContent="flex-start"
            alignItems="center"
            padding={20}
          >
            <Touchable
              testID="back-icon"
              onPress={(): boolean => navigation.navigate('Home')}
              hitSlop={DEFAULT_HITSLOP}
            >
              <Icon
                name="ios-arrow-round-back"
                type="ionicon"
                size={LARGEST_ICON_SIZE}
                color={colors.white}
              />
            </Touchable>
            <Container mx="auto">
              <Logo />
            </Container>
            <Container width={30} />
          </Container>
          <NotificationsConsumer>
            {(notificationsContext): ReactElement => {
              return (
                <AuthConsumer>
                  {(auth): ReactElement => {
                    return (
                      <Login
                        loginPress={(email: string, password: string): Promise<void> =>
                          loginPress(auth, email, password, notificationsContext)
                        }
                        registrationPress={registrationPress}
                        forgotPasswordPress={forgotPwdPress}
                        loginLoading={loading}
                        loginError={loginError}
                      />
                    );
                  }}
                </AuthConsumer>
              );
            }}
          </NotificationsConsumer>
        </KeyboardAvoidingView>
      </Screen>
    </BackgroundImage>
  );
};

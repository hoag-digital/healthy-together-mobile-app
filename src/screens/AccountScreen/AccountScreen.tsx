import React, { FC, ReactElement, useEffect, useState } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';

import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { colors, fonts, fontSizes, space } from '../../styles';
import { Container } from '../../components/Container';
import { Touchable } from '../../components/Touchable';
import { Datepicker } from '../../components/Datepicker';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { DEFAULT_ICON_SIZE, t } from '../../utils';
import { USER_ROLE } from '../../utils/maternityOnboarding';
// import { safelyOpenUrl } from '../../utils/safelyOpenUrl';
import {
  MeDocument,
  useMeQuery,
  useReportPregnancyLossMutation,
  useUpdateMaternityProfileMutation,
} from '../../graphql/types';

interface Role {
  label: string;
  value: string;
}

const MATERNITY_ROLES: Role[] = [
  {
    label: 'Prenatal',
    value: 'pregnant',
  },
  // {
  //   label: 'New Parent',
  //   value: 'new-parent',
  // },
];

export const AccountScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const { data, loading } = useMeQuery({
    fetchPolicy: 'cache-and-network',
  });

  const navigateToNotificationSettingsScreen = (): void => {
    navigation.navigate('NotificationSettings');
  };

  const [loaded, setLoaded] = useState(false);
  const [maternityRole, setMaternityRole] = useState<string | undefined>(undefined);
  const [showEddDatePicker, setShowEddDatePicker] = useState(false);

  useEffect(() => {
    function initialized(): void {
      const maternity = data?.me?.maternityProfile;

      setMaternityRole(maternity?.role);
      setLoaded(true);
    }

    if (!loaded && data) {
      initialized();
    }
  }, [data, loaded]);

  const renderBiocircuitSettings = (): ReactElement => {
    const { tokenExpirationDate } = data.me;
    return (
      <>
        <Container
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
          px={3}
          py={2}
          mt={3}
          bg={colors.whiteSmoke}
        >
          <Text fontSize={5} {...fonts.bold}>
            Biocircuit Settings
          </Text>
        </Container>
        <Container flexDirection="row" justifyContent="space-between" px={3} py={2}>
          <Text fontSize={3} {...fonts.bold}>
            Token Expiration:
          </Text>
          <Container flexDirection="row" alignItems="center">
            <Text fontSize={3} mx={3}>
              {tokenExpirationDate}
            </Text>
          </Container>
        </Container>
      </>
    );
  };

  const [updateMaternityProfile] = useUpdateMaternityProfileMutation({
    refetchQueries: [{ query: MeDocument }],
    awaitRefetchQueries: true,
  });

  const updateMaternityRole = async (role: string): Promise<void> => {
    const { maternityProfile } = data.me;
    await updateMaternityProfile({
      variables: {
        lastMenstrualPeriod: maternityProfile.lastMenstrualPeriod,
        estimatedDueDate: maternityProfile.estimatedDueDate,
        role,
      },
    });
  };

  const updateMaternityEstimatedDueDate = async (value: Date): Promise<void> => {
    const { maternityProfile } = data.me;
    await updateMaternityProfile({
      variables: {
        lastMenstrualPeriod: null,
        estimatedDueDate: value,
        role: maternityProfile?.role,
      },
    });
  };

  const [reportPregnancyLoss] = useReportPregnancyLossMutation({
    refetchQueries: [{ query: MeDocument }],
    awaitRefetchQueries: true,
  });

  const reportChangeInPregnancy = async (): Promise<void> => {
    reportPregnancyLoss();
    navigation.navigate('PrenancyLossLetter');
  };

  const renderMaternitySettings = (): ReactElement => {
    const { maternityProfile } = data.me;

    return (
      <>
        <Container
          flexDirection="row"
          justifyContent="space-between"
          px={3}
          py={2}
          mt={3}
          bg={colors.whiteSmoke}
        >
          <Text fontSize={5} {...fonts.bold}>
            Maternity Settings
          </Text>
        </Container>
        <Container
          alignItems="center"
          flexDirection="row"
          justifyContent="space-between"
          px={3}
          py={2}
        >
          <Text fontSize={3} {...fonts.bold}>
            Stage:
          </Text>
          <Container flexDirection="row" alignItems="center">
            <RNPickerSelect
              onValueChange={(value: string): void => {
                setMaternityRole(value);
                updateMaternityRole(value);
              }}
              items={MATERNITY_ROLES}
              value={maternityRole}
              useNativeAndroidPickerStyle={false}
              placeholder={{ label: 'Not Pregnant', value: null }}
              Icon={(): ReactElement => {
                return (
                  <Icon
                    name="caret-down"
                    type="font-awesome"
                    size={DEFAULT_ICON_SIZE}
                    color={colors.gray}
                  />
                );
              }}
              style={{
                inputIOS: { fontSize: fontSizes[3], color: colors.black },
                inputAndroid: { fontSize: fontSizes[3] },
                inputIOSContainer: { paddingRight: space[6] },
                inputAndroidContainer: {
                  paddingRight: space[6],
                  justifyContent: 'center',
                },
                chevronContainer: { opacity: 0 },
              }}
            />
          </Container>
        </Container>
        {maternityRole === USER_ROLE.PREGNANT ? (
          <>
            <Container
              alignItems="center"
              flexDirection="row"
              justifyContent="space-between"
              px={3}
              py={2}
            >
              <Text fontSize={3} {...fonts.bold}>
                Est. Due Date:
              </Text>
              <Container flexDirection="row" alignItems="center">
                <Datepicker
                  title={t('maternity.onboarding.secondStep.estimatedDueDate')}
                  isVisible={showEddDatePicker}
                  setIsVisible={setShowEddDatePicker}
                  setSelectedDate={updateMaternityEstimatedDueDate}
                  defaultDate={maternityProfile.estimatedDueDate}
                  selectedDate={maternityProfile.estimatedDueDate}
                  displayFormat="String"
                  iconRight
                  icon={
                    <Icon
                      name="caret-down"
                      type="font-awesome"
                      size={DEFAULT_ICON_SIZE}
                      color={colors.gray}
                    />
                  }
                />
              </Container>
            </Container>
            <Touchable onPress={(): Promise<void> => reportChangeInPregnancy()}>
              <Container flexDirection="row" justifyContent="center" px={3} py={2}>
                <Text
                  fontSize={3}
                  {...fonts.bold}
                  textDecorationLine="underline"
                  color={colors.cornflowerBlue}
                >
                  {t('account.reportPregnancyLoss')}
                </Text>
              </Container>
            </Touchable>
          </>
        ) : null}
      </>
    );
  };

  const renderScreenContent = (): ReactElement => {
    if (loading || !loaded) {
      return <CenteredLoadingSpinner />;
    }

    const user = data || {};
    const { me } = user;
    const hasMaternityProfile = me && me.maternityProfile;
    const hasBiocircuitToken = me && me.tokenExpirationDate;
    return (
      <>
        <Container
          alignItems="center"
          flexDirection="row"
          justifyContent="space-between"
          px={3}
          py={2}
        >
          <Text fontSize={3} {...fonts.bold}>
            {t('account.name')}:
          </Text>
          <Text fontSize={3}>{`${me.firstName} ${me.lastName}`}</Text>
        </Container>
        <Touchable onPress={navigateToNotificationSettingsScreen}>
          <Container
            alignItems="center"
            flexDirection="row"
            justifyContent="space-between"
            px={3}
            py={2}
          >
            <Text fontSize={3} {...fonts.bold}>
              {t('account.notifications')}:
            </Text>
            <Icon name="chevron-right" size={DEFAULT_ICON_SIZE} />
          </Container>
        </Touchable>
        {hasMaternityProfile ? renderMaternitySettings() : null}
        {hasBiocircuitToken ? renderBiocircuitSettings() : null}
      </>
    );
  };

  return (
    <Screen modalHeader screenTitle="Account" headerColor={colors.gainsboro}>
      <Container width="100%" height="100%" bg={colors.white} py={2}>
        {renderScreenContent()}
      </Container>
    </Screen>
  );
};

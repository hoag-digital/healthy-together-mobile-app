import React, { FC, ReactElement, useState } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { RefreshControl, FlatList, Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Swipeable from 'react-native-swipeable';
import dayjs from 'dayjs';

import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { colors } from '../../styles';
import {
  SCREEN_WIDTH,
  SCROLLVIEW_BOTTOM_PADDING,
  LARGER_ICON_SIZE,
  DEFAULT_CONTENT_DESCRIPTION_LENGTH,
  t,
} from '../../utils';
import { SwipeButtonLayout } from '../MaternityScreen/Layouts';
import { Container } from '../../components/Container';
import { Touchable } from '../../components/Touchable';
import { Text } from '../../components/Text';
import { trimText } from '../../utils/trimText';
import { useMyNotesQuery, useDeleteMyNoteMutation, Note } from '../../graphql/types';

export const NoteListScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const onRefreshNotes = navigation.getParam('onRefresh');
  const [currentSwipeable, setCurrentSwipeable] = useState(null);

  const {
    error: myNotesError,
    data: myNotesData,
    loading: myNotesLoading,
    refetch: onRefresh,
  } = useMyNotesQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [deleteMyNote, { error: deleteMyNoteError }] = useDeleteMyNoteMutation();

  if (myNotesError || deleteMyNoteError) {
    //TODO: handle graphql error
  }

  const handleCancelDeleteRow = (): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }
  };

  const handleDeleteRow = (id: string): void => {
    deleteMyNote({
      variables: {
        id,
      },
    }).then(() => {
      onRefresh();
      onRefreshNotes();
    });
  };

  const onDeleteRowPress = (id: string): void => {
    Alert.alert(
      'Delete Note',
      'Are you sure you want to delete?',
      [
        { text: 'Cancel', onPress: (): void => handleCancelDeleteRow(), style: 'cancel' },
        { text: 'Delete', onPress: (): void => handleDeleteRow(id) },
      ],
      { cancelable: false }
    );
  };

  const onOpenSwipeable = (event, gestureState, swipeable): void => {
    if (currentSwipeable && currentSwipeable !== swipeable) {
      currentSwipeable.recenter();
    }

    setCurrentSwipeable(swipeable);
  };

  const onCloseSwipeable = (): void => {
    setCurrentSwipeable(null);
  };

  const navigateToScreen = (noteId: string, name: string, item?: Note): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }

    navigation.navigate(name, {
      noteId,
      myCurrentNote: item,
      onRefresh,
    });
  };

  const onEditNotePress = (item: Note): void => {
    navigateToScreen(item.id, 'NoteEditScreen', item);
  };

  const renderItem = ({ item }): ReactElement => {
    const createdAt = dayjs(Number(item.createdAt)).format('MMMM DD YYYY');

    const editButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-create"
        bgColor={colors.tangerineYellow}
        onPress={(itemData: any): void => onEditNotePress(itemData)}
        rowHeight={70}
      />
    );

    const deleteButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-trash"
        bgColor={colors.cinnabar}
        onPress={(itemData: any): void => onDeleteRowPress(itemData)}
        rowHeight={70}
      />
    );

    const swipeContainerRightButtons = [editButton, deleteButton];

    return (
      <Swipeable
        rightButtons={swipeContainerRightButtons}
        onRightButtonsOpenRelease={onOpenSwipeable}
        onRightButtonsCloseRelease={onCloseSwipeable}
      >
        <Touchable
          key={item.id}
          backgroundColor={colors.solitudeLight}
          onPress={(): void => {
            navigateToScreen(item.id, 'NoteDetailsModal');
          }}
        >
          <Container flexDirection="row" justifyContent="center" py={2} height={70}>
            <Container flex={0.9} pl={4} justifyContent="center">
              <Text>{createdAt}</Text>
              <Text>
                {item.contents ? trimText(item.contents, DEFAULT_CONTENT_DESCRIPTION_LENGTH) : ''}
              </Text>
            </Container>
            <Container flex={0.1} justifyContent="center">
              <Icon name="chevron-right" size={LARGER_ICON_SIZE} />
            </Container>
          </Container>
        </Touchable>
      </Swipeable>
    );
  };

  const renderContent = (): ReactElement => {
    const notes = myNotesData?.myNotes || [];

    return (
      <FlatList
        data={notes}
        contentContainerStyle={{
          paddingBottom: SCROLLVIEW_BOTTOM_PADDING,
        }}
        renderItem={renderItem}
        refreshControl={<RefreshControl refreshing={myNotesLoading} onRefresh={onRefresh} />}
        ItemSeparatorComponent={(): ReactElement => (
          <Container alignItems="center" fullWidth bg={colors.solitudeLight}>
            <Container bg={colors.white} width={SCREEN_WIDTH * 0.9} height={1} />
          </Container>
        )}
      />
    );
  };

  return (
    <LayeredContainerScreen
      screenTitle={t('maternity.notes.listView.screenTitle')}
      headerColor={colors.tangerineYellow}
      headerFontColor={colors.white}
      containerColor={colors.white}
    >
      <Container pt={2} width={SCREEN_WIDTH} height="100%" flex={1}>
        {renderContent()}
      </Container>
    </LayeredContainerScreen>
  );
};

export default withNavigation(NoteListScreen);

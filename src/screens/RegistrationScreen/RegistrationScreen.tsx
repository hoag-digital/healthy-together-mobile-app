import * as Sentry from '@sentry/react-native';
import React, { FC, ReactElement, useState } from 'react';
import { ImageBackground, StyleSheet, KeyboardAvoidingView, StatusBar } from 'react-native';
import { NavigationSwitchScreenProps, NavigationAction } from 'react-navigation';
import styled from '@emotion/native';
import { Registration, RegisterCallbackParams } from '../../components/Registration';
import { Screen } from '../../components/Screen';
import Logo from '../../assets/images/hoag-logo-white.svg';
import bgImage from '../../assets/images/hoag-background.jpg';
import { Container } from '../../components/Container';
import { AuthConsumer } from '../../lib/AuthConsumer';
import ENV from '../../env';
import { trackLogin } from '../../services/analytics';
import { useSignupMutation } from '../../graphql/types';

const { USE_SENTRY } = ENV;

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

type NavigationRedirectParam = {
  action: NavigationAction;
  callback?(): void;
};

interface RegistrationNavigationParams {
  redirect: NavigationRedirectParam;
}

export const RegistrationScreen: FC<NavigationSwitchScreenProps<RegistrationNavigationParams>> = ({
  navigation,
}) => {
  const [signUp] = useSignupMutation();
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  const registerUser = async (auth: any, signupPayload: RegisterCallbackParams): Promise<void> => {
    try {
      setErrors({});

      const { data: signUpData } = await signUp({
        variables: {
          signupPayload,
        },
      });

      const token = signUpData?.signup?.token;
      const user = signUpData?.signup?.user;

      if (token) {
        await auth.saveToken(token);

        if (user) {
          if (USE_SENTRY) {
            Sentry.setUser({ id: user.id });
          }

          // tracking login event in analytics
          trackLogin(user, 'Create Account Screen');
        }

        setLoading(false);

        const redirect = navigation.getParam('redirect');

        if (redirect) {
          navigation.dispatch(redirect.action);
          redirect.callback?.();
        } else {
          navigation.navigate('Home');
        }
      }
    } catch (error) {
      Sentry.captureException(error);
      const message = error?.message;

      if (message) {
        const newErrors = {} as Record<string, any>;
        const userNameTaken = /username/i.test(message);

        if (userNameTaken) {
          newErrors.email = 'Email is already taken';
        }

        setErrors(newErrors);
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <BackgroundImage source={bgImage} resizeMode="cover">
      <Screen testID="registrationScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <KeyboardAvoidingView behavior="position" enabled>
          <Container justifyContent="flex-start" alignItems="center" padding={20}>
            <Logo />
          </Container>
          <AuthConsumer>
            {(auth): ReactElement => (
              <Registration
                errors={errors}
                registrationLoading={loading}
                onCancelRegistration={(): boolean => navigation.goBack()}
                onRegister={(params: RegisterCallbackParams): Promise<void> =>
                  registerUser(auth, params)
                }
              />
            )}
          </AuthConsumer>
        </KeyboardAvoidingView>
      </Screen>
    </BackgroundImage>
  );
};

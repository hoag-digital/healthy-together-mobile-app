import React, { FC, ReactElement, useState, useCallback } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { Divider } from 'react-native-elements';
import { FlatList, RefreshControl } from 'react-native';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import dayjs from 'dayjs';

import { Text } from '../../components/Text';
import { Screen } from '../../components/Screen';
import { Container } from '../../components/Container';
import { OnDemandEventCard, EventGroupCard } from '../../components/Card';
import { SortAndFilter, SelectableOption } from '../../components/SortAndFilter';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING, t } from '../../utils';
import { Event, useEventCategoriesQuery, useUniqueEventsQuery } from '../../graphql/types';
import { ContentErrorToast } from '../../components/Toastr';

// Note: WP Event Category IDs are 2-digits, so these should not conflict
const LIVESTREAM_FILTER_ID = '111';
const ON_DEMAND_FILTER_ID = '222';

const CLASS_TYPE_FILTER_OPTIONS = [
  {
    value: LIVESTREAM_FILTER_ID,
    label: 'Livestream',
  },
  {
    value: ON_DEMAND_FILTER_ID,
    label: 'On-Demand',
  },
];

const CLASS_TYPE_FILTER_VALUES = CLASS_TYPE_FILTER_OPTIONS.map(option => option.value);

// Adding this as a hardcoded constant for usage with the SortAndFilter component
const SORT_TYPES = ['mostRecent', 'oldestFirst', 'greatestDuration', 'leastDuration'];

const filterEventsByTypeAndCategory = (
  activeClassTypeFilterIds: string[],
  activeCategoryFilterIds: string[]
) => ({ isOnDemand, eventCategoryId }: Event): boolean => {
  const classTypeFilteringIsActive = !!activeClassTypeFilterIds.length;
  const categoryFilteringIsActive = !!activeCategoryFilterIds.length;
  const isLivestream = !isOnDemand;

  // first filter event based on whether its class type is actively filtered
  if (classTypeFilteringIsActive) {
    const livestreamFilterIsActive = activeClassTypeFilterIds.includes(LIVESTREAM_FILTER_ID);
    const onDemandFilterIsActive = activeClassTypeFilterIds.includes(ON_DEMAND_FILTER_ID);

    if (isOnDemand && !onDemandFilterIsActive) return false;
    if (isLivestream && !livestreamFilterIsActive) return false;
  }

  // note we are comparing the int type eventCategoryId on the event with the string type id from the classCategories query.
  if (categoryFilteringIsActive && !activeCategoryFilterIds.includes(`${eventCategoryId}`)) {
    return false;
  }

  return true;
};

// NOTE: the data sorting is using event.postDate which is a stringified utc timestamp integer, hence
// the need for the parseInt() function. This should be resolved on the backend in the future.
const sortEventsByActiveSortType = (activeSortType: string) => (
  eventA: Event,
  eventB: Event
): number => {
  switch (activeSortType) {
    case 'greatestDuration':
      if (eventA.duration === eventB.duration) return 0;

      return eventA.duration < eventB.duration ? 1 : -1;
    case 'leastDuration':
      if (eventA.duration === eventB.duration) return 0;

      return eventA.duration > eventB.duration ? 1 : -1;
    case 'mostRecent':
      return dayjs(parseInt(eventA.postDate, 10)).isBefore(dayjs(parseInt(eventB.postDate, 10)))
        ? 1
        : -1;
    case 'oldestFirst':
      return dayjs(parseInt(eventB.postDate, 10)).isBefore(dayjs(parseInt(eventA.postDate, 10)))
        ? 1
        : -1;
    default:
      return 0;
  }
};

const ClassListScreenContent = ({ navigation, withContentTitle = false }): ReactElement => {
  const [activeClassTypeFilterIds, setActiveClassTypeFilterIds] = useState([] as string[]);
  const [activeCategoryFilterIds, setActiveCategoryFilterIds] = useState([] as string[]);
  const [activeSortType, setActiveSortType] = useState('mostRecent');

  // using function currying to pass the state fields into these functions and hoist them out of render
  const filterEventsFunction = filterEventsByTypeAndCategory(
    activeClassTypeFilterIds,
    activeCategoryFilterIds
  );

  const sortEventsFunction = sortEventsByActiveSortType(activeSortType);

  const {
    error: uniqueEventsError,
    data: uniqueEventsData = {},
    loading: uniqueEventsLoading,
    refetch: refetchUniqueEvents,
  } = useUniqueEventsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const { error: classCategoriesError, data: classCategoriesData = {} } = useEventCategoriesQuery({
    fetchPolicy: 'cache-and-network',
  });

  const updateClassTypeFilterById = useCallback(
    (classTypeId): void => {
      let updatedClassTypeFilterIds = activeClassTypeFilterIds;

      if (activeClassTypeFilterIds.includes(classTypeId)) {
        updatedClassTypeFilterIds = updatedClassTypeFilterIds.filter(
          (id): boolean => id !== classTypeId
        );
      } else {
        updatedClassTypeFilterIds = [...updatedClassTypeFilterIds, classTypeId];
      }

      setActiveClassTypeFilterIds(updatedClassTypeFilterIds);
    },
    [activeClassTypeFilterIds]
  );

  const updateCategoryFilterById = useCallback(
    (categoryId): void => {
      let updatedCategoryFilterIds = activeCategoryFilterIds;

      if (activeCategoryFilterIds.includes(categoryId)) {
        updatedCategoryFilterIds = updatedCategoryFilterIds.filter(
          (id): boolean => id !== categoryId
        );
      } else {
        updatedCategoryFilterIds = [...updatedCategoryFilterIds, categoryId];
      }

      setActiveCategoryFilterIds(updatedCategoryFilterIds);
    },
    [activeCategoryFilterIds]
  );

  const toggleFilterOption = useCallback(
    (filterOptionId: string): void => {
      if (!filterOptionId) return;

      if (CLASS_TYPE_FILTER_VALUES.includes(filterOptionId)) {
        updateClassTypeFilterById(filterOptionId);
      } else {
        updateCategoryFilterById(filterOptionId);
      }
    },
    [updateCategoryFilterById, updateClassTypeFilterById]
  );

  // there must always be one sort option selected!
  const selectSortOption = useCallback(
    (sortOptionValue: string): void => {
      if (!sortOptionValue) return;

      // extra layer of security
      const isValidSortOption = SORT_TYPES.includes(sortOptionValue);

      if (!isValidSortOption) return;

      if (activeSortType !== sortOptionValue) {
        setActiveSortType(sortOptionValue);
      }
    },
    [activeSortType]
  );

  const navigateToOnDemandClassScreen = (eventId?: string | null): void => {
    if (!eventId) return;

    navigation.navigate('OnDemandClass', { eventId });
  };

  const navigateToClassDetailsScreen = (event?: Event | null): void => {
    // we are currently using the title of an eventGroup as its id
    if (event) {
      navigation.navigate('ClassDetails', {
        eventGroupName: event.name,
      });
    }
  };

  const renderEventCardForEventType = ({ item: event }): ReactElement => {
    return (
      <Container key={event.name} pb={2}>
        {event.isOnDemand ? (
          <OnDemandEventCard
            onPress={(): void => navigateToOnDemandClassScreen(event.eventId)}
            {...event}
          />
        ) : (
          <EventGroupCard
            {...event}
            withBadge
            onPress={(): void => navigateToClassDetailsScreen(event)}
          />
        )}
      </Container>
    );
  };

  const hasNoValidEvents = !uniqueEventsData?.uniqueEvents?.length;

  // We are applying the filtering and sorting of events here.
  // IMPROVEMENT: move sort and filter variables to the api endpoint for the events query
  const renderClasses = (): ReactElement[] | ReactElement => {
    if (hasNoValidEvents) return [];

    const { uniqueEvents: events } = uniqueEventsData;

    const filteringIsActive = !!activeClassTypeFilterIds.length || !!activeCategoryFilterIds.length;

    // 1. filter the events if filtering is active; Always filter before sorting!
    const filteredClasses = filteringIsActive ? events.filter(filterEventsFunction) : events;

    if (!filteredClasses.length) {
      return (
        <Container fill fullWidth centerContent pt={5} px={5}>
          <Text textAlign="center" fontSize={3} color={colors.eclipse} pt={3} pb={1}>
            {t('classes.classes.noneForFilters')}
          </Text>
        </Container>
      );
    }

    // 2. sort the events if sorting is active
    const sortedClasses = [...filteredClasses].sort(sortEventsFunction);

    return (
      <FlatList
        data={sortedClasses}
        renderItem={renderEventCardForEventType}
        keyExtractor={(event): string => event.id}
      />
    );
  };

  /**
   * Normalizes the sorting and filtering options, and renders the SortAndFilter component with
   * callbacks
   */
  const renderSortAndFilterOptions = (): ReactElement | null => {
    const sortOptions = SORT_TYPES.map(
      (sortType: string): SelectableOption => ({
        value: sortType,
        label: t(`sort.${sortType}`),
      })
    );

    const categoryFilterOptions =
      classCategoriesData?.eventCategories?.map(
        ({ id, name }): SelectableOption => ({
          value: id,
          label: name,
          meta: 'class-category',
        })
      ) || [];

    const filterOptions = [...CLASS_TYPE_FILTER_OPTIONS, ...categoryFilterOptions];
    const activeFilterOptionIds = [...activeClassTypeFilterIds, ...activeCategoryFilterIds];

    return (
      <SortAndFilter
        filterOptions={filterOptions}
        activeFilterOptionIds={activeFilterOptionIds}
        onFilterSelect={toggleFilterOption}
        sortOptions={sortOptions}
        onSortSelect={selectSortOption}
        activeSortOptionId={activeSortType}
      />
    );
  };

  if (uniqueEventsError || classCategoriesError) {
    <ContentErrorToast />;
  }

  if (uniqueEventsLoading && !Object.keys(uniqueEventsData).length) {
    return <CenteredLoadingSpinner withScrollviewPadding />;
  }

  /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
  return (
    <ScrollView
      scrollIndicatorInsets={{ right: 1 }}
      refreshControl={
        <RefreshControl refreshing={uniqueEventsLoading} onRefresh={refetchUniqueEvents} />
      }
      contentContainerStyle={{ flexGrow: 1 }}
    >
      {hasNoValidEvents ? (
        <Container fill fullWidth centerContent pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} px={5}>
          <Text textAlign="center" fontSize={3} color={colors.eclipse} pb={1}>
            {t('classes.classes.noneAvailable')}
          </Text>
        </Container>
      ) : (
        <Container pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
          {withContentTitle ? (
            <>
              <Text testID="on-demand-events-screen-header" fontWeight="bold" fontSize={5} py={4}>
                {t('classes.classes.header')}
              </Text>
              <Divider style={{ backgroundColor: colors.silver }} />
            </>
          ) : null}
          {renderSortAndFilterOptions()}
          {renderClasses()}
        </Container>
      )}
    </ScrollView>
  );
};

/**
 * This screen displays the complete list of ondemand and upcoming livestream classes in the app
 */
export const ClassListScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  return (
    <Screen
      testID="class-list-screen"
      screenHeader
      backgroundColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      <ClassListScreenContent navigation={navigation} withContentTitle />
    </Screen>
  );
};

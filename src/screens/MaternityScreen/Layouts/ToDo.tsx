import React, { FC, useState, ReactElement, ReactNode } from 'react';
import { CheckBox } from 'react-native-elements';
import { RefreshControl, SectionList } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import analytics from '@react-native-firebase/analytics';
import { Text } from '../../../components/Text';
import { Container } from '../../../components/Container';
import { Touchable } from '../../../components/Touchable';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { CenteredLoadingSpinner } from '../../../components/CenteredLoadingSpinner';
import { colors } from '../../../styles';
import { SCROLLVIEW_BOTTOM_PADDING, SCREEN_HEIGHT, TAB_BAR_HEIGHT } from '../../../utils';
import {
  useRecordBabyItemsTodoListItemMutation,
  TodoSectionDocument,
  useRecordDeliveryTimeTodoListItemMutation,
  useTodoSectionQuery,
} from '../../../graphql/types';
import { ReadMoreText } from '../../../components/ReadMoreText';

export interface ToDoPost {
  id: string;
  selected: boolean;
  title: string;
}

enum TAB_INDEXES {
  deliveryTime,
  babyItems,
}

/**
 * ToDo layout for maternity
 */
export const ToDoLayout: FC<SubNavLayoutProps> = ({ title, hideTitle = false, isActive }) => {
  const [selectedTodoId, setSelectedTodoId] = useState<string | null>(null);
  const [activeTab, setActiveTab] = useState(0);
  const IS_DELIVERY_ITEMS_TAB_ACTIVE = activeTab === 0;

  const { data, loading, refetch } = useTodoSectionQuery({
    fetchPolicy: 'cache-and-network',
  });

  const textStyle = {
    paddingBottom: 1,
    marginHorizontal: 5,
    fontSize: 14,
  };

  const TAB_BUTTONS = data?.todoSection
    ? data.todoSection.map(item => {
        return {
          label: item?.title,
          description: (
            <Container>
              <ReadMoreText text={item?.description} />
            </Container>
          ),
        };
      })
    : [];

  const [recordBabyItemsTodoListItem] = useRecordBabyItemsTodoListItemMutation({
    update: cache => {
      const { todoSection: cachedTodoSection } = cache.readQuery({
        query: TodoSectionDocument,
      });

      const updatedBabyItemsTodos = cachedTodoSection[TAB_INDEXES.babyItems].todoSubSection.map(
        subSection => {
          const data = subSection.data.map(todo => {
            if (todo.id === selectedTodoId) return { ...todo, selected: !todo.selected };

            return { ...todo };
          });

          return { ...subSection, data };
        }
      );

      const updatedTodoSection = [...cachedTodoSection];
      updatedTodoSection[TAB_INDEXES.babyItems].todoSubSection = updatedBabyItemsTodos;

      cache.writeQuery({
        query: TodoSectionDocument,
        data: { todoSection: updatedTodoSection },
      });
    },
  });

  const [recordDeliveryTimeTodoListItem] = useRecordDeliveryTimeTodoListItemMutation({
    update: cache => {
      const { todoSection: cachedTodoSection } = cache.readQuery({
        query: TodoSectionDocument,
      });

      const updatedDeliveryTimeTodos = cachedTodoSection[
        TAB_INDEXES.deliveryTime
      ].todoSubSection.map(subSection => {
        const data = subSection.data.map(todo => {
          if (todo.id === selectedTodoId) return { ...todo, selected: !todo.selected };

          return { ...todo };
        });

        return { ...subSection, data };
      });

      const updatedTodoSection = [...cachedTodoSection];

      updatedTodoSection[TAB_INDEXES.deliveryTime].todoSubSection = updatedDeliveryTimeTodos;

      cache.writeQuery({
        query: TodoSectionDocument,
        data: { todoSection: updatedTodoSection },
      });
    },
  });

  const toggleBabyItemsTodo = async (post: ToDoPost, selected: boolean): Promise<void> => {
    const variables = {
      item: { postId: Number(post.id), title: post.title, selected },
    };

    analytics().logEvent('todo_interaction', variables);

    await recordBabyItemsTodoListItem({
      variables,
    });
  };

  const toggleDeliveryTimesTodo = async (post: ToDoPost, selected: boolean): Promise<void> => {
    const variables = {
      item: { postId: Number(post.id), title: post.title, selected },
    };

    analytics().logEvent('todo_interaction', variables);

    await recordDeliveryTimeTodoListItem({
      variables,
    });
  };

  const toggleTodo = async (post: ToDoPost, selected: boolean): Promise<void> => {
    if (IS_DELIVERY_ITEMS_TAB_ACTIVE) {
      return await toggleDeliveryTimesTodo(post, selected);
    } else {
      return await toggleBabyItemsTodo(post, selected);
    }
  };

  const renderActiveSelector = (): ReactElement => {
    return (
      <Container
        mt={2}
        height={7}
        width={150}
        backgroundColor={colors.toDoActive}
        borderTopRightRadius={25}
        borderTopLeftRadius={25}
      />
    );
  };

  const renderDescriptions = (): ReactElement => {
    const activeTabDescription = TAB_BUTTONS.find((tab, i) => i === activeTab);

    return (
      <Container py={2} mx={2}>
        {activeTabDescription?.description}
      </Container>
    );
  };

  const renderTabSwitcher = (): ReactElement => {
    return (
      <>
        <Container flexDirection="row" justifyContent="center" mt={4}>
          {TAB_BUTTONS.map((tab, i) => (
            <Container key={tab?.label?.split(' ').join('')} flex={0.45} alignItems="center">
              <Touchable onPress={(): void => setActiveTab(i)}>
                <Text fontSize={4} fontWeight="bold" textAlign="center" px={3}>
                  {tab.label}
                </Text>
                {activeTab === i ? renderActiveSelector() : null}
              </Touchable>
            </Container>
          ))}
        </Container>
        <LinearGradient
          colors={['rgba(200, 200, 218, 0.35)', 'rgba(200, 200, 218, 0.1)', '#eafcff']}
          style={{ height: 5 }}
        />
      </>
    );
  };

  const renderLoaderScreenContent = (): ReactElement => {
    return <CenteredLoadingSpinner />;
  };

  const renderLoadingScreenContent = (): ReactNode => {
    return <Container height={SCREEN_HEIGHT * 0.5}>{renderLoaderScreenContent()}</Container>;
  };

  const renderToDoItem = (item: ToDoPost): ReactElement => {
    return (
      <Container key={item.id} px={1}>
        <Container
          flexDirection="row"
          pl={10}
          fullWidth
          alignItems="center"
          justifyContent="space-between"
        >
          <Container flex={1} marginVertical={5}>
            <Text fontSize={2} textAlign="justify" flex={1}>
              {item.title || ''}
            </Text>
          </Container>
          <CheckBox
            checked={item.selected}
            onPress={async (): Promise<void> => {
              await setSelectedTodoId(item.id);
              toggleTodo(item, !item.selected);
            }}
            iconType="ionicon"
            checkedIcon="md-checkbox"
            uncheckedIcon="md-square-outline"
            checkedColor={colors.green}
          />
        </Container>
      </Container>
    );
  };

  const renderToDoScreenContent = (): ReactNode => {
    console.log(
      'todo',
      data?.todoSection?.length ? data.todoSection[activeTab]?.todoSubSection || [] : []
    );

    const activeTabToDos = data?.todoSection?.length
      ? data.todoSection[activeTab]?.todoSubSection || []
      : [];

    if (!activeTabToDos.length) {
      return null;
    }

    return (
      <SectionList
        scrollIndicatorInsets={{ right: 1 }}
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: TAB_BAR_HEIGHT + Number(SCROLLVIEW_BOTTOM_PADDING),
          paddingHorizontal: 24,
        }}
        refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        sections={activeTabToDos}
        keyExtractor={(item, index): string => item?.id || '' + index}
        renderItem={({ item }): ReactElement => renderToDoItem(item as ToDoPost, false)}
        renderSectionHeader={({ section }): ReactElement => (
          <Text fontWeight="bold" fontSize={3} bg="white" py={2}>
            {section.title}
          </Text>
        )}
      />
    );
  };

  const renderScreenContent = (): ReactElement => {
    return (
      <Container>
        {renderToDoScreenContent()}
        {renderLoadingScreenContent()}
      </Container>
    );
  };

  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive} scrollEnabled={false}>
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        height={SCREEN_HEIGHT * 0.65}
        borderWidth={1}
        borderColor={colors.white}
        screenMargin={false}
        bg={colors.white}
        top={-10}
        paddingTop={4}
        pb={TAB_BAR_HEIGHT + 30}
        mx={-19}
      >
        {renderTabSwitcher()}
        {renderDescriptions()}
        {renderScreenContent()}
      </Container>
    </SubNavLayout>
  );
};

export default ToDoLayout;

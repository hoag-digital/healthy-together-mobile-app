import React, { FC, ReactElement } from 'react';
import * as Sentry from '@sentry/react-native';

import { withNavigation } from 'react-navigation';
import { RefreshControl } from 'react-native';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Text } from '../../../components/Text';
import { Container } from '../../../components/Container';
import { colors } from '../../../styles';
import { AddKickTrackingContent } from '../Content/Tracking/AddKickTrackingContent';
import { KickTrackingListContent } from '../Content/Tracking/KickTrackingListContent';
import { useMyKickTrackerQuery, KickCounter } from '../../../graphql/types';
import { t } from '../../../utils';

/**
 * Tracking layout for maternity
 */
export const TrackingLayout: FC<SubNavLayoutProps> = ({
  navigation,
  title,
  hideTitle = false,
  isActive,
}) => {
  const {
    error: myKickCounterError,
    data: myKickCounterData,
    loading: myKickCounterLoading,
    refetch: onRefresh,
  } = useMyKickTrackerQuery({
    fetchPolicy: 'cache-and-network',
  });

  let counterSession: KickCounter;
  let counterSessionId: string;

  if (myKickCounterData?.myKickTracker && myKickCounterData.myKickTracker.length) {
    const session = myKickCounterData.myKickTracker.find(
      (counter): boolean => !!counter && !!counter.isActive
    );

    if (session) {
      const tempStartAt = new Date(session.startAt);
      const expiredAt = new Date(tempStartAt.setHours(tempStartAt.getHours() + 1));
      const currentTime = new Date().getTime();

      if (
        new Date(session.startAt).getTime() <= currentTime &&
        currentTime <= expiredAt.getTime()
      ) {
        counterSession = session;
        counterSessionId = session.id;
      }
    }
  }

  if (myKickCounterError) {
    Sentry.captureException(myKickCounterError);
  }

  const renderHeader = (): ReactElement => {
    return (
      <Container height={30} display="flex" alignItems="center" justifyContent="center">
        <Text fontWeight="500">{t('maternity.tracking.title')}</Text>
      </Container>
    );
  };

  const renderAddKickTrackingScreenContent = (): ReactElement => {
    return <AddKickTrackingContent counterSession={counterSession} onRefresh={onRefresh} />;
  };

  const renderKickTrackingListScreenContent = (): ReactElement => {
    return (
      <KickTrackingListContent
        sessionId={counterSessionId}
        navigation={navigation}
        trackingData={myKickCounterData}
        isRefreshing={myKickCounterLoading}
        isCounterSession={!!counterSession}
        onRefresh={onRefresh}
      />
    );
  };

  return (
    <SubNavLayout
      title={title}
      refreshControl={<RefreshControl refreshing={myKickCounterLoading} onRefresh={onRefresh} />}
      hideTitle={hideTitle}
      isActive={isActive}
    >
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        screenMargin={false}
        bg={colors.white}
        top={-10}
        paddingTop={4}
        mx={-19}
      >
        {renderHeader()}
        {renderAddKickTrackingScreenContent()}
        {renderKickTrackingListScreenContent()}
      </Container>
    </SubNavLayout>
  );
};

export default withNavigation(TrackingLayout);

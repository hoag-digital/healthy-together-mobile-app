import React, { FC, ReactElement } from 'react';
import { withNavigation } from 'react-navigation';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';

import { Container } from '../../../components/Container';
import { SCREEN_HEIGHT, TAB_BAR_HEIGHT } from '../../../utils';
import { colors } from '../../../styles';
import { CreateNoteContent } from '../Content/Notes/CreateNoteContent';
import { NotesListContent } from '../Content/Notes/NotesListContent';
import { useMyNotesQuery } from '../../../graphql/types';

/**
 * Notes layout for maternity
 */
export const NotesLayout: FC<SubNavLayoutProps> = ({
  navigation,
  title,
  hideTitle = false,
  isActive,
}) => {
  const {
    error: myNotesError,
    data: myNotesData,
    loading: myNotesLoading,
    refetch: onRefresh,
  } = useMyNotesQuery({
    fetchPolicy: 'cache-and-network',
    variables: { limit: 5 },
  });

  if (myNotesError) {
    //TODO: handle graphql errors
  }

  const renderAddNoteScreenContent = (): ReactElement => {
    return <CreateNoteContent onRefresh={onRefresh} />;
  };

  const rendePreviousNoteContent = (): ReactElement => {
    return (
      <NotesListContent
        navigation={navigation}
        myNotesData={myNotesData}
        isRefreshing={myNotesLoading}
        onRefresh={onRefresh}
      />
    );
  };

  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive} scrollEnabled={false}>
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        height={SCREEN_HEIGHT * 0.65}
        screenMargin={false}
        bg={colors.white}
        top={-10}
        paddingTop={4}
        pb={TAB_BAR_HEIGHT - 43}
        mx={-19}
      >
        {renderAddNoteScreenContent()}
        {rendePreviousNoteContent()}
      </Container>
    </SubNavLayout>
  );
};

export default withNavigation(NotesLayout);

import React, { FC } from 'react';
import { Icon } from 'react-native-elements';
import { Container } from '../../../components/Container';
import { colors } from '../../../styles';
import { Touchable } from '../../../components/Touchable';
import { SubNavLayoutProps } from '../../../components/SubNavLayout';
import { DEFAULT_ICON_SIZE } from '../../../utils';

/**
 * SwipeButton layout for maternity
 */
export const SwipeButtonLayout: FC<SubNavLayoutProps> = ({
  noteId,
  name,
  bgColor,
  onPress,
  rowHeight,
}) => {
  return (
    <Touchable
      onPress={(): void => {
        onPress(noteId);
      }}
      style={{ height: rowHeight, backgroundColor: bgColor, alignItems: 'flex-start' }}
    >
      <Container display="flex" flex={1} width={80} justifyContent="center" alignItems="center">
        <Icon size={DEFAULT_ICON_SIZE} name={name} type="ionicon" color={colors.white} />
      </Container>
    </Touchable>
  );
};

export default SwipeButtonLayout;

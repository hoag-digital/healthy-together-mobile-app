import React, { FC } from 'react';

import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Text } from '../../../components/Text';

/**
 * Appointment layout for maternity
 */
export const AppointmentLayout: FC<SubNavLayoutProps> = ({
  title,
  hideTitle = false,
  isActive,
}) => {
  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive}>
      <Text>Coming Soon!</Text>
    </SubNavLayout>
  );
};

export default AppointmentLayout;

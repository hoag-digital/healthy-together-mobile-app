import React, { FC, ReactElement, useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Animated, FlatList } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import { Icon } from 'react-native-elements';

import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Container } from '../../../components/Container';
import { Text } from '../../../components/Text';
import { colors, space } from '../../../styles';
import { DEFAULT_ICON_SIZE, IS_ANDROID, SCREEN_WIDTH } from '../../../utils';
import { useBabySizesQuery, useMeQuery } from '../../../graphql/types';
import { Touchable } from '../../../components/Touchable';

const HEADER_ITEM_WIDTH = SCREEN_WIDTH / 12;
const SELECTED_HEADER_ITEM_WIDTH = 2 * HEADER_ITEM_WIDTH;
const CAROUSEL_ITEM_WIDTH = IS_ANDROID ? 120 : 180;
const CAROUSEL_ITEM_HEIGHT = IS_ANDROID ? 120 : 180;

// 1. create total offset from width of all list items
// 2. subtract half of the width of the scrollbar (minus its horizontal padding)
// 3. subtract a quarter of an item width (which is half of the total extra width of the active item)
const getOffsetForWeek = (week: number): number => {
  return HEADER_ITEM_WIDTH * week - (SCREEN_WIDTH - 2 * space[2]) / 2 + HEADER_ITEM_WIDTH / 4;
};

const getTrimesterOrdinal = (week: number): string => {
  if (week < 14) return '1st';
  if (week < 27) return '2nd';

  return '3rd';
};

/**
 * Baby Size layout for maternity
 *
 * The snap-carousel is the “controller” and the “trimester weeks” header reacts to its position.
 * Challenges of the other potential implementations:
 * - Having the carousel update the header, and the header update the carousel would have required
 * 2-way binding, which feels like a 1-way ticket to bug hell.
 * - Having the “Trimester Week Carousel Header” control the carousel would have required a
 * “complete” data set to function. (i.e. a “Baby Size” for every single week… all 40)
 *
 * Having the carousel be the only interactable element and control the header allows for 1-way
 * binding and the usage of an incomplete data set.
 */
export const BabySizeLayout: FC<SubNavLayoutProps> = ({ title, hideTitle = false, isActive }) => {
  const carousel = useRef();
  const header = useRef();
  const [babySizes, setBabySizes] = useState([]);
  const [activeSlideIndex, setActiveSlideIndex] = useState(0);
  const opacity = useRef(new Animated.Value(0));

  const { data: meData, loading: meLoading } = useMeQuery({
    fetchPolicy: 'network-only',
  });

  const { data: babySizesData, loading: babySizesLoading } = useBabySizesQuery({
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    const sizes = (babySizesData?.babySizes || [])
      .map(size => {
        const week = parseInt(size.title.replace(/\D/g, ''), 10);
        const trimester = getTrimesterOrdinal(week);

        return {
          title: size?.title,
          image: size?.mediumImage || '',
          trimester,
          week,
          length: size?.babySize,
          weight: size?.babyWeight,
        };
      })
      .sort((a, b) => a.week - b.week);

    setBabySizes(sizes);
  }, [babySizesData, babySizesLoading]);

  function snapToIndex(index, animated = true): void {
    header?.current?.scrollToOffset({
      animated,
      offset: getOffsetForWeek(index + 1),
    });

    carousel?.current?.snapToItem(index, animated);
  }

  useEffect(() => {
    if (isActive && !meLoading && !babySizesLoading) {
      setActiveSlideIndex(0);
      Animated.spring(opacity.current, { toValue: 1 }).start();
      // snapToIndex(meData?.me?.maternityProfile?.gestationalAge?.weeks - 1, true);
    } else {
      opacity.current.setValue(0);
    }
  }, [babySizesLoading, isActive, meData, meLoading]);

  useEffect(() => {
    snapToIndex(activeSlideIndex);
  }, [activeSlideIndex]);

  const activeBabySize = babySizes[activeSlideIndex] || {};
  const { length, weight, week: activeWeek } = activeBabySize;

  const renderCarouselItem = ({ item }): ReactElement => {
    return (
      <Container centerContent p={1}>
        <Container
          style={{
            flex: 1,
            borderRadius: 15,
            width: CAROUSEL_ITEM_WIDTH,
            height: CAROUSEL_ITEM_HEIGHT,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            elevation: 2,
            backgroundColor: 'white',
          }}
        >
          <FastImage
            resizeMode="contain"
            accessibilityLabel={`${item.title} image`}
            style={{
              width: CAROUSEL_ITEM_WIDTH,
              height: CAROUSEL_ITEM_HEIGHT,
              borderRadius: 15,
            }}
            source={{ uri: item.image }}
          />
        </Container>
        <Text pt={IS_ANDROID ? 2 : 3} fontWeight="bold" fontSize={IS_ANDROID ? 2 : 4}>
          {item.title}
        </Text>
      </Container>
    );
  };

  const renderCarouselHeaderItem = ({ item, index }): ReactElement => {
    if (item.week === activeWeek) {
      return (
        <Container
          width={SELECTED_HEADER_ITEM_WIDTH}
          bg={colors.wewak}
          borderRadius={15}
          py={1}
          centerContent
        >
          <Text color={colors.white} fontWeight="bold" fontSize={3} px={4}>
            {item.week}
          </Text>
        </Container>
      );
    }

    return (
      <Container width={HEADER_ITEM_WIDTH} centerContent>
        <Text color={colors.gray} fontWeight="bold" fontSize={2}>
          {item.week}
        </Text>
      </Container>
    );
  };

  const renderCarouselHeader = (): ReactElement => {
    return (
      <Container centerContent>
        <Container flexDirection="row" centerContent py={2}>
          <Touchable
            pr={2}
            pl={4}
            onPress={(): void => setActiveSlideIndex(Math.max(activeSlideIndex - 1, 0))}
          >
            <Icon name="chevron-left" size={DEFAULT_ICON_SIZE} color={colors.gray} />
          </Touchable>
          <Container justifyContent="center">
            <Text fontSize={IS_ANDROID ? 2 : 3} fontWeight="bold">
              {`${getTrimesterOrdinal(activeWeek)} Trimester Week`}
            </Text>
          </Container>
          <Touchable
            pl={2}
            pr={4}
            onPress={(): void =>
              setActiveSlideIndex(Math.min(activeSlideIndex + 1, babySizes.length - 1))
            }
          >
            <Icon name="chevron-right" size={DEFAULT_ICON_SIZE} color={colors.gray} />
          </Touchable>
        </Container>
        <Container borderRadius={15} py={1} my={IS_ANDROID ? 0 : 2} bg={colors.offYellow} mx={3}>
          <FlatList
            ref={header}
            style={{ flexGrow: 0 }}
            showsHorizontalScrollIndicator={false}
            scrollEnabled={false}
            horizontal
            data={babySizes}
            renderItem={renderCarouselHeaderItem}
            keyExtractor={(item, index): string => `${item.title}-${index}`}
          />
        </Container>
      </Container>
    );
  };

  const renderCarousel = (): ReactElement => {
    return (
      <Container py={3} centerContent>
        <Carousel
          ref={carousel}
          enableSnap
          shouldOptimizeUpdates
          data={babySizes}
          renderItem={renderCarouselItem}
          sliderWidth={SCREEN_WIDTH}
          itemWidth={SCREEN_WIDTH * 0.5}
          contentContainerCustomStyle={{ alignItems: 'center' }}
          onBeforeSnapToItem={(index): void => setActiveSlideIndex(index)}
        />
      </Container>
    );
  };

  const renderCarouselFooter = (): ReactElement => {
    return (
      <Container fullWidth my={IS_ANDROID ? 0 : 2}>
        <Container flexDirection="row" justifyContent="center">
          <Container
            bg={colors.offYellow}
            centerContent
            px={3}
            py={2}
            borderRadius={15}
            mx={3}
            width="42.5%"
          >
            <Text fontSize={IS_ANDROID ? 1 : 2}>Length</Text>
            <Text height fontSize={IS_ANDROID ? 1 : 2} fontWeight="bold">
              {length}
            </Text>
          </Container>
          <Container
            bg={colors.offYellow}
            centerContent
            px={3}
            py={2}
            borderRadius={15}
            mx={3}
            width="42.5%"
          >
            <Text fontSize={IS_ANDROID ? 1 : 2}>Weight</Text>
            <Text fontSize={IS_ANDROID ? 1 : 2} fontWeight="bold">
              {weight}
            </Text>
          </Container>
        </Container>
      </Container>
    );
  };

  const animatedStyle = {
    opacity: opacity.current,
  };

  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive} scrollEnabled={false}>
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        borderWidth={0}
        borderColor={colors.white}
        screenMargin={false}
        bg={colors.white}
        paddingTop={4}
        pb={IS_ANDROID ? 80 : 99}
        mx={-19}
        fill
      >
        <Animated.View style={animatedStyle}>
          {renderCarouselHeader()}
          {renderCarousel()}
          {renderCarouselFooter()}
          <Container centerContent py={2}>
            <Text>Sizes listed may vary from your baby's actual size.</Text>
          </Container>
        </Animated.View>
        <Container
          pointerEvents="none"
          fill
          centerContent
          position="absolute"
          height="100%"
          width="100%"
        >
          {meLoading || babySizesLoading ? <ActivityIndicator /> : null}
        </Container>
      </Container>
    </SubNavLayout>
  );
};

export default BabySizeLayout;

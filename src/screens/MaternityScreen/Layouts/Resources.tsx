import React, { FC, ReactElement, ReactNode, useCallback, useEffect, useState } from 'react';
import { RefreshControl, FlatList, ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';

import { Icon } from 'react-native-elements';

import {
  MeDocument,
  Post,
  useMaternityResourceContentQuery,
  useMeQuery,
  useResetPregnancyLossMutation,
} from '../../../graphql/types';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Text } from '../../../components/Text';
import { Container } from '../../../components/Container';
import { colors } from '../../../styles';
import { Touchable } from '../../../components/Touchable';
import {
  SCREEN_WIDTH,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_HEIGHT,
  LARGER_ICON_SIZE,
  t,
} from '../../../utils';
import { CenteredLoadingSpinner } from '../../../components/CenteredLoadingSpinner';
import { ContentErrorToast } from '../../../components/Toastr';
import { Button } from '../../../components/Button';

const LEFT_POSITION = -20;
const KEY_LOSS = 'loss';
const KEY_FAMILY_PLANNING = 'planning';

interface TabItem {
  label: string;
  key: string;
}

const RESOURCE_SECTIONS: {
  HAS_LOSS: TabItem[];
  OTHER: TabItem[];
} = {
  HAS_LOSS: [
    {
      label: 'Pregnancy Loss',
      key: KEY_LOSS,
    },
  ],
  OTHER: [
    {
      label: 'Family Planning',
      key: KEY_FAMILY_PLANNING,
    },
  ],
};

/**
 * Resources layout for maternity
 */
export const ResourcesLayout: FC<SubNavLayoutProps> = ({
  title,
  hideTitle = false,
  isActive,
  navigation,
}) => {
  const { data: { me } = {}, loading: meLoading, error: meError } = useMeQuery();

  const [resetPregnancyLoss] = useResetPregnancyLossMutation({
    refetchQueries: [{ query: MeDocument }],
    awaitRefetchQueries: true,
  });

  const [currentLossStatus, setLossStatus] = useState(false);
  const { maternityProfile } = me;
  const { hasPregnancyLoss } = maternityProfile;
  const [activeTab, setActiveTab] = useState(hasPregnancyLoss ? KEY_LOSS : KEY_FAMILY_PLANNING);

  const {
    error: contentError,
    data: contentData = {},
    loading: contentLoading,
    refetch: refetchContent,
  } = useMaternityResourceContentQuery({
    fetchPolicy: 'cache-and-network',
  });

  const resetLoss = async (): Promise<void> => {
    try {
      await resetPregnancyLoss();
    } catch (error) {}
  };

  const getItemsFromRole = (): TabItem[] => {
    if (meLoading) return [];
    if (hasPregnancyLoss === true) return RESOURCE_SECTIONS.HAS_LOSS;

    return RESOURCE_SECTIONS.OTHER;
  };

  const menuItems = getItemsFromRole();

  const updateLossStatus = useCallback(async (): Promise<void> => {
    await setLossStatus(maternityProfile.hasPregnancyLoss);
    //reset the active tab
    const tab = maternityProfile.hasPregnancyLoss === true ? KEY_LOSS : KEY_FAMILY_PLANNING;
    setActiveTab(tab);
  }, [maternityProfile.hasPregnancyLoss]);

  //This will watch for a change in maternity role, when this occurs it needs to update the subnav menu items correctly
  useEffect(() => {
    if (currentLossStatus !== maternityProfile.hasPregnancyLoss) {
      updateLossStatus();
    }
  }, [currentLossStatus, maternityProfile.hasPregnancyLoss, updateLossStatus]);

  const contentHasLoaded = contentData && Object.keys(contentData).length;

  const isLoading = meLoading;

  const navigateToDetailsScreen = (post: Post): void => {
    navigation.navigate('ContentDetailsModal', {
      postId: post.id,
      hideLabel: true,
      hideImage: true,
    });
  };

  const renderItem = ({ item, index }): ReactElement => {
    return (
      <Touchable
        onPress={(): void => {
          navigateToDetailsScreen(item);
        }}
      >
        <Container
          key={index}
          flexDirection="row"
          justifyContent="center"
          py={2}
          borderBottomWidth={2}
          borderColor={colors.gainsboro}
        >
          <Container flex={0.9}>
            <Text>{item.title}</Text>
          </Container>
          <Container flex={0.1}>
            <Icon name="chevron-right" size={LARGER_ICON_SIZE} />
          </Container>
        </Container>
      </Touchable>
    );
  };

  const renderResources = (): ReactElement => {
    const content = contentData || {};
    const { pregnancyLoss } = content;
    return (
      <Container fullWidth>
        <FlatList
          style={{ flexGrow: 0, paddingBottom: 5 }}
          data={activeTab === KEY_LOSS ? pregnancyLoss : []}
          renderItem={renderItem}
        />
      </Container>
    );
  };

  const renderActiveSelector = (): ReactElement => {
    return (
      <Container
        height={7}
        width={150}
        marginTop={1}
        backgroundColor={colors.cornflowerBlue3}
        borderTopRightRadius={25}
        borderTopLeftRadius={25}
      />
    );
  };

  const renderSectionSwitcher = (): ReactElement => {
    return (
      <Container flexDirection="row" justifyContent="center" py={4}>
        {menuItems.map(tab => (
          <Container key={tab.key} flex={0.4} alignItems="center">
            <Touchable onPress={(): void => setActiveTab(tab.key)}>
              <Text textAlign="center" px={3}>
                {tab.label}
              </Text>
              {activeTab === tab.key ? renderActiveSelector() : null}
            </Touchable>
          </Container>
        ))}
      </Container>
    );
  };

  const renderScreenContent = (): ReactElement => {
    if (contentLoading && !Object.keys(contentData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollEnabled
        scrollIndicatorInsets={{ right: 1 }}
        style={{ width: SCREEN_WIDTH, height: SCREEN_HEIGHT, paddingLeft: 10 }}
        refreshControl={<RefreshControl refreshing={contentLoading} onRefresh={refetchContent} />}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={5} mr={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          {renderResources()}
        </Container>
      </ScrollView>
    );
  };

  const renderCloseLossContent = (): ReactNode => {
    if (hasPregnancyLoss) {
      return (
        <Button
          label={t('maternity.pregnancyLoss.resetButton')}
          backgroundColor={colors.pearlLusta}
          color={colors.black}
          fontSize={3}
          onPress={(): void => {
            resetLoss();
          }}
        />
      );
    }

    return null;
  };

  if (contentError) {
    <ContentErrorToast />;
  }

  return (
    <SubNavLayout
      isLoading={isLoading && !contentHasLoaded}
      title={title}
      hideTitle={hideTitle}
      isActive={isActive}
      testId={`${title}-layout`}
      refreshControl={<RefreshControl refreshing={contentLoading} onRefresh={refetchContent} />}
    >
      <Container
        bg={colors.white}
        width={SCREEN_WIDTH}
        height={SCREEN_HEIGHT}
        p={2}
        style={{ left: LEFT_POSITION, position: 'relative' }}
      >
        {renderSectionSwitcher()}
        {renderCloseLossContent()}
        {renderScreenContent()}
      </Container>
    </SubNavLayout>
  );
};

export default withNavigation(ResourcesLayout);

import React, { FC, ReactNode } from 'react';
import { withNavigation } from 'react-navigation';
import { Badge, Icon } from 'react-native-elements';
import { Text } from '../../../components/Text';
import { Button } from '../../../components/Button';
import { Container } from '../../../components/Container';
import { Touchable } from '../../../components/Touchable';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { CenteredLoadingSpinner } from '../../../components/CenteredLoadingSpinner';

import { useBirthplanCategoriesQuery, ContentCategory } from '../../../graphql/types';
import { colors } from '../../../styles';
import { LARGEST_ICON_SIZE, SCREEN_WIDTH, t } from '../../../utils';
import { ContentErrorToast } from '../../../components/Toastr';
import { ReadMoreText } from '../../../components/ReadMoreText';

/**
 * Planning layout for maternity
 */
export const BirthPlanLayout: FC<SubNavLayoutProps> = ({
  title,
  hideTitle = false,
  isActive,
  navigation,
}) => {
  const {
    error: categoriesError,
    data: categoriesData = {},
    loading: categoriesLoading,
  } = useBirthplanCategoriesQuery({
    fetchPolicy: 'cache-and-network',
  });

  const changeIndex = (currentIndex: number): void => {
    navigation.navigate('BirthplanCategory', {
      category: categoriesData.birthplanCategories[currentIndex],
      currentIndex,
      changeIndex,
      isLastCategory: categoriesData.birthplanCategories.length === currentIndex + 1,
    });
  };

  const navigateToCategoryDetails = (category: ContentCategory, currentIndex: number): void => {
    navigation.navigate('BirthplanCategory', {
      category,
      currentIndex,
      changeIndex,
      isLastCategory: categoriesData.birthplanCategories.length === currentIndex + 1,
    });
  };

  const renderCategoryItem = (category: ContentCategory, index: number): ReactNode => {
    const isLastItem = index === categoriesData.birthplanCategories.length - 1;
    const badgeValue = category.numberSelected || 0; // converts null value to int for comparison
    const hasCheckedItems = badgeValue > 0;
    return (
      <Touchable key={category.id} onPress={(): void => navigateToCategoryDetails(category, index)}>
        <Container
          borderBottomWidth={isLastItem ? 0 : 1}
          borderBottomColor={colors.solitude}
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Text fontSize={3} py={3}>
            {category.name}
          </Text>
          <Container flexDirection="row" alignItems="center">
            <Badge
              value={badgeValue}
              badgeStyle={{
                backgroundColor: hasCheckedItems ? colors.wisteria : colors.transparent,
                borderRadius: 5,
              }}
              textStyle={{ color: hasCheckedItems ? colors.white : colors.black }}
            />
            <Icon name="chevron-right" size={LARGEST_ICON_SIZE} />
          </Container>
        </Container>
      </Touchable>
    );
  };

  const renderCategories = (): ReactNode => {
    if (categoriesLoading && !Object.keys(categoriesData).length) {
      return <CenteredLoadingSpinner />;
    }

    const { birthplanCategories } = categoriesData;
    return birthplanCategories.map((category: ContentCategory, index: number) =>
      renderCategoryItem(category, index)
    );
  };

  if (categoriesError) {
    <ContentErrorToast />;
  }

  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive}>
      <Container
        bg={colors.white}
        width={SCREEN_WIDTH}
        minHeight={675}
        position="relative"
        left={-20}
        px={3}
      >
        <ReadMoreText text={t('maternity.birthplan.explanation')} />
        {renderCategories()}
        <Button
          label="View My Plan"
          onPress={(): boolean => navigation.navigate('BirthplanDetails')}
          backgroundColor={colors.bouquet}
          color={colors.white}
          fontSize={4}
          p={2}
          mt={2}
        />
      </Container>
    </SubNavLayout>
  );
};

export default withNavigation(BirthPlanLayout);

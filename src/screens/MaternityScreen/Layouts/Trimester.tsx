import React, { FC, useEffect, useState, useRef, ReactElement, ReactNode } from 'react';
import { FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Text } from '../../../components/Text';
import { Container } from '../../../components/Container';
import { Touchable } from '../../../components/Touchable';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { CenteredLoadingSpinner } from '../../../components/CenteredLoadingSpinner';

import { colors } from '../../../styles';
import {
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_WIDTH,
  SCREEN_HEIGHT,
  TOUCHABLE_PRESS_DELAY,
  TAB_BAR_HEIGHT,
  t,
} from '../../../utils';
import {
  useMilestoneCategoriesQuery,
  useMilestoneContentByCategoryLazyQuery,
  usePotentialLabCategoriesQuery,
  usePotentialLabContentByCategoryLazyQuery,
  Content,
  ContentCategory,
  Maybe,
  Post,
} from '../../../graphql/types';
import { ContentErrorToast } from '../../../components/Toastr';
import { ReadMoreText } from '../../../components/ReadMoreText';

const KEY_TESTS = 'tests';
const KEY_MILESTONE = 'milestones';

const TAB_BUTTONS = [
  {
    label: 'Tests',
    key: KEY_TESTS,
  },
  {
    label: 'Milestones',
    key: KEY_MILESTONE,
  },
];

/**
 * Trimester layout for maternity
 */
export const TrimesterLayout: FC<SubNavLayoutProps> = ({ title, hideTitle = false, isActive }) => {
  const {
    error: potentialLabCategoriesError,
    data: potentialLabCategoriesData,
    loading: potentialLabCategoriesLoading,
  } = usePotentialLabCategoriesQuery({ fetchPolicy: 'cache-and-network' });

  const {
    error: milestoneCategoriesError,
    data: milestoneCategoriesData,
    loading: milestoneCategoriesLoading,
  } = useMilestoneCategoriesQuery({
    fetchPolicy: 'cache-and-network',
  });

  const skipPostOfInitialCategory =
    !potentialLabCategoriesData ||
    !potentialLabCategoriesData.potentialLabCategories ||
    potentialLabCategoriesData.potentialLabCategories.length === 0;

  const initialCategoryId =
    potentialLabCategoriesData?.potentialLabCategories?.length &&
    potentialLabCategoriesData?.potentialLabCategories[0]
      ? potentialLabCategoriesData.potentialLabCategories[0].id
      : '';

  const skipMilestoneOfInitialCategory =
    !milestoneCategoriesData ||
    !milestoneCategoriesData.milestoneCategories ||
    milestoneCategoriesData.milestoneCategories.length === 0;

  const initialMilestoneCategoryId =
    milestoneCategoriesData?.milestoneCategories?.length &&
    milestoneCategoriesData?.milestoneCategories[0]
      ? milestoneCategoriesData.milestoneCategories[0].id
      : '';

  const [activeTab, setActiveTab] = useState(KEY_TESTS);
  const [activeTestsTab, setActiveTestsTab] = useState(initialCategoryId);
  const [activeMilestoneTab, setActiveMilestoneTab] = useState(initialMilestoneCategoryId);

  const _flatList = useRef<FlatList>(null);

  const [
    getPostsForCategory,
    {
      error: postsForContentCategoryError,
      data: postsForContentCategoryData,
      loading: postsForContentCategoryLoading,
    },
  ] = usePotentialLabContentByCategoryLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  if (
    !skipPostOfInitialCategory &&
    !postsForContentCategoryData &&
    !postsForContentCategoryLoading
  ) {
    getPostsForCategory({
      variables: {
        categoryId: initialCategoryId,
      },
    });
  }

  const [
    getMilestoneContentByCategory,
    {
      error: milestoneContentByCategoryError,
      data: milestoneContentByCategoryData,
      loading: milestoneContentByCategoryLoading,
    },
  ] = useMilestoneContentByCategoryLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  if (
    !skipMilestoneOfInitialCategory &&
    !milestoneContentByCategoryData &&
    !milestoneContentByCategoryLoading
  ) {
    getMilestoneContentByCategory({
      variables: {
        categoryId: initialMilestoneCategoryId,
      },
    });
  }

  if (
    potentialLabCategoriesError ||
    postsForContentCategoryError ||
    milestoneCategoriesError ||
    milestoneContentByCategoryError
  ) {
    <ContentErrorToast />;
  }

  const setScrollPosition = (): void => {
    if (_flatList && _flatList.current) {
      let selectedTabCategoryData: Maybe<Array<Maybe<ContentCategory>>> = [];
      let activeSubTab = '';

      if (activeTab === KEY_TESTS) {
        selectedTabCategoryData = potentialLabCategoriesData?.potentialLabCategories || [];
        activeSubTab = activeTestsTab;
      } else {
        selectedTabCategoryData = milestoneCategoriesData?.milestoneCategories || [];
        activeSubTab = activeMilestoneTab;
      }

      if (selectedTabCategoryData.length) {
        if (activeSubTab) {
          const scrollIndex = selectedTabCategoryData.findIndex(
            category => category && category.id === activeSubTab
          );

          if (scrollIndex !== -1) {
            _flatList.current.scrollToIndex({ animated: true, index: scrollIndex });
          }
        } else {
          _flatList.current.scrollToIndex({ animated: true, index: 0 });
        }
      }
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setScrollPosition();
    }, 100);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeTab]);

  const onSelectCategory = (selectedTabId, scrollIndex): void => {
    activeTab === KEY_TESTS
      ? setActiveTestsTab(selectedTabId)
      : setActiveMilestoneTab(selectedTabId);

    if (_flatList && _flatList.current) {
      _flatList.current.scrollToIndex({ animated: true, index: scrollIndex });
    }

    const getDataForCurrentTab =
      activeTab === KEY_TESTS ? getPostsForCategory : getMilestoneContentByCategory;

    getDataForCurrentTab({
      variables: {
        categoryId: selectedTabId,
      },
    });
  };

  const renderActiveSelector = (): ReactElement => {
    return (
      <Container
        mt={2}
        height={7}
        width={150}
        backgroundColor={colors.trimesterActive}
        borderTopRightRadius={25}
        borderTopLeftRadius={25}
      />
    );
  };

  const renderTabSwitcher = (): ReactElement => {
    return (
      <Container flexDirection="row" justifyContent="center" mt={4}>
        {TAB_BUTTONS.map(tab => (
          <Container key={tab.key} flex={0.45} alignItems="center">
            <Touchable onPress={(): void => setActiveTab(tab.key)}>
              <Text fontSize={4} fontWeight="bold" textAlign="center" px={3}>
                {tab.label}
              </Text>
              {activeTab === tab.key ? renderActiveSelector() : null}
            </Touchable>
          </Container>
        ))}
      </Container>
    );
  };

  const renderLoaderScreenContent = (): ReactElement => {
    return <CenteredLoadingSpinner />;
  };

  const renderLoadingScreenContent = (): ReactNode => {
    if (activeTab === KEY_TESTS) {
      if (potentialLabCategoriesLoading) {
        return <Container height={SCREEN_HEIGHT * 0.5}>{renderLoaderScreenContent()}</Container>;
      }

      if (postsForContentCategoryLoading) {
        return <Container height={SCREEN_HEIGHT * 0.4}>{renderLoaderScreenContent()}</Container>;
      }
    }

    if (activeTab === KEY_MILESTONE) {
      if (milestoneCategoriesLoading) {
        return <Container height={SCREEN_HEIGHT * 0.5}>{renderLoaderScreenContent()}</Container>;
      }

      if (milestoneContentByCategoryLoading) {
        return <Container height={SCREEN_HEIGHT * 0.4}>{renderLoaderScreenContent()}</Container>;
      }
    }

    return null;
  };

  const renderSubTabItem = ({ item, index }): ReactNode => {
    let selectedTabCategoryData: Maybe<Array<Maybe<ContentCategory>>> = [];
    let activeSubTab = '';

    if (activeTab === KEY_TESTS) {
      selectedTabCategoryData = potentialLabCategoriesData?.potentialLabCategories || [];
      activeSubTab = activeTestsTab;
    } else {
      selectedTabCategoryData = milestoneCategoriesData?.milestoneCategories || [];
      activeSubTab = activeMilestoneTab;
    }

    if (!selectedTabCategoryData.length) {
      return null;
    }

    const isFirst = index === 0;
    const isLast = index === selectedTabCategoryData.length - 1;
    const isSubTabActive = item.id === activeSubTab || (isFirst && activeSubTab === '');

    return (
      <Touchable
        delayPressIn={TOUCHABLE_PRESS_DELAY}
        key={`${item}-${index}`}
        borderRadius={50}
        minWidth={85}
        height={35}
        ml={isFirst ? 6 : 1}
        mr={isLast ? 6 : 1}
        bg={isSubTabActive ? colors.white : colors.whiteTransparent}
        onPress={(): void => onSelectCategory(item.id, index)}
        centerContent
      >
        <Container flexDirection="row" alignItems="center" opacity={isSubTabActive ? 1 : 0.4}>
          <Container
            bg={colors.royalBlue}
            alignItems="center"
            justifyContent="center"
            borderRadius={50}
            width={35}
            height={35}
          >
            <Text color={colors.white}>{index + 1}</Text>
          </Container>
          <Text
            fontSize={2}
            px={2}
            fontWeight="bold"
            color={isSubTabActive ? colors.black : colors.gray}
          >
            {item.name}
          </Text>
        </Container>
      </Touchable>
    );
  };

  const renderTrimesterTabSwitcher = (): ReactNode => {
    const selectedTabCategoryData: Maybe<Array<Maybe<ContentCategory>>> =
      activeTab === KEY_TESTS
        ? potentialLabCategoriesData?.potentialLabCategories || []
        : milestoneCategoriesData?.milestoneCategories || [];

    if (!selectedTabCategoryData.length) {
      return null;
    }

    return (
      <Container
        bg={colors.lightGray}
        height={64}
        fullWidth
        alignItems="center"
        justifyContent="center"
        flexDirection="row"
      >
        <FlatList
          style={{ flexGrow: 0 }}
          data={selectedTabCategoryData}
          renderItem={renderSubTabItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          ref={_flatList}
        />
      </Container>
    );
  };

  const renderHorizontalLine = (isLastPost: boolean): ReactNode => {
    if (isLastPost) {
      return null;
    }

    return <Container height={1} maxWidth={SCREEN_WIDTH * 0.5} bg={colors.lineGray} my={7} />;
  };

  const renderPostContent = (item: Post, isLastPost: boolean): ReactElement => {
    return (
      <Container key={item.id} px={4}>
        <Container>
          <Text fontWeight="bold" fontSize={3} textAlign="justify">
            {item.title || ''}
          </Text>
          {!!item.description && (
            <Text color={colors.black} lineHeight={3} fontSize={2} opacity={0.8} mt={2}>
              {item.description}
            </Text>
          )}
        </Container>
        {renderHorizontalLine(isLastPost)}
      </Container>
    );
  };

  const renderTrimesterScreenContent = (): ReactNode => {
    let subPostsForContentCategory: Maybe<Array<Maybe<Content>>> = [];

    if (activeTab === KEY_TESTS) {
      subPostsForContentCategory = postsForContentCategoryData?.potentialLabContentByCategory || [];
    }

    if (activeTab === KEY_MILESTONE) {
      subPostsForContentCategory = milestoneContentByCategoryData?.milestoneContentByCategory || [];
    }

    if (!subPostsForContentCategory.length) {
      return null;
    }

    const totalCategoryPosts = subPostsForContentCategory.length;

    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: TAB_BAR_HEIGHT * 2,
        }}
      >
        <Container px={2} pt={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          {subPostsForContentCategory.map((post, index) => {
            const isLastPost: boolean = index === totalCategoryPosts - 1;
            return renderPostContent(post as Post, isLastPost);
          })}
        </Container>
      </ScrollView>
    );
  };

  const renderScreenContent = (): ReactElement => {
    return (
      <Container>
        {renderTrimesterTabSwitcher()}
        {renderTrimesterScreenContent()}
        {renderLoadingScreenContent()}
      </Container>
    );
  };

  const renderDescriptionText = (): ReactElement => {
    let descriptionText = t('maternity.tabDescription.milestoneDescription');

    if (activeTab === KEY_TESTS) {
      descriptionText = t('maternity.tabDescription.testDescription');
    }

    //TODO: implement custom ReadMore component
    return <ReadMoreText text={descriptionText} />;
  };

  return (
    <SubNavLayout title={title} hideTitle={hideTitle} isActive={isActive} scrollEnabled={false}>
      <Container
        borderTopLeftRadius={15}
        borderTopRightRadius={15}
        height={SCREEN_HEIGHT * 0.65}
        borderWidth={0}
        borderColor={colors.white}
        screenMargin={false}
        bg={colors.white}
        top={-10}
        paddingTop={4}
        pb={TAB_BAR_HEIGHT}
        mx={-19}
      >
        {renderTabSwitcher()}
        {renderDescriptionText()}
        {renderScreenContent()}
      </Container>
    </SubNavLayout>
  );
};

export default TrimesterLayout;

import React, { FC, ReactNode, ReactElement } from 'react';
import { RefreshControl } from 'react-native';
import { withNavigation } from 'react-navigation';

import { Icon } from 'react-native-elements';

import analytics from '@react-native-firebase/analytics';
import {
  Event,
  Post,
  useMeQuery,
  useMaternityOverviewContentQuery,
  useWebUrlsQuery,
  Content,
} from '../../../graphql/types';
import { OnDemandEventCard, EventGroupCard, ContentCard } from '../../../components/Card';
import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Text } from '../../../components/Text';
import { TrimesterProgress } from '../../../components/TrimesterProgress';
import { PromoBanner } from '../../../components/PromoBanner';
import { USER_ROLE } from '../../../utils/maternityOnboarding';
import { Container } from '../../../components/Container';
import { colors } from '../../../styles';
import GenericNewsImage from '../../../assets/images/generic_news.jpg';
import { Banner } from '../../../components/Banner';
import { Touchable } from '../../../components/Touchable';
import { IS_IPHONE, safelyOpenUrl, t } from '../../../utils';
import { ContentErrorToast } from '../../../components/Toastr';

const babyLinePhone = '949-764-2229';
const phoneNumber = babyLinePhone.replace(/-/g, '');
const phoneUrl = IS_IPHONE ? `telprompt:${phoneNumber}` : `tel:${phoneNumber}`;
const listItemStyle = { my: 1, pb: 2 };

/**
 * Overview layout for maternity
 */
export const OverviewLayout: FC<SubNavLayoutProps> = ({
  title,
  hideTitle = false,
  isActive,
  setParentActiveItem,
  navigation,
}) => {
  const { data: { me } = {}, loading: meLoading, error: meError } = useMeQuery();
  const { trimester } = me?.maternityProfile?.gestationalAge || {};

  const { error: webUrlsError, data: webUrlsData } = useWebUrlsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const maternityPreAdmitRegistrationUrl = webUrlsData?.webUrls?.maternityPreAdmission || '';

  const {
    data: maternityOverviewContent,
    loading: maternityOverviewContentLoading,
    refetch: refetchMaternityOverviewContent,
    error: maternityOverviewContentError,
  } = useMaternityOverviewContentQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      trimester,
    },
  });

  // const { maternityPosts, featuredMaternityPosts, maternityPromoContent, maternityEvents } =
  //   maternityOverviewContent || {};

  const { maternityEvents, maternityPromoContent, maternityOverviewArticleContent } =
    maternityOverviewContent || {};

  const navigateToPostDetailsScreen = (post: Content): void => {
    analytics().logEvent('view_maternity_article', { title: post.title });
    navigation.navigate('ContentDetailsModal', { postId: post.id, hideLabel: true });
  };

  const navigateToOnDemandClassScreen = (eventId?: string | null): void => {
    if (!eventId) return;

    navigation.navigate('OnDemandClassModal', { eventId });
  };

  const navigateToClassDetailsScreen = (event?: Event | null): void => {
    // we are currently using the title of an eventGroup as its id
    if (event) {
      navigation.navigate('ClassDetailsModal', {
        eventGroupName: event.name,
      });
    }
  };

  const renderEventCardForEventType = (event: Event): ReactElement => {
    return (
      <Container key={event.name} pb={2}>
        {event.isOnDemand ? (
          <OnDemandEventCard
            onPress={(): void => navigateToOnDemandClassScreen(event.eventId)}
            {...event}
          />
        ) : (
          <EventGroupCard
            {...event}
            withBadge
            onPress={(): void => navigateToClassDetailsScreen(event)}
          />
        )}
      </Container>
    );
  };

  const renderPost = (post: Content): ReactElement => {
    const { id, title: postTitle, description, mediumImage, isFeatured } = post;

    return (
      <Container key={id} pb={2}>
        <ContentCard
          id={id}
          heading={postTitle}
          text={description}
          cardLabel={isFeatured ? 'Featured' : null}
          cardLabelBackgroundColor={colors.vistaBlue}
          imageSrc={mediumImage}
          imageAsset={GenericNewsImage}
          onPress={(): void => navigateToPostDetailsScreen(post)}
        />
      </Container>
    );
  };

  const renderPromoContent = (contentPosts?: Content[]): ReactElement | null => {
    if (!contentPosts || !contentPosts.length) return null;

    return (
      <>
        {contentPosts.map(contentPost => {
          return (
            <Container key={contentPost.id} {...listItemStyle}>
              <PromoBanner {...contentPost} />
            </Container>
          );
        })}
      </>
    );
  };

  const renderEvents = (events?: Event[]): ReactElement | null => {
    if (!events || !events.length) return null;

    return <>{events.map(renderEventCardForEventType)}</>;
  };

  const renderPosts = (posts?: Content[]): ReactElement | null => {
    if (!posts || !posts.length) return null;

    return <>{posts.map(renderPost)}</>;
  };

  const renderPromoContentSection = (): ReactElement | null => {
    if (!maternityPromoContent?.length) return null;

    return <>{renderPromoContent(maternityPromoContent)}</>;
  };

  const renderEventsSection = (): ReactElement | null => {
    if (!maternityEvents?.length) return null;

    return <>{renderEvents(maternityEvents)}</>;
  };

  const renderArticlesSection = (): ReactElement | null => {
    if (!maternityOverviewArticleContent?.length) return null;

    return <>{renderPosts(maternityOverviewArticleContent)}</>;
  };

  // const renderFeaturedArticlesSection = (): ReactElement | null => {
  //   if (!featuredMaternityPosts?.length) return null;

  //   return <>{renderPosts(featuredMaternityPosts)}</>;
  // };

  const onBannerPress = (): void => {
    try {
      analytics().logEvent('maternity_pre_registration_click', {
        maternityPreAdmitRegistrationUrl,
      });

      safelyOpenUrl(maternityPreAdmitRegistrationUrl);
    } catch (err) {
      console.log(err);
    }
  };

  const onPhonePress = (): void => {
    try {
      analytics().logEvent('maternity_call_baby_line_click');

      safelyOpenUrl(phoneUrl);
    } catch (err) {
      console.log(err);
    }
  };

  const onTrimesterPress = (): void => {
    analytics().logEvent('maternity_overview_trimester_progress_click');
    setParentActiveItem ? setParentActiveItem('trimester') : null;
  };

  const renderBodyContent = (): ReactNode => {
    if (!me || !maternityOverviewContent) return null;

    const { maternityProfile } = me;
    const { gestationalAge = {}, role } = maternityProfile || {};

    if (role === USER_ROLE.PREGNANT) {
      return (
        <>
          <Container {...listItemStyle} mt={0}>
            <Banner backgroundColor={colors.bouquet}>
              <Touchable fill fullWidth flexDirection="row" onPress={onBannerPress} centerContent>
                <Text color={colors.white} fontSize={4} fontWeight="700" mx={2}>
                  {t('maternity.overview.preRegistration')}
                </Text>
              </Touchable>
            </Banner>
          </Container>
          {/* {renderFeaturedArticlesSection()} */}
          {renderArticlesSection()}
          <Container {...listItemStyle}>
            <Touchable onPress={onTrimesterPress}>
              <TrimesterProgress gestationalAge={gestationalAge} />
            </Touchable>
          </Container>
          <Container {...listItemStyle}>
            <Banner backgroundColor={colors.bouquet}>
              <Touchable fill fullWidth flexDirection="row" onPress={onPhonePress} centerContent>
                <Container borderRadius={15} bg={colors.strikemaster} p={2} mx={2}>
                  <Icon name="phone-in-talk" type="material-icons" color={colors.white} size={35} />
                </Container>
                <Text color={colors.white} fontSize={4} fontWeight="700" mx={2}>
                  {t('maternity.overview.babyline')}
                </Text>
              </Touchable>
            </Banner>
          </Container>
          {renderPromoContentSection()}
          {renderEventsSection()}
        </>
      );
    }

    return <Text>Coming Soon....</Text>;
  };

  if (webUrlsError || meError || maternityOverviewContentError) {
    <ContentErrorToast />;
  }

  const contentHasLoaded = maternityOverviewContent && Object.keys(maternityOverviewContent).length;
  const isLoading = meLoading || maternityOverviewContentLoading;

  return (
    <SubNavLayout
      isLoading={isLoading && !contentHasLoaded}
      title={title}
      hideTitle={hideTitle}
      isActive={isActive}
      testId={`${title}-layout`}
      refreshControl={
        <RefreshControl
          refreshing={maternityOverviewContentLoading}
          onRefresh={refetchMaternityOverviewContent}
        />
      }
    >
      {renderBodyContent()}
    </SubNavLayout>
  );
};

export default withNavigation(OverviewLayout);

import React, { FC, ReactElement } from 'react';
import { RefreshControl } from 'react-native';
import { withNavigation } from 'react-navigation';

import { SubNavLayoutProps, SubNavLayout } from '../../../components/SubNavLayout';
import { Banner } from '../../../components/Banner';
import { Text } from '../../../components/Text';
import { Touchable } from '../../../components/Touchable';
import { ContentCard } from '../../../components/Card';
import { Container } from '../../../components/Container';
import { colors } from '../../../styles';
import FaqIcon from '../../../assets/images/maternity/faqs.svg';
import { useMaternityPromoContentQuery, Content } from '../../../graphql/types';
import { ContentErrorToast } from '../../../components/Toastr';

/**
 * Notes layout for maternity
 */
export const MoreLayout: FC<SubNavLayoutProps> = ({
  title,
  hideTitle = false,
  isActive,
  navigation,
}) => {
  const {
    data: promoContentData,
    loading: promoContentLoading,
    refetch: refetchPromoContent,
    error: promoContentError,
  } = useMaternityPromoContentQuery({
    fetchPolicy: 'cache-and-network',
  });

  const navigateToPromoDetailsScreen = (content: Content): void => {
    navigation.navigate('PromoDetailsModal', { postId: content.id });
  };

  const renderNicuFaq = (): ReactElement => {
    return (
      <Container pb={3}>
        <Banner backgroundColor={colors.cornflowerBlue3}>
          <Touchable
            fill
            fullWidth
            flexDirection="row"
            onPress={(): boolean => navigation.navigate('NicuFaq')}
            centerContent
          >
            <Container borderRadius={15} bg={colors.royalBlue} p={2} mx={2}>
              <FaqIcon height="35" width="35" />
            </Container>
            <Text color={colors.white} fontSize={4} fontWeight="700" mx={2}>
              NICU FAQs
            </Text>
          </Touchable>
        </Banner>
      </Container>
    );
  };

  const renderPromoContentCard = (content: Content | null): ReactElement | null => {
    if (!content) return null;

    const { id, title: postTitle, description, mediumImage } = content;

    return (
      <Container key={id} pb={2}>
        <ContentCard
          id={id}
          heading={postTitle}
          text={description}
          cardLabelBackgroundColor={colors.vistaBlue}
          imageSrc={mediumImage}
          onPress={(): void => navigateToPromoDetailsScreen(content)}
        />
      </Container>
    );
  };

  const renderPromoContent = (): ReactElement | null => {
    if (!promoContentData || !Object.keys(promoContentData)) return null;

    const { content } = promoContentData;

    if (!content || !content.length) return null;

    return <>{content.map(renderPromoContentCard)}</>;
  };

  if (promoContentError) {
    <ContentErrorToast />;
  }

  const refetchContent = (): void => {
    refetchPromoContent();
  };

  const promoContentLoaded = promoContentData && Object.keys(promoContentData).length;
  const isLoading = promoContentData;

  return (
    <SubNavLayout
      title={title}
      hideTitle={hideTitle}
      isActive={isActive}
      isLoading={isLoading && !promoContentLoaded}
      refreshControl={
        <RefreshControl refreshing={promoContentLoading} onRefresh={refetchContent} />
      }
    >
      {renderNicuFaq()}
      {renderPromoContent()}
    </SubNavLayout>
  );
};

export default withNavigation(MoreLayout);

import React, { FC, ReactElement, useCallback, useEffect, useState } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import analytics from '@react-native-firebase/analytics';
import { HorizontalPicker } from '../../components/HorizontalPicker';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';

import { colors } from '../../styles';
import { USER_ROLE } from '../../utils/maternityOnboarding';
import { SubNavLayoutProps } from '../../components/SubNavLayout';
import AppointmentsIcon from '../../assets/images/maternity/appointments.svg';
import GuidesIcon from '../../assets/images/maternity/guides.svg';
import MoreIcon from '../../assets/images/maternity/more.svg';
import OverviewIcon from '../../assets/images/maternity/overview.svg';
import ToDoIcon from '../../assets/images/maternity/todo.svg';
import TrackingIcon from '../../assets/images/maternity/tracking.svg';
import TrimesterIcon from '../../assets/images/maternity/trimester.svg';
// import PlanningIcon from '../../assets/images/maternity/planning.svg';
import NotesIcon from '../../assets/images/maternity/notes.svg';
import BabySizeIcon from '../../assets/images/maternity/baby-size.svg';
import BirthPlanIcon from '../../assets/images/maternity/planning.svg';

import { useMeQuery } from '../../graphql/types';
import {
  AppointmentLayout,
  BabySizeLayout,
  BirthPlanLayout,
  MoreLayout,
  NotesLayout,
  OverviewLayout,
  ResourcesLayout,
  ToDoLayout,
  TrackingLayout,
  TrimesterLayout,
} from './Layouts';

interface SubNavItem {
  id: string;
  name: string;
  hideTitle?: boolean;
  Layout?: FC<SubNavLayoutProps>;
  Icon?: ReactElement;
}

export const MATERNITY_SECTIONS: {
  PRENATAL: SubNavItem[];
  POSTNATAL: SubNavItem[];
  UNKNOWN: SubNavItem[];
} = {
  UNKNOWN: [
    {
      id: 'resources',
      name: 'Resources',
      hideTitle: true,
      Layout: ResourcesLayout,
      Icon: OverviewIcon,
    },
  ],
  PRENATAL: [
    {
      id: 'overview',
      name: 'Overview',
      hideTitle: true,
      Layout: OverviewLayout,
      Icon: OverviewIcon,
    },
    {
      id: 'trimester',
      name: 'Trimester',
      hideTitle: true,
      Layout: TrimesterLayout,
      Icon: TrimesterIcon,
    },
    {
      id: 'todo',
      name: 'To-Do',
      hideTitle: true,
      Layout: ToDoLayout,
      Icon: ToDoIcon,
    },
    {
      id: 'baby-size',
      name: 'Baby Size',
      hideTitle: true,
      Layout: BabySizeLayout,
      Icon: BabySizeIcon,
    },
    {
      id: 'tracking',
      name: 'Tracking',
      hideTitle: true,
      Layout: TrackingLayout,
      Icon: TrackingIcon,
    },
    {
      id: 'notes',
      name: 'Notes',
      hideTitle: true,
      Layout: NotesLayout,
      Icon: NotesIcon,
    },
    // {
    //   id: 'appointments',
    //   name: 'Appts.',
    //   Layout: AppointmentLayout,
    //   Icon: AppointmentsIcon,
    // },
    {
      id: 'birthplan',
      name: 'Birth Plan',
      hideTitle: true,
      Layout: BirthPlanLayout,
      Icon: BirthPlanIcon,
    },
    {
      id: 'more',
      name: 'More',
      Layout: MoreLayout,
      Icon: MoreIcon,
      hideTitle: true,
    },
  ],
  POSTNATAL: [
    {
      id: 'overview',
      name: 'Overview',
      hideTitle: true,
      Layout: OverviewLayout,
      Icon: OverviewIcon,
    },
    {
      id: 'guides',
      name: 'Guides',
      Icon: GuidesIcon,
    },
    {
      id: 'appointments',
      name: 'Appts.',
      Layout: AppointmentLayout,
      Icon: AppointmentsIcon,
    },
    {
      id: 'more',
      name: 'More',
      Layout: MoreLayout,
      Icon: MoreIcon,
      hideTitle: true,
    },
  ],
};

export const MaternityScreen: FC<NavigationTabScreenProps> = () => {
  const { data, loading } = useMeQuery({
    fetchPolicy: 'cache-and-network',
  });

  const user = data || {};
  const { me } = user;
  const { maternityProfile } = me;

  const [activeSectionId, setActiveSectionId] = useState('overview');
  const [loaded, setLoaded] = useState(false);
  const [currentRole, setCurrentRole] = useState(USER_ROLE.NEW_PARENT);

  const getItemsFromRole = (): SubNavItem[] => {
    if (currentRole === USER_ROLE.PREGNANT) return MATERNITY_SECTIONS.PRENATAL;
    if (currentRole === USER_ROLE.NEW_PARENT) return MATERNITY_SECTIONS.POSTNATAL;

    return MATERNITY_SECTIONS.UNKNOWN;
  };

  const menuItems = getItemsFromRole();
  //currentRole === USER_ROLE.PREGNANT ? MATERNITY_SECTIONS.PRENATAL : MATERNITY_SECTIONS.POSTNATAL;

  const updateUserRole = useCallback(async (): Promise<void> => {
    await setCurrentRole(maternityProfile.role);
    //reset the active tab
    const activeTab = maternityProfile.role === null ? 'resources' : 'overview';
    setActiveSectionId(activeTab);
  }, [maternityProfile.role]);

  //This will watch for a change in maternity role, when this occurs it needs to update the subnav menu items correctly
  useEffect(() => {
    if (currentRole !== maternityProfile.role) {
      updateUserRole();
    }
  }, [currentRole, maternityProfile.role, updateUserRole]);

  const renderSectionPicker = (): ReactElement => {
    if (me?.maternityProfile && !loading && !loaded) {
      const { role } = maternityProfile;

      setCurrentRole(role);
      if (role == null) setActiveSectionId('resources');
      setLoaded(true);
    }

    return (
      <HorizontalPicker
        items={menuItems}
        activeId={activeSectionId}
        onPress={(): void => {
          analytics().logEvent('maternity_nav_tab_press', { tab: activeSectionId });
          setActiveSectionId(activeSectionId);
        }}
      />
    );
  };

  const renderSections = (): ReactElement | null => {
    /**
     * Renders the Layout Component from the SECTIONS
     */
    return (
      <>
        {menuItems.map(item => {
          const { Layout, id, name, hideTitle } = item;

          if (Layout) {
            return (
              <Layout
                key={id}
                title={name}
                isActive={activeSectionId === id}
                hideTitle={hideTitle || false}
                setParentActiveItem={setActiveSectionId} //TODO revist this for better solution (should be done with true nav likely)
              />
            );
          }

          return null;
        })}
      </>
    );
  };

  const renderMaternityScreenContent = (): ReactElement => {
    return (
      <Container fill fullWidth pt={5}>
        {renderSectionPicker()}
        {renderSections()}
      </Container>
    );
  };

  return (
    <Screen
      testID="maternity-screen"
      activeModule="maternity"
      moduleHeader
      backgroundColor={colors.pearlLusta}
      fill
      height="100%"
      paddingTop={0}
    >
      {renderMaternityScreenContent()}
    </Screen>
  );
};

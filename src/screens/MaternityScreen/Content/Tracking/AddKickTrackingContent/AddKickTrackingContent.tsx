import React, { FC, ReactElement, useState, ReactNode, useEffect } from 'react';
import dayjs from 'dayjs';
import { Icon } from 'react-native-elements';
import { ApolloQueryResult } from 'apollo-boost';
import Toast from 'react-native-root-toast';
import analytics from '@react-native-firebase/analytics';
import { Container } from '../../../../../components/Container';
import { colors } from '../../../../../styles';
import { Text } from '../../../../../components/Text';
import { Touchable } from '../../../../../components/Touchable';
import {
  MyKickTrackerQueryVariables,
  MyKickTrackerQuery,
  useCreateMyKickCounterMutation,
  useEditMyKickCounterMutation,
  KickCounter,
} from '../../../../../graphql/types';
import { t } from '../../../../../utils';
import FeetIcon from '../../../../../assets/images/maternity/feet.svg';

interface MyKickTrackingProps {
  onRefresh: (
    variables?: MyKickTrackerQueryVariables | undefined
  ) => Promise<ApolloQueryResult<MyKickTrackerQuery>>;
  counterSession: KickCounter | undefined;
}

let intervalVal;

export const AddKickTrackingContent: FC<MyKickTrackingProps> = ({ counterSession, onRefresh }) => {
  const [kicks, setKicks] = useState(0);
  const [sessionId, setSessionId] = useState('');
  const [startAt, setStartAt] = useState('');
  const [duration, setDuration] = useState(0);

  const [createKickCounter] = useCreateMyKickCounterMutation();
  const [updateKickCounter] = useEditMyKickCounterMutation();

  const showToastView = (message: string): void => {
    Toast.show(message, {
      duration: 5000,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  };

  const getDurationFromSeconds = (): string => {
    let seconds = '00';
    let minutes = '00';
    let hours = '00';

    if (duration < 60) {
      seconds = duration.toString();
    } else {
      minutes = Math.floor(duration / 60).toString();
      const tempMin = Number(minutes);

      if (tempMin >= 60) {
        hours = Math.floor(tempMin / 60).toString();
        minutes = (tempMin % 60).toString();
      }

      seconds = (duration % 60).toString();
    }

    if (seconds.length === 1) {
      seconds = '0' + seconds;
    }

    if (minutes.length === 1) {
      minutes = '0' + minutes;
    }

    return hours + ':' + minutes + ':' + seconds;
  };

  const resetCounterSession = (): void => {
    setSessionId('');
    setDuration(0);
    setKicks(0);
    setStartAt('');
  };

  const updateDuration = (recordedAt: string | undefined): void => {
    if (intervalVal) {
      clearInterval(intervalVal);
      intervalVal = null;
    }

    intervalVal = setInterval(() => {
      const durationDiff = dayjs(new Date()).diff(new Date(recordedAt || ''), 's');
      setDuration(durationDiff);

      if (durationDiff >= 3600) {
        clearInterval(intervalVal);
        intervalVal = null;
        resetCounterSession();
      }
    }, 1000);
  };

  const setSessionCounter = (session: KickCounter): void => {
    setSessionId(session.id);
    setKicks(Number(session.kicks));
    setStartAt(session.startAt);
    updateDuration(session.startAt);
  };

  const handleStartCounterSessionPress = (): void => {
    analytics().logEvent('add_to_kick_count');

    const kickCount = kicks + 1;
    setKicks(kickCount);

    if (sessionId) {
      updateKickCounter({
        variables: {
          id: sessionId,
          kicks: kickCount,
        },
      }).then(() => {
        onRefresh();
      });
    } else {
      const timesstamp = new Date().toString();

      setStartAt(timesstamp);
      createKickCounter({
        variables: {
          startAt: timesstamp,
          kicks: 1,
          duration: 0,
          isActive: true,
        },
      })
        .then(res => {
          const id = res?.data?.createMyKickCounter?.id;

          if (id) {
            updateDuration(res.data?.createMyKickCounter?.startAt);
            setSessionId(id);
          }

          onRefresh();
        })
        .catch(() => {
          showToastView(t('maternity.tracking.error.startCounter'));
        });
    }
  };

  const handleStopCounterSessionPress = (): void => {
    updateKickCounter({
      variables: {
        id: sessionId,
        duration: Number(duration),
        isActive: false,
      },
    }).then(() => {
      clearInterval(intervalVal);
      intervalVal = null;
      resetCounterSession();
      onRefresh();
    });
  };

  useEffect(() => {
    if (counterSession) {
      setSessionCounter(counterSession);
    }

    return (): void => {
      clearInterval(intervalVal);
      intervalVal = null;
    };

    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (duration === 3600) {
      updateKickCounter({
        variables: {
          id: sessionId,
          duration: 3600,
        },
      }).then(() => {
        onRefresh();
      });
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [duration]);

  const renderSessionCounterDetails = (label: string, value: string | number): ReactElement => {
    const labelKey = 'maternity.tracking.table.header.' + label;
    return (
      <Container display="flex" alignItems="center" flex={0.3}>
        <Text color={colors.gray} fontSize={1}>
          {t(labelKey)}
        </Text>
        <Text fontSize={2} fontWeight="500">
          {value}
        </Text>
      </Container>
    );
  };

  const renderSessionCounterContent = (): ReactNode => {
    if (!sessionId) {
      return null;
    }

    const sessionStartedAt = dayjs(startAt).format('hh:mm a');
    const durationOfSession = getDurationFromSeconds();

    return (
      <Container px={2} height={50} alignItems="center" justifyContent="center">
        <Container
          px={6}
          py={1}
          bg={colors.offYellow}
          display="flex"
          borderRadius={13}
          flexDirection="row"
        >
          {renderSessionCounterDetails('start', sessionStartedAt)}
          {renderSessionCounterDetails('kicks', kicks)}
          {renderSessionCounterDetails('duration', durationOfSession)}
        </Container>
      </Container>
    );
  };

  const renderCounterButton = (): ReactElement => {
    const startButton = (
      <Container flex={1} alignItems="center" justifyContent="center">
        <Touchable
          backgroundColor="#99e8e8"
          testID="kick-counter-icon"
          onPress={handleStartCounterSessionPress}
          height={100}
          width={100}
          borderRadius={100}
          borderColor="#7ac6c5"
          borderWidth={8}
          zIndex={50}
          justifyContent="center"
          alignItems="center"
        >
          <FeetIcon width="70%" height="70%" />
        </Touchable>

        <Text pt={2} color={colors.turquoiseBlue} fontWeight="500" fontSize={1}>
          {sessionId
            ? t('maternity.tracking.buttons.kickCounterAdd')
            : t('maternity.tracking.buttons.kickCounterStart')}
        </Text>
      </Container>
    );

    const stopButton = (
      <Container flex={1} alignItems="center" justifyContent="center">
        <Touchable
          backgroundColor={colors.vistaBlue}
          testID="kick-counter-stop-icon"
          onPress={handleStopCounterSessionPress}
          height={100}
          width={100}
          borderRadius={100}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={100} />
        </Touchable>

        <Text pt={2} color={colors.trimesterActive} fontWeight="500" fontSize={1}>
          {t('maternity.tracking.buttons.kickCounterStop')}
        </Text>
      </Container>
    );

    return (
      <Container flex={1} flexDirection="row">
        {startButton}
        {!!sessionId && stopButton}
      </Container>
    );
  };

  return (
    <Container borderBottomWidth={1} borderBottomColor={colors.solitude} height={200} fullWidth>
      {renderSessionCounterContent()}
      {renderCounterButton()}
    </Container>
  );
};

export default AddKickTrackingContent;

import React, { FC, ReactElement, useState } from 'react';
import { Icon } from 'react-native-elements';
import { FlatList, View, RefreshControl, Alert } from 'react-native';
import Swipeable from 'react-native-swipeable';
import dayjs from 'dayjs';

import { ApolloQueryResult } from 'apollo-boost';
import Toast from 'react-native-root-toast';
import { SwipeButtonLayout } from '../../../Layouts';
import { Container } from '../../../../../components/Container';
import { Text } from '../../../../../components/Text';
import { colors } from '../../../../../styles';
import { Touchable } from '../../../../../components/Touchable';
import { SCREEN_WIDTH, SCREEN_WIDTH_RATIO, t } from '../../../../../utils';
import { SubNavLayoutProps } from '../../../../../components/SubNavLayout';
import {
  useDeleteMyKickCounterMutation,
  MyKickTrackerQuery,
  MyKickTrackerQueryVariables,
} from '../../../../../graphql/types';

interface MyKickTrackingListProps {
  trackingData: MyKickTrackerQuery | undefined;
  isRefreshing: boolean;
  isCounterSession: boolean;
  sessionId: string;
  onRefresh: (
    variables?: MyKickTrackerQueryVariables | undefined
  ) => Promise<ApolloQueryResult<MyKickTrackerQuery>>;
}

export const KickTrackingListContent: FC<MyKickTrackingListProps & SubNavLayoutProps> = ({
  sessionId,
  navigation,
  trackingData,
  isRefreshing,
  isCounterSession,
  onRefresh,
}) => {
  const [currentSwipeable, setCurrentSwipeable] = useState(null);

  const [deleteMyKickCounter] = useDeleteMyKickCounterMutation();

  const showToastView = (message: string): void => {
    Toast.show(message, {
      duration: 5000,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  };

  const handleRecenterSwipable = (): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }
  };

  const handleDeleteRow = (id: string): void => {
    deleteMyKickCounter({
      variables: {
        id,
      },
    })
      .then(() => {
        onRefresh();
      })
      .catch(() => {
        showToastView('Sorry, unable to delete this kick counter. Please try again!!');
      });
  };

  const handleCancelDeleteRow = (): void => {
    handleRecenterSwipable();
  };

  const onDeleteRowPress = (id: string): void => {
    if (id === sessionId) {
      showToastView(t('maternity.tracking.alert.delete.notAllowedOnCurrentSession'));
      handleRecenterSwipable();
    } else {
      Alert.alert(
        t('maternity.tracking.alert.delete.confirmation.title'),
        t('maternity.tracking.alert.delete.confirmation.description'),
        [
          {
            text: t('maternity.tracking.buttons.cancel'),
            onPress: (): void => handleCancelDeleteRow(),
            style: 'cancel',
          },
          {
            text: t('maternity.tracking.buttons.delete'),
            onPress: (): Promise<void> => handleDeleteRow(id),
          },
        ],
        { cancelable: false }
      );
    }
  };

  const navigateToScreen = (id: string, name: string, item, isEditMode?: boolean): void => {
    handleRecenterSwipable();

    navigation.navigate(name, {
      id,
      currentKickTracking: item,
      onRefresh,
      isEditMode,
    });
  };

  const onOpenSwipeable = (event, gestureState, swipeable): void => {
    if (currentSwipeable && currentSwipeable !== swipeable) {
      handleRecenterSwipable();
    }

    setCurrentSwipeable(swipeable);
  };

  const onCloseSwipeable = (): void => {
    setCurrentSwipeable(null);
  };

  const handleAddOutsideEntryPress = (): void => {
    handleRecenterSwipable();

    if (isCounterSession) {
      showToastView(t('maternity.tracking.alert.manualAdd.alreadyActiveSession'));
    } else {
      navigateToScreen('', 'KickTrackingEditScreen', {});
    }
  };

  const onEditPress = (item): void => {
    if (item.id === sessionId) {
      showToastView(t('maternity.tracking.alert.edit.notAllowedOnCurrentSession'));
      handleRecenterSwipable();
    } else {
      navigateToScreen(item.id, 'KickTrackingEditScreen', item, true);
    }
  };

  const getDurationFromMinutes = (duration: number): string => {
    let seconds = '00';
    let minutes = '00';
    let hours = '00';

    if (duration < 60) {
      seconds = duration.toString();
    } else {
      minutes = Math.floor(duration / 60).toString();
      const tempMin = Number(minutes);

      if (tempMin >= 60) {
        hours = Math.floor(tempMin / 60).toString();
        minutes = (tempMin % 60).toString();
      }

      seconds = (duration % 60).toString();
    }

    if (minutes.length === 1) {
      minutes = '0' + minutes;
    }

    if (hours.length === 1) {
      hours = '0' + hours;
    }

    if (seconds.length === 1) {
      seconds = '0' + seconds;
    }

    const durationOfSession = hours + ':' + minutes + ':' + seconds;
    return durationOfSession;
  };

  const renderLabel = (label: string): ReactElement => {
    const labelKey = 'maternity.tracking.table.header.' + label;

    return (
      <Container display="flex" alignItems="center" justifyContent="center" flex={0.3}>
        <Text color={colors.gray} fontSize={1}>
          {t(labelKey)}
        </Text>
      </Container>
    );
  };

  const renderHeaderContent = (): ReactElement => {
    return (
      <Container>
        <View
          style={{
            shadowColor: colors.solitude,
            shadowOffset: { width: 0, height: 10 },
            shadowOpacity: 0.35,
            marginBottom: 7,
            elevation: 5,
            backgroundColor: colors.solitude,
            width: '100%',
          }}
        >
          <Container bg={colors.white} height={35} flexDirection="row" justifyContent="center">
            {renderLabel('dateTime')}
            {renderLabel('kicks')}
            {renderLabel('duration')}
          </Container>
        </View>
      </Container>
    );
  };

  const renderTrackOutsideEntry = (): ReactElement => {
    return (
      <Touchable
        backgroundColor={colors.white}
        onPress={handleAddOutsideEntryPress}
        height={35}
        justifyContent="center"
        alignItems="center"
      >
        <Text fontSize={1} color={colors.skyBlue}>
          {t('maternity.tracking.buttons.addOutsideEntry')}
        </Text>
      </Touchable>
    );
  };

  const renderField = (value: string | number): ReactElement => {
    return (
      <Container display="flex" alignItems="center" justifyContent="center" flex={0.3}>
        <Text fontSize={2} fontWeight="500">
          {value}
        </Text>
      </Container>
    );
  };

  const renderItem = ({ item }): ReactElement => {
    const recordedAt = item.startAt ? dayjs(item.startAt).format('MMM DD - hh:mm a') : '';

    const editButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-create"
        bgColor={colors.tangerineYellow}
        onPress={(): void => onEditPress(item)}
        rowHeight={50}
      />
    );

    const deleteButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-trash"
        bgColor={colors.cinnabar}
        onPress={(): void => onDeleteRowPress(item.id)}
        rowHeight={50}
      />
    );

    const swipeContainerRightButtons = [editButton, deleteButton];
    const durationOfSession = getDurationFromMinutes(item.duration);

    return (
      <Swipeable
        rightButtons={swipeContainerRightButtons}
        onRightButtonsOpenRelease={onOpenSwipeable}
        onRightButtonsCloseRelease={onCloseSwipeable}
      >
        <Container flexDirection="row" justifyContent="center" py={2} height={50}>
          {renderField(recordedAt)}
          {renderField(item?.kicks || '')}
          {renderField(durationOfSession)}
        </Container>
      </Swipeable>
    );
  };

  const renderListContent = (): ReactElement => {
    const kickTracking = trackingData?.myKickTracker || [];

    return (
      <FlatList
        contentContainerStyle={{
          display: 'flex',
          backgroundColor: colors.solitudeLight,
        }}
        data={kickTracking}
        renderItem={renderItem}
        refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />}
        ItemSeparatorComponent={(): ReactElement => (
          <Container alignItems="center" fullWidth bg={colors.solitudeLight}>
            <Container bg={colors.white} width={SCREEN_WIDTH * SCREEN_WIDTH_RATIO} height={1} />
          </Container>
        )}
      />
    );
  };

  return (
    <Container bg={colors.white}>
      {renderHeaderContent()}
      {renderTrackOutsideEntry()}
      {renderListContent()}
    </Container>
  );
};

export default KickTrackingListContent;

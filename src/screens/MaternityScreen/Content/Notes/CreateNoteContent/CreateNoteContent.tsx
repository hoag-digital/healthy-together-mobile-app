import React, { FC, ReactElement, useState } from 'react';
import dayjs from 'dayjs';
import { ScrollView } from 'react-native-gesture-handler';
import { ApolloQueryResult } from 'apollo-boost';
import Toast from 'react-native-root-toast';

import { Container } from '../../../../../components/Container';
import { Text } from '../../../../../components/Text';
import { colors } from '../../../../../styles';
import { TextInput } from '../../../../../components/TextInput';
import { SCREEN_HEIGHT, t } from '../../../../../utils';
import { Button } from '../../../../../components/Button';
import {
  useCreateMyNoteMutation,
  MyNotesQueryVariables,
  MyNotesQuery,
} from '../../../../../graphql/types';

interface MyNotesListProps {
  onRefresh: (
    variables?: MyNotesQueryVariables | undefined
  ) => Promise<ApolloQueryResult<MyNotesQuery>>;
}

export const CreateNoteContent: FC<MyNotesListProps> = ({ onRefresh }) => {
  const [note, setNote] = useState('');

  const [createMyNote, { loading: isSavingMyNote }] = useCreateMyNoteMutation();

  const showToastView = (message: string, bgColor: string): void => {
    Toast.show(message, {
      duration: 5000,
      position: Toast.positions.CENTER,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: bgColor,
    });
  };

  const handleChange = (value): void => {
    setNote(value);
  };

  const handleSave = async (): Promise<void> => {
    if (note) {
      try {
        await createMyNote({
          variables: {
            contents: note.trim(),
          },
        });

        setNote('');
        onRefresh();
        showToastView('Note saved successfully!!', colors.vistaBlue);
      } catch (_error) {
        showToastView(
          'An error has occured, please try again to save the note!!',
          colors.deepBlush
        );
      }
    }
  };

  const renderHeaderContent = (): ReactElement => {
    const currentDate = dayjs().format('MMMM D');

    return (
      <Container
        display="flex"
        height={40}
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        mt={1}
      >
        <Container bg={colors.tangerineYellow} height={3} width={70} />
        <Container bg={colors.white} height={30} p={1} m={2}>
          <Text fontWeight="700">{currentDate}</Text>
        </Container>
        <Container bg={colors.tangerineYellow} height={3} width={70} />
      </Container>
    );
  };

  const renderNoteContent = (): ReactElement => {
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        contentContainerStyle={{
          flexGrow: 1,
        }}
      >
        <Container height={160}>
          <TextInput
            placeholder={t('maternity.notes.createNote.placeholder')}
            borderColor={colors.white}
            value={note}
            onChangeText={handleChange}
            multiline
            fontSize={3}
            letterSpacing={0.2}
          />
        </Container>
      </ScrollView>
    );
  };

  const renderFooterContent = (): ReactElement => {
    const saveBtnLabel =
      isSavingMyNote && !!note
        ? 'maternity.notes.createNote.savingLabel'
        : 'maternity.notes.createNote.saveLabel';

    return (
      <Container height={60} mb={3} justifyContent="center" alignItems="center">
        <Button
          disabled={isSavingMyNote}
          label={t(saveBtnLabel)}
          backgroundColor={colors.trimesterActive}
          color={colors.white}
          borderRadius={20}
          width={100}
          height={43}
          onPress={handleSave}
        />
      </Container>
    );
  };

  return (
    <Container
      borderBottomWidth={1}
      borderBottomColor={colors.solitude}
      height={SCREEN_HEIGHT * 0.3}
      flex={3}
      fullWidth
    >
      {renderHeaderContent()}
      {renderNoteContent()}
      {renderFooterContent()}
    </Container>
  );
};

export default CreateNoteContent;

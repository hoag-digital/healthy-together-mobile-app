import React, { FC, ReactElement, useState } from 'react';
import { Icon } from 'react-native-elements';
import { FlatList, View, RefreshControl, Alert } from 'react-native';
import Swipeable from 'react-native-swipeable';
import dayjs from 'dayjs';

import { ApolloQueryResult } from 'apollo-boost';
import { SwipeButtonLayout } from '../../../Layouts';
import { Container } from '../../../../../components/Container';
import { Text } from '../../../../../components/Text';
import { colors } from '../../../../../styles';
import { Touchable } from '../../../../../components/Touchable';
import {
  LARGER_ICON_SIZE,
  DEFAULT_ICON_SIZE,
  DEFAULT_CONTENT_DESCRIPTION_LENGTH,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_WIDTH,
  SCREEN_HEIGHT,
  SCREEN_HEIGHT_RATIO,
  SCREEN_WIDTH_RATIO,
  t,
} from '../../../../../utils';
import { SubNavLayoutProps } from '../../../../../components/SubNavLayout';
import { trimText } from '../../../../../utils/trimText';
import {
  useDeleteMyNoteMutation,
  MyNotesQuery,
  MyNotesQueryVariables,
  Note,
} from '../../../../../graphql/types';

interface MyNotesListProps {
  myNotesData: MyNotesQuery | undefined;
  isRefreshing: boolean;
  onRefresh: (
    variables?: MyNotesQueryVariables | undefined
  ) => Promise<ApolloQueryResult<MyNotesQuery>>;
}

export const NotesListContent: FC<SubNavLayoutProps & MyNotesListProps> = ({
  navigation,
  myNotesData,
  isRefreshing,
  onRefresh,
}) => {
  const [currentSwipeable, setCurrentSwipeable] = useState(null);

  const [deleteMyNote, { error: deleteMyNoteError }] = useDeleteMyNoteMutation();

  if (deleteMyNoteError) {
    //TODO: handle graphql error
  }

  const navigateToListScreen = (): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }

    navigation.navigate('NoteListScreen', {
      onRefresh,
    });
  };

  const handleDeleteRow = async (id: string): Promise<void> => {
    await deleteMyNote({
      variables: {
        id,
      },
    }).then(() => {
      onRefresh({ limit: 5 });
    });
  };

  const handleCancelDeleteRow = (): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }
  };

  const onDeleteRowPress = (id: string): void => {
    Alert.alert(
      'Delete Note',
      'Are you sure you want to delete?',
      [
        { text: 'Cancel', onPress: (): void => handleCancelDeleteRow(), style: 'cancel' },
        { text: 'Delete', onPress: (): void => handleDeleteRow(id) },
      ],
      { cancelable: false }
    );
  };

  const onOpenSwipeable = (event, gestureState, swipeable): void => {
    if (currentSwipeable && currentSwipeable !== swipeable) {
      currentSwipeable.recenter();
    }

    setCurrentSwipeable(swipeable);
  };

  const onCloseSwipeable = (): void => {
    setCurrentSwipeable(null);
  };

  const navigateToScreen = (noteId: string, name: string, item?: Note): void => {
    if (currentSwipeable) {
      currentSwipeable.recenter();
    }

    navigation.navigate(name, {
      noteId,
      myCurrentNote: item,
      onRefresh,
    });
  };

  const onEditNotePress = (item: Note): void => {
    navigateToScreen(item.id, 'NoteEditScreen', item);
  };

  const renderHeaderContent = (): ReactElement => {
    return (
      <Container>
        <View
          style={{
            shadowColor: colors.solitude,
            shadowOffset: { width: 0, height: 10 },
            shadowOpacity: 0.6,
            marginBottom: 7,
            elevation: 5,
            backgroundColor: colors.solitude,
            width: '100%',
          }}
        >
          <Container bg={colors.white} height={50} flexDirection="row" justifyContent="center">
            <Container flex={0.5} pl={4} justifyContent="center">
              <Text>{t('maternity.notes.listView.contentTitle')}</Text>
            </Container>
            <Container flex={0.5} alignItems="flex-end" justifyContent="center">
              <Touchable
                padding={2}
                height={42}
                width={80}
                onPress={navigateToListScreen}
                justifyContent="center"
              >
                <Text color={colors.trimesterActive}>{t('maternity.notes.listView.viewAll')}</Text>
              </Touchable>
            </Container>
          </Container>
        </View>
      </Container>
    );
  };

  const renderItem = ({ item }): ReactElement => {
    const createdAt = item.createdAt ? dayjs(Number(item.createdAt)).format('MMMM DD YYYY') : '';

    const editButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-create"
        bgColor={colors.tangerineYellow}
        onPress={(itemData: any): void => onEditNotePress(itemData)}
        rowHeight={50}
      />
    );

    const deleteButton = (
      <SwipeButtonLayout
        noteId={item.id}
        name="ios-trash"
        bgColor={colors.cinnabar}
        onPress={(itemData: any): void => onDeleteRowPress(itemData)}
        rowHeight={50}
      />
    );

    const swipeContainerRightButtons = [editButton, deleteButton];

    return (
      <Swipeable
        rightButtons={swipeContainerRightButtons}
        onRightButtonsOpenRelease={onOpenSwipeable}
        onRightButtonsCloseRelease={onCloseSwipeable}
      >
        <Touchable
          key={item.id}
          backgroundColor={colors.solitudeLight}
          onPress={(): void => {
            navigateToScreen(item.id, 'NoteDetailsModal');
          }}
        >
          <Container flexDirection="row" justifyContent="center" py={2} height={55}>
            <Container flex={0.9} pl={5} justifyContent="center">
              <Text>{createdAt}</Text>
              <Text>
                {item?.contents ? trimText(item?.contents, DEFAULT_CONTENT_DESCRIPTION_LENGTH) : ''}
              </Text>
            </Container>
            <Container flex={0.1} pr={3} justifyContent="center">
              <Icon name="chevron-right" size={LARGER_ICON_SIZE} />
            </Container>
          </Container>
        </Touchable>
      </Swipeable>
    );
  };

  const renderListContent = (): ReactElement => {
    const notes = myNotesData?.myNotes || [];

    return (
      <FlatList
        contentContainerStyle={{
          backgroundColor: colors.solitudeLight,
          paddingBottom: SCROLLVIEW_BOTTOM_PADDING,
        }}
        data={notes}
        renderItem={renderItem}
        refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />}
        ItemSeparatorComponent={(): ReactElement => (
          <Container alignItems="center" fullWidth bg={colors.solitudeLight}>
            <Container bg={colors.white} width={SCREEN_WIDTH * SCREEN_WIDTH_RATIO} height={1} />
          </Container>
        )}
      />
    );
  };

  return (
    <Container height={SCREEN_HEIGHT * SCREEN_HEIGHT_RATIO} bg={colors.solitudeLight}>
      {renderHeaderContent()}
      {renderListContent()}
    </Container>
  );
};

export default NotesListContent;

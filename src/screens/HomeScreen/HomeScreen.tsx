import React, { FC, ReactElement, ReactNode, useState, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import analytics from '@react-native-firebase/analytics';
import { OnDemandEventCard, EventGroupCard, ContentCard } from '../../components/Card';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import {
  Post,
  EventGroup,
  Event,
  useHomeScreenContentQuery,
  useMeQuery,
} from '../../graphql/types';
import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import GenericNewsImage from '../../assets/images/generic_news.jpg';
import { ContentErrorToast } from '../../components/Toastr';
import { MaternityStatus } from '../../components/MaternityStatus';
import AuthUtils, { LoggedInContent } from '../../lib/AuthUtils';
import { Touchable } from '../../components/Touchable';

export const HomeScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const navigateToPostDetailsScreen = (post: Post): void => {
    navigation.navigate('PostDetails', { postId: post.id });
  };

  const navigateToClassDetailsScreen = (eventGroupName?: string | null): void => {
    // NOTE/TODO: We need to update the navigation logic in case the user logs in from the class details
    // screen nested in the Home tab... (i.e. redirect needs to take them to the same tab)
    // leaving old nav params for now to further indicate how this is different from the class stack

    if (eventGroupName) {
      navigation.navigate('ClassDetails', {
        eventGroupName,
        // eventCategoryName,
        // eventCategoryId,
      });
    }
  };

  const navigateToOnDemandClassScreen = (eventId?: string | null): void => {
    if (!eventId) return;

    navigation.navigate('OnDemandClass', { eventId });
  };

  const {
    error: homeScreenContentError,
    data: homeScreenContentData = {},
    loading: homeScreenContentLoading,
    refetch: refetchHomeScreenContent,
  } = useHomeScreenContentQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [isLoaded, setIsLoaded] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const { data: meData } = useMeQuery({ skip: !isAuthenticated });

  const getAuthData = async (): Promise<void> => {
    const authToken = await AuthUtils.retrieveToken();
    setIsLoaded(true);
    setIsAuthenticated(authToken !== null);
  };

  useEffect(() => {
    if (!isLoaded) {
      getAuthData();
    }
  }, [isLoaded]);

  const handleMaternityStatusClick = (): void => {
    analytics().logEvent('maternity_status_click_from_home_screen');
    navigation.navigate('MaternityOnboarding');
  };

  const renderMaternityStatus = (): ReactNode => {
    if (!meData) return;
    const user = meData || {};
    const { me } = user;
    const { maternityProfile } = me;

    if (!maternityProfile) {
      return;
    }

    return (
      <Touchable onPress={(): void => handleMaternityStatusClick()}>
        <MaternityStatus gestationalAge={maternityProfile.gestationalAge} />
      </Touchable>
    );
  };

  const renderPost = (post: Post): ReactElement => {
    const { id, title, description, mediumImage, isFeatured } = post;

    return (
      <Container key={id} pb={2}>
        <ContentCard
          id={id}
          heading={title}
          text={description}
          label=""
          cardLabel={isFeatured ? 'Featured News' : 'News'}
          cardLabelBackgroundColor={isFeatured ? colors.sunsetOrange : colors.summerSky}
          imageSrc={mediumImage}
          imageAsset={GenericNewsImage}
          onPress={(): void => navigateToPostDetailsScreen(post)}
        />
      </Container>
    );
  };

  const renderPosts = (posts?: Post[]): ReactElement | null => {
    if (!posts || !posts.length) return null;

    return <>{posts.map(renderPost)}</>;
  };

  const renderEventGroup = (eventGroup: EventGroup): ReactElement => {
    return (
      <Container key={eventGroup.name} pb={2}>
        <EventGroupCard
          {...eventGroup}
          withBadge
          onPress={(): void => navigateToClassDetailsScreen(eventGroup.name)}
        />
      </Container>
    );
  };

  const renderOnDemandEvent = (event: Event): ReactElement => {
    return (
      <Container key={event.eventId} pb={2}>
        <OnDemandEventCard
          onPress={(): void => navigateToOnDemandClassScreen(event.eventId)}
          hideMetadata
          {...event}
        />
      </Container>
    );
  };

  const renderEventGroups = (eventGroups?: EventGroup[]): ReactElement | null => {
    if (!eventGroups || !eventGroups.length) return null;

    return <>{eventGroups.map(renderEventGroup)}</>;
  };

  const renderOnDemandEvents = (events?: Event[]): ReactElement | null => {
    if (!events || !events.length) return null;

    return <>{events.map(renderOnDemandEvent)}</>;
  };

  const renderPostsAndEvents = (): ReactElement => {
    const content = homeScreenContentData || {};

    const { featuredOnDemandEvents, featuredEventGroups, featuredPosts, posts } = content;

    return (
      <Container px={5}>
        {renderPosts(featuredPosts)}
        {renderEventGroups(featuredEventGroups)}
        {renderOnDemandEvents(featuredOnDemandEvents)}
        {renderPosts(posts)}
      </Container>
    );
  };

  const renderHomeScreenContent = (): ReactElement => {
    if (homeScreenContentLoading && !Object.keys(homeScreenContentData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={homeScreenContentLoading}
            onRefresh={refetchHomeScreenContent}
          />
        }
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <LoggedInContent>{renderMaternityStatus()}</LoggedInContent>
          {renderPostsAndEvents()}
        </Container>
      </ScrollView>
    );
  };

  if (homeScreenContentError) {
    <ContentErrorToast />;
  }

  return (
    <Screen
      testID="home-screen"
      screenHeader
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
    >
      {renderHomeScreenContent()}
    </Screen>
  );
};

import React, { FC } from 'react';
import { StatusBar } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import { Button } from '../../components/Button';
import { colors, fonts } from '../../styles';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { SCREEN_HEIGHT, t } from '../../utils';
import { Text } from '../../components/Text';

export const MaternityOnboardingScreen3: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const onNo = async (): Promise<void> => {
    navigation.navigate('CustomizeMaternityExperience');
  };

  const onSignUp = async (): Promise<void> => {
    //Navigate to the form or Mychart or whatever is decided :)
    navigation.navigate('CustomizeMaternityExperience');
  };

  return (
    <>
      <Screen testID="maternityOnboardingScreen3" backgroundColor={colors.pearlLusta}>
        <StatusBar barStyle="light-content" />
        <Container
          justifyContent="flex-start"
          alignItems="center"
          pt={SCREEN_HEIGHT * 0.1}
          height={SCREEN_HEIGHT}
        >
          <Text fontSize={7} color={colors.chino} {...fonts.light} textAlign="center">
            {t('maternity.onboarding.thirdStep.title')}
          </Text>
          <Text fontSize={5} color={colors.gray} textAlign="center" py={9}>
            {t('maternity.onboarding.thirdStep.subtitle')}
          </Text>

          <Container fullWidth flexDirection="row" justifyContent="center">
            <Container width="50%" alignItems="flex-end" mx={2}>
              <Button
                label={t('maternity.onboarding.thirdStep.buttons.no')}
                backgroundColor={colors.chino}
                width="55%"
                py={2}
                fontSize={6}
                fontWeight="300"
                color={colors.white}
                borderRadius={35}
                onPress={onNo}
              />
            </Container>
            <Container width="50%" alignItems="flex-start" mx={2}>
              <Button
                label={t('maternity.onboarding.thirdStep.buttons.signup')}
                backgroundColor={colors.wewak}
                width="70%"
                py={2}
                fontSize={6}
                fontWeight="300"
                color={colors.white}
                borderRadius={35}
                onPress={onSignUp}
              />
            </Container>
          </Container>
        </Container>
      </Screen>
    </>
  );
};

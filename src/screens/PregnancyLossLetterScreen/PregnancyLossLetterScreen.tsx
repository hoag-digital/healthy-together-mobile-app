import React, { FC } from 'react';
import styled from '@emotion/native';

import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { ScrollView } from 'react-native-gesture-handler';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import welcomeImg from '../../assets/images/welcome-bg.png';
import { Button } from '../../components/Button';
import { Container } from '../../components/Container';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import { colors } from '../../styles';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const PregnancyLossLetterScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  return (
    <BackgroundImage source={welcomeImg} resizeMode="stretch">
      <Screen testID="welcomeScreen" transparent margin={20}>
        <StatusBar barStyle="dark-content" />

        <Container mt={7} bg={colors.white} opacity={0.6} p={2} borderRadius={35}>
          <Container mx={2} opacity={1}>
            <ScrollView
              scrollIndicatorInsets={{ right: 1 }}
              style={{
                paddingLeft: 10,
              }}
            >
              <Text fontWeight="600">{t('maternity.pregnancyLoss.letter')}</Text>

              <Button
                label="Close"
                backgroundColor={colors.pearlLusta}
                color={colors.black}
                fontSize={3}
                onPress={(): void => {
                  navigation.navigate('Maternity');
                }}
                mb={SCROLLVIEW_BOTTOM_PADDING}
              />
            </ScrollView>
          </Container>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};

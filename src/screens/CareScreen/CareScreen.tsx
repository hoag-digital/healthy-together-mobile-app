import React, { FC, ReactElement, ReactNode, useEffect, useState, useCallback } from 'react';
import { ActivityIndicator, FlatList, Alert } from 'react-native';
import { Divider, Icon } from 'react-native-elements';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import Geolocation from '@react-native-community/geolocation';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Button } from '../../components/Button';
import { UrgentCareCard } from '../../components/UrgentCareCard';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING, SCREEN_HEIGHT } from '../../utils';
import { WaitData, useWaitTimesQuery } from '../../graphql/types';
import getDistanceFromLocation from '../../utils/getDistanceFromLocation';
import hasLocationPermissions from '../../utils/hasLocationPermissions';
import { trackUrgentCareCardPress, trackSelfAssessmentStarted } from '../../services/analytics';
import { locations as LOCATIONS } from './locationData';

const KEY_NEAREST = 'nearest';
const KEY_SHORTEST_WAIT = 'shortest-wait';

const TAB_BUTTONS = [
  {
    labelKey: 'nearestLocation',
    key: KEY_NEAREST,
    icon: 'map-marker',
  },
  {
    labelKey: 'shortestWait',
    key: KEY_SHORTEST_WAIT,
    icon: 'clock',
  },
];

const sortBasedOnWaitTime = (l1, l2): number => {
  // push all "closed" hospitals to the bottom
  // NOTE: '== null' catches both null and undefined while permitting 0 as a valid value
  if (l2?.waits?.nextAvailableVisit == null) return -1;

  return l1?.waits?.nextAvailableVisit - l2?.waits?.nextAvailableVisit;
};

const sortBasedOnDistance = (l1, l2): number => {
  // push all hospitals without distances (i.e. missing data) to the bottom
  // NOTE: '== null' catches both null and undefined while permitting 0 as a valid value
  if (l2?.hospital?.distance == null) return -1;

  return l1?.hospital?.distance - l2?.hospital?.distance;
};

export const CareScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const [activeForm, setActiveForm] = useState(KEY_NEAREST);
  const [userCoords, setUserCoords] = useState({} as any);
  const [geolocationUpdating, setGeolocationUpdating] = useState(true);

  const {
    data: waitTimesData = {},
    loading: waitTimesLoading,
    networkStatus: waitTimesNetworkStatus,
    refetch: refetchWaitTimes,
  } = useWaitTimesQuery({
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const locationWaitTimes: WaitData[] = (waitTimesData && waitTimesData.waitTimes) || [];

  const updateGeolocation = useCallback((): void => {
    setGeolocationUpdating(true);

    hasLocationPermissions().then(hasPermission => {
      try {
        if (hasPermission) {
          // if location is turned off getCurrentPosition will cause error, evidenced with location = 'NONE' on simulator
          Geolocation.getCurrentPosition(
            userInfo => {
              if (userInfo?.coords) {
                setUserCoords(userInfo.coords);
              }
            },
            () => {
              // The try...catch will still allowing a failure when location was not available, but there is an error callback that handles this
              Alert.alert('Location Error', 'An error has occured retrieving your location');
            }
          );
        }
      } catch (error) {
        // handle error gracefully and set data to default from api
      } finally {
        // adding a little spinner to make the UI smoother when recomputing list order after tab
        // change and geolocation update
        setTimeout(() => {
          setGeolocationUpdating(false);
        }, 500);
      }
    });
  }, []);

  // 0. refetch wait times and user geolocation when pulling to refresh
  const refreshCareScreen = async (): Promise<void> => {
    // if you experience issues refetching in the simulator, try refreshing.
    // see: https://github.com/apollographql/react-apollo/issues/3862
    refetchWaitTimes();
    updateGeolocation();
  };

  // 1. fetch geolocation on the first mount
  useEffect(() => {
    updateGeolocation();
  }, [updateGeolocation]);

  // 2. refetch geolocation when the 'nearest' tab is selected
  useEffect(() => {
    if (activeForm === KEY_NEAREST) {
      updateGeolocation();
    }
  }, [activeForm, updateGeolocation]);

  const renderCareOptions = (): ReactNode => {
    return (
      <>
        <Button
          backgroundColor={colors.cerulean}
          borderRadius={5}
          color={colors.white}
          flex={1}
          fontSize={3}
          fontWeight="700"
          label={t('care.telehealth')}
          mb={5}
          mt={25}
          mx={4}
          py={5}
          onPress={(): void => {
            navigation.navigate('Telehealth');
          }}
        />
        <Button
          backgroundColor={colors.terra}
          borderRadius={5}
          color={colors.white}
          flex={1}
          fontSize={3}
          fontWeight="700"
          label={t('care.selfAssessment')}
          mb={5}
          mt={10}
          mx={4}
          py={5}
          onPress={(): void => {
            // tracking the self-assessment link in analytics
            trackSelfAssessmentStarted();

            navigation.navigate('Self Assessment');
          }}
        />
      </>
    );
  };

  const renderUrgentCareHeader = (): ReactNode => {
    return (
      <>
        <Container flexDirection="row" alignItems="center" mb={4}>
          <Text testID="careScreenSubHeading" fontWeight="bold" fontSize={4} pr={2}>
            {t('care.subhead')}
          </Text>

          <Divider
            style={{
              flex: 1,
              backgroundColor: colors.silver,
            }}
          />
        </Container>
        <Container alignItems="center" flexDirection="row" justifyContent="center" mb={5}>
          {TAB_BUTTONS.map(button => (
            <Button
              key={button.key}
              backgroundColor={button.key === activeForm ? colors.balticSea : colors.whiteSmoke}
              mx={1}
              label={t(`care.${button.labelKey}`)}
              color={button.key === activeForm ? colors.white : colors.balticSea50}
              borderRadius={5}
              onPress={(): void => {
                setActiveForm(button.key);
              }}
              pl={2}
              pr={3}
              py={2}
              renderIcon={(): ReactNode => (
                <Container mr={1}>
                  <Icon
                    name={button.icon}
                    type="material-community"
                    color={button.key === activeForm ? colors.white : colors.balticSea50}
                  />
                </Container>
              )}
            />
          ))}
        </Container>
      </>
    );
  };

  /**
   * The header of the FlatList
   */
  const renderHeaderContents = (): ReactElement => {
    return (
      <>
        <Container pt={3} px={5} bg={colors.white}>
          {renderCareOptions()}
          {renderUrgentCareHeader()}
        </Container>
        {geolocationUpdating ? (
          //Giving this a height that allows it to take up space the cards normally would be taking up or it creates an odd screen behavior
          <ActivityIndicator
            style={{
              backgroundColor: colors.sky,
              paddingVertical: 10,
              height: SCREEN_HEIGHT * 0.4,
            }}
          />
        ) : null}
      </>
    );
  };

  /** Renders an Urgent Care location "card" */
  const renderCareLocationCard = ({ item }): ReactElement | null => {
    if (!item.hospital) return null;

    const { id, name, todaysBusinessHours } = item.hospital;
    const { currentWait } = item.waits;
    const image = LOCATIONS[id]?.image; //TODO: determine if there is a default image that should be displayed

    return (
      <Container pt={5} px={5} backgroundColor={colors.sky}>
        <UrgentCareCard
          id={id}
          name={name}
          closes={todaysBusinessHours}
          wait={currentWait}
          onPress={(): void => {
            // tracking the urgent care card press in analytics
            trackUrgentCareCardPress(item.hospital);

            navigation.navigate('Appointment Form', {
              location: item.hospital,
            });
          }}
          locationImage={image}
        />
      </Container>
    );
  };

  /**
   * The footer in the FlatList. Allows for extra space at the bottom
   */
  const renderFooterContents = (): ReactElement => {
    return <Container bg={colors.sky} fullWidth pb={SCROLLVIEW_BOTTOM_PADDING} />;
  };

  const renderScreenContents = (): ReactElement => {
    if (waitTimesLoading && !Object.keys(waitTimesData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    // networkStatus 4 indicates a refetch while 1 indicates the initial query fetch...
    // only show the scrollview loading spinner on an explicit user-requested refetch
    const isRefetching = waitTimesLoading && waitTimesNetworkStatus === 4;

    // if we have a geolocation for the user, compute distances for each location
    const locationsToSort = userCoords?.latitude
      ? locationWaitTimes.map(item => {
          const { hospital } = item;

          // dont attempt to compute distance if there is no hospital info present
          if (!hospital) return item;

          return {
            ...item,
            hospital: {
              ...hospital,
              distance: getDistanceFromLocation(
                userCoords.latitude,
                userCoords.longitude,
                item.hospital.latitude,
                item.hospital.longitude
              ),
            },
          };
        })
      : locationWaitTimes;

    // .concat() method duplicates array to prevent bugs from .sort sorting in-place
    const sortFunction =
      activeForm === KEY_SHORTEST_WAIT ? sortBasedOnWaitTime : sortBasedOnDistance;

    const sortedLocations = locationsToSort.concat().sort(sortFunction);

    return (
      <FlatList
        data={sortedLocations}
        keyExtractor={(item, index): string => `${item?.hospital?.id}-${index}`}
        renderItem={renderCareLocationCard}
        ListHeaderComponent={renderHeaderContents}
        ListFooterComponent={renderFooterContents}
        refreshing={isRefetching}
        onRefresh={refreshCareScreen}
      />
    );
  };

  return (
    <Screen
      testID="care-screen"
      screenHeader
      backgroundColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
    >
      {renderScreenContents()}
    </Screen>
  );
};

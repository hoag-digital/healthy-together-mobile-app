/**
 *
 * Location Id and Name returned from ClockwiseMD as of 4-23-2020
 * added attribute of image to display
 * This is NOT ideal, ideal solution would be that the api would return the image
 *
 */
import ALISO_VIEJO from '../../assets/images/care-locations/aliso_viejo.jpg';
import FOOTHILL_RANCH from '../../assets/images/care-locations/foothill_ranch.jpg';
import HUNTINGTON_BEACH from '../../assets/images/care-locations/huntington_beach.jpg';
import HUNTINGTON_HARBOUR from '../../assets/images/care-locations/huntington_harbour.jpg';
import LOS_OLIVIOS from '../../assets/images/care-locations/los_olivios.jpg';
import NEWPORT_BEACH from '../../assets/images/care-locations/newport_beach.jpg';
import NEWPORT_COAST from '../../assets/images/care-locations/newport_coast.jpg';
import ORCHARD_HILLS from '../../assets/images/care-locations/orchard_hills.jpg';
import SAND_CANYON from '../../assets/images/care-locations/sand_canyon.jpg'; //We don't have a sand canyon location returned
import TUSTIN_LEGACY from '../../assets/images/care-locations/tustin_legacy.jpg';
import TUSTIN_RANCH from '../../assets/images/care-locations/tustin_ranch.jpg';
import WOODBRIDGE from '../../assets/images/care-locations/woodbridge.jpg';
import WOODBURY from '../../assets/images/care-locations/woodbury.jpg';

export const locations = {
  1403: {
    id: '1403',
    name: 'Hoag Urgent Care Irvine Los Olivos ',
    image: LOS_OLIVIOS,
  },
  1400: {
    id: '1400',
    name: 'Hoag Urgent Care Aliso Viejo',
    image: ALISO_VIEJO,
  },
  1404: {
    id: '1404',
    name: 'Hoag Urgent Care Irvine Sand Canyon ',
    image: SAND_CANYON,
  },

  2625: {
    id: '2625',
    name: 'COVID-19 TESTING FOR THOSE WHO HAVE BEEN ISSUED A VOUCHER',
    image: null,
  },
  2627: {
    id: '2627',
    name: 'Hoag Urgent Care Tustin Ranch',
    image: TUSTIN_RANCH,
  },
  4435: {
    id: '4435',
    name: 'Hoag Urgent Care Foothill Ranch',
    image: FOOTHILL_RANCH,
  },
  4436: {
    id: '4436',
    name: 'Hoag Urgent Care & Family Medicine Newport Coast',
    image: NEWPORT_COAST,
  },
  4274: {
    id: '4274',
    name: 'Hoag Urgent Care & Family Medicine - Irvine Orchard Hills',
    image: ORCHARD_HILLS,
  },
  1405: {
    id: '1405',
    name: 'Hoag Urgent Care & Family Medicine - Irvine Woodbury',
    image: WOODBURY,
  },
  1406: {
    id: '1406',
    name: 'Hoag Urgent Care Newport Beach',
    image: NEWPORT_BEACH,
  },
  1402: {
    id: '1402',
    name: 'Hoag Urgent Care Irvine Woodbridge',
    image: WOODBRIDGE,
  },
  2626: {
    id: '2626',
    name: 'Hoag Urgent Care Huntington Harbour',
    image: HUNTINGTON_HARBOUR,
  },
  2628: {
    id: '2628',
    name: 'Hoag Urgent Care Tustin Legacy',
    image: TUSTIN_LEGACY,
  },
  1401: {
    id: '1401',
    name: 'Hoag Urgent Care Huntington Beach ',
    image: HUNTINGTON_BEACH,
  },
};

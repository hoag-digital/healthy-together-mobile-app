import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import { REMOVE_HEADER_FOOTERS_HOAG_IO } from '../../lib/InjectedJavascript';
import { useWebUrlsQuery } from '../../graphql/types';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

export const TelehealthScreen: FC<NavigationStackScreenProps> = () => {
  const { error: webUrlsError, data: webUrlsData, loading: webUrlsLoading } = useWebUrlsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const telehealthUrl = webUrlsData?.webUrls?.telehealth || '';

  if (webUrlsError) {
    throw new Error('Error loading URLS');
  }

  return (
    <Screen
      testID="telehealth-screen"
      modalHeader
      screenTitle={t('care.telehealth')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      {webUrlsLoading ? (
        <CenteredLoadingSpinner />
      ) : (
        <WebView
          source={{ uri: telehealthUrl }}
          containerStyle={StyleSheet.absoluteFillObject}
          startInLoadingState
          injectedJavaScript={REMOVE_HEADER_FOOTERS_HOAG_IO}
        />
      )}
    </Screen>
  );
};

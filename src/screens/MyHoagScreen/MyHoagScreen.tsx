import React, { FC, useState, ReactElement } from 'react';
import { Divider } from 'react-native-elements';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';

import { NavigationActions } from 'react-navigation';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { ModuleCard } from '../../components/Card';
import { LoginModal } from '../../components/LoginModal';

import { colors } from '../../styles';
import { t, SCROLLVIEW_BOTTOM_PADDING } from '../../utils';
import { AuthConsumer } from '../../lib/AuthConsumer';
import Biocircuit from '../../assets/images/biocircuit.jpg';
import Maternity from '../../assets/images/maternity.jpg';

const capitalize = (lowercaseString): string | null => {
  if (!lowercaseString || !lowercaseString.length) return null;

  return lowercaseString.charAt(0).toUpperCase() + lowercaseString.slice(1);
};

// Action that is dispatched from the login screen to navigate back to program details.
const AUTH_TO_MY_HOAG_REDIRECT_ACTION = NavigationActions.navigate({
  routeName: 'Home',
  action: NavigationActions.navigate({
    routeName: 'MyHoag',
  }),
});

export const MyHoagScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const [selectedModule, setSelectedModule] = useState(''); //required in order to have the onCreateAccount and onLogin know to where to go after

  const openModule = (moduleName: string): void => {
    navigation.navigate(`${capitalize(moduleName)}Onboarding`);
  };

  const onCreateAccount = (): void => {
    navigation.navigate('Registration', {
      redirect: {
        action: AUTH_TO_MY_HOAG_REDIRECT_ACTION,
        callback: (): void => openModule(selectedModule),
      },
    });
  };

  const onLogin = (): void => {
    navigation.navigate('Login', {
      redirect: {
        action: AUTH_TO_MY_HOAG_REDIRECT_ACTION,
        callback: (): void => openModule(selectedModule),
      },
    });
  };

  return (
    <Screen
      testID="my-hoag-screen"
      screenHeader
      backgroundColor={colors.transparent}
      fill
      height="100%"
      paddingTop={0}
    >
      <ScrollView>
        <Container px={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          <Text testID="my-hoag-screen-header" fontWeight="bold" fontSize={5} py={4}>
            {t('navigation.myHoag')}
          </Text>
          <Divider style={{ backgroundColor: colors.silver }} />
          <Container flexDirection="row" justifyContent="flex-end" p={2} />
          <AuthConsumer>
            {(auth): ReactElement => {
              const { token: authToken } = auth;
              // Note the "authToken" here is the JWT token for validating api resuests,
              return (
                <>
                  <Container pb={2}>
                    <ModuleCard
                      primaryColor={colors.tangerineYellow}
                      secondaryColor={colors.eclipse}
                      label="Biocircuit"
                      secondaryLabel="Learn More"
                      imageAsset={Biocircuit}
                      onPress={(): void => {
                        setSelectedModule('biocircuit');
                        openModule('biocircuit');
                      }}
                    />
                  </Container>
                  <Container pb={2}>
                    <ModuleCard
                      primaryColor={colors.gray}
                      secondaryColor={colors.white}
                      imageAsset={Maternity}
                      label="Maternity"
                      secondaryLabel="Welcome"
                      onPress={(): void => {
                        setSelectedModule('maternity');
                        authToken ? openModule('maternity') : setLoginModalVisible(true);
                      }}
                    />
                  </Container>
                  {__DEV__ ? (
                    <>
                      {/* a place to add future modules that can be worked on and visible only in DEV mode
                       <Container pb={2}>
                        <ModuleCard
                          label="New Module"
                          primaryColor={colors.balticSea}
                          secondaryColor={colors.cinnabar}
                          imageAsset={GenericImage}
                        />
                      </Container> */}
                    </>
                  ) : null}

                  <LoginModal
                    isVisible={loginModalVisible}
                    setIsVisible={setLoginModalVisible}
                    createAccount={onCreateAccount}
                    login={onLogin}
                  />
                </>
              );
            }}
          </AuthConsumer>
        </Container>
      </ScrollView>
    </Screen>
  );
};

import React, { FC, useEffect, useState, ReactNode } from 'react';
import { Platform } from 'react-native';
import VersionNumber from 'react-native-version-number';

import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { useMinimumAppVersionQuery } from '../../graphql/types';
import DefaultLogo from '../../assets/images/hoag-logo-solid.svg';
import { ContentErrorToast } from '../../components/Toastr';

const { appVersion } = VersionNumber;

interface UpdateAppVersionScreenProps {
  /**
   * Callback to handle set update version mode
   */
  handleSetAppUpdationRequired: (isShow: boolean) => void;
}

export const UpdateAppVersionScreen: FC<UpdateAppVersionScreenProps> = props => {
  const [isAppUpdationRequired, setAppUpdationRequired] = useState(false);
  const [isMinimumVersionFetched, setMinimumVersionFetched] = useState(false);

  const { error: minimumAppVersionError, data: minimumAppVersionData } = useMinimumAppVersionQuery({
    fetchPolicy: 'cache-and-network',
    skip: isMinimumVersionFetched,
  });

  if (minimumAppVersionError) {
    <ContentErrorToast />;
  }

  const compareVersionNumber = (current: string, latest: string): number => {
    // Both version are same
    if (current === latest) {
      return 0;
    }

    const currentVersionField = current.split('.');
    const latestVersionField = latest.split('.');

    const minLength = Math.min(currentVersionField.length, latestVersionField.length);

    for (let fieldIndex = 0; fieldIndex < minLength; fieldIndex++) {
      // Current version is greater
      if (Number(currentVersionField[fieldIndex]) > Number(latestVersionField[fieldIndex])) {
        return 1;
      }

      // Latest version is greater
      if (Number(currentVersionField[fieldIndex]) < Number(latestVersionField[fieldIndex])) {
        return -1;
      }
    }

    // Current version is greater
    if (currentVersionField.length > latestVersionField.length) {
      return 1;
    }

    // Latest version is greater
    if (currentVersionField.length < latestVersionField.length) {
      return -1;
    }

    //Both version are same
    return 0;
  };

  useEffect(() => {
    const latestAppVersion = minimumAppVersionData?.minimumAppVersion?.[Platform.OS] || '';

    if (minimumAppVersionData && !isMinimumVersionFetched) {
      setMinimumVersionFetched(true);

      if (compareVersionNumber(appVersion, latestAppVersion) === -1) {
        setAppUpdationRequired(true);
        props.handleSetAppUpdationRequired(true);
      } else {
        setAppUpdationRequired(false);
        props.handleSetAppUpdationRequired(false);
      }
    }

    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [minimumAppVersionData]);

  const renderLoaderScreen = (): ReactNode => {
    if (isAppUpdationRequired) {
      return null;
    }

    return <CenteredLoadingSpinner />;
  };

  const renderScreenContent = (): ReactNode => {
    if (!isAppUpdationRequired) {
      return null;
    }

    return (
      <Container
        width="100%"
        px={6}
        py={4}
        alignItems="center"
        justifyContent="center"
        bg={colors.white}
      >
        <Container pt={4} alignItems="center" height={50}>
          <DefaultLogo />
        </Container>

        <Text fontSize={4} fontWeight="600" pt={4}>
          New version available
        </Text>

        <Text fontSize={4} fontWeight="400" pt={4} mb={4}>
          Your app version is not up to date, please update to access the app.
        </Text>
      </Container>
    );
  };

  return (
    <Container
      fullWidth
      bg={isAppUpdationRequired ? colors.gray : colors.white}
      flex={1}
      justifyContent="center"
      alignItems="center"
    >
      <Container mx={6}>
        {renderLoaderScreen()}
        {renderScreenContent()}
      </Container>
    </Container>
  );
};

import React, { FC, ReactElement } from 'react';
import { Switch } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';

import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';

import { colors } from '../../styles';
import { t } from '../../utils';
import {
  NotificationPreferencesDocument,
  useDisableClassReminderNotificationsMutation,
  useDisableMaternityNotificationsMutation,
  useDisableNotificationsMutation,
  useEnableClassReminderNotificationsMutation,
  useEnableMaternityNotificationsMutation,
  useEnableNotificationsMutation,
  useNotificationPreferencesQuery,
  useMeQuery,
  User,
} from '../../graphql/types';

export const NotificationSettingsScreen: FC<NavigationStackScreenProps> = () => {
  const {
    data: { notificationPreferences = {} } = {},
    loading: notificationPreferencesLoading,
    error: notificationPreferencesError,
  } = useNotificationPreferencesQuery({
    fetchPolicy: 'cache-and-network',
  });

  const { data: { me = {} as User } = {} } = useMeQuery({
    fetchPolicy: 'cache-and-network',
  });

  const hasMaternityProfile = me?.maternityProfile;

  const [enableNotifications] = useEnableNotificationsMutation({
    optimisticResponse: {
      enableNotifications: {
        ...notificationPreferences,
        notificationsEnabled: true,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.enableNotifications,
        },
      });
    },
  });

  const [disableNotifications] = useDisableNotificationsMutation({
    optimisticResponse: {
      disableNotifications: {
        ...notificationPreferences,
        notificationsEnabled: false,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.disableNotifications,
        },
      });
    },
  });

  const [enableClassReminderNotifications] = useEnableClassReminderNotificationsMutation({
    optimisticResponse: {
      enableClassReminderNotifications: {
        ...notificationPreferences,
        classReminderNotificationsEnabled: true,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.enableClassReminderNotifications,
        },
      });
    },
  });

  const [disableClassReminderNotifications] = useDisableClassReminderNotificationsMutation({
    optimisticResponse: {
      disableClassReminderNotifications: {
        ...notificationPreferences,
        classReminderNotificationsEnabled: false,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.disableClassReminderNotifications,
        },
      });
    },
  });

  const [enableMaternityNotifications] = useEnableMaternityNotificationsMutation({
    optimisticResponse: {
      enableMaternityNotifications: {
        ...notificationPreferences,
        maternityNotificationsEnabled: true,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.enableMaternityNotifications,
        },
      });
    },
  });

  const [disableMaternityNotifications] = useDisableMaternityNotificationsMutation({
    optimisticResponse: {
      disableMaternityNotifications: {
        ...notificationPreferences,
        maternityNotificationsEnabled: false,
      },
    },
    update: (cache, { data }): void => {
      cache.writeQuery({
        query: NotificationPreferencesDocument,
        data: {
          notificationPreferences: data?.disableMaternityNotifications,
        },
      });
    },
  });

  const { notificationsEnabled, classReminderNotificationsEnabled, maternityNotificationsEnabled } =
    notificationPreferences || {};

  const toggleAllNotifications = async (): Promise<void> => {
    if (notificationsEnabled) {
      disableNotifications();
    } else {
      enableNotifications();
    }
  };

  const toggleClassReminderNotifications = async (): Promise<void> => {
    if (classReminderNotificationsEnabled) {
      disableClassReminderNotifications();
    } else {
      enableClassReminderNotifications();
    }
  };

  const toggleMaternityNotifications = async (): Promise<void> => {
    if (maternityNotificationsEnabled) {
      disableMaternityNotifications();
    } else {
      enableMaternityNotifications();
    }
  };

  const renderNotificationModuleOptions = (): ReactElement | null => {
    return (
      <>
        <Container fullWidth flexDirection="row" px={5} py={2} my={1}>
          <Text fontWeight="bold">{t('account.notificationModuleSettingsStatement')}</Text>
        </Container>
        <Container
          flexDirection="row"
          justifyContent="space-between"
          px={5}
          py={2}
          my={1}
          alignItems="center"
        >
          <Text fontSize={3}>{t('account.classReminderNotifications')}</Text>
          <Switch
            trackColor={{ false: colors.eclipse, true: colors.cornflowerBlue3 }}
            ios_backgroundColor={colors.eclipse}
            thumbColor={colors.white}
            onValueChange={toggleClassReminderNotifications}
            value={!!classReminderNotificationsEnabled} // coercing null/undefined to false to address TS error
          />
        </Container>
        {hasMaternityProfile ? (
          <Container
            flexDirection="row"
            justifyContent="space-between"
            px={5}
            py={2}
            my={1}
            alignItems="center"
          >
            <Text fontSize={3}>{t('account.maternityNotifications')}</Text>
            <Switch
              trackColor={{ false: colors.eclipse, true: colors.cornflowerBlue3 }}
              ios_backgroundColor={colors.eclipse}
              thumbColor={colors.white}
              onValueChange={toggleMaternityNotifications}
              value={!!maternityNotificationsEnabled} // coercing null/undefined to false to address TS error
            />
          </Container>
        ) : null}
      </>
    );
  };

  const renderScreenContents = (): ReactElement => {
    if (notificationPreferencesLoading) {
      return <CenteredLoadingSpinner />;
    }

    return (
      <Container>
        <Container
          flexDirection="row"
          justifyContent="space-between"
          px={5}
          py={2}
          my={1}
          alignItems="center"
        >
          <Text fontSize={3}>
            {notificationsEnabled
              ? t('account.notificationsEnabledStatement')
              : t('account.notificationsDisabledStatement')}
          </Text>
          <Switch
            trackColor={{ false: colors.eclipse, true: colors.cornflowerBlue3 }}
            ios_backgroundColor={colors.eclipse}
            thumbColor={colors.white}
            onValueChange={toggleAllNotifications}
            value={!!notificationsEnabled} // coercing null/undefined to false to address TS error
          />
        </Container>
        {notificationsEnabled ? renderNotificationModuleOptions() : null}
      </Container>
    );
  };

  if (notificationPreferencesError) {
    // handle error
  }

  return (
    <Screen modalHeader screenTitle="Notifications" headerColor={colors.gainsboro}>
      <Container width="100%" height="100%" bg={colors.white} py={4}>
        {renderScreenContents()}
      </Container>
    </Screen>
  );
};

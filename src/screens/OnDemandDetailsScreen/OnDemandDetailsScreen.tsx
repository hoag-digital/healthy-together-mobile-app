import React, { FC, ReactElement } from 'react';
import { StatusBar, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import dayjs from 'dayjs';

import { Button } from '../../components/Button';
import { Container } from '../../components/Container';
import { DetailsScreenLabel } from '../../components/DetailsScreenLabel';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { DEFAULT_ICON_SIZE, SCROLLVIEW_BOTTOM_PADDING, SCREEN_HEIGHT, t } from '../../utils';
import { safelyOpenUrl } from '../../utils/safelyOpenUrl';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import { trackOnDemandVideoPlay } from '../../services/analytics';
import { useEventQuery } from '../../graphql/types';
import { ContentErrorToast } from '../../components/Toastr';

// eslint-disable-next-line
const relativeTime = require('dayjs/plugin/relativeTime');

dayjs.extend(relativeTime);

const IMAGE_HEIGHT = SCREEN_HEIGHT * 0.4;

/**
 */
export const OnDemandDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const eventId = navigation.getParam('eventId');

  const {
    error: eventError,
    data: eventData = {},
    loading: eventLoading,
    refetch: refetchEvent,
  } = useEventQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      eventId: parseInt(eventId, 10), // typing mismatch between response and query variable
    },
  });

  const goBack = (): void => {
    navigation.goBack();
  };

  const openYoutubeLink = (): void => {
    const event = eventData?.event || {};
    const { youtubeLiveLink } = event;

    if (!youtubeLiveLink) return;

    // tracking the on-demand video event in analytics
    trackOnDemandVideoPlay(event);

    safelyOpenUrl(youtubeLiveLink);
  };

  const renderOnDemandClassScreenContents = (): ReactElement => {
    if (eventLoading && !Object.keys(eventData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    const event = eventData?.event || {};
    const { name, description, mediumImage, startTime } = event;

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        <Touchable
          position="absolute"
          // TODO: future-improvement -> dynamic positioning
          top={45}
          right={20}
          testID="close-icon"
          onPress={goBack}
          bg={colors.white}
          height={DEFAULT_ICON_SIZE}
          width={DEFAULT_ICON_SIZE}
          borderRadius={DEFAULT_ICON_SIZE}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
        </Touchable>
        <Container position="relative">
          <TransparencyImageOverlay height={IMAGE_HEIGHT / 2} />
          <FastImage
            accessibilityLabel={`${name} image`}
            style={{
              width: '100%',
              height: IMAGE_HEIGHT,
              resizeMode: 'cover',
            }}
            source={mediumImage ? { uri: mediumImage } : GenericClassImage}
          />
          <DetailsScreenLabel iconType="ondemand" label={t('classes.onDemandClass.label')} />
        </Container>
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={<RefreshControl refreshing={eventLoading} onRefresh={refetchEvent} />}
        >
          <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
            <Container
              pt={1}
              pb={3}
              flexDirection="row"
              justifyContent="space-between"
              alignItems="flex-start"
            >
              <Container width="70%">
                <Text fontSize={5} fontWeight="bold">
                  {name}
                </Text>
              </Container>
              {startTime ? (
                <Text fontSize={3} fontWeight="bold">
                  {dayjs(startTime).fromNow()}
                </Text>
              ) : null}
            </Container>
            <Text pb={3} px={1} fontSize={3} textAlign="justify">
              {description}
            </Text>
            <Button
              label={t('classes.onDemandClass.watchVideo')}
              color={colors.white}
              onPress={openYoutubeLink}
              backgroundColor={colors.youtubeRed}
              borderRadius={5}
              py={3}
              mt={5}
              mb={5}
              flex={1}
              mx={2}
              fontSize={3}
              fontWeight="700"
            />
          </Container>
        </ScrollView>
      </>
    );
  };

  if (eventError) {
    <ContentErrorToast />;
  }

  return (
    <Screen
      testID="on-demand-class-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderOnDemandClassScreenContents()}
    </Screen>
  );
};

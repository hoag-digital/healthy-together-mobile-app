import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import { CLEANUP_SCHEDULE_VIEW_JAVASCRIPT } from '../../lib/InjectedJavascript';
import { useWebUrlsQuery } from '../../graphql/types';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

export const ScheduleFoothillAppointmentScreen: FC<NavigationStackScreenProps> = () => {
  const { error: webUrlsError, data: webUrlsData, loading: webUrlsLoading } = useWebUrlsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const apptUrl = webUrlsData?.webUrls?.foothillAppointment || '';

  if (webUrlsError) {
    throw new Error('Error loading URLS');
  }

  return (
    <Screen
      testID="care-screen"
      modalHeader
      screenTitle={t('care.scheduleAppointment')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      {webUrlsLoading ? (
        <CenteredLoadingSpinner />
      ) : (
        <WebView
          source={{ uri: apptUrl }}
          containerStyle={StyleSheet.absoluteFillObject}
          injectedJavaScript={CLEANUP_SCHEDULE_VIEW_JAVASCRIPT}
          startInLoadingState
        />
      )}
    </Screen>
  );
};

import React, { FC, ReactElement, useState } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';
import Toast from 'react-native-root-toast';
import DateTimePicker from 'react-native-modal-datetime-picker';
import dayjs from 'dayjs';
import { Icon } from 'react-native-elements';

import { colors } from '../../styles';
import { Container } from '../../components/Container';
import { t, DEFAULT_ICON_SIZE } from '../../utils';
import { TextInput } from '../../components/TextInput';
import { Button } from '../../components/Button';
import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { useEditMyKickCounterMutation, useCreateMyKickCounterMutation } from '../../graphql/types';
import { Touchable } from '../../components/Touchable';
import { Text } from '../../components/Text';

export const KickTrackingEditContent: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const currentKickTracking = navigation.getParam('currentKickTracking');
  const isEditMode = navigation.getParam('isEditMode');
  const onRefresh = navigation.getParam('onRefresh');

  let dateTime = new Date();

  if (currentKickTracking.startAt) {
    dateTime = new Date(currentKickTracking.startAt);
  }

  const [kicks, setKicks] = useState(currentKickTracking.kicks || '');
  const [duration, setDuration] = useState(currentKickTracking.duration || '');
  const [startAt, setStartAt] = useState(dateTime);
  const [isDateTimePickerVisible, setDateTimePickerVisible] = useState(false);

  const inputStyle = {
    fontSize: 3,
    height: 44,
    justifyContent: 'center',
    letterSpacing: 0.2,
    borderColor: colors.gainsboro,
  };

  const [createKickCounter, { loading: isSaving }] = useCreateMyKickCounterMutation();
  const [updateKickCounter, { loading: isUpdating }] = useEditMyKickCounterMutation();

  const handleShowDateTimePicker = (): void => {
    setDateTimePickerVisible(true);
  };

  const handleHideDateTimePicker = (): void => {
    setDateTimePickerVisible(false);
  };

  const handleConfirmDateTime = (value): void => {
    setStartAt(value);
    handleHideDateTimePicker();
  };

  const handleChangeText = (value: string, key: string): void => {
    if (!isNaN(Number(value))) {
      if (key === 'duration') {
        setDuration(value);
      }

      if (key === 'kicks') {
        setKicks(value);
      }
    }
  };

  const showToastView = (message: string): void => {
    Toast.show(message, {
      duration: 5000,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  };

  const handleResetState = (): void => {
    setKicks('');
    setDuration('');
    setStartAt(new Date());
  };

  const handleUpdate = (): void => {
    if (!kicks || !duration || !startAt) {
      showToastView(t('maternity.tracking.alert.allFieldsRequired'));
    } else {
      updateKickCounter({
        variables: {
          id: currentKickTracking.id,
          duration: Number(duration),
          startAt: startAt.toString(),
          kicks: Number(kicks),
        },
      })
        .then(() => {
          onRefresh();
          navigation.goBack();
        })
        .catch(() => {
          showToastView('Unable to update your counter. Please try again!!');
        });
    }
  };

  const handleAddKickCounter = (): void => {
    if (!kicks || !duration || !startAt) {
      showToastView(t('maternity.tracking.alert.allFieldsRequired'));
    } else {
      createKickCounter({
        variables: {
          startAt: startAt.toString(),
          kicks: Number(kicks),
          duration: Number(duration),
          isActive: false,
        },
      })
        .then(() => {
          onRefresh();
          handleResetState();
          navigation.goBack();
        })
        .catch(() => {
          showToastView('Unable to add manual counter. Please try again!!');
        });
    }
  };

  const renderKickTrackingContent = (): ReactElement => {
    const dateVal = dayjs(startAt).format('MMM DD - hh:mm:ss a');

    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        contentContainerStyle={{
          flexGrow: 1,
          height: '100%',
          paddingLeft: 4,
        }}
      >
        <Container mx={4}>
          <Container mb={4} display="flex">
            {dateVal ? (
              <Text color={colors.silver} fontSize={2} marginVertical={0.5}>
                {t('maternity.tracking.field.placeholder.date')}
              </Text>
            ) : null}
            <Touchable onPress={handleShowDateTimePicker}>
              <Container fullWidth my={1}>
                <Container
                  borderWidth={1}
                  display="flex"
                  alignItems="center"
                  flexDirection="row"
                  {...inputStyle}
                  justifyContent="space-between"
                  px={4}
                >
                  <Text>{dateVal}</Text>
                  <Icon
                    name="caret-down"
                    type="font-awesome"
                    size={DEFAULT_ICON_SIZE}
                    color={colors.black}
                  />
                </Container>
              </Container>
            </Touchable>

            <DateTimePicker
              isVisible={isDateTimePickerVisible}
              date={startAt}
              cancelTextIOS={t('maternity.tracking.buttons.cancel')}
              confirmTextIOS={t('maternity.tracking.buttons.confirm')}
              maximumDate={new Date()}
              mode="datetime"
              onConfirm={handleConfirmDateTime}
              onCancel={handleHideDateTimePicker}
            />
          </Container>

          <Container mb={4}>
            <TextInput
              placeholder={t('maternity.tracking.field.placeholder.kicks')}
              value={kicks.toString()}
              onChangeText={(value): void => handleChangeText(value, 'kicks')}
              topLabel={kicks ? t('maternity.tracking.field.placeholder.kicks') : null}
              {...inputStyle}
            />
          </Container>
          <TextInput
            placeholder={t('maternity.tracking.field.placeholder.duration')}
            value={duration.toString()}
            onChangeText={(value): void => handleChangeText(value, 'duration')}
            topLabel={duration ? t('maternity.tracking.field.placeholder.duration') : null}
            {...inputStyle}
          />
        </Container>
      </ScrollView>
    );
  };

  const renderFooterContent = (): ReactElement => {
    const updateBtnLabel = isEditMode
      ? isUpdating && !!kicks
        ? 'maternity.tracking.buttons.updating'
        : 'maternity.tracking.buttons.update'
      : isSaving
      ? 'maternity.tracking.buttons.saving'
      : 'maternity.tracking.buttons.save';

    const isDisabled = isEditMode ? isUpdating : isSaving;

    return (
      <Container height={60} mb={2} justifyContent="center" alignItems="center">
        <Button
          disabled={isDisabled}
          label={t(updateBtnLabel)}
          backgroundColor={colors.trimesterActive}
          color={colors.white}
          borderRadius={20}
          width={100}
          height={43}
          onPress={isEditMode ? handleUpdate : handleAddKickCounter}
        />
      </Container>
    );
  };

  const renderScreenContent = (): ReactElement => {
    return (
      <Container pt={4} height="100%" pb={50}>
        {renderKickTrackingContent()}
        {renderFooterContent()}
      </Container>
    );
  };

  return (
    <LayeredContainerScreen
      screenTitle={t(
        isEditMode
          ? 'maternity.tracking.edit.screenTitle'
          : 'maternity.tracking.addManual.screenTitle'
      )}
      headerColor={isEditMode ? colors.tangerineYellow : colors.turquoiseBlue}
      headerFontColor={colors.white}
      containerColor={colors.white}
    >
      {renderScreenContent()}
    </LayeredContainerScreen>
  );
};

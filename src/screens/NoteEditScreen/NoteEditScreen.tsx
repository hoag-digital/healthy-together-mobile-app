import React, { FC, ReactElement, useState } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';
import dayjs from 'dayjs';
import Toast from 'react-native-root-toast';

import { colors } from '../../styles';
import { Container } from '../../components/Container';
import { t } from '../../utils';
import { TextInput } from '../../components/TextInput';
import { Note, useEditMyNoteMutation } from '../../graphql/types';
import { Button } from '../../components/Button';
import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { Text } from '../../components/Text';

export const NoteEditScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const myCurrentNote: Note = navigation.getParam('myCurrentNote');
  const onRefresh = navigation.getParam('onRefresh');

  const [note, setNote] = useState(myCurrentNote.contents || '');

  const [editMyNote, { loading: isUpdatingNote }] = useEditMyNoteMutation();

  const showToastView = (message: string): void => {
    Toast.show(message, {
      duration: 5000,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  };

  const handleUpdate = async (): Promise<void> => {
    if (note && myCurrentNote.id) {
      try {
        await editMyNote({
          variables: {
            id: myCurrentNote.id,
            contents: note.trim(),
          },
        });

        onRefresh();
        showToastView('Note updated successfully!!');
      } catch (error) {
        showToastView('An error has occured, please try again to update the note!!');
      }
    }
  };

  const handleChange = (value: string): void => {
    setNote(value);
  };

  const renderHeaderContent = (): ReactElement => {
    const currentDate = myCurrentNote.createdAt
      ? dayjs(Number(myCurrentNote.createdAt)).format('MMMM DD YYYY')
      : '';

    return (
      <Container
        display="flex"
        height={40}
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        mt={1}
      >
        <Container bg={colors.tangerineYellow} height={3} width={70} />
        <Container bg={colors.white} height={30} p={1} m={2}>
          <Text fontWeight="700">{currentDate}</Text>
        </Container>
        <Container bg={colors.tangerineYellow} height={3} width={70} />
      </Container>
    );
  };

  const renderNoteContent = (): ReactElement => {
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        contentContainerStyle={{
          flexGrow: 1,
          height: '100%',
          paddingLeft: 4,
        }}
      >
        <TextInput
          placeholder={t('maternity.notes.createNote.placeholder')}
          borderColor={colors.white}
          value={note}
          onChangeText={handleChange}
          multiline
          fontSize={3}
          letterSpacing={0.2}
        />
      </ScrollView>
    );
  };

  const renderFooterContent = (): ReactElement => {
    const updateBtnLabel =
      isUpdatingNote && !!note
        ? 'maternity.notes.editNote.updatingLabel'
        : 'maternity.notes.editNote.updateLabel';

    return (
      <Container height={60} mb={2} justifyContent="center" alignItems="center">
        <Button
          disabled={isUpdatingNote}
          label={t(updateBtnLabel)}
          backgroundColor={colors.trimesterActive}
          color={colors.white}
          borderRadius={20}
          width={100}
          height={43}
          onPress={handleUpdate}
        />
      </Container>
    );
  };

  const renderScreenContent = (): ReactElement => {
    return (
      <Container pt={4} height="100%" pb={50}>
        {renderHeaderContent()}
        {renderNoteContent()}
        {renderFooterContent()}
      </Container>
    );
  };

  return (
    <LayeredContainerScreen
      screenTitle={t('maternity.notes.editNote.screenTitle')}
      headerColor={colors.tangerineYellow}
      headerFontColor={colors.white}
      containerColor={colors.white}
    >
      {renderScreenContent()}
    </LayeredContainerScreen>
  );
};

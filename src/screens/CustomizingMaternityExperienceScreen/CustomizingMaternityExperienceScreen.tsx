import React, { FC, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import styled from '@emotion/native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import * as Animatable from 'react-native-animatable';
import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import { colors, space } from '../../styles';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { t } from '../../utils';
import { Text } from '../../components/Text';
import backgroundImg from '../../assets/images/pink-background.png';
import Logo from '../../assets/images/hoag-logo-white.svg';
import {
  getLastMenstrualPeriod,
  getEstimatedDueDate,
  getUserStatus,
  clearAllMaternityOnboarding,
} from '../../utils/maternityOnboarding';
import { MeDocument, useCreateMaternityProfileMutation } from '../../graphql/types';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const CustomizingMaternityExperienceScreen: FC<NavigationStackScreenProps> = ({
  navigation,
}) => {
  const [createProfile] = useCreateMaternityProfileMutation();
  const [isDone, setIsDone] = useState(false);

  useEffect(() => {
    async function setProfile(): Promise<void> {
      try {
        const lmp = await getLastMenstrualPeriod();
        const edd = await getEstimatedDueDate();
        const role = await getUserStatus();

        await createProfile({
          variables: {
            lastMenstrualPeriod: dayjs(lmp).format('YYYY-MM-DD'),
            estimatedDueDate: dayjs(edd).format('YYYY-MM-DD'),
            role,
          },
          refetchQueries: [{ query: MeDocument }],
        });

        // clearing out the asyncstorage once the maternity profile has been created
        clearAllMaternityOnboarding();
      } catch (error) {}
    }

    if (!isDone) {
      setIsDone(true);
      setProfile();
    }

    setTimeout(async () => {
      navigation.navigate('Maternity');
    }, 4000);
  }, [isDone, createProfile, navigation]);

  return (
    <BackgroundImage source={backgroundImg} resizeMode="stretch">
      <Screen testID="customizeMaternityExperienceScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Animatable.View
            animation="fadeOutUp"
            easing="ease-in-out"
            duration={1000}
            delay={500}
            style={{ paddingTop: space[7], position: 'absolute', top: 200 }}
          >
            <Text color={colors.white} fontSize={6} textAlign="center">
              {t('maternity.onboarding.customization.line1')}
            </Text>
          </Animatable.View>
          <Animatable.View
            animation="fadeInUp"
            duration={3000}
            delay={1000}
            style={{ paddingTop: space[7], position: 'absolute', top: 200 }}
          >
            <Text color={colors.white} fontSize={6} textAlign="center">
              {t('maternity.onboarding.customization.line2')}
            </Text>
          </Animatable.View>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};

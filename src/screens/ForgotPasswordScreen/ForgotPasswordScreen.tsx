import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t } from '../../utils';
import {
  REMOVE_FORGOT_PASSWORD_LINKS,
  REMOVE_HEADER_FOOTERS_HOAG_IO,
} from '../../lib/InjectedJavascript';
import { useWebUrlsQuery } from '../../graphql/types';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

export const ForgotPasswordScreen: FC<NavigationStackScreenProps> = () => {
  const { error: webUrlsError, data: webUrlsData, loading: webUrlsLoading } = useWebUrlsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const passwordResetUrl = webUrlsData?.webUrls?.passwordReset || '';

  if (webUrlsError) {
    throw new Error('Error loading URLS');
  }

  return (
    <Screen
      testID="forgot-password-screen"
      modalHeader
      screenTitle={t('login.passwordReset')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      {webUrlsLoading ? (
        <CenteredLoadingSpinner />
      ) : (
        <WebView
          source={{ uri: passwordResetUrl }}
          containerStyle={StyleSheet.absoluteFillObject}
          startInLoadingState
          injectedJavaScript={REMOVE_FORGOT_PASSWORD_LINKS}
        />
      )}
    </Screen>
  );
};

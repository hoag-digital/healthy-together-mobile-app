import React, { FC, ReactElement } from 'react';
import { StatusBar, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { DetailsScreenLabel } from '../../components/DetailsScreenLabel';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import GenericNewsImage from '../../assets/images/generic_news.jpg';
import { DEFAULT_ICON_SIZE, SCROLLVIEW_BOTTOM_PADDING, SCREEN_HEIGHT, t } from '../../utils';
import { PostRichText } from '../../components/PostRichText';
import { useContentByIdQuery } from '../../graphql/types';
import { ContentErrorToast } from '../../components/Toastr';
import { Button } from '../../components/Button';

const IMAGE_HEIGHT = SCREEN_HEIGHT * 0.4;

/**
 * This screen shows the details of a content (aka wordpress post with post type APP CONTENT)
 */
export const ContentDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const postId = navigation.getParam('postId');
  const hideLabel = navigation.getParam('hideLabel');
  const hideImage = navigation.getParam('hideImage');
  const currentIndex = navigation.getParam('currentIndex');
  const changeIndex = navigation.getParam('changeIndex');
  const showButton = navigation.getParam('showButton');
  const isLastItem = navigation.getParam('isLastItem');
  const overlayHeight = IMAGE_HEIGHT / (hideImage ? 4 : 2);

  let content;

  const goBack = (): void => {
    navigation.goBack();
  };

  const { loading, error, data, refetch } = useContentByIdQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      postId,
    },
  });

  if (error) {
    <ContentErrorToast />;
  }

  const renderButtons = !!showButton && (
    <Container flexDirection="row" width="100%" paddingLeft={10} marginBottom={10}>
      {currentIndex !== 0 && (
        <Button
          flex={1}
          py={3}
          px={6}
          marginRight={10}
          color={colors.white}
          backgroundColor={colors.wisteria}
          borderRadius={2}
          label="Previous"
          onPress={(): void => changeIndex(currentIndex - 1)}
        />
      )}
      {!isLastItem && (
        <Button
          flex={1}
          py={3}
          px={6}
          marginRight={10}
          color={colors.white}
          backgroundColor={colors.wisteria}
          borderRadius={2}
          label="Next"
          onPress={(): void => changeIndex(currentIndex + 1)}
        />
      )}
    </Container>
  );

  const renderContentDetailsScreenContents = (): ReactElement => {
    if (loading || !Object.keys(data).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    const contentToRender = content || (data ? data.contentById : {});
    const { title, rawHtml, mediumImage, isFeatured } = contentToRender;

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        >
          <Container flex={1} pb={SCROLLVIEW_BOTTOM_PADDING}>
            <Touchable
              position="absolute"
              // TODO: future-improvement -> dynamic positioning
              top={45}
              right={20}
              testID="close-icon"
              onPress={goBack}
              bg={colors.white}
              height={DEFAULT_ICON_SIZE}
              width={DEFAULT_ICON_SIZE}
              borderRadius={DEFAULT_ICON_SIZE}
              zIndex={50}
            >
              <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
            </Touchable>
            <TransparencyImageOverlay height={overlayHeight} />
            {!hideImage ? (
              <FastImage
                accessibilityLabel={`${title} image`}
                style={{
                  width: '100%',
                  height: IMAGE_HEIGHT,
                  resizeMode: 'cover',
                }}
                source={mediumImage ? { uri: mediumImage } : GenericNewsImage}
              />
            ) : (
              <Container bg={colors.transparent} width="100%" height={overlayHeight} />
            )}
            {!hideLabel ? (
              <DetailsScreenLabel
                backgroundColor={isFeatured ? colors.sunsetOrange : colors.summerSky}
                // TODO we need to update the label, when we have app specific content the label may not make sense at all
                label={isFeatured ? t('news.featuredLabel') : t('news.label')}
                iconType="post"
              />
            ) : null}
            <Text pt={5} pb={2} mx={5} fontSize={5} fontWeight="bold">
              {title}
            </Text>
            {rawHtml && <PostRichText richText={rawHtml} />}
          </Container>
          {renderButtons}
        </ScrollView>
      </>
    );
  };

  return (
    <Screen
      testID="content-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderContentDetailsScreenContents()}
    </Screen>
  );
};

export default ContentDetailsScreen;

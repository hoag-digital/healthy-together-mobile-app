import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';

import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';

import { colors } from '../../styles';

export const ClassesWebViewScreen: FC<NavigationStackScreenProps> = () => {
  const classesURL = 'https://www.hoag.org/community-education-classes/';

  return (
    <Screen
      testID="classes-webview-screen"
      modalHeader
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      <WebView source={{ uri: classesURL }} containerStyle={StyleSheet.absoluteFillObject} />
    </Screen>
  );
};

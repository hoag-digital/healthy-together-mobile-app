import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';

import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';

import { colors } from '../../styles';

export const BioCircutWebViewScreen: FC<NavigationStackScreenProps> = () => {
  const biocircutURL = 'https://hoagfoothillranch.com/biocircuit/';

  return (
    <Screen
      testID="biocircut-webview-screen"
      modalHeader
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={0}
    >
      <WebView source={{ uri: biocircutURL }} containerStyle={StyleSheet.absoluteFillObject} />
    </Screen>
  );
};

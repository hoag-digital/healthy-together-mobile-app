/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { FC, ReactElement } from 'react';
import { RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';
import { colors } from '../../styles';
import { useContentByIdQuery } from '../../graphql/types';
import { Button } from '../../components/Button';

/**
 * This screen shows the details of a sunsetting post
 * This component was based on the PromoDetailsScreen
 */
export const SunsettingDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const { loading, error, refetch } = useContentByIdQuery({
    fetchPolicy: 'cache-and-network',
  });

  if (error) {
    // TODO: handle graphql errors
  }

  const expirationDate = new Date('10/30/2021');
  const sunsetted = new Date() >= expirationDate;

  const goBack = (): void => {
    navigation.navigate('Home');
  };

  const renderSunsettingScreenContents = (): ReactElement => {
    if (loading) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        >
          <Container flex={1} pt={10} pb={35}>
            <Text mt={15} mx={5} fontSize={4}>
              Thank you for being a valued user of the{' '}
              <Text fontSize={4} fontStyle="italic">
                Hoag Healthy Together
              </Text>{' '}
              app. Starting in September, the Hoag Healthy Together App will no longer be supported.
            </Text>
            <Text mt={15} mx={5} fontSize={4}>
              We are taking the time to bring you new and improved experiences to help you care for
              you and your loved ones. We are taking the app down from the app stores. This means
              that we will no longer support updates and continued functionality of this specific
              app.
            </Text>
            <Text mt={15} mx={5} fontSize={4}>
              What will happen when I open my app?
            </Text>
            <Text mt={1} mx={5} fontSize={4}>
              If you have your app already on your mobile device, your app will continue to open and
              provide content, tools, and resources until October 30th. During this time, there will
              be no additional updates to those content, tools, and resources.
            </Text>
            <Text mt={15} mx={5} fontSize={4}>
              Starting October 30th, you will no longer be able to access the Hoag Healthy Together{' '}
              <Text fontSize={4} fontStyle="italic">
                app
              </Text>
              . Please visit{' '}
              <Text
                fontSize={4}
                fontWeight="bold"
                color={colors.malibu}
                onPress={() => navigation.navigate('HoagWebView')}
              >
                Hoag.org
              </Text>{' '}
              for information on{' '}
              <Text
                fontSize={4}
                fontWeight="bold"
                color={colors.malibu}
                onPress={() => navigation.navigate('ClassesWebView')}
              >
                live and virtual classes
              </Text>
              , the{' '}
              <Text
                fontSize={4}
                fontWeight="bold"
                color={colors.malibu}
                onPress={() => navigation.navigate('BioCircutWebView')}
              >
                Biocircuit
              </Text>{' '}
              or maternity information needs.
            </Text>
            <Text mt={15} mx={5} fontSize={4} fontStyle="italic">
              Who should I contact from Hoag team for support with my health journey?
            </Text>
            <Text mt={1} mx={5} fontSize={4}>
              While there are transitions happening with your app, the support{' '}
              <Text mt={15} fontSize={4} fontStyle="italic">
                Hoag
              </Text>{' '}
              provides remains. You can access all our services at{' '}
              <Text
                fontSize={4}
                fontWeight="bold"
                color={colors.malibu}
                onPress={() => navigation.navigate('BioCircutWebView')}
              >
                Hoag.org
              </Text>
              .
            </Text>
            {!sunsetted ? (
              <Button
                width="20%"
                mx={170}
                mt={4}
                py={2}
                label="Okay"
                color={colors.white}
                borderRadius={5}
                onPress={goBack}
              />
            ) : null}
          </Container>
        </ScrollView>
      </>
    );
  };

  return (
    <Screen
      testID="sunsetting-details-screen"
      screenHeader
      drawerEnabled={false}
      loginBanner={false}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
      backgroundColor={colors.transparent}
    >
      {renderSunsettingScreenContents()}
    </Screen>
  );
};

export default SunsettingDetailsScreen;

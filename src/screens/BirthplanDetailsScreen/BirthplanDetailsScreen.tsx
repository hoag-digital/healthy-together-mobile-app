import React, { createRef, FC, ReactNode, ReactElement } from 'react';
import { ScrollView, RefreshControl } from 'react-native';
import { Icon } from 'react-native-elements';
import ViewShot, { captureRef } from 'react-native-view-shot';
import Share from 'react-native-share';
import { asEnumerable } from 'linq-es2015';

import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { FloatingButton } from '../../components/FloatingButton';
import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { useMyBirthplanQuery } from '../../graphql/types';
import { SCREEN_WIDTH, SCROLLVIEW_BOTTOM_PADDING, t } from '../../utils';
import { ContentErrorToast } from '../../components/Toastr';

interface MyPlanCategory {
  category: string;
  count: number;
  items: [
    {
      title: string;
    }
  ];
}

export const BirthplanDetailsScreen: FC = ({}) => {
  const { data = {}, error, loading, refetch } = useMyBirthplanQuery();

  const _viewShotRef = createRef<ViewShot>();
  const shareTitle = t('maternity.birthplan.share.title');
  const message = t('maternity.birthplan.share.message');

  const sharePlan = (): void => {
    captureRef(_viewShotRef, {
      format: 'png',
      quality: 0.8,
      result: 'data-uri',
    })
      .then(uri => {
        const shareOptions = {
          title: shareTitle,
          subject: shareTitle,
          message: message,
          url: uri,
          failOnCancel: false,
        };

        Share.open(shareOptions);
      })
      .catch(err => console.log('User did not share', err));
  };

  const renderCategory = (category: MyPlanCategory): ReactNode => {
    return (
      <>
        <Container flexDirection="row" justifyContent="flex-start" pt={3}>
          <Text fontSize={4} fontWeight="700">
            {category.category}
          </Text>
        </Container>
        {category.items.map(item => {
          return (
            <Container
              borderBottomWidth={1}
              py={2}
              mx={5}
              borderBottomColor={colors.solitude}
              flexDirection="row"
              alignItems="flex-start"
            >
              <Text fontSize={3}>{item.title}</Text>
            </Container>
          );
        })}
      </>
    );
  };

  const renderBirthplan = (): ReactNode => {
    if (loading && !Object.keys(data).length) {
      return <CenteredLoadingSpinner />;
    }

    if (error) {
      <ContentErrorToast />;
    }

    const { myBirthplan } = data;

    // reference: https://github.com/ENikS/LINQ/wiki/groupby
    const groupedData = asEnumerable(myBirthplan)
      .GroupBy(
        item => item.category,
        b => b,
        (key, items): MyPlanCategory => {
          return {
            category: key,
            count: items.length,
            items: asEnumerable(items).ToArray(),
          };
        }
      )
      .ToArray();

    return groupedData.map((category: MyPlanCategory) => renderCategory(category));
  };

  return (
    <LayeredContainerScreen
      screenTitle={t('maternity.birthplan.title')}
      headerColor={colors.wisteria}
      headerFontColor={colors.white}
      containerColor={colors.white}
    >
      <ScrollView
        scrollIndicatorInsets={{
          right: 1,
        }}
        style={{
          width: SCREEN_WIDTH,
          backgroundColor: colors.transparent,
          paddingLeft: 10,
        }}
        refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <ViewShot ref={_viewShotRef} options={{ format: 'jpg', quality: 0.9 }}>
          <Container pb={SCROLLVIEW_BOTTOM_PADDING} bg={colors.white}>
            {renderBirthplan()}
          </Container>
        </ViewShot>
      </ScrollView>
      <FloatingButton
        borderRadius={50}
        width={0.15}
        onPress={sharePlan}
        icon={(): ReactElement => <Icon name="share" color={colors.white} />}
        backgroundColor={colors.cornflowerBlue2}
      />
    </LayeredContainerScreen>
  );
};

import React, { FC, useEffect } from 'react';
import styled from '@emotion/native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';

import { colors, fonts } from '../../styles';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { t } from '../../utils';
import { Text } from '../../components/Text';
import Logo from '../../assets/images/hoag-logo-white.svg';
import bgImage from '../../assets/images/biocircuit/confirmation.jpg';

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

export const BiocircuitWelcomeScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  useEffect(() => {
    setTimeout(async () => {
      navigation.navigate('Biocircuit');
    }, 2500); //TODO: figure out the best time to give the illusion of loading/customizing even at 2.5 seconds it feels pretty quick
  });

  return (
    <BackgroundImage source={bgImage} resizeMode="stretch">
      <Screen testID="welcomeScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Text fontSize={5} color={colors.white} py={10} {...fonts.medium}>
            {t('biocircuit.welcome.title')}
          </Text>
          <Text fontSize={6} color={colors.white} py={10} {...fonts.medium}>
            {t('biocircuit.welcome.subtitle')}
          </Text>
          <Container style={{ position: 'absolute', top: 275 }}>
            <LottieView
              source={require('../../assets/animations/heartbeat.json')}
              autoPlay={true}
              loop={true}
              speed={0.5}
              enableMergePathsAndroidForKitKatAndAbove
              style={{ height: 150, width: 150, backgroundColor: colors.transparent }}
            />
          </Container>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};

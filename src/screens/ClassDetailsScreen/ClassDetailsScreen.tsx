import * as Sentry from '@sentry/react-native';
import React, { FC, useState, ReactElement } from 'react';
import { StatusBar, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { Divider, Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-community/async-storage';
import dayjs from 'dayjs';

import { DetailsScreenLabel } from '../../components/DetailsScreenLabel';
import { ClassReservationModal } from '../../components/ClassReservationModal';
import { EventListItem } from '../../components/EventListItem';
import { VirtualEventListItem } from '../../components/VirtualEventListItem';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import {
  Event,
  MyScheduledEventsDocument,
  useEventRegistrationMutation,
  useHoagEventGroupsQuery,
  useMyScheduledEventsQuery,
} from '../../graphql/types';
import {
  ASYNCSTORAGE_FCM_KEY,
  DEFAULT_ICON_SIZE,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_HEIGHT,
  t,
} from '../../utils';
import GenericClassImage from '../../assets/images/generic_class.jpg';
import { ErrorModal } from '../../components/ErrorModal';
import { trackClassReservation } from '../../services/analytics';
import { NotificationsConsumer } from '../../services/notifications';
import { ContentErrorToast } from '../../components/Toastr';
import { PostRichText } from '../../components/PostRichText';

const IMAGE_HEIGHT = SCREEN_HEIGHT * 0.4;
const dividerStyle = { backgroundColor: colors.silver, marginVertical: 15 };

const classIsFull = (event: Event): boolean => !event.spacesRemaining;

const isRegistered = (events: Event[], postId): boolean =>
  events.some(event => event.postId === postId);

/**
 * This screen displays the expanded details of an eventGroup (referred to as a "class" to the user)
 * including its full description, image, and available slot times for user registration.
 */
export const ClassDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const eventGroupName = navigation.getParam('eventGroupName');
  const eventCategoryName = navigation.getParam('eventCategoryName');
  const eventCategoryId = navigation.getParam('eventCategoryId');

  const [registerForEvent, { loading: isRegistering }] = useEventRegistrationMutation({
    refetchQueries: [
      {
        query: MyScheduledEventsDocument,
      },
    ],
    // Don't complete the mutation until the new data is done refetching
    awaitRefetchQueries: true,
  });

  const {
    error: eventGroupError,
    data: eventGroupData = {},
    loading: eventGroupLoading,
    refetch: refetchEventGroup,
  } = useHoagEventGroupsQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      eventGroupName,
    },
  });

  const IS_ALL_EVENT_PAST = eventGroupData.eventGroup?.events?.every(
    (event: any): boolean => new Date(event.date) < new Date()
  );

  const DOES_ONE_FUTURE_EVENT_HAVE_YOUTUBE_LINK = eventGroupData.eventGroup?.events?.some(
    (event: any): boolean => new Date(event.date) > new Date() && event.youtubeLiveLink
  );

  const IS_ONDEMAND = eventGroupData.eventGroup?.events?.some(
    (event: any): boolean => event.isOnDemand
  );

  const LABEL =
    IS_ALL_EVENT_PAST && IS_ONDEMAND
      ? 'On-Demand'
      : DOES_ONE_FUTURE_EVENT_HAVE_YOUTUBE_LINK
      ? 'Live stream'
      : 'Event';

  const ICON =
    IS_ALL_EVENT_PAST && IS_ONDEMAND
      ? 'ondemand'
      : DOES_ONE_FUTURE_EVENT_HAVE_YOUTUBE_LINK
      ? 'class'
      : 'post';

  const {
    data: myEventsData = {
      myEvents: [],
    },
  } = useMyScheduledEventsQuery({
    fetchPolicy: 'cache-and-network',
  });

  // Note that a "classId" is actually an event.postId (the wordpress post id belonging to the wordpress event)
  const [selectedClassId, selectClassId] = useState(null);
  const [reservationModalVisible, setReservationModalVisible] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const goBack = (): void => {
    navigation.goBack();
  };

  // This function opens the class reservation modal (Step 1)
  // NOTE: this "classId" is actually the "postId" of the individual event
  const initiateClassReservation = (postId?: string | null): void => {
    if (!postId) return;

    selectClassId(postId);
    setReservationModalVisible(true);
  };

  // This is the callback function invoked when a class is reserved from the modal (Step 2)
  const onReserveClass = async (postId: string, notificationsContext: any): Promise<string> => {
    // TODO: put in a try/catch block and handle failure, when we have an error message UI.
    try {
      const success = await registerForEvent({
        variables: {
          postId,
        },
      });

      // TODO: move to pulling the reservedClass from refetched scheduled events as a security measure
      // const hoagClass =
      //   myEventsData?.myEvents?.find((event: Event): boolean => event.postId === classId) ??
      //   ({} as Event);

      const reservedClass =
        eventGroupData?.eventGroup?.events?.find(
          (event: Event): boolean => event.postId === postId
        ) ?? ({} as Event);

      // tracking the class reservation in analytics
      trackClassReservation(reservedClass);

      setReservationModalVisible(false);

      navigation.navigate('ReservationConfirmation', {
        type: 'class',
        reservation: {
          date: reservedClass.date,
          startTime: reservedClass.startTime,
          endTime: reservedClass.endTime,
        },
        actions: {
          primary: StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Home',
                action: NavigationActions.navigate({
                  routeName: 'Schedule',
                }),
              }),
            ],
          }),
          secondary: NavigationActions.navigate({
            routeName: 'Classes',
          }),
        },
      });

      const fcmToken = await AsyncStorage.getItem(ASYNCSTORAGE_FCM_KEY);

      // if there is no fcmToken saved, the notifications need to be initialized
      if (!fcmToken) {
        notificationsContext.setNotificationsShouldBeInitialized(true);
      }

      Promise.resolve(success);
    } catch (error) {
      Sentry.captureException(error);

      const message =
        error.toString() === `Error: GraphQL error: This Class is at capacity`
          ? t('errors.classCapacity')
          : t('errors.general');

      setErrorMessage(message);
      setReservationModalVisible(false);
      setTimeout(() => {
        // Error Modal will not appear when other modal is present and it takes a moment
        setErrorModalVisible(true);
      }, 400);
    }
  };

  // TODO: add a use effect to wipe the selected class time when the modal becomes invisible...
  const renderClassReservationModal = (): ReactElement => {
    const eventGroup = eventGroupData ? eventGroupData.eventGroup : {};
    const { name, mediumImage, events } = eventGroup;

    // TODO: test this for synchronicity issues
    const selectedClass = events.find(({ postId }) => postId === selectedClassId) || ({} as Event);

    return (
      <NotificationsConsumer>
        {(notificationsContext): ReactElement => {
          return (
            <ClassReservationModal
              isRegistering={isRegistering}
              className={name}
              isVisible={reservationModalVisible}
              setIsVisible={setReservationModalVisible}
              classImageUrl={mediumImage}
              classTimeId={selectedClassId}
              startTime={selectedClass.startTime}
              endTime={selectedClass.endTime}
              date={selectedClass.date}
              onReserveClass={(postId): Promise<string> =>
                onReserveClass(postId, notificationsContext)
              }
            />
          );
        }}
      </NotificationsConsumer>
    );
  };

  const renderErrorModal = (): ReactElement => {
    return (
      <ErrorModal
        error={errorMessage}
        isVisible={errorModalVisible}
        setIsVisible={setErrorModalVisible}
        buttonColor={colors.malibu}
      />
    );
  };

  const renderEventListItem = (event: Event): ReactElement | null => {
    // temprorarily filter out events that already occurred
    // todo: move logic to the backend
    if (dayjs(event.startTime).isBefore(dayjs())) {
      return null;
    }

    const myEvents = myEventsData ? myEventsData.myEvents : [];

    const classId = event.postId;

    // we are passing all accumulated navigation params so that we can recreate the navigation
    // ...history when redirected to and from login/account creation screens
    const navigationParams = {
      eventCategoryName,
      eventGroupName,
      eventCategoryId,
    };

    // There is separate UI behavior for virtual and in-facility event types
    if (event.isVirtual) {
      return (
        <VirtualEventListItem
          key={event.id}
          {...event}
          isFull={classIsFull(event)}
          isRegistered={isRegistered(myEvents, classId)}
          initiateClassReservation={initiateClassReservation}
          spacesRemaining={event.spacesRemaining}
          navigationParams={navigationParams}
        />
      );
    }

    return (
      <EventListItem
        key={event.id}
        {...event}
        isFull={classIsFull(event)}
        isRegistered={isRegistered(myEvents, classId)}
        initiateClassReservation={initiateClassReservation}
        spacesRemaining={event.spacesRemaining}
        navigationParams={navigationParams}
      />
    );
  };

  const renderClassDetailsScreenContents = (): ReactElement => {
    if (eventGroupLoading && !Object.keys(eventGroupData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    const eventGroup = eventGroupData ? eventGroupData.eventGroup : {};

    const { name, description, mediumImage, events, rawHtml } = eventGroup;

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        {renderClassReservationModal()}
        {renderErrorModal()}
        <Touchable
          position="absolute"
          // TODO: future-improvement -> dynamic positioning
          top={45}
          right={20}
          testID="close-icon"
          onPress={goBack}
          bg={colors.white}
          height={DEFAULT_ICON_SIZE}
          width={DEFAULT_ICON_SIZE}
          borderRadius={DEFAULT_ICON_SIZE}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
        </Touchable>
        <Container position="relative">
          <TransparencyImageOverlay height={IMAGE_HEIGHT / 2} />
          <FastImage
            accessibilityLabel={`${name} image`}
            style={{
              resizeMode: 'cover',
              aspectRatio: 16 / 9,
            }}
            source={mediumImage ? { uri: mediumImage } : GenericClassImage}
          />
          {/*
            Hardcoding all to livestream for now.
            Need to add logic to render classes.details.liveStreamLabel.
            The issue is that this screen pertains to an event group which can contain both class types...
          */}
          <DetailsScreenLabel iconType={ICON} label={LABEL} />
        </Container>
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={
            <RefreshControl refreshing={eventGroupLoading} onRefresh={refetchEventGroup} />
          }
        >
          <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
            <Text pb={2} fontSize={5} fontWeight="bold">
              {name}
            </Text>
            {rawHtml ? (
              <PostRichText richText={rawHtml} />
            ) : (
              <Text pb={2} fontSize={3} textAlign="justify">
                {description}
              </Text>
            )}

            <Divider style={dividerStyle} />
            {events.map(renderEventListItem)}
          </Container>
        </ScrollView>
      </>
    );
  };

  if (eventGroupError) {
    <ContentErrorToast />;
  }

  return (
    <Screen
      testID="class-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderClassDetailsScreenContents()}
    </Screen>
  );
};

import React, { FC, ReactElement } from 'react';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import { StatusBar, RefreshControl, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import dayjs from 'dayjs';

import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { SCROLLVIEW_BOTTOM_PADDING, DEFAULT_ICON_SIZE } from '../../utils';
import { Touchable } from '../../components/Touchable';
import { TransparencyImageOverlay } from '../../components/TransparencyImageOverlay';
import { useMyNoteContentByIdQuery } from '../../graphql/types';

export const NoteDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const noteId = navigation.getParam('noteId');

  const {
    error: myNoteContentByIdError,
    data: myNoteContentByIdData,
    loading: myNoteContentByIdLoading,
    refetch: onRefresh,
  } = useMyNoteContentByIdQuery({
    fetchPolicy: 'cache-and-network',
    variables: { id: noteId },
  });

  if (myNoteContentByIdError) {
    //TODO: handle graphql error
  }

  const goBack = (): void => {
    navigation.goBack();
  };

  const renderLoadingScreen = (): ReactElement => {
    return (
      <Container fill pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
        <Container fill fullWidth centerContent>
          <ActivityIndicator />
        </Container>
      </Container>
    );
  };

  const renderScreenContent = (): ReactElement => {
    if (myNoteContentByIdLoading || !Object.keys(myNoteContentByIdData || {}).length) {
      return renderLoadingScreen();
    }

    const note = myNoteContentByIdData?.myNoteContentById;
    const createdAt = note?.createdAt ? dayjs(Number(note.createdAt)).format('MMMM DD YYYY') : '';

    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        <Touchable
          position="absolute"
          top={45}
          right={20}
          testID="close-icon"
          onPress={goBack}
          bg={colors.white}
          height={DEFAULT_ICON_SIZE}
          width={DEFAULT_ICON_SIZE}
          borderRadius={DEFAULT_ICON_SIZE}
          zIndex={50}
        >
          <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} />
        </Touchable>
        <TransparencyImageOverlay height={80} />
        <Container bg={colors.transparent} width="100%" height={80} />

        <Container flexDirection="row" alignItems="center" justifyContent="center" height={50}>
          <Container bg={colors.tangerineYellow} height={3} width={70} />
          <Container bg={colors.white} p={1} m={2}>
            <Text fontWeight="700">{createdAt}</Text>
          </Container>
          <Container bg={colors.tangerineYellow} height={3} width={70} />
        </Container>
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
            paddingBottom: SCROLLVIEW_BOTTOM_PADDING,
          }}
          refreshControl={
            <RefreshControl refreshing={myNoteContentByIdLoading} onRefresh={onRefresh} />
          }
        >
          <Container pb={SCROLLVIEW_BOTTOM_PADDING} pl={6}>
            <Text font>{note?.contents || ''}</Text>
          </Container>
        </ScrollView>
      </>
    );
  };

  return (
    <Screen
      testID="note-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
    >
      {renderScreenContent()}
    </Screen>
  );
};

export default NoteDetailsScreen;

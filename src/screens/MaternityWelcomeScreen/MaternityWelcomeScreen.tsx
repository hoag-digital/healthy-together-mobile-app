import React, { FC, useEffect } from 'react';
import styled from '@emotion/native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { ImageBackground, StatusBar, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';

import { colors, fonts } from '../../styles';
import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { t } from '../../utils';
import { Text } from '../../components/Text';
import Logo from '../../assets/images/hoag-logo-white.svg';
import welcomeImg from '../../assets/images/welcome-bg.png';
import { useMeQuery } from '../../graphql/types';

const animationSize = 250;

const BackgroundImage = styled(ImageBackground)`
  ${StyleSheet.absoluteFillObject};
`;

enum NAVIGATION {
  ONBOARDING = 'MaternityOnboarding2', //TODO set to Onboarding1 when we have role selection again
  MATERNITY_HOME = 'Maternity',
  MATERNTIY_UNKNOWN = 'MaternityUnknown',
}

export const MaternityWelcomeScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  const { data, loading } = useMeQuery({
    fetchPolicy: 'cache-and-network',
  });

  const executeNavigation = (route: string): void => {
    navigation.navigate(route);
  };

  useEffect(() => {
    async function navigate(): Promise<void> {
      const user = data || {};
      const { me } = user;
      const onboarded = me && me.maternityProfile;
      // const hasRole = me?.maternityProfile?.role !== null;

      setTimeout(async () => {
        !onboarded
          ? executeNavigation(NAVIGATION.ONBOARDING)
          : // : hasRole
            executeNavigation(NAVIGATION.MATERNITY_HOME);
        // : executeNavigation(NAVIGATION.MATERNTIY_UNKNOWN);
      }, 2250);
    }

    if (data && !loading) {
      //this ensures that we have pulled users profile first
      navigate();
    }
  });

  return (
    <BackgroundImage source={welcomeImg} resizeMode="stretch">
      <Screen testID="welcomeScreen" transparent margin={20}>
        <StatusBar barStyle="light-content" />
        <Container justifyContent="center" alignItems="center" pt={100}>
          <Logo height="100" width="100" />
          <Text fontSize={5} color={colors.white} py={10} {...fonts.light}>
            {t('maternity.onboarding.welcome.title')}
          </Text>
          <Text fontSize={6} color={colors.chino} py={10} {...fonts.light}>
            {t('maternity.onboarding.welcome.subtitle')}
          </Text>
          <Container style={{ position: 'absolute', top: 300 }}>
            <LottieView
              source={require('../../assets/animations/teal-color-heart.json')}
              autoPlay={true}
              loop={true}
              speed={0.5}
              enableMergePathsAndroidForKitKatAndAbove
              style={{
                height: animationSize,
                width: animationSize,
                backgroundColor: colors.transparent,
              }}
            />
          </Container>
        </Container>
      </Screen>
    </BackgroundImage>
  );
};

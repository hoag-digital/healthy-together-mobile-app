import * as Sentry from '@sentry/react-native';
import React, { FC, useState, ReactElement, useEffect, ReactNode } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { ScrollView, RefreshControl } from 'react-native';
import dayjs from 'dayjs';
import { StackActions, NavigationActions } from 'react-navigation';

import { Screen } from '../../components/Screen';
import { RxTokenModal } from '../../components/RxTokenModal';
import { TokenInvalidModal } from '../../components/TokenInvalidModal';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';
import { ClassReservationModal } from '../../components/ClassReservationModal';
import { EventListItem } from '../../components/EventListItem';
import { ErrorModal } from '../../components/ErrorModal';
import { Container } from '../../components/Container';

import {
  Event,
  useBiocircuitEventsQuery,
  useEventRegistrationMutation,
  useMeQuery,
  useRedeemTokenMutation,
  useMyScheduledEventsQuery,
  MyScheduledEventsDocument,
  MeDocument,
} from '../../graphql/types';
import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING, t } from '../../utils';
import { trackClassReservation } from '../../services/analytics';
import { ContentErrorToast } from '../../components/Toastr';

const classIsFull = (event: Event): boolean => !event.spacesRemaining;

const isRegistered = (events: Event[], postId): boolean =>
  events.some(event => event.postId === postId);

export const BiocircuitDetailsScreen: FC<NavigationStackScreenProps> = ({ navigation }) => {
  // Note that a "classId" is actually an event.postId (the wordpress post id belonging to the wordpress event)
  const [selectedClassId, selectClassId] = useState(null);
  const [useRxToken] = useRedeemTokenMutation();
  const [hasRxToken, setHasRxToken] = useState(false);
  const [userRxToken, setUserRxToken] = useState('');
  const [rxTokenExpiration, setRxTokenExpiration] = useState();
  const [rxTokenError, setRxTokenError] = useState(false);
  const [rxTokenModalIsVisible, setRxTokenModalIsVisible] = useState(false);
  const [rxTokenInvalidModalIsVisible, setRxTokenInvalidModalIsVisible] = useState(true);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const { data: meData, loading: meLoading } = useMeQuery({
    fetchPolicy: 'network-only', // Need to be able to get latest token and caching was storing old value
  });

  const [registerForEvent, { loading: isRegistering }] = useEventRegistrationMutation({
    refetchQueries: [
      {
        query: MyScheduledEventsDocument,
      },
    ],
    // Don't complete the mutation until the new data is done refetching
    awaitRefetchQueries: true,
  });

  const {
    data: myEventsData = {
      myEvents: [],
    },
  } = useMyScheduledEventsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const {
    error: biocircuitGroupError,
    data: biocircuitGroupData = {},
    loading: biocircuitGroupLoading,
    refetch: refetchBiocircuitGroup,
  } = useBiocircuitEventsQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    async function persistRxToken(): Promise<void> {
      if (!meData || meLoading) return;

      const { tokenExpirationDate: rxTokenExpirationDate } = meData?.me;

      if (rxTokenExpirationDate === null) {
        setHasRxToken(false);
        setRxTokenModalIsVisible(true);

        return;
      }

      const validRxToken = dayjs(rxTokenExpirationDate).diff(dayjs(), 'day') > 0;
      setHasRxToken(validRxToken);
      setRxTokenModalIsVisible(!validRxToken);
      setRxTokenExpiration(rxTokenExpirationDate);
    }

    persistRxToken();
  }, [meData, meLoading, setHasRxToken]);

  const redeemRxToken = async (): Promise<void> => {
    try {
      //r eset tokenError in case it is a re-submit
      setRxTokenError(false);

      // for some reason linter is complaining about this implementation but it works fine and is same as login...
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const { data } = await useRxToken({
        variables: { token: userRxToken },
        refetchQueries: [
          {
            query: MeDocument,
          },
        ],
        // Don't complete the mutation until the new data is done refetching
        awaitRefetchQueries: true,
      });

      if (data.useToken === null) {
        setRxTokenError(true);

        //token was not valid, should eventually get back an error from API
        return;
      }

      setHasRxToken(true);
      setRxTokenModalIsVisible(false);
    } catch (error) {
      Sentry.captureException(error);

      setRxTokenError(true);
    }
  };

  const toggleRxTokenModal = (): void => {
    if (!hasRxToken) {
      navigation.pop();
    }

    setRxTokenModalIsVisible(!rxTokenModalIsVisible);
  };

  const renderErrorModal = (): ReactElement => {
    return (
      <ErrorModal
        error={errorMessage}
        isVisible={errorModalVisible}
        setIsVisible={setErrorModalVisible}
      />
    );
  };

  const [reservationModalVisible, setReservationModalVisible] = useState(false);

  // This function opens the class reservation modal (Step 1)
  // NOTE: this "classId" is actually the "postId" of the individual event
  const initiateClassReservation = (postId: string): void => {
    selectClassId(postId);
    setReservationModalVisible(true);
    setRxTokenInvalidModalIsVisible(true);
  };

  // This is the callback function invoked when a class is reserved from the modal (Step 2)
  const onReserveClass = async (postId: string): Promise<string> => {
    // TODO: put in a try/catch block and handle failure, when we have an error message UI.
    try {
      const success = await registerForEvent({
        variables: {
          postId,
        },
      });

      // TODO: move to pulling the reservedClass from refetched scheduled events as a security measure
      // const hoagClass =
      //   myEventsData?.myEvents?.find((event: Event): boolean => event.postId === classId) ??
      //   ({} as Event);

      const reservedClass =
        biocircuitGroupData?.eventGroup?.events.find(
          (program: Event): boolean => program.postId === postId
        ) ?? ({} as Event);

      // tracking the class reservation in analytics
      trackClassReservation(reservedClass);

      setReservationModalVisible(false);

      navigation.navigate('ReservationConfirmation', {
        type: 'program',
        reservation: {
          date: reservedClass.date,
          startTime: reservedClass.startTime,
          endTime: reservedClass.endTime,
        },
        actions: {
          primary: StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Home',
                action: NavigationActions.navigate({
                  routeName: 'Schedule',
                }),
              }),
            ],
          }),
          secondary: NavigationActions.navigate({
            routeName: 'BiocircuitDetails',
          }),
        },
      });

      Promise.resolve(success);
    } catch (error) {
      Sentry.captureException(error);

      let message = t('errors.general');

      if (error.toString() === `Error: GraphQL error: This Class is at capacity`) {
        message = t('errors.classCapacity');
      }

      if (
        error.toString() === `Error: GraphQL error: User does not have a valid token for this event`
      ) {
        message = t('errors.classTokenExpired');
      }

      setErrorMessage(message);
      setReservationModalVisible(false);
      setTimeout(() => {
        // Error Modal will not appear when other modal is present and it takes a moment
        setErrorModalVisible(true);
      }, 400);
    }
  };

  // TODO: add a use effect to wipe the selected class time when the modal becomes invisible...
  const renderClassReservationModal = (): ReactNode => {
    if (!biocircuitGroupData?.eventGroup?.events) return null;

    const selectedClass =
      biocircuitGroupData?.eventGroup?.events.find(
        ({ postId: classId }): boolean => classId === selectedClassId
      ) || ({} as Event);

    if (dayjs(selectedClass.date).diff(dayjs(rxTokenExpiration), 'day') > 0) {
      return (
        <TokenInvalidModal
          isVisible={rxTokenInvalidModalIsVisible}
          setIsVisible={setRxTokenInvalidModalIsVisible}
          tokenExpiration={rxTokenExpiration}
        />
      );
    }

    return (
      <ClassReservationModal
        isRegistering={isRegistering}
        className={t('programs.biocircuit.title')}
        isVisible={reservationModalVisible}
        isProgram={true}
        setIsVisible={setReservationModalVisible}
        classTimeId={selectedClassId}
        startTime={selectedClass.startTime}
        endTime={selectedClass.endTime}
        date={selectedClass.date}
        onReserveClass={onReserveClass}
      />
    );
  };

  const renderEventListItem = (event: Event): ReactElement | null => {
    // temprorarily filter out events that occured before now on frontend
    if (dayjs(event.startTime).isBefore(dayjs())) return null;

    const myEvents = myEventsData ? myEventsData.myEvents : [];
    const classId = event.postId;

    return (
      <EventListItem
        key={event.id}
        {...event}
        isFull={classIsFull(event)}
        isRegistered={isRegistered(myEvents, classId)}
        initiateClassReservation={initiateClassReservation}
        isProgram={true}
      />
    );
  };

  const renderClassDetailsScreenContents = (): ReactElement => {
    if (biocircuitGroupLoading && !Object.keys(biocircuitGroupData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      // todo: remove this nested Screen layout
      <Screen
        testID="biocircuit-details-screen"
        modalHeader
        screenTitle="Biocircuit"
        backgroundColor={colors.white}
        headerColor={colors.tangerineYellow}
        flex={1}
        height="100%"
        paddingTop={0}
        paddingBottom={0}
      >
        {renderClassReservationModal()}
        {renderErrorModal()}
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={biocircuitGroupLoading}
              onRefresh={refetchBiocircuitGroup}
            />
          }
        >
          {biocircuitGroupLoading || meLoading ? null : (
            <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING} mx={5}>
              {biocircuitGroupData?.eventGroup?.events
                ? biocircuitGroupData?.eventGroup?.events.map(renderEventListItem)
                : null}
            </Container>
          )}
        </ScrollView>
      </Screen>
    );
  };

  if (biocircuitGroupError) {
    <ContentErrorToast />;
  }

  return (
    <Screen
      testID="program-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {!meLoading && !biocircuitGroupLoading ? (
        <>
          {renderClassDetailsScreenContents()}
          <RxTokenModal
            isVisible={rxTokenModalIsVisible}
            setIsVisible={toggleRxTokenModal}
            userTokenValue={userRxToken}
            setTokenValue={(code: string): void => setUserRxToken(code)}
            tokenError={rxTokenError}
            validate={(): Promise<void> => redeemRxToken()}
          />
        </>
      ) : (
        <CenteredLoadingSpinner withScrollviewPadding />
      )}
    </Screen>
  );
};

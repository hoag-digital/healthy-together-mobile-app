import React, { ReactElement, ReactNode, Component } from 'react';
import { DevSettings } from 'react-native';
import { Button } from '../../components/Button';
import DefaultLogo from '../../assets/images/hoag-logo-solid.svg';
import { Container } from '../../components/Container';
import { Text } from '../../components/Text';
import { colors } from '../../styles/colors';

interface ErrorInfo {
  componentStack: string;
}

interface State {
  hasError: boolean;
}

/** This screen provides a graceful fallback UI in the event of any render errors */
export class ErrorBoundaryScreen extends Component<{}, State> {
  state = { hasError: false };

  static getDerivedStateFromError(_error: any): any {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(_error: Error, _errorInfo: ErrorInfo): void {
    // TODO: log the error
  }

  // TODO: add "Refresh App" button once upgraded from RN 0.61 to 0.62: https://reactnative.dev/docs/devsettings#reload
  render(): ReactElement | ReactNode {
    if (this.state.hasError) {
      return (
        <Container fill fullWidth justifyContent="center" p={5}>
          <Container fill alignItems="center" justifyContent="space-around" pb={5}>
            <Container />
            <DefaultLogo height={120} width={120} />
          </Container>
          <Text fontWeight="bold" fontSize={5} pb={2}>
            Oops, something went wrong.
          </Text>
          <Text color={colors.black} fontSize={3}>
            The error has been reported and our support team is looking into it.
          </Text>

          <Button
            label="Reload"
            backgroundColor={colors.cornflowerBlue2}
            color={colors.white}
            fontSize={4}
            onPress={(): void => DevSettings.reload()}
            py={2}
            my={5}
          />

          <Container fill />
        </Container>
      );
    }

    return this.props.children;
  }
}

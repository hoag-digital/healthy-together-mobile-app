import React, { FC, ReactElement, useState } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { RefreshControl, FlatList } from 'react-native';
import { Icon } from 'react-native-elements';

import { Text } from '../../components/Text';
import { Container } from '../../components/Container';
import { Touchable } from '../../components/Touchable';
import { SubNavLayoutProps } from '../../components/SubNavLayout';
import { LayeredContainerScreen } from '../../components/LayeredContainerScreen';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

import { colors } from '../../styles';
import { SCROLLVIEW_BOTTOM_PADDING, SCREEN_WIDTH, LARGER_ICON_SIZE } from '../../utils';
import { Post, useNicuFaqQuery } from '../../graphql/types';
import { ContentErrorToast } from '../../components/Toastr';

const KEY_IRVINE = 'irvine';
const KEY_NEWPORT = 'newport-beach';

const TAB_BUTTONS = [
  {
    label: 'Newport Beach',
    key: KEY_NEWPORT,
  },
  {
    label: 'Irvine',
    key: KEY_IRVINE,
  },
];

export const NicuFaq: FC<SubNavLayoutProps> = ({ navigation }) => {
  const {
    error: faqError,
    data: faqData = {},
    loading: faqLoading,
    refetch: refetchFaqContent,
  } = useNicuFaqQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [activeTab, setActiveTab] = useState(KEY_NEWPORT);

  const changeDetailsScreen = (currentIndex: number): void => {
    const content = faqData || {};
    const data = activeTab === KEY_IRVINE ? content.irvine : content.newportBeach;
    navigation.navigate('ContentDetailsModal', {
      postId: data[currentIndex].id,
      hideLabel: true,
      hideImage: true,
      changeIndex: changeDetailsScreen,
      currentIndex,
      isLastItem: data.length === currentIndex + 1,
      showButton: true,
    });
  };

  const navigateToDetailsScreen = (post: Post, currentIndex: number): void => {
    const content = faqData || {};
    const data = activeTab === KEY_IRVINE ? content.irvine : content.newportBeach;
    navigation.navigate('ContentDetailsModal', {
      postId: post.id,
      hideLabel: true,
      hideImage: true,
      changeIndex: changeDetailsScreen,
      currentIndex,
      isLastItem: data.length === currentIndex + 1,
      showButton: true,
    });
  };

  const renderItem = ({ item, index }): ReactElement => {
    return (
      <Touchable
        onPress={(): void => {
          navigateToDetailsScreen(item, index);
        }}
      >
        <Container
          key={index}
          flexDirection="row"
          justifyContent="center"
          py={2}
          borderBottomWidth={2}
          borderColor={colors.gainsboro}
        >
          <Container flex={0.9}>
            <Text>{item.title}</Text>
          </Container>
          <Container flex={0.1}>
            <Icon name="chevron-right" size={LARGER_ICON_SIZE} />
          </Container>
        </Container>
      </Touchable>
    );
  };

  const renderFaqs = (): ReactElement => {
    const content = faqData || {};
    const { irvine, newportBeach } = content;
    return (
      <Container fullWidth>
        {/* TODO(ratkinson): we get a yellowbox warning for rendering this inside of a ScrollView */}
        <FlatList
          style={{ flexGrow: 0, paddingBottom: 5 }}
          data={activeTab === KEY_IRVINE ? irvine : newportBeach}
          renderItem={renderItem}
        />
      </Container>
    );
  };

  const renderActiveSelector = (): ReactElement => {
    return (
      <Container
        height={7}
        width={150}
        marginTop={1}
        backgroundColor={colors.cornflowerBlue3}
        borderTopRightRadius={25}
        borderTopLeftRadius={25}
      />
    );
  };

  const renderLocationSwitcher = (): ReactElement => {
    return (
      <Container flexDirection="row" justifyContent="center" mt={4}>
        {TAB_BUTTONS.map(tab => (
          <Container key={tab.key} flex={0.4} alignItems="center">
            <Touchable onPress={(): void => setActiveTab(tab.key)}>
              <Text textAlign="center" px={3}>
                {tab.label}
              </Text>
              {activeTab === tab.key ? renderActiveSelector() : null}
            </Touchable>
          </Container>
        ))}
      </Container>
    );
  };

  const renderScreenContent = (): ReactElement => {
    if (faqLoading && !Object.keys(faqData).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
          https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <ScrollView
        scrollIndicatorInsets={{ right: 1 }}
        style={{ width: SCREEN_WIDTH, backgroundColor: colors.solitudeLight, paddingLeft: 10 }}
        refreshControl={<RefreshControl refreshing={faqLoading} onRefresh={refetchFaqContent} />}
      >
        {/* SCROLLVIEW_BOTTOM_PADDING is necessary for screens that scroll behind the tab bar */}
        <Container pt={5} pb={SCROLLVIEW_BOTTOM_PADDING}>
          {renderFaqs()}
        </Container>
      </ScrollView>
    );
  };

  if (faqError) {
    <ContentErrorToast />;
  }

  return (
    <LayeredContainerScreen
      screenTitle="NICU FAQs"
      headerColor={colors.cornflowerBlue3}
      containerColor={colors.white}
      headerFontColor={colors.white}
    >
      <Text fontSize={6} fontWeight="700" pt={3} color={colors.black} textAlign="center">
        Top Questions
      </Text>
      {renderLocationSwitcher()}
      {renderScreenContent()}
    </LayeredContainerScreen>
  );
};

import React, { FC } from 'react';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import { Screen } from '../../components/Screen';
import { colors } from '../../styles';
import { t, TAB_BAR_HEIGHT } from '../../utils';
import { REMOVE_HEADER_FOOTERS_HOAG_IO } from '../../lib/InjectedJavascript';
import { useWebUrlsQuery } from '../../graphql/types';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';

/** Have noticed there is a JS warning and it seems to be related to the JS
 *  on the actual webpage for the chatbot, this is also why the heading does not hide with our
 * injected javascript
 * https://github.com/react-native-community/react-native-webview/issues/341#issuecomment-466639820 */

export const SelfAssessmentScreen: FC<NavigationStackScreenProps> = () => {
  const { error: webUrlsError, data: webUrlsData, loading: webUrlsLoading } = useWebUrlsQuery({
    fetchPolicy: 'cache-and-network',
  });

  const selfAssessmentUrl = webUrlsData?.webUrls?.selfAssessment || '';

  if (webUrlsError) {
    throw new Error('Error loading URLS');
  }

  return (
    <Screen
      testID="telehealth-screen"
      modalHeader
      screenTitle={t('care.selfAssessment')}
      headerColor={colors.malibu}
      backgroundColor={colors.white}
      headerFontColor={colors.white}
      flex={1}
      height="100%"
      paddingTop={0}
      marginBottom={TAB_BAR_HEIGHT} //This gives a consistent look across devices when tab bar is not visible
    >
      {webUrlsLoading ? (
        <CenteredLoadingSpinner />
      ) : (
        <WebView
          source={{ uri: selfAssessmentUrl }}
          containerStyle={StyleSheet.absoluteFillObject}
          startInLoadingState
          injectedJavaScript={REMOVE_HEADER_FOOTERS_HOAG_IO}
        />
      )}
    </Screen>
  );
};

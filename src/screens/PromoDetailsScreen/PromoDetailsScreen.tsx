import React, { FC, ReactElement, useState, useEffect } from 'react';
import { StatusBar, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import VideoPlayer from 'react-native-video-controls';
import { Icon } from 'react-native-elements';
import FastImage from 'react-native-fast-image';

import { Container } from '../../components/Container';
import { Screen } from '../../components/Screen';
import { Text } from '../../components/Text';
import { Touchable } from '../../components/Touchable';
import { PostRichText } from '../../components/PostRichText';
import { CenteredLoadingSpinner } from '../../components/CenteredLoadingSpinner';
import { colors } from '../../styles';
import { useContentByIdQuery } from '../../graphql/types';
import {
  DEFAULT_ICON_SIZE,
  SCROLLVIEW_BOTTOM_PADDING,
  SCREEN_WIDTH,
  LARGEST_ICON_SIZE,
} from '../../utils';

/**
 * This screen shows the details of a promotional post (aka wordpress content)
 * This component was based on the ContentDetailsScreen
 */
export const PromoDetailsScreen: FC<NavigationTabScreenProps> = ({ navigation }) => {
  const postId = navigation.getParam('postId');
  const [pauseVideo, setPauseVideo] = useState(true);
  const [showThumbnail, setShowThumbnail] = useState(true);

  const resetVideo = (): void => {
    setShowThumbnail(true);
  };

  const endVideo = (): void => {
    setShowThumbnail(true);
  };

  const playVideo = (): void => {
    setShowThumbnail(false);
    setPauseVideo(false);
  };

  useEffect(() => {
    const willFocus = navigation.addListener('willFocus', () => resetVideo());
    const willBlur = navigation.addListener('willBlur', () => endVideo());

    return (): void => {
      willFocus.remove();
      willBlur.remove();
    };
  });

  let content;

  const goBack = (): void => {
    navigation.goBack();
  };

  const { loading, error, data, refetch } = useContentByIdQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      postId,
    },
  });

  if (error) {
    // TODO: handle graphql errors
  }

  const renderPromoScreenContents = (): ReactElement => {
    if (loading || !Object.keys(data).length) {
      return <CenteredLoadingSpinner withScrollviewPadding />;
    }

    const contentToRender = content || (data ? data.contentById : {});
    const { title, rawHtml, mediumImage, featuredVideo } = contentToRender;

    /* scrollIndicatorInsets fixes an iOS 13 scrollbar bug:
      https://github.com/facebook/react-native/issues/26610#issuecomment-539843444 */
    return (
      <>
        <StatusBar
          translucent
          animated
          backgroundColor={colors.transparent}
          barStyle="light-content"
        />
        <ScrollView
          scrollIndicatorInsets={{ right: 1 }}
          refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        >
          <Container flex={1} pb={SCROLLVIEW_BOTTOM_PADDING}>
            <Touchable
              position="absolute"
              // TODO: future-improvement -> dynamic positioning
              top={45}
              right={20}
              testID="close-icon"
              onPress={goBack}
              bg={colors.white}
              height={DEFAULT_ICON_SIZE}
              width={DEFAULT_ICON_SIZE}
              borderRadius={DEFAULT_ICON_SIZE}
              zIndex={50}
            >
              <Icon name="ios-close" type="ionicon" size={DEFAULT_ICON_SIZE} color={colors.black} />
            </Touchable>
            {showThumbnail ? (
              <Touchable onPress={(): void => playVideo()}>
                <FastImage source={{ uri: mediumImage }} style={{ aspectRatio: 16 / 9 }} />
                <Container
                  position="absolute"
                  top="50%"
                  mt={-(LARGEST_ICON_SIZE / 2)}
                  left={(SCREEN_WIDTH - LARGEST_ICON_SIZE) / 2}
                >
                  {!!featuredVideo && (
                    <Icon
                      name="play-circle"
                      type="font-awesome"
                      size={LARGEST_ICON_SIZE}
                      color={colors.white}
                    />
                  )}
                </Container>
              </Touchable>
            ) : (
              <VideoPlayer
                source={{
                  uri: featuredVideo,
                }}
                disableBack
                disableVolume
                disableFullscreen
                paused={pauseVideo} // this keeps it from autoplaying when loading
                resizeMode="cover"
                repeat={false}
                onEnd={(): void => resetVideo()}
                style={{
                  width: SCREEN_WIDTH,
                  height: 250,
                  backgroundColor: colors.blackTransparent,
                }}
              />
            )}
            <Text pt={5} pb={2} mx={5} fontSize={5} fontWeight="bold">
              {title}
            </Text>
            {rawHtml && <PostRichText richText={rawHtml} />}
          </Container>
        </ScrollView>
      </>
    );
  };

  return (
    <Screen
      testID="promo-details-screen"
      backgroundColor={colors.transparent}
      flex={1}
      height="100%"
      paddingTop={0}
      paddingBottom={0}
    >
      {renderPromoScreenContents()}
    </Screen>
  );
};

export default PromoDetailsScreen;

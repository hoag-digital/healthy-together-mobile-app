import Entities from 'html-entities';
const entities = new Entities.XmlEntities();

const decodeHtml = (value: string | null | undefined): string => {
  return entities.decode(value as string);
};

export default decodeHtml;

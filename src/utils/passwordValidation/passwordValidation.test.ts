import {strongPassword, matchingPassword} from './passwordValidation'

describe('passwordValidation', () => {
   describe('strongPassword', () => {
    test('does not require a special character', () => {
      expect(strongPassword('Password123')).toBe(true)
    })
    test('is false when passwords do not have number', () => {
      expect(strongPassword('Password!')).toBe(false)
    })
    test('is false when passwords do not have upper case', () => {
      expect(strongPassword('password123!')).toBe(false)
    })
    test('is false when passwords do not have lower case', () => {
      expect(strongPassword('PASSWORD123!')).toBe(false)
    })
    test('is false when passwords do not have eight characters', () => {
      expect(strongPassword('Pwd123!')).toBe(false)
    })
    test('is true when passwords has upper, lower, symbol, number and 8 characters', () => {
      expect(strongPassword('Password123!')).toBe(true)
    })
  })
  
  describe('matchingPassword', () => {
    test('is false when passwords not equal', () => {
      expect(matchingPassword('foo', 'foofoo')).toBe(false)
    })
    test('is true when passwords are equal', () => {
      expect(matchingPassword('foo', 'foo')).toBe(true)
    })
  })
  
})

const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');

export const strongPassword = (value: string): boolean => {
  return strongRegex.test(value);
};

export const matchingPassword = (value1: string, value2: string): boolean => {
  return value1 === value2;
};

export default [strongPassword, matchingPassword];

import stringifyTrimester, { FIRST_TRIMESTER, SECOND_TRIMESTER, THIRD_TRIMESTER } from './stringifyTrimester'

describe('stringifyTrimester', () => {
  it('returns 1st Trimester when passed 1', () => {
    expect(stringifyTrimester(1)).toBe(FIRST_TRIMESTER)
  })

   it('returns 2nd Trimester when passed 2', () => {
     expect(stringifyTrimester(2)).toBe(SECOND_TRIMESTER);
   });
   
   it('returns 2nd Trimester when passed 2', () => {
     expect(stringifyTrimester(3)).toBe(THIRD_TRIMESTER);
   });

   it('returns UNKNOWN when null value', () => {
     expect(stringifyTrimester(null)).toBe('UNKNOWN');
   });

    it('returns UNKNOWN when undefined value', () => {
      expect(stringifyTrimester(undefined)).toBe('UNKNOWN');
    });
    
    it('returns UNKNOWN when no value passed', () => {
      expect(stringifyTrimester()).toBe('UNKNOWN');
    });
})

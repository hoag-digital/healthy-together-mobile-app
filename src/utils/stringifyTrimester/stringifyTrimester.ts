export const FIRST_TRIMESTER = '1st Trimester';
export const SECOND_TRIMESTER = '2nd Trimester';
export const THIRD_TRIMESTER = '3rd Trimester';

const stringifyTrimester = (trimester?: number | null | undefined): string => {
  switch (trimester) {
    case 1:
      return FIRST_TRIMESTER;
    case 2:
      return SECOND_TRIMESTER;
    case 3:
      return THIRD_TRIMESTER;

    default:
      return 'UNKNOWN';
  }
};

export default stringifyTrimester;

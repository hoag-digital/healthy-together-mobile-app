import getGestationalProgress from './getGestationalProgress'

describe('getGestationalProgress', () => {
  test('returns correct value for the week of the gestation', () => {
    expect(getGestationalProgress({ weeks: null}).complete).toBe('0%');
    expect(getGestationalProgress({ weeks: null}).remaining).toBe('100%')
    expect(getGestationalProgress({ weeks: -5}).complete).toBe('0%');
    expect(getGestationalProgress({ weeks: -5}).remaining).toBe('100%')
    expect(getGestationalProgress({ weeks: 0}).complete).toBe('0%');
    expect(getGestationalProgress({ weeks: 0}).remaining).toBe('100%')
    expect(getGestationalProgress({ weeks: 4}).complete).toBe('10%');
    expect(getGestationalProgress({ weeks: 4}).remaining).toBe('90%')
    expect(getGestationalProgress({ weeks: 8}).complete).toBe('20%');
    expect(getGestationalProgress({ weeks: 8}).remaining).toBe('80%')
    expect(getGestationalProgress({ weeks: 10}).complete).toBe('25%');
    expect(getGestationalProgress({ weeks: 10}).remaining).toBe('75%')
    expect(getGestationalProgress({ weeks: 20}).complete).toBe('50%');
    expect(getGestationalProgress({ weeks: 20}).remaining).toBe('50%')
    expect(getGestationalProgress({ weeks: 30}).complete).toBe('75%');
    expect(getGestationalProgress({ weeks: 30}).remaining).toBe('25%')
    expect(getGestationalProgress({ weeks: 40}).complete).toBe('100%');
    expect(getGestationalProgress({ weeks: 40}).remaining).toBe('0%')
    expect(getGestationalProgress({ weeks: 42}).complete).toBe('100%');
    expect(getGestationalProgress({ weeks: 42}).remaining).toBe('0%')
  })
})

import { GestationalAge } from '../../graphql/types';

export interface GestationalProgress {
  /**The percent complete of the trimester, displayed with % and used for determining width */
  complete: string;
  /**The percent remaining of the trimester, displayed with % and used for determining width */
  remaining: string;
}

const GESTATION_WEEKS = 40;
const COMPLETED_GESTATION = { complete: '100%', remaining: '0%' };

const getGestationalProgress = (gestationalAge: GestationalAge): GestationalProgress => {
  const { weeks } = gestationalAge;
  if (!weeks || weeks < 0) return { complete: '0%', remaining: '100%' };
  if (weeks >= 40) return COMPLETED_GESTATION;

  const percentComplete = ((weeks / GESTATION_WEEKS) * 100).toFixed(0);
  const percentRemaining = (100 - Number(percentComplete)).toString();

  return { complete: `${percentComplete}%`, remaining: `${percentRemaining}%` };
};

export default getGestationalProgress;

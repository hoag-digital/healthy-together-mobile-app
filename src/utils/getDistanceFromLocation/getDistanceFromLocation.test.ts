import getDistanceFromLocation from './getDistanceFromLocation';

describe('no arguments', () => {
  const irvineLosOliviosLocation = {
    latitude: '33.6446202',
    longitude: '-117.7447986',
  };

  const newportBeach = {
    latitude: '33.6298649',
    longitude: '-117.9291926',
  };
//distances are "as the crow flys"
  it('returns distance in miles', () => {
    expect(
      getDistanceFromLocation(
        irvineLosOliviosLocation.latitude,
        irvineLosOliviosLocation.longitude,
        newportBeach.latitude,
        newportBeach.longitude
      )
    ).toBe(10.655526875618797);
  });
  it('returns distance in kilometers', () => {
    expect(
      getDistanceFromLocation(
        irvineLosOliviosLocation.latitude,
        irvineLosOliviosLocation.longitude,
        newportBeach.latitude,
        newportBeach.longitude,
        'K'
      )
    ).toBe(17.14840824411586);
  });
});

import { NavigationTabProp } from 'react-navigation-tabs';

import getParentRouteFromNavigation from './getParentRouteFromNavigation';

describe('getParentRouteFromNavigation', () => {
  test('is should return null with no input', () => {
    const mockNavigation: unknown = {
      dangerouslyGetParent: (): null => null
    };

    const parentRoute = getParentRouteFromNavigation(mockNavigation as NavigationTabProp);

    expect(parentRoute).toBe(null);
  });

  test('is should return a string with valid navigation input', () => {
    const mockNavigation: unknown = {
      dangerouslyGetParent: (): any => ({
        state: {
          routeName: 'MOCK_ROUTE',
        }
      })
    };

    const parentRoute = getParentRouteFromNavigation(mockNavigation as NavigationTabProp);

    expect(parentRoute).toBe('MOCK_ROUTE');
  });
});

import { checkMultiple, PERMISSIONS, RESULTS } from 'react-native-permissions';

const hasLocationPermissions = (): Promise<boolean> => {
  return checkMultiple([
    PERMISSIONS.IOS.LOCATION_ALWAYS,
    PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
  ]).then(statuses => {
    return Object.values(statuses).includes(RESULTS.GRANTED);
  });
};

export default hasLocationPermissions;

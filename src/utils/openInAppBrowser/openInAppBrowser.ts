import InAppBrowser from 'react-native-inappbrowser-reborn';
import analytics from '@react-native-firebase/analytics';
import { colors } from '../../../src/styles/colors';

export const openInAppBrowser = async (url?: string | null): Promise<any> => {
  if (!url) return null;

  const isAvailable = await InAppBrowser.isAvailable();

  if (isAvailable) {
    try {
      InAppBrowser.open(url, {
        dismissButtonStyle: 'cancel',
        preferredBarTintColor: colors.news,
        preferredControlTintColor: colors.white,
        modalTransitionStyle: 'crossDissolve',
        toolbarColor: colors.news,
        secondaryToolbarColor: colors.news,
        enableUrlBarHiding: true,
        forceCloseOnRedirection: true,
        modalPresentationStyle: 'fullScreen',
        animations: {
          startEnter: 'slide_in_bottom',
          startExit: 'slide_out_bottom',
          endEnter: 'slide_in_bottom',
          endExit: 'slide_out_bottom',
        },
      });

      analytics().logEvent('external_link_clicked', {
        url,
      });
    } catch (error) {
      console.log('error', error);
    }
  }

  return null;
};

export default openInAppBrowser;

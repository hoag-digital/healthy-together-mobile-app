/**
 * Assortment of constants used thoughout the application for consistency.
 * Note that any padding "scale" value exported will be in terms of "styled-system"s space scale.
 * Therefore PADDING_SCALE: 1 will map to the value of theme.space[1].
 */
import { Dimensions, Platform } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

const SMALLEST_ICON_SIZE = 18;
const DEFAULT_ICON_SIZE = 25;
const LARGER_ICON_SIZE = 30;
const LARGEST_ICON_SIZE = 35;
const DEFAULT_AVATAR_SIZE = 40;
const LARGE_AVATAR_SIZE = 70;
const CARD_BORDER_RADIUS = 10;
const MODAL_LAYOUT_RADIUS = 30;
const MODAL_MULTILINE_HEIGHT = 150;
const STATUS_BAR_PADDING_SCALE = 1;
const TOUCHABLE_PRESS_DELAY = 75;
const TAB_BAR_HEIGHT = 65;
const DEFAULT_ANIMATION_DURATION = 200;
const DEFAULT_CONTENT_DESCRIPTION_LENGTH = 100;
const SCREEN_HEIGHT_RATIO = 0.27;
const SCREEN_WIDTH_RATIO = 0.9;
const IS_ANDROID = Platform.OS === 'android';
const IS_IPHONE = Platform.OS === 'ios';
const IS_IPHONE_X = isIphoneX();
const TIME_LABEL_FORMAT = 'h:mma';
const TIME_LABEL_FORMAT_CAPS = 'h:mm A';
const ASYNCSTORAGE_KEY_BASE = '@FoothillRanchStore';
const ASYNCSTORAGE_AUTH_KEY = `${ASYNCSTORAGE_KEY_BASE}:token`;
const ASYNCSTORAGE_FCM_KEY = `${ASYNCSTORAGE_KEY_BASE}:fcmToken`;
const SCREEN_DIMENSIONS = Dimensions.get('screen');
const SCREEN_WIDTH = SCREEN_DIMENSIONS.width;
const SCREEN_HEIGHT = SCREEN_DIMENSIONS.height;
const MENU_IMAGE_SIZE = IS_ANDROID ? SCREEN_HEIGHT * 0.2 : SCREEN_HEIGHT * 0.25;
const MENU_WIDTH = SCREEN_WIDTH * 0.6;
const DEFAULT_HITSLOP = { top: 50, bottom: 50, left: 50, right: 50 };
const SMALL_HITSLOP = { top: 10, bottom: 10, left: 10, right: 10 };

const SCROLLVIEW_BOTTOM_PADDING = Platform.select({
  ios: IS_IPHONE_X ? 120 : 80,
  android: 80,
});

const SCREEN_BOTTOM_PADDING_SCALE = Platform.select({
  ios: IS_IPHONE_X ? 0 : 4,
  android: 4,
});

const DATE_FORMATS = {
  STANDARD: 'MMMM DD, YYYY',
  TERSE: 'MMM D YYYY',
};

export {
  ASYNCSTORAGE_AUTH_KEY,
  ASYNCSTORAGE_FCM_KEY,
  ASYNCSTORAGE_KEY_BASE,
  CARD_BORDER_RADIUS,
  DATE_FORMATS,
  DEFAULT_ANIMATION_DURATION,
  DEFAULT_AVATAR_SIZE,
  DEFAULT_CONTENT_DESCRIPTION_LENGTH,
  DEFAULT_HITSLOP,
  DEFAULT_ICON_SIZE,
  IS_ANDROID,
  IS_IPHONE,
  LARGE_AVATAR_SIZE,
  LARGER_ICON_SIZE,
  LARGEST_ICON_SIZE,
  MENU_IMAGE_SIZE,
  MENU_WIDTH,
  MODAL_LAYOUT_RADIUS,
  MODAL_MULTILINE_HEIGHT,
  SCREEN_BOTTOM_PADDING_SCALE,
  SCREEN_HEIGHT,
  SCREEN_WIDTH,
  SCREEN_HEIGHT_RATIO,
  SCREEN_WIDTH_RATIO,
  SCROLLVIEW_BOTTOM_PADDING,
  SMALL_HITSLOP,
  SMALLEST_ICON_SIZE,
  STATUS_BAR_PADDING_SCALE,
  TAB_BAR_HEIGHT,
  TIME_LABEL_FORMAT_CAPS,
  TIME_LABEL_FORMAT,
  TOUCHABLE_PRESS_DELAY,
};

/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Platform, Text } from 'react-native';
import React, { ReactElement } from 'react';

// utils.js
// One Plus Fix for Oxygen OS and its painful Slate font truncating on bold text
// https://github.com/facebook/react-native/issues/15114
export const textFontFix = () => {
  if (Platform.OS !== 'android') {
    return;
  }

  const oldRender = Text.render;

  Text.render = function(...args): ReactElement {
    const origin = oldRender.call(this, ...args);
    return React.cloneElement(origin, {
      style: [{ fontFamily: 'Helvetica' }, origin.props.style], //replace font with our default fontFamily
    });
  };
};

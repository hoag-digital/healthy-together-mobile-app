/**
 * gets the current screen from navigation state
 */
export function getActiveRouteName(navigationState: any): string | null {
  if (!navigationState) {
    return null;
  }

  const route = navigationState.routes[navigationState.index];

  // dive into nested navigators recursively
  if (route.routes) {
    return getActiveRouteName(route);
  }

  return route.routeName;
}

export default getActiveRouteName;

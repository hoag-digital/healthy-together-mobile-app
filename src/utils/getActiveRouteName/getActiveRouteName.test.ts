import getActiveRouteName from './getActiveRouteName';

const SHALLOW_ROUTE_NAME = 'Shallow Route';
const NESTED_ROUTE_NAME = 'Nested Route';

const SHALLOW_MOCK_NAV_STATE = {
  index: 0,
  routes: [{
    routeName: SHALLOW_ROUTE_NAME,
  }],
};

const NESTED_MOCK_NAV_STATE = {
  index: 1,
  routes: [{
      routeName: SHALLOW_ROUTE_NAME,
    },
    { 
      index: 0,
      routes: [{
        routeName: NESTED_ROUTE_NAME,
      }]
    },
  ],
};

describe('getActiveRouteName', () => {
  test('returns null without a valid input', () => {
    expect(getActiveRouteName(null)).toBe(null);
  });

  test('returns a shallow active route', () => {
    expect(getActiveRouteName(SHALLOW_MOCK_NAV_STATE)).toBe(SHALLOW_ROUTE_NAME);
  });

  test('returns a nested active route', () => {
    expect(getActiveRouteName(NESTED_MOCK_NAV_STATE)).toBe(NESTED_ROUTE_NAME);
  });
});

import { Linking } from 'react-native';

export const safelyOpenUrl = async (url?: string | null): Promise<any> => {
  if (!url) return null;

  try {
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      return Linking.openURL(url);
    }

    return null;
  } catch (error) {
    throw error;
  }
};

export default safelyOpenUrl;

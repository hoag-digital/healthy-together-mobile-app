import i18n from 'i18n-js';
export * from './constants';
export { translate, translate as t, setI18nConfig } from '../utils/language';
export { safelyOpenUrl } from './safelyOpenUrl';

export { i18n };

export * from './form';

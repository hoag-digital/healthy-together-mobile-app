import getTrimesterProgress from './getTrimesterProgress'

describe('getTrimesterProgress', () => {
  it('returns correct value for the week of the trimester', () => {
    expect(getTrimesterProgress({ weeks:12, days:2, trimester:1}).complete).toBe("100%");
    expect(getTrimesterProgress({ weeks: 12, days: 2, trimester: 1 }).remaining).toBe('0%');
    expect(getTrimesterProgress({ weeks: 13, days: 2, trimester: 2 }).complete).toBe('0%');
    expect(getTrimesterProgress({ weeks: 13, days: 2, trimester: 2 }).remaining).toBe('100%');
    expect(getTrimesterProgress({ weeks: 40, days: 2, trimester: 3 }).complete).toBe('100%');
    expect(getTrimesterProgress({ weeks: 40, days: 2, trimester: 3 }).remaining).toBe('0%');
    expect(getTrimesterProgress({ weeks: 42, days: 2, trimester: 3 }).complete).toBe('100%');
    expect(getTrimesterProgress({ weeks: 42, days: 2, trimester: 3 }).remaining).toBe('0%');
  })

  it('returns STARTING trimester if the trimester number is invalid',() => {
    const progress = getTrimesterProgress({weeks:100, days:0, trimester:4});
    expect(progress.complete).toBe('0%')
    expect(progress.remaining).toBe('100%');
  })
})

import { GestationalAge } from '../../graphql/types';
export interface TrimesterProgress {
  /**The percent complete of the trimester, displayed with % and used for determining width */
  complete: string;
  /**The percent remaining of the trimester, displayed with % and used for determining width */
  remaining: string;
}
const WEEKS_IN_TRIMESTER = 13;
const STARTING_TRIMESTER = { complete: '0%', remaining: '100%' };
const COMPLETED_TRIMESTER = { complete: '100%', remaining: '0%' };

const TRIMESTERS = {
  1: { beginningWeek: 1, endingWeek: 12 },
  2: { beginningWeek: 13, endingWeek: 26 },
  3: { beginningWeek: 27, endingWeek: 40 },
};

const VALID_TRIMESTERS = [1, 2, 3];

const isValidTrimester = (trimester: number): boolean => {
  return VALID_TRIMESTERS.includes(trimester);
};

const getTrimesterProgress = (gestationalAge: GestationalAge): TrimesterProgress => {
  const { trimester, weeks } = gestationalAge;
  if (!trimester || !weeks || !isValidTrimester(trimester)) return STARTING_TRIMESTER;
  if (weeks >= 40) return COMPLETED_TRIMESTER;

  const weeksRemaining = TRIMESTERS[trimester.toString()].endingWeek - weeks;

  const percentComplete = (
    ((WEEKS_IN_TRIMESTER - weeksRemaining) / WEEKS_IN_TRIMESTER) *
    100
  ).toFixed(0);

  const percentRemaining = (100 - Number(percentComplete)).toString();
  return {
    complete: `${percentComplete}%`,
    remaining: `${percentRemaining}%`,
  };
};

export default getTrimesterProgress;

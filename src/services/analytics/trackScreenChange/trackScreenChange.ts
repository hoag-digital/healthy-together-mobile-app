import analytics from '@react-native-firebase/analytics';

import { getScreenNameForAnalytics } from '../getScreenNameForAnalytics';
import { CUSTOM_EVENTS } from '../constants';

type NavigationRoute = string | null;

/**
 * This provides firebase analytics screen tracking.
 * There are varying reports of the built-in "screen_view" method duplicating calls in some instances,
 * so I am including a custom event, user_navigated, to compare or use directly in reporting.
 */
export function trackScreenChange(
  currentRouteName?: NavigationRoute,
  previousRouteName?: NavigationRoute
): void {
  const previousScreenNameForAnalytics = getScreenNameForAnalytics(previousRouteName);
  const currentScreenNameForAnalytics = getScreenNameForAnalytics(currentRouteName);

  if (currentScreenNameForAnalytics) {
    // parameters: screen_name, screen_class_override
    analytics().setCurrentScreen(currentScreenNameForAnalytics, currentScreenNameForAnalytics);

    // NOTE: adding a custom event in addition to the built-in screen_view event for better control
    // parameters: custom_event_name, custom_event_params
    analytics().logEvent(CUSTOM_EVENTS.NAVIGATION, {
      currentScreen: currentScreenNameForAnalytics,
      previousScreen: previousScreenNameForAnalytics,
    });
  }
}

export default trackScreenChange;

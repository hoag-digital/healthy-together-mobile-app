jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    setCurrentScreen: jest.fn(),
    logEvent: jest.fn(),
  });
});

import trackScreenChange from './trackScreenChange';

const MOCK_ROUTE_1 = 'Mock Route 1';
const MOCK_ROUTE_2 = 'Mock Route 2';

describe('trackScreenChange', () => {
  test('does not throw an error', () => {
    expect(() => trackScreenChange(MOCK_ROUTE_1, MOCK_ROUTE_2)).not.toThrow();
  });
});

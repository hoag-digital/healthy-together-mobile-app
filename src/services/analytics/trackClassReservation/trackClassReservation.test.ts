jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
  });
});

import trackClassReservation from './trackClassReservation';
import { Event } from '../../../graphql/types';

describe('trackClassReservation', () => {
  test('does not throw an error', () => {
    expect(() => trackClassReservation({} as Event)).not.toThrow();
  });
});

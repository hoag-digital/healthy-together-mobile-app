import analytics from '@react-native-firebase/analytics';

import { Event } from '../../../graphql/types';
import { CUSTOM_EVENTS } from '../constants';

/**
 * This provides firebase analytics for class reservations.
 */
export function trackClassReservation(event: Event): void {
  // parameters: custom_event_name, custom_event_params
  analytics().logEvent(CUSTOM_EVENTS.CLASS_RESERVATION, {
    event,
  });
}

export default trackClassReservation;

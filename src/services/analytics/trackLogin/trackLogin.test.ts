jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logLogin: jest.fn(),
    setUserId: jest.fn(),
    setUserProperties: jest.fn(),
  });
});

import trackLogin from './trackLogin';

const MOCK_USER = {
  id: 'abcd',
};

describe('trackLogin', () => {
  test('doesn\'t throw an error', () => {
    expect(() => trackLogin(MOCK_USER)).not.toThrow();
  });
});

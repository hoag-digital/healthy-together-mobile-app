import analytics from '@react-native-firebase/analytics';
import { User } from '../../../graphql/types';

export const DEFAULT_LOGIN_METHOD = 'Login Screen';

/**
 * This provides firebase analytics login event tracking.
 */
export function trackLogin(user: User, loginMethod: string = DEFAULT_LOGIN_METHOD): void {
  // parameters: login_event_params
  analytics().logLogin({
    method: loginMethod,
  });

  const { id: userId, firstName, lastName, email } = user;

  // parameters: user_id
  analytics().setUserId(userId);

  // parameters: user_properties
  analytics().setUserProperties({
    firstName: firstName || '',
    lastName: lastName || '',
    email: email || '',
  });
}

export default trackLogin;

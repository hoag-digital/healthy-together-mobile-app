import analytics from '@react-native-firebase/analytics';

import { CUSTOM_EVENTS } from '../constants';

/**
 * This provides firebase analytics for the covid-19 self-assessment webview.
 */
export function trackSelfAssessmentStarted(): void {
  // parameters: custom_event_name, custom_event_params
  analytics().logEvent(CUSTOM_EVENTS.SELF_ASSESSMENT);
}

export default trackSelfAssessmentStarted;

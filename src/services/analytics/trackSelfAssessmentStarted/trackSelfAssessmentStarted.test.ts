jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
  });
});

import trackSelfAssessmentStarted from './trackSelfAssessmentStarted';

describe('trackSelfAssessmentStarted', () => {
  test('does not throw an error', () => {
    expect(() => trackSelfAssessmentStarted()).not.toThrow();
  });
});

export const CUSTOM_EVENTS = {
  CLASS_RESERVATION: 'class_reservation',
  LOGOUT: 'user_logged_out',
  NAVIGATION: 'user_navigated',
  ON_DEMAND_VIDEO: 'on_demand_video_played',
  SELF_ASSESSMENT: 'self_assessment_started',
  URGENT_CARE: 'urgent_care_card_pressed',
};

export default CUSTOM_EVENTS;

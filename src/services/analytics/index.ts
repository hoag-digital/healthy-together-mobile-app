import { getScreenNameForAnalytics } from './getScreenNameForAnalytics';
import { trackClassReservation } from './trackClassReservation';
import { trackLogin } from './trackLogin';
import { trackLogout } from './trackLogout';
import { trackOnDemandVideoPlay } from './trackOnDemandVideoPlay';
import { trackScreenChange } from './trackScreenChange';
import { trackSelfAssessmentStarted } from './trackSelfAssessmentStarted';
import { trackUrgentCareCardPress } from './trackUrgentCareCardPress';

export {
  getScreenNameForAnalytics,
  trackClassReservation,
  trackLogin,
  trackLogout,
  trackOnDemandVideoPlay,
  trackScreenChange,
  trackSelfAssessmentStarted,
  trackUrgentCareCardPress,
};

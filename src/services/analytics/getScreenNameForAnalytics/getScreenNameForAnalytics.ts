/**
 * This object provides a map of navigation routes to formatted screen names for tracking in analytics.
 * Duplicates in the navigation tree, such as ClassDetails, are only required once in the map.
 * Comments have been added in the object to help show each routes position in the navigation tree.
 */
export const SCREEN_NAMES_FOR_ANALYTICS = {
  /** GuestNav */
  Login: 'Login Screen',
  Registration: 'Create Account Screen',
  ForgotPassword: 'Forgot Password Screen',
  /** RootNav */
  Account: 'User Profile Screen',
  NotificationSettings: 'Notification Settings Screen',
  BirthplanCategory: 'Birthplan Category Screen',
  BirthplanDetails: 'Birthplan Details Screen',
  ContentDetailsModal: 'App Content Details Modal',
  PromoDetailsModal: 'Promo App Content Details Modal',
  BiocircuitOnboarding: 'Biocircuit Onboarding Screen',
  PostDetailsModal: 'News Article Details Modal',
  OnDemandClassModal: 'On-Demand Class Details Modal',
  ClassDetailsModal: 'Class Details Modal',
  NicuFaq: 'Nicu FAQ Screen',
  ReservationConfirmation: 'Reservation Confirmation Screen',
  /** DrawerNav */
  /** TabNav */
  /** HomeStackNav */
  Home: 'Home Screen',
  PostDetails: 'News Article Details Screen',
  /** ClassStackNav */
  Classes: 'Classes Screen',
  ClassDetails: 'Class Details Screen',
  OnDemandClass: 'On-Demand Class Details Screen',
  /** MyHoagScreen */
  MyHoag: 'My Hoag Screen',
  Maternity: 'Maternity Module Screen',
  /** ScheduleStackNav */
  Schedule: 'My Schedule Screen',
  /** CareNav */
  Care: 'Care Screen',
  ['Appointment Form']: 'Appointment Form Screen',
  ['Schedule Foothill Appointment']: 'Schedule Foothill Appointment Screen',
  Womens: "Women's Care Screen",
  Telehealth: 'Telehealth Screen',
  ['Self Assessment']: 'Self Assessment Screen',
  /** BiocircuitStackNav */
  BiocircuitIntro: 'Biocircuit Intro Screen',
  BiocircuitDetails: 'Biocircuit Details Screen',
  /** MaternityOnboardingStackNav */
  MaternityWelcome: 'Maternity Welcome Screen',
  MaternityOnboarding1: 'Maternity Onboarding Screen Step 1',
  MaternityOnboarding2: 'Maternity Onboarding Screen Step 2',
  CustomizeMaternityExperience: 'Customizing Maternity Experience Screen',
  MaternityUnknown: 'Maternity Status Unknown Screen',
};

/**
 * returns a more appropriately labeled sceen name from the navigation
 * route for analytics tracking
 */
export function getScreenNameForAnalytics(routeName?: string | null): string | null {
  if (!routeName) {
    return null;
  }

  return SCREEN_NAMES_FOR_ANALYTICS[routeName] || routeName;
}

export default getScreenNameForAnalytics;

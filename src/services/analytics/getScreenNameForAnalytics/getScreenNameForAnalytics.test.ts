import getScreenNameForAnalytics, { SCREEN_NAMES_FOR_ANALYTICS } from './getScreenNameForAnalytics';

const ROUTE_TO_TEST = 'Home';
const NON_EXISTENT_ROUTE = 'TigerKingScreen';

describe('getScreenNameForAnalytics', () => {
  test('returns the mapped screen name for a specific navigation route', () => {
    expect(getScreenNameForAnalytics(ROUTE_TO_TEST)).toEqual(SCREEN_NAMES_FOR_ANALYTICS[ROUTE_TO_TEST]);
  });

  test('returns the route name for a route without a mapped screen name', () => {
    expect(getScreenNameForAnalytics(NON_EXISTENT_ROUTE)).toEqual(NON_EXISTENT_ROUTE);
  });

  test('returns null when no route name is input', () => {
    expect(getScreenNameForAnalytics()).toEqual(null);
  });
});

jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
  });
});

import trackOnDemandVideoPlay from './trackOnDemandVideoPlay';
import { Event } from '../../../graphql/types';

describe('trackOnDemandVideoPlay', () => {
  test('does not throw an error', () => {
    expect(() => trackOnDemandVideoPlay({} as Event)).not.toThrow();
  });
});

import analytics from '@react-native-firebase/analytics';

import { Event } from '../../../graphql/types';
import { CUSTOM_EVENTS } from '../constants';

/**
 * This provides firebase analytics for playing an on-demand video.
 */
export function trackOnDemandVideoPlay(event: Event): void {
  // parameters: custom_event_name, custom_event_params
  analytics().logEvent(CUSTOM_EVENTS.ON_DEMAND_VIDEO, {
    event,
  });
}

export default trackOnDemandVideoPlay;

import analytics from '@react-native-firebase/analytics';

import { Hospital } from '../../../graphql/types';
import { CUSTOM_EVENTS } from '../constants';

/**
 * This provides firebase analytics for pressing an urgent care card.
 */
export function trackUrgentCareCardPress(hospital: Hospital): void {
  // parameters: custom_event_name, custom_event_params
  analytics().logEvent(CUSTOM_EVENTS.URGENT_CARE, {
    hospital,
  });
}

export default trackUrgentCareCardPress;

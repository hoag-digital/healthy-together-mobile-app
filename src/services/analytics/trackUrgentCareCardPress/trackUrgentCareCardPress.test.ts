jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
  });
});

import trackUrgentCareCardPress from './trackUrgentCareCardPress';
import { Hospital } from '../../../graphql/types';

describe('trackUrgentCareCardPress', () => {
  test('does not throw an error', () => {
    expect(() => trackUrgentCareCardPress({} as Hospital)).not.toThrow();
  });
});

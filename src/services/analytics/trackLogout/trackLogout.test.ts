jest.mock('../../../../node_modules/@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
    setUserId: jest.fn(),
    setUserProperties: jest.fn(),
  });
});

import trackLogout from './trackLogout';

describe('trackLogout', () => {
  test('doesn\'t throw an error', () => {
    expect(trackLogout).not.toThrow();
  });
});

import analytics from '@react-native-firebase/analytics';

import { CUSTOM_EVENTS } from '../constants';

/**
 * This provides firebase analytics logout event tracking.
 */
export function trackLogout(): void {
  // parameters: custom_event_name, custom_event_params
  analytics().logEvent(CUSTOM_EVENTS.LOGOUT);

  // parameters: user_id
  analytics().setUserId(null);

  // parameters: user_properties
  analytics().setUserProperties({
    firstName: null,
    lastName: null,
    email: null,
  });
}

export default trackLogout;

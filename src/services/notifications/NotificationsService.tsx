/**
 * Custom notification service implementation using firebase cloud messaging (FCM) for device/user
 * registration, and react-native-notifications for notification presentation and event handling.
 */
import {
  Notifications,
  Notification,
  Registered,
  RegistrationError,
} from 'react-native-notifications';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions } from 'react-navigation';

import { IS_ANDROID } from '../../utils';
import NavigationService from '../../navigation/NavigationService';
import { ASYNCSTORAGE_FCM_KEY } from '../../utils/constants';

export interface NotificationsService {
  saveFcmToken: (args: any) => void;
  messaging: any;
  getInitialNotification: any;
  onNotificationOpened: any;
}

export class NotificationsService {
  constructor() {
    this.saveFcmToken = (_args: any): void => {};
    this.messaging = messaging();
    this.getInitialNotification = null;
    this.onNotificationOpened = null;
  }

  navigateToClassDetails(eventGroupName: string): void {
    // Navigating to the ClassDetails page and setting the Schedule tab as the active tab
    NavigationService.navigate(
      'Schedule',
      {},
      NavigationActions.navigate({
        routeName: 'ClassDetails',
        params: {
          eventGroupName: eventGroupName,
        },
      })
    );
  }

  /**
   * FCM-based notification permission function for iOS. Android automatically resolves to true.
   * Potentially unused in lieu of RNNotifications permissions.
   */
  requestFCMPermission = async (): Promise<string> => {
    const granted = this.messaging.requestPermission();

    if (granted) {
      if (__DEV__) {
        console.log('User granted messaging permissions!');
      }
    } else {
      if (__DEV__) {
        console.log('User declined messaging permissions :(');
      }
    }

    return granted;
  };

  /**
   * Apps using iOS must first register the app with FCM before being able to receive messages.
   * Calling this method allows FCM to send messages to your device, however if you wish to display a
   * visible notification further steps are required.
   * On Android, no registration is required and this method successfully resolves instantly.
   */
  registerAppWithFCM = async (): Promise<void> => {
    try {
      // updated from messaging().registerForRemoteNotifications to comply with v7 deprecation warning
      const register = await this.messaging.registerDeviceForRemoteMessages();

      if (!register) {
        return;
      }

      // Adding this method to suppress a firebase warning. If this actual event handler is desired,
      // move to this.attachNotificationEventHandlers.
      this.messaging.setBackgroundMessageHandler(_remoteMessage => {});
    } catch (err) {
      if (__DEV__) {
        console.log(err);
      }

      return;
    }
  };

  /**
   * Initial setup functions required before attaching RNN event handlers
   * TODO: deprecate and remove RNN once fcm resolves Android onNotificationOpened event
   */
  configureRNNotifications = async (): Promise<void> => {
    Notifications.events().registerRemoteNotificationsRegistered((event: Registered) => {
      // NOTE: this token does not resemble a fcmToken on iOS! it is returning an APNS
      // token which must be converted to an fcm token via an additional POST to another service.
      // see: https://github.com/wix/react-native-notifications/issues/408#issuecomment-591734138
      // On Android this will be identical to the fcm token returned in the function above.
      if (__DEV__) {
        console.log('(RNN) Token Received', event.deviceToken);
      }
    });

    Notifications.events().registerRemoteNotificationsRegistrationFailed(
      (event: RegistrationError) => {
        if (__DEV__) {
          console.log(event);
        }
      }
    );

    // Request permissions on iOS, refresh token on Android
    await Notifications.registerRemoteNotifications();
  };

  /**
   * This method attaches all of the event handlers. They are added as class properties to make sure
   * they are only initialized once each.
   *
   * Note: Most of these are FCM-based, however we are still using RNN as there are some issues with
   * FCM Android notifications.
   *
   * See:
   * https://github.com/invertase/react-native-firebase/pull/3339
   * https://github.com/invertase/react-native-firebase/pull/3339#issuecomment-608319179
   * https://rnfb-docs.netlify.app/messaging/notifications#handling-interaction
   * https://github.com/wix/react-native-notifications/issues/561
   *
   * If foregroundMessageReceived or backgroundMessageReceived event processing is ever required,
   * use this.messaging.onMessage and this.messaging.setMessageBackgroundHandler, respectively.
   */
  attachNotificationEventHandlers(): void {
    /* This provides access to a PN which triggered an app cold-start */
    // NOTE: switching from RNN to FCM to move toward complete RNN removal
    if (!this.getInitialNotification) {
      this.getInitialNotification = this.messaging.getInitialNotification().then(remoteMessage => {
        if (remoteMessage) {
          if (__DEV__) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.notification
            );
          }

          const { data } = remoteMessage;

          // check the notification for details of an upcoming class
          if (data && data.eventGroupName) {
            this.navigateToClassDetails(data.eventGroupName);
          }
        }
      });
    }

    /*
     * This triggers when the notification is opened (foreground or background), not received.
     * Note for iOS, we are using the FCM event handler. For Android, we are using the RNN event handler.
     * TODO: If the FCM-based event handler is fixed to support android, remove the ternary here and
     * all RNN-based logic and dependencies.
     */
    if (!this.onNotificationOpened) {
      this.onNotificationOpened = IS_ANDROID
        ? Notifications.events().registerNotificationOpened(
            // NOTE: the docs suggest some boilerplate that throws TS errors, see: https://github.com/wix/react-native-notifications/pull/499
            (notification: Notification, completion: () => void): void => {
              const { payload } = notification;

              if (__DEV__) {
                console.log(`Notification opened: ${payload}`);
              }

              // check the notification for details of an upcoming class
              if (payload && payload.eventGroupName) {
                this.navigateToClassDetails(payload.eventGroupName);
              }

              completion();
            }
          )
        : this.messaging.onNotificationOpenedApp(remoteMessage => {
            if (__DEV__) {
              console.log('Notification caused app to open from background state:', remoteMessage);
            }

            const { data } = remoteMessage;

            // check the notification for details of an upcoming class
            if (data && data.eventGroupName) {
              this.navigateToClassDetails(data.eventGroupName);
            }
          });
    }
  }

  /**
   * Removes the notification event handlers
   */
  removeNotificationEventHandlers(): void {
    if (this.onNotificationOpened && this.onNotificationOpened.remove) {
      this.onNotificationOpened.remove();
    }
  }

  /**
   * This is the function to "tear down" the notification service
   */
  deinitialize = async (): Promise<void> => {
    // remove notifications from the tray
    Notifications.removeAllDeliveredNotifications();

    // tear down event handlers
    this.removeNotificationEventHandlers();

    // tear down FCM...iOS only
    this.messaging.unregisterDeviceForRemoteMessages();

    // removing the token from the api is the only way to prevent notifications on Android in a
    // logged-out state
    // TODO: test with chron
    await this.saveFcmToken({ variables: { fcmToken: '' } });
  };

  /**
   * This is the main function for registering for push notification services which invloves
   * initializing first with react-native-firebase (FCM) and then react-native-notifications (RNN).
   *
   * Steps:
   * 1. permissions (FCM)
   * 2. FCM registration (FCM)
   * 3. getting token (FCM)
   * 4. setting up messaging handlers (RNN)
   *
   * @param saveFcmToken - the callback function for saving the fcmToken to the server
   */
  initialize = async (saveFcmToken): Promise<void> => {
    let fcmToken;

    // saving the token api call onto the class instance
    this.saveFcmToken = saveFcmToken;

    // Using the permissions util from FCM. Alternatively react-native-permissions could be used.
    const hasPermission = await this.requestFCMPermission();

    if (!hasPermission) return;

    // NOTE: onTokenRefresh seems unecessary but Im leaving this snippet for reference
    // this.messaging.onTokenRefresh((refreshedFcmToken: string) => {
    //   console.log(`token refreshed: ${refreshedFcmToken}`);
    // });

    await this.registerAppWithFCM();

    try {
      // There are reports of this not working on the first run...
      // We are using this token from FCM in lieu of the deviceToken from RNN
      fcmToken = await this.messaging.getToken();

      if (__DEV__) {
        console.log(`(FCM) Token Received: ${fcmToken}`);
      }
    } catch (e) {
      if (__DEV__) {
        console.log('No Token Received Error:', e);
      }

      return;
    }

    if (fcmToken) {
      try {
        // save the fcm token to the user on the server
        this.saveFcmToken({ variables: { fcmToken } });

        // set the fcm token in async storage to init notifications on next app start
        AsyncStorage.setItem(ASYNCSTORAGE_FCM_KEY, fcmToken);
      } catch (error) {
        if (__DEV__) {
          console.log(error);
        }
      }
    }

    await this.configureRNNotifications();

    this.attachNotificationEventHandlers();
  };
}

export default NotificationsService;

import React, { FC, ReactNode, ReactElement, createContext, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

import { AuthUtils } from '../../lib/AuthUtils';
import { ASYNCSTORAGE_FCM_KEY } from '../../utils';
import { useSaveDeviceMutation } from '../../graphql/types';
import { NotificationsService } from './NotificationsService';

// instantiating the NotificationService class; intialization occurs separately
export const Notifications = new NotificationsService();

interface NotificationsProviderProps {
  /** the notifications context object */
  notificationsContext: {
    /** whether the notifications are ready to be initialized */
    notificationsShouldBeInitialized: boolean;
    /** the function to set the notifications as being ready to initialize */
    setNotificationsShouldBeInitialized: (arg: boolean) => void;
  };
  /** child component tree to render */
  children?: ReactNode;
}

const NotificationsContext = createContext({});

export default NotificationsContext;

/**
 * Leveraging context to provide the app state necessary for programatically intializing the push
 * notifications.
 */
export const NotificationsProvider: FC<NotificationsProviderProps> = ({
  notificationsContext,
  children,
}): ReactElement => {
  const [saveFcmToken] = useSaveDeviceMutation();

  /** on startup, check for an fcmToken and an authToken and if both are present, init notifications */
  useEffect((): void => {
    const initNotificationsOnRestart = async (): Promise<void> => {
      const [authToken, fcmToken] = await Promise.all([
        AuthUtils.retrieveToken(),
        AsyncStorage.getItem(ASYNCSTORAGE_FCM_KEY),
      ]);

      if (authToken && fcmToken) {
        Notifications.initialize(saveFcmToken);
      }
    };

    initNotificationsOnRestart();
  }, [saveFcmToken]);

  /** this is potentially triggered from external conditions in the app such as login or class reservation */
  if (notificationsContext.notificationsShouldBeInitialized) {
    // All notification lib code is occuring in this function
    Notifications.initialize(saveFcmToken).then(() => {
      // set this back to false so that it only executes once
      notificationsContext.setNotificationsShouldBeInitialized(false);
    });
  }

  return (
    <NotificationsContext.Provider value={notificationsContext}>
      {children}
    </NotificationsContext.Provider>
  );
};

export const NotificationsConsumer = NotificationsContext.Consumer;

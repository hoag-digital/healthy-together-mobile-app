const displayNone = `.style.display = 'none';`;

export const REMOVE_HEADER_FOOTERS_HOAG_IO = `
(function() {
  var headerDiv = document.querySelector('#header');
  var entryTitle = document.querySelector('.entry-title');
  var footers = document.querySelectorAll('footer');
  var article = document.querySelector('article');

  if(headerDiv){
    headerDiv${displayNone}
  };
  if(entryTitle){
    entryTitle${displayNone}
  };
  if(footers){
    footers[0]${displayNone}
  }

  if(article){
    article.style.paddingBottom = '100px';
  }
})();
`;

export const REMOVE_FORGOT_PASSWORD_LINKS = `(function() {
  // On the intial page
  var loginDiv = document.querySelector('#nav');
  var backDiv = document.querySelector('#backtoblog');
  var footerDiv = document.querySelector('.fixed-bottom');
  var headerDiv = document.querySelector('#header');
  var footers = document.querySelectorAll('footer');

  if(headerDiv){
    headerDiv${displayNone}
  };
  if(footers){
    footers[0]${displayNone}
  }

  // On the second page
  var loginForm = document.querySelector('#loginform');


  if (loginDiv) {
    loginDiv${displayNone}
  }

  if (backDiv) {
    backDiv${displayNone}
  }

  if (footerDiv) {
    footerDiv${displayNone}
  }

  if (loginForm) {
    loginForm${displayNone}
  }

})();`;

export const CLEANUP_SCHEDULE_VIEW_JAVASCRIPT = `(function() {
  // On the intial physician list and physician details pages
  var headerDiv = document.querySelector('#header');
  var contentDiv = document.querySelector('#content');
  var primaryDiv = document.querySelector('#primary');

  if (headerDiv) {
    header.style.display = 'none';
  }

  if (contentDiv) {
    content.style.paddingTop = '0';
  }

  if (primaryDiv) {
    primary.style.marginTop = '0';
  }

  // On the actual appointment scheduling page
  var headerPanelDiv = document.querySelector('#HeaderPanel');
  var scheduleThisDoctorDiv = document.querySelector('#ScheduleThisDoctor');
  var footerZoneDiv = document.querySelector('#FooterZone');
  var calendarContainerDiv = document.querySelector('.calendar-container');
  var calendarMainDiv = document.querySelector('.calendar-main');

  if (headerPanelDiv) {
    headerPanelDiv.style.display = 'none';
  }

  if (scheduleThisDoctorDiv) {
    scheduleThisDoctorDiv.style.marginTop = '0';
  }

  if (footerZoneDiv) {
    footerZoneDiv.style.display = 'none';
  }

  if (calendarContainerDiv) {
    calendarContainerDiv.style.width = '100%';
  }

  if (calendarMainDiv) {
    calendarMainDiv.style.display = 'block';
  }
})();`;

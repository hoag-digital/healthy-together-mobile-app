import React, { FC } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { ASYNCSTORAGE_AUTH_KEY } from '../utils/constants';
import { AuthConsumer } from './AuthConsumer';

export type AuthPropType = {
  token: string | null;
  saveToken: Function;
  clearToken: Function;
  loginModalVisible: boolean;
  setLoginModalVisible: Function;
};

/**
 * Utils for token manipulation.
 */
export class AuthUtils {
  static saveToken = async (token: string): Promise<void> =>
    await AsyncStorage.setItem(ASYNCSTORAGE_AUTH_KEY, token);

  static retrieveToken = async (): Promise<string | null> =>
    await AsyncStorage.getItem(ASYNCSTORAGE_AUTH_KEY);

  static clearToken = async (): Promise<void> =>
    await AsyncStorage.removeItem(ASYNCSTORAGE_AUTH_KEY);
}

/**
 * Helper to show content only if the user is logged in.
 */
export const LoggedInContent: FC<{}> = props => (
  <AuthConsumer>
    {(auth): React.ReactNode => (auth && auth.token ? props.children : null)}
  </AuthConsumer>
);

/**
 * Helper to show content only if the user is logged out.
 */
export const LoggedOutContent: FC<{}> = props => (
  <AuthConsumer>
    {(auth): React.ReactNode => (!auth || !auth.token ? props.children : null)}
  </AuthConsumer>
);

export default AuthUtils;

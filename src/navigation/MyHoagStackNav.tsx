import { createStackNavigator } from 'react-navigation-stack';

import { MyHoagScreen } from '../screens/MyHoagScreen';
import { MaternityScreen } from '../screens/MaternityScreen';
import { BiocircuitStackNav } from './BiocircuitStackNav';

/**
 * MyHoagStackNav is the nav stack available from the My Hoag tab of the TabNav,
 * contains a list of "hoag modules", starting with "maternity".
 */
export const MyHoagStackNav = createStackNavigator(
  {
    MyHoag: {
      screen: MyHoagScreen,
    },
    Maternity: {
      screen: MaternityScreen,
    },
    Biocircuit: {
      screen: BiocircuitStackNav,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'MyHoag',
  }
);

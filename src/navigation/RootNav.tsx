import { createAppContainer, createSwitchNavigator, NavigationContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// import { ClassWaiverScreen } from '../screens/ClassWaiverScreen';
import { AccountScreen } from '../screens/AccountScreen';
import { BiocircuitWelcomeScreen } from '../screens/BiocircuitWelcomeScreen';
import { BirthPlanCategoryScreen } from '../screens/BirthPlanCategoryScreen';
import { BirthplanDetailsScreen } from '../screens/BirthplanDetailsScreen';
import { ClassDetailsScreen } from '../screens/ClassDetailsScreen';
import { ContentDetailsScreen } from '../screens/ContentDetailsScreen';
import { NicuFaq } from '../screens/NicuFaq';
import { NotificationSettingsScreen } from '../screens/NotificationSettingsScreen';
import { OnDemandDetailsScreen } from '../screens/OnDemandDetailsScreen';
import { PostDetailsScreen } from '../screens/PostDetailsScreen';
import { PromoDetailsScreen } from '../screens/PromoDetailsScreen';
import { SunsettingDetailsScreen } from '../screens/SunsettingDetailsScreen';
import { BioCircutWebViewScreen } from '../screens/SunsettingDetailsScreen/BioCircutWebView';
import { ClassesWebViewScreen } from '../screens/SunsettingDetailsScreen/ClassesWebView';
import { HoagWebViewScreen } from '../screens/SunsettingDetailsScreen/HoagWebView';
import { ReservationConfirmationScreen } from '../screens/ReservationConfirmationScreen';
import { NoteDetailsScreen } from '../screens/NoteDetailsScreen';
import { NoteListScreen } from '../screens/NoteListScreen';
import { NoteEditScreen } from '../screens/NoteEditScreen';
import { PregnancyLossLetterScreen } from '../screens/PregnancyLossLetterScreen';
import { KickTrackingEditContent } from '../screens/KickTrackingEditScreen';
import { DrawerNav } from './DrawerNav';
import { GuestNav } from './GuestNav';
import { MaternityOnboardingStackNav } from './MaternityOnboardingStackNav';

/**
 * AppNav is the primary stack visible to a logged-in user.
 */
const AppNav = createStackNavigator(
  {
    Home: {
      screen: DrawerNav,
      navigationOptions: {
        // header: () => <ScreenHeader drawerEnabled />,
      },
    },
    Sunsetting: {
      screen: SunsettingDetailsScreen,
    },
    // If you need a top level modal, for logged in users, put it here
    // TODO(ratkinson): merge Account and Notification into a Stack and leverage card style transitions.
    // postponed temporarily due to issues navigating back to the homescreen.
    Account: {
      screen: AccountScreen,
    },
    BiocircuitOnboarding: {
      screen: BiocircuitWelcomeScreen,
    },
    BirthplanCategory: {
      screen: BirthPlanCategoryScreen,
    },
    BirthplanDetails: {
      screen: BirthplanDetailsScreen,
    },
    ClassDetailsModal: {
      screen: ClassDetailsScreen,
    },
    ContentDetailsModal: {
      screen: ContentDetailsScreen,
    },
    MaternityOnboarding: {
      screen: MaternityOnboardingStackNav,
    },
    NicuFaq: {
      screen: NicuFaq,
    },
    NoteDetailsModal: {
      screen: NoteDetailsScreen,
    },
    NoteEditScreen: {
      screen: NoteEditScreen,
    },
    NotificationSettings: {
      screen: NotificationSettingsScreen,
    },
    NoteListScreen: {
      screen: NoteListScreen,
    },
    OnDemandClassModal: {
      screen: OnDemandDetailsScreen,
    },
    PrenancyLossLetter: {
      screen: PregnancyLossLetterScreen,
    },
    PromoDetailsModal: {
      screen: PromoDetailsScreen,
    },
    BioCircutWebView: {
      screen: BioCircutWebViewScreen,
    },
    ClassesWebView: {
      screen: ClassesWebViewScreen,
    },
    HoagWebView: {
      screen: HoagWebViewScreen,
    },

    //Duplicated PostDetailsRoute that is used as a modal not from Home Stack
    PostDetailsModal: {
      screen: PostDetailsScreen,
    },
    ReservationConfirmation: {
      screen: ReservationConfirmationScreen,
    },
    KickTrackingEditScreen: {
      screen: KickTrackingEditContent,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Sunsetting',
    mode: 'modal',
  }
);

/**
 * Creates a top level Switch Navigator, using currentUser to set the
 * initial route (App or Guest Nav)
 */
export interface CreateRootNavParams {
  /** Current user of the app. If truthy, renders AppNav. Otherwise, GuestNav. Typically an object, but booleans and strings will also work. */
  currentUser?: Record<string, string> | boolean | string;
}

export const createRootNav = ({}: CreateRootNavParams): NavigationContainer => {
  const rootNav = createSwitchNavigator(
    {
      AppNav,
      GuestNav,
    },
    {
      initialRouteName: 'AppNav', // currentUser ? 'AppNav' : 'GuestNav',
    }
  );

  return createAppContainer(rootNav);
};

import { createStackNavigator } from 'react-navigation-stack';

import { ClassListScreen } from '../screens/ClassListScreen';
import { ClassDetailsScreen } from '../screens/ClassDetailsScreen';
import { OnDemandDetailsScreen } from '../screens/OnDemandDetailsScreen';

/**
 * ClassStackNav is the StackNavigator available from the "Classes" tab of the TabBar.
 * Note that "classes" is the user-facing terminology, and that they correspond to "events" in the codebase.
 * The term "events" comes from the fact that classes originate from a wordpress-events manager plugin,
 * and that "Class" is a reserved programming word which is difficult to use as a variable.
 */
export const ClassStackNav = createStackNavigator(
  {
    Classes: {
      screen: ClassListScreen,
    },
    ClassDetails: {
      screen: ClassDetailsScreen,
    },
    OnDemandClass: {
      screen: OnDemandDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Classes',
  }
);

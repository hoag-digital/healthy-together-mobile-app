import React from 'react';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';

import { MaternityWelcomeScreen } from '../screens/MaternityWelcomeScreen';
import { MaternityOnboardingScreen1 } from '../screens/MaternityOnboardingScreen1';
import { MaternityOnboardingScreen2 } from '../screens/MaternityOnboardingScreen2';
import { CustomizingMaternityExperienceScreen } from '../screens/CustomizingMaternityExperienceScreen';
import { MaternityRoleUnknownScreen } from '../screens/MaternityRoleUnknownScreen';

/**
 * MaternityOnboardingStackNav is the StackNavigator available when first launching the applicaiton and going through
 * onboarding if applicable
 */
export const MaternityOnboardingStackNav = createAnimatedSwitchNavigator(
  {
    MaternityWelcome: {
      screen: MaternityWelcomeScreen,
    },
    MaternityOnboarding1: {
      screen: MaternityOnboardingScreen1,
    },
    MaternityOnboarding2: {
      screen: MaternityOnboardingScreen2,
    },
    CustomizeMaternityExperience: {
      screen: CustomizingMaternityExperienceScreen,
    },
    MaternityUnknown: {
      screen: MaternityRoleUnknownScreen,
    },
  },
  {
    initialRouteName: 'MaternityWelcome',
    transition: (
      <Transition.Together>
        <Transition.Out type="fade" durationMs={800} interpolation="easeOut" />
        <Transition.In type="fade" durationMs={600} delayMs={200} interpolation="easeIn" />
      </Transition.Together>
    ),
  }
);

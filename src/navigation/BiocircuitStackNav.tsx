import { createStackNavigator } from 'react-navigation-stack';

import { BiocircuitIntroScreen } from '../screens/BiocircuitIntroScreen';
import { BiocircuitDetailsScreen } from '../screens/BiocircuitDetailsScreen';

/**
 * ProgramStackNav is the StackNavigator available from the "Programs" tab of the TabBar.
 * Programs are a specific subset of classes which require an Rx token, including Biocircuit and Nutrition.
 * Note that "classes" is the user-facing terminology, and that they correspond to "events" in the codebase.
 */
export const BiocircuitStackNav = createStackNavigator(
  {
    BiocircuitIntro: {
      screen: BiocircuitIntroScreen,
    },
    BiocircuitDetails: {
      screen: BiocircuitDetailsScreen,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'BiocircuitIntro',
  }
);

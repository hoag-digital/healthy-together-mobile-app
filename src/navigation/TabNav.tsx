import React, { ReactElement } from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { colors } from '../styles';
import { DEFAULT_ICON_SIZE, LARGEST_ICON_SIZE, TAB_BAR_HEIGHT } from '../utils/constants';

import { space } from '../styles/margins';

import HomeIcon from '../assets/images/menu/home.svg';
import ClassIcon from '../assets/images/menu/classes.svg';
import CareIcon from '../assets/images/menu/care.svg';
import MyHoagIcon from '../assets/images/menu/hoag-nav-icon.svg';
import ScheduleIcon from '../assets/images/menu/my-classes.svg';
import { MyHoagStackNav } from './MyHoagStackNav';
import { HomeStackNav } from './HomeStackNav';
import { ClassStackNav } from './ClassStackNav';
import { CareNav } from './CareNav';
import { ScheduleStackNav } from './ScheduleStackNav';

/**
 * Main Nav is main interface of the app, defaults to tabs.
 */
export const TabNav = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStackNav,
      navigationOptions: {
        tabBarTestID: 'home-screen',
        tabBarIcon: (): ReactElement => (
          <HomeIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    Classes: {
      screen: ClassStackNav,
      navigationOptions: {
        tabBarTestID: 'classes-screen',
        tabBarIcon: (): ReactElement => (
          <ClassIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    MyHoag: {
      screen: MyHoagStackNav,
      navigationOptions: {
        tabBarTestID: 'my-hoag-screen',
        tabBarIcon: (): ReactElement => (
          <MyHoagIcon height={LARGEST_ICON_SIZE} width={LARGEST_ICON_SIZE} />
        ),
        tabBarLabel: 'My Hoag',
      },
    },
    Schedule: {
      screen: ScheduleStackNav,
      navigationOptions: {
        tabBarTestID: 'my-schedule-screen',
        tabBarIcon: (): ReactElement => (
          <ScheduleIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
    Care: {
      screen: CareNav,
      navigationOptions: {
        tabBarTestID: 'care-screen',
        tabBarIcon: (): ReactElement => (
          <CareIcon height={DEFAULT_ICON_SIZE} width={DEFAULT_ICON_SIZE} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    lazy: true,
    tabBarOptions: {
      activeTintColor: colors.black,
      inactiveTintColor: colors.balticSea50,
      showLabel: true,
      style: {
        backgroundColor: colors.whiteTransparent,
        position: 'absolute', // must have positioning or it will force the screen elements to stop above the tabs versus go behind them
        left: 0,
        bottom: 0,
        right: 0,
        paddingTop: space[2],
        paddingBottom: space[2],
        height: TAB_BAR_HEIGHT,
      },
    },
    // tabBarComponent: props => {
    //   return <AnimatedTabBar {...props} />;
    // },
  }
);

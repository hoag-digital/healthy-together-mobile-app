import gql from 'graphql-tag';

gql`
  query minimumAppVersion {
    minimumAppVersion {
      ios
      android
    }
  }
`;

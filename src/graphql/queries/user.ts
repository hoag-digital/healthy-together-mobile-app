import gql from 'graphql-tag';

export const LOGIN_MUTATION = gql`
  mutation login($email: String, $password: String) {
    login(email: $email, password: $password) {
      token
      user {
        id
        email
      }
    }
  }
`;

export const ME_QUERY = gql`
  query ME {
    me {
      id
      email
      firstName
      lastName
      tokenExpirationDate
      device {
        FCMToken
      }
      maternityProfile {
        userId
        role
        lastMenstrualPeriod
        estimatedDueDate
        hasPregnancyLoss
        gestationalAge {
          weeks
          days
          trimester
          currentBabySize {
            description
            image
          }
        }
      }
    }
  }
`;

export const SAVE_FCM_TOKEN_MUTATION = gql`
  mutation saveDevice($fcmToken: String) {
    createDevice(FCMToken: $fcmToken) {
      id
      userId
      FCMToken
    }
  }
`;

export const SIGN_UP_MUTATION = gql`
  mutation signup($signupPayload: SignupPayload) {
    signup(signupPayload: $signupPayload) {
      token
      user {
        id
        email
      }
    }
  }
`;

gql`
  query notificationPreferences {
    notificationPreferences {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation enableNotifications {
    enableNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation disableNotifications {
    disableNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation enableClassReminderNotifications {
    enableClassReminderNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation disableClassReminderNotifications {
    disableClassReminderNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation enableMaternityNotifications {
    enableMaternityNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

gql`
  mutation disableMaternityNotifications {
    disableMaternityNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;

import gql from 'graphql-tag';

gql`
  query homeScreenContent {
    posts(includeFeatured: false, limit: 15) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
    featuredPosts {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
    featuredEventGroups {
      name
      description
      mediumImage
      rawHtml
    }
    featuredOnDemandEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
      rawHtml
    }
  }
`;

// gql`
//   query maternityOverviewContent($trimester: Int) {
//     maternityPosts(trimester: $trimester) {
//       id
//       title
//       date
//       description
//       thumbnailImage
//       mediumImage
//       isFeatured
//       rawHtml
//     }
//     featuredMaternityPosts {
//       id
//       title
//       date
//       description
//       thumbnailImage
//       mediumImage
//       isFeatured
//       rawHtml
//     }
// maternityPromoContent: content(contentType: "Promo", appModule: "Maternity") {
//   id
//   title
//   link
//   description
//   date
//   mediumImage
//   thumbnailImage
//   isFeatured
// }
// maternityEvents {
//   description
//   duration
//   endTime
//   eventCategoryId
//   eventId
//   id
//   name
//   postDate
//   postId
//   startTime
//   mediumImage
//   isOnDemand
// }
//   }
// `;

import gql from 'graphql-tag';

export const CONTENT_BY_CATEGORY_QUERY = gql`
  query postsForContentCategory($categoryId: ID) {
    contentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      #contentCategoryId
      selected
    }
  }
`;

gql`
  query contentById($postId: ID) {
    contentById(id: $postId) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
      featuredVideo
    }
  }
`;

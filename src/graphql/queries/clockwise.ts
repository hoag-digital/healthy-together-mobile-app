import gql from 'graphql-tag';

export const WAIT_TIMES_QUERY = gql`
  query waitTimes {
    waitTimes {
      hospital {
        id
        name
        todaysBusinessHours
        latitude
        longitude
        fullAddress
      }
      waits {
        nextAvailableVisit
        currentWait
        queueLength
        queueTotal
      }
    }
  }
`;

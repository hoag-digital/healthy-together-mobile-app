import gql from 'graphql-tag';

export const POST_BY_ID_QUERY = gql`
  query post($postId: ID) {
    post(id: $postId) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;

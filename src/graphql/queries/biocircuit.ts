import gql from 'graphql-tag';

export const REDEEM_RX_TOKEN_MUTATION = gql`
  mutation redeemToken($token: String) {
    useToken(token: $token) {
      id
      token
      userId
      dateUsed
      daysValid
    }
  }
`;

export const BIOCIRCUIT_EVENT_GROUP_QUERY = gql`
  query biocircuitEvents {
    eventGroup(eventGroupId: "Biocircuit Studio") {
      name
      postContent
      events {
        date
        endTime
        id
        name
        postDate
        postId
        spacesRemaining
        startTime
        totalSpaces
      }
    }
  }
`;

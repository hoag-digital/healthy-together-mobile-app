import gql from 'graphql-tag';

gql`
  query webUrls {
    webUrls {
      foothillAppointment
      maternityPreAdmission
      passwordReset
      selfAssessment
      telehealth
    }
  }
`;

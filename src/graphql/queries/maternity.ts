import gql from 'graphql-tag';

export const CREATE_MATERNITY_PROFILE = gql`
  mutation createMaternityProfile(
    $lastMenstrualPeriod: String
    $estimatedDueDate: String
    $role: String
  ) {
    createMaternityProfile(
      lastMenstrualPeriod: $lastMenstrualPeriod
      estimatedDueDate: $estimatedDueDate
      role: $role
    ) {
      id
      lastMenstrualPeriod
      estimatedDueDate
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;

gql`
  mutation updateMaternityProfile(
    $lastMenstrualPeriod: String
    $estimatedDueDate: String
    $role: String
  ) {
    updateMaternityProfile(
      lastMenstrualPeriod: $lastMenstrualPeriod
      estimatedDueDate: $estimatedDueDate
      role: $role
    ) {
      id
      lastMenstrualPeriod
      estimatedDueDate
      role
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;

gql`
  mutation reportPregnancyLoss {
    reportPregnancyLoss {
      id
      userId
      role
      lastMenstrualPeriod
      estimatedDueDate
      hasPregnancyLoss
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;

gql`
  mutation resetPregnancyLoss {
    resetPregnancyLoss {
      id
      userId
      role
      lastMenstrualPeriod
      estimatedDueDate
      hasPregnancyLoss
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;

gql`
  query nicuFAQ {
    irvine: content(contentType: "faq", contentProgram: "nicu-irvine", appModule: "Maternity") {
      id
      title
      link
      description
      # rawHtml
      date
      mediumImage
      isFeatured
    }
    newportBeach: content(
      contentType: "faq"
      contentProgram: "nicu-newport-beach"
      appModule: "Maternity"
    ) {
      id
      title
      link
      # description
      # rawHtml
      date
      mediumImage
      isFeatured
    }
  }
`;

gql`
  query maternityPosts($trimester: Int) {
    maternityPosts(trimester: $trimester) {
      id
      title
      date
      description
      thumbnailImage
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;

gql`
  query featuredMaternityPosts {
    featuredMaternityPosts {
      id
      title
      date
      description
      thumbnailImage
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;

gql`
  query maternityPromoContent {
    content(contentType: "Promo", appModule: "Maternity") {
      id
      title
      link
      description
      date
      mediumImage
      thumbnailImage
      isFeatured
    }
  }
`;

gql`
  query birthplanCategories {
    birthplanCategories {
      id
      name
      numberSelected
    }
  }
`;

gql`
  query myBirthplan {
    myBirthplan {
      id
      title
      category
    }
  }
`;

gql`
  query birthplanContentCategory($categoryId: ID) {
    birthplanContentByCategory(contentCategoryId: $categoryId) {
      id
      contentCategoryId
      selected
      title
    }
  }
`;

gql`
  mutation recordBirthplanPreference($category: CategoryInput, $preference: BirthplanInput) {
    recordBirthplanPreference(category: $category, preference: $preference) {
      id
      userId
      title
    }
  }
`;

gql`
  query maternityEvents {
    maternityEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;

gql`
  query potentialLabCategories {
    potentialLabCategories {
      id
      name
      numberSelected
    }
  }
`;

gql`
  query potentialLabContentByCategory($categoryId: ID) {
    potentialLabContentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      #contentCategoryId
      selected
    }
  }
`;

gql`
  query milestoneCategories {
    milestoneCategories {
      id
      name
      numberSelected
    }
  }
`;

gql`
  query milestoneContentByCategory($categoryId: ID) {
    milestoneContentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      #contentCategoryId
      selected
    }
  }
`;

gql`
  query myNotes($limit: Int) {
    myNotes(limit: $limit) {
      id
      createdAt
      userId
      contents
    }
  }
`;

gql`
  query myNoteContentById($id: ID!) {
    myNoteContentById(id: $id) {
      id
      createdAt
      userId
      contents
    }
  }
`;

gql`
  mutation createMyNote($contents: String!) {
    createMyNote(contents: $contents) {
      id
      createdAt
      userId
      contents
    }
  }
`;

gql`
  mutation deleteMyNote($id: ID!) {
    deleteMyNote(id: $id) {
      id
      status
    }
  }
`;

gql`
  mutation editMyNote($id: ID!, $contents: String!) {
    editMyNote(id: $id, contents: $contents) {
      id
      status
    }
  }
`;

gql`
  query maternityResourceContent {
    pregnancyLoss: content(contentType: "resources", contentProgram: "pregnancy-loss") {
      id
      title
      link
      description
      # rawHtml
      date
      mediumImage
      isFeatured
    }
  }
`;

gql`
  query myKickTracker {
    myKickTracker {
      id
      startAt
      kicks
      duration
      isActive
    }
  }
`;

gql`
  mutation createMyKickCounter(
    $startAt: String!
    $kicks: Int!
    $duration: Int!
    $isActive: Boolean!
  ) {
    createMyKickCounter(
      startAt: $startAt
      kicks: $kicks
      duration: $duration
      isActive: $isActive
    ) {
      id
      startAt
      kicks
      duration
      isActive
    }
  }
`;

gql`
  mutation deleteMyKickCounter($id: ID!) {
    deleteMyKickCounter(id: $id) {
      id
      status
    }
  }
`;

gql`
  mutation editMyKickCounter(
    $id: ID!
    $startAt: String
    $kicks: Int
    $duration: Int
    $isActive: Boolean
  ) {
    editMyKickCounter(
      id: $id
      startAt: $startAt
      kicks: $kicks
      duration: $duration
      isActive: $isActive
    ) {
      id
      status
    }
  }
`;

gql`
  query maternityOverviewContent($trimester: Int) {
    maternityOverviewArticleContent(contentTrimester: $trimester) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      #contentCategoryId
      selected
    }
    maternityPromoContent: content(contentType: "Promo", appModule: "Maternity") {
      id
      title
      link
      description
      date
      mediumImage
      thumbnailImage
      isFeatured
    }
    maternityEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;

gql`
  query babySizes {
    babySizes {
      id
      title
      mediumImage
      babySize
      babyWeight
    }
  }
`;

gql`
  query predefinedBabyItemsTodosList {
    predefinedBabyItemsTodosList {
      id
      contentCategoryId
      selected
      title
    }
  }
`;

gql`
  query predefinedDeliveryTimeTodosList {
    predefinedDeliveryTimeTodosList {
      id
      contentCategoryId
      selected
      title
    }
  }
`;

gql`
  query todoSection {
    todoSection {
      title
      description
      todoSubSection {
        title
        description
        data {
          id
          title
          selected
        }
      }
    }
  }
`;

gql`
  query myDeliveryTimeTodoList {
    deliveryTimeTodoList {
      id
      title
    }
  }
`;

gql`
  query myBabyItemsTodoList {
    babyItemsTodoList {
      id
      title
    }
  }
`;

gql`
  mutation recordDeliveryTimeTodoListItem($item: TodoInput) {
    recordDeliveryTimeTodoListItem(item: $item) {
      id
      title
    }
  }
`;

gql`
  mutation recordBabyItemsTodoListItem($item: TodoInput) {
    recordBabyItemsTodoListItem(item: $item) {
      id
      title
    }
  }
`;

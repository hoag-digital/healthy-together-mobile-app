import gql from 'graphql-tag';

export const CANCEL_EVENT_REGISTRATION = gql`
  mutation eventCancelRegistration($postId: String) {
    eventCancelRegistration(postId: $postId)
  }
`;

export const EVENT_CATEGORIES_QUERY = gql`
  query eventCategories {
    eventCategories {
      id
      name
      description
      count
      mediumImage
    }
  }
`;

export const EVENT_GROUPS_FOR_EVENT_CATEGORY_QUERY = gql`
  query eventGroupsForEventCategory($eventCategoryId: Int) {
    eventGroups(categoryId: $eventCategoryId) {
      name
      description
      mediumImage
    }
  }
`;

// Note that eventGroups are artificial groupings and thus do not possess ids correlating to any db
// entity. We are using the name of an avent group as its unique identifier.
export const EVENT_QUERY = gql`
  query event($eventId: Int) {
    event(eventId: $eventId) {
      description
      duration
      endTime
      eventEnd
      eventEndDate
      eventEndTime
      eventStart
      eventStartDate
      eventStartTime
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
      rawHtml
    }
  }
`;

// Note that eventGroups are artificial groupings and thus do not possess ids correlating to any db
// entity. We are using the name of an avent group as its unique identifier.
export const HOAG_EVENT_GROUP_QUERY = gql`
  query hoagEventGroups($eventGroupName: String) {
    eventGroup(eventGroupId: $eventGroupName) {
      name
      description
      mediumImage
      rawHtml
      events {
        date
        endTime
        eventStartDate
        id
        isVirtual
        name
        postId
        spacesRemaining
        startTime
        totalSpaces
        youtubeLiveLink
        isOnDemand
        isFeatured
      }
    }
  }
`;

export const MY_SCHEDULED_EVENTS_QUERY = gql`
  query myScheduledEvents {
    myEvents {
      postId
      date
      eventId
      name
      eventStartDate
      eventStart
      startTime
      endTime
    }
  }
`;

export const ON_DEMAND_EVENTS_QUERY = gql`
  query onDemandEvents {
    onDemandEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
    }
  }
`;

// Note that an event is really a wordpress "post" with extra properties. In order to register for an
// event, we use the id of the underlying wordpress post entity.
export const REGISTER_USER_FOR_EVENT_MUTATION = gql`
  mutation EventRegistration($postId: String) {
    eventRegistration(postId: $postId)
  }
`;

export const UNIQUE_UPCOMING_EVENTS_QUERY = gql`
  query uniqueEvents {
    uniqueEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

/** Appointment Queue Information */
export type AppointmentQueue = {
  __typename?: 'AppointmentQueue';
  /** The Queue ID (11624) */
  queueId: Scalars['ID'];
  /** The wait information for this queue */
  queueWaits?: Maybe<WaitInformation>;
};

/** the is the return type when someone logs in. */
export type AuthPayload = {
  __typename?: 'AuthPayload';
  /** The current JWT token. Should be used in Authentication header */
  token: Scalars['String'];
  /** the User type is returned when someone logs in */
  user?: Maybe<User>;
};

export type BirthplanInput = {
  postId?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  selected?: Maybe<Scalars['Boolean']>;
};

export type BirthplanPost = {
  __typename?: 'BirthplanPost';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
};

/** A birthplan preference is a part of the maternity module and stores a reference to a "post" from wordpress */
export type BirthplanPreference = {
  __typename?: 'BirthplanPreference';
  id: Scalars['ID'];
  /** The id of the user in wordpress */
  userId?: Maybe<Scalars['Int']>;
  /** id of the post in wordpress */
  postId?: Maybe<Scalars['Int']>;
  /** The title of the post in wordpress */
  title?: Maybe<Scalars['String']>;
  /** The category of the post in wordpress */
  category?: Maybe<Scalars['String']>;
  /** The category id of the post in wordpress */
  categoryId?: Maybe<Scalars['Int']>;
};

export enum CacheControlScope {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE',
}

export type CategoryInput = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

/**
 * Content (Post) is a generic data entity based on the structure of a Post
 * intended to be used in a variety of app contexts. Whereas Posts are primarily
 * used to signify "articles", Content is more generic and static content.
 * (Sometimes still rendered similarly to an article).
 * Content has been separated from Posts due to the complexity it adds to the
 * sql querying and the graphql schema.
 */
export type Content = {
  __typename?: 'Content';
  /** Unique identifier of the content post */
  id: Scalars['ID'];
  /** The timestamp of the content post */
  date?: Maybe<Scalars['String']>;
  /** The title of the content post */
  title?: Maybe<Scalars['String']>;
  /** An expanded description of the content post */
  description?: Maybe<Scalars['String']>;
  /** Rich raw HTML provided for the content post */
  rawHtml?: Maybe<Scalars['String']>;
  /** What is this */
  link?: Maybe<Scalars['String']>;
  /** The related medium-sized media asset source location */
  mediumImage?: Maybe<Scalars['String']>;
  /** The related thumbnail media asset source location */
  thumbnailImage?: Maybe<Scalars['String']>;
  /** Is this a featured post */
  isFeatured?: Maybe<Scalars['Boolean']>;
  /** Featured Video with this post */
  featuredVideo?: Maybe<Scalars['String']>;
  /** Optional paramter that displays if the post has been saved as a preference */
  selected?: Maybe<Scalars['Boolean']>;
  /** The baby length for baby-size content type post */
  babySize?: Maybe<Scalars['String']>;
  /** The baby weight for baby-size content type post */
  babyWeight?: Maybe<Scalars['String']>;
  /** App content category that is optional on a post */
  contentCategoryId?: Maybe<Scalars['Int']>;
  /** Full category details optionally available */
  contentCategory?: Maybe<ContentCategory>;
};

export type ContentCategory = {
  __typename?: 'ContentCategory';
  /** Unique identifier of the app content category */
  id: Scalars['ID'];
  /** The name of the app content category */
  name?: Maybe<Scalars['String']>;
  /** Computed value of preferences selected by user in the category */
  numberSelected?: Maybe<Scalars['Int']>;
};

export type CurrentBabySize = {
  __typename?: 'CurrentBabySize';
  description?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['String']>;
};

/** Result returned by delete mutation for kick counter */
export type DeleteKickCounterResult = {
  __typename?: 'DeleteKickCounterResult';
  /** Id of kick counter being deleted */
  id: Scalars['ID'];
  /** Status of kick counter delete mutation */
  status: Scalars['Boolean'];
};

/** Result returned by delete mutation for note */
export type DeleteNoteResult = {
  __typename?: 'DeleteNoteResult';
  /** Id of note being deleted */
  id: Scalars['ID'];
  /** Status of note delete mutation */
  status: Scalars['Boolean'];
};

/**
 * A device is a phone/app-installation combo that belongs to a specific user.
 * We can use a unique firebase cloud messaging (FCM) token to send push notifications to these devices.
 */
export type Device = {
  __typename?: 'Device';
  id: Scalars['ID'];
  /** The firebase cloud messaging token generated from a user's app installation */
  FCMToken: Scalars['String'];
  /** The id of the user in wordpress */
  userId?: Maybe<Scalars['Int']>;
};

/** Minimum mobile app version types */
export type DeviceType = {
  __typename?: 'DeviceType';
  /** IOS app version */
  ios?: Maybe<Scalars['String']>;
  /** Android app version */
  android?: Maybe<Scalars['String']>;
};

/** Result returned by edit mutation for kick counter */
export type EditKickCounterResult = {
  __typename?: 'EditKickCounterResult';
  /** Id of kick counter being edited */
  id: Scalars['ID'];
  /** Status of kick counter edit mutation */
  status: Scalars['Boolean'];
};

/** Result returned by edit mutation for note */
export type EditNoteResult = {
  __typename?: 'EditNoteResult';
  /** Id of note being edited */
  id: Scalars['ID'];
  /** Status of note edit mutation */
  status: Scalars['Boolean'];
};

export type Event = {
  __typename?: 'Event';
  /** Unique identifier of the wordpress event */
  id: Scalars['ID'];
  /** Unique identifier of the wordpress event */
  eventId?: Maybe<Scalars['String']>;
  /** Unique identifier of the wordpress post */
  postId?: Maybe<Scalars['String']>;
  /** the slug of the wordpress event */
  eventSlug?: Maybe<Scalars['String']>;
  /** the event status seems like default is 1 */
  eventStatus?: Maybe<Scalars['Int']>;
  /** the name of the event */
  name?: Maybe<Scalars['String']>;
  /** The related meium-sized media asset source location */
  mediumImage?: Maybe<Scalars['String']>;
  /** The related thumbnail media asset source location */
  thumbnailImage?: Maybe<Scalars['String']>;
  /** the formatted date the event starts without the time */
  date?: Maybe<Scalars['String']>;
  /** the formatted description generated from decoding the html of the post_content */
  description?: Maybe<Scalars['String']>;
  /** the duration of the event (in minutes) as computed from the eventStartTime and eventEndTime */
  duration?: Maybe<Scalars['Int']>;
  /** the date the event starts without the time */
  eventStartDate?: Maybe<Scalars['String']>;
  /** the date the event ends without the time */
  eventEndDate?: Maybe<Scalars['String']>;
  /** The start time of the event */
  eventStartTime?: Maybe<Scalars['String']>;
  /** The end time of the class */
  eventEndTime?: Maybe<Scalars['String']>;
  /** The date and time of the start of the event */
  eventStart?: Maybe<Scalars['String']>;
  /** The date and time of the end of the event */
  eventEnd?: Maybe<Scalars['String']>;
  /** The start time of the event with the date conjoined */
  startTime?: Maybe<Scalars['String']>;
  /** The end time of the event with the date conjoined */
  endTime?: Maybe<Scalars['String']>;
  /** the post content of the event */
  postContent?: Maybe<Scalars['String']>;
  /** the status of the post */
  postStatus?: Maybe<Scalars['String']>;
  /** The date of the of the post */
  postDate?: Maybe<Scalars['String']>;
  /** The url to the event on the website */
  guid?: Maybe<Scalars['String']>;
  /** The title of the class type (same as ClassGroup) */
  title?: Maybe<Scalars['String']>;
  /** The number of spaces booked for the event */
  bookedSpaces?: Maybe<Scalars['Int']>;
  /** The total number of event spaces */
  totalSpaces?: Maybe<Scalars['Int']>;
  /** The number of spaces remaining for the event */
  spacesRemaining?: Maybe<Scalars['Int']>;
  /** this means that the class is a virtual class if checked */
  isVirtual?: Maybe<Scalars['Boolean']>;
  /** youtube live link is a url for the class to watch it virtually */
  youtubeLiveLink?: Maybe<Scalars['String']>;
  /** this is a special set of classes meant to be displayed with priority on the frontend */
  isFeatured?: Maybe<Scalars['Boolean']>;
  /** whether this class contains the is_on_demand custom field */
  isOnDemand?: Maybe<Scalars['Boolean']>;
  /** TODO: improve to resolve list of event category objects */
  eventCategoryId?: Maybe<Scalars['Int']>;
  /** Rich Text HTML */
  rawHtml?: Maybe<Scalars['String']>;
};

export type EventCategory = {
  __typename?: 'EventCategory';
  /** Unique identifier of the wordpress event category */
  id: Scalars['ID'];
  /** Display name of the category from wordpress */
  name?: Maybe<Scalars['String']>;
  /** Full description of the cateogry from wordpress */
  description?: Maybe<Scalars['String']>;
  /** Classes under category */
  count?: Maybe<Scalars['Int']>;
  /** The related meium-sized media asset source location */
  mediumImage?: Maybe<Scalars['String']>;
  /** The related thumbnail media asset source location */
  thumbnailImage?: Maybe<Scalars['String']>;
};

export type EventGroup = {
  __typename?: 'EventGroup';
  /** Unique identifier of the wordpress event */
  id: Scalars['ID'];
  /** Unique identifier of the wordpress event */
  eventId?: Maybe<Scalars['String']>;
  /** Unique identifier of the wordpress post */
  postId?: Maybe<Scalars['String']>;
  /** the slug of the wordpress event */
  eventSlug?: Maybe<Scalars['String']>;
  /** the event status seems like default is 1 */
  eventStatus?: Maybe<Scalars['Int']>;
  /** The related meium-sized media asset source location */
  mediumImage?: Maybe<Scalars['String']>;
  /** The related thumbnail media asset source location */
  thumbnailImage?: Maybe<Scalars['String']>;
  /** the name of the event */
  name?: Maybe<Scalars['String']>;
  /** Unique identifier of the class type */
  title?: Maybe<Scalars['String']>;
  /** the post content of the event */
  postContent?: Maybe<Scalars['String']>;
  /** the formatted description generated from decoding the html of the post_content */
  description?: Maybe<Scalars['String']>;
  /** The events sharing this specific class type */
  events?: Maybe<Array<Maybe<Event>>>;
  /** Raw HTML for Event */
  rawHtml?: Maybe<Scalars['String']>;
};

export type GestationalAge = {
  __typename?: 'GestationalAge';
  weeks?: Maybe<Scalars['Int']>;
  days?: Maybe<Scalars['Int']>;
  trimester?: Maybe<Scalars['Int']>;
  currentBabySize?: Maybe<CurrentBabySize>;
};

/** Information about a Hoag Facility (Hospital or Urgent Care Center) */
export type Hospital = {
  __typename?: 'Hospital';
  /** The ID of the record (220) */
  id: Scalars['ID'];
  /** The full name (Hoag Urgent Care Aliso Viejo) */
  name?: Maybe<Scalars['String']>;
  /** Latitude of the facility (33.5773195) */
  latitude?: Maybe<Scalars['String']>;
  /** Longitude of the facility (-117.725834) */
  longitude?: Maybe<Scalars['String']>;
  /** Full Address (Address 1, Address 2, City, State ZIP, USA) */
  fullAddress?: Maybe<Scalars['String']>;
  /** Phone Int (123-123-1234) */
  phoneInt?: Maybe<Scalars['String']>;
  /** Time Zone (America/Los_Angeles) */
  timeZone?: Maybe<Scalars['String']>;
  /** Business Hours (08:00 am - 08:00 pm) */
  todaysBusinessHours?: Maybe<Scalars['String']>;
};

export type HospitalGroup = {
  __typename?: 'HospitalGroup';
  /** The ID of the record (220) */
  id: Scalars['ID'];
  /** The name of the group (Hoag Urgent Care) */
  name?: Maybe<Scalars['String']>;
  /** Hospitals or Facilities under the group */
  hospitals: Array<Hospital>;
};

/** KickCounter is a part of maternity module and stores kick counter sessions added by user */
export type KickCounter = {
  __typename?: 'KickCounter';
  /** Id of kick counter */
  id: Scalars['ID'];
  /** Timestamp for kick counter session creation */
  startAt: Scalars['String'];
  /** User's id */
  userId?: Maybe<Scalars['Int']>;
  /** Number of kicks recorded during counter session */
  kicks: Scalars['Int'];
  /** Duration of kick counter session(recorded in seconds) */
  duration: Scalars['Int'];
  /** To check if it is active session user is tracking */
  isActive?: Maybe<Scalars['Boolean']>;
};

/** A maternity profile is a module specific profile in One Hoag that provides information for the users experience */
export type MaternityProfile = {
  __typename?: 'MaternityProfile';
  id: Scalars['ID'];
  /** The id of the user in wordpress */
  userId?: Maybe<Scalars['Int']>;
  /** The user entered date of Last Menstrual Cycle */
  lastMenstrualPeriod?: Maybe<Scalars['String']>;
  /** The user entered date of the estimated due date */
  estimatedDueDate?: Maybe<Scalars['String']>;
  /** Computed values for gestational age */
  gestationalAge?: Maybe<GestationalAge>;
  /** users role in maternity module, parent or pregnant */
  role?: Maybe<Scalars['String']>;
  /** if the user has experienced a pregnancy loss, allows us to surface resources */
  hasPregnancyLoss?: Maybe<Scalars['Boolean']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  login?: Maybe<AuthPayload>;
  signup?: Maybe<AuthPayload>;
  /** This mutation stores the preferences for a user within a given category */
  updateBirthplanPreferences?: Maybe<Array<Maybe<BirthplanPreference>>>;
  /**
   * This mutation updates the value of a given preference based on selection
   * (true/false) within a given category but only one item
   */
  recordBirthplanPreference?: Maybe<BirthplanPreference>;
  /** This mutation creates a device record containg an FCM token and a user id */
  createDevice?: Maybe<Device>;
  eventRegistration?: Maybe<Scalars['Boolean']>;
  eventCancelRegistration?: Maybe<Scalars['Boolean']>;
  eventNotifyTest?: Maybe<Scalars['Boolean']>;
  /** This mutation creates a maternity profile for the user id from context and allows use of this module */
  createMaternityProfile?: Maybe<MaternityProfile>;
  /** This mutation allows the user to update their maternity profile */
  updateMaternityProfile?: Maybe<MaternityProfile>;
  /** This mutation allows a user to report a pregnancy loss */
  reportPregnancyLoss?: Maybe<MaternityProfile>;
  /** This mutation allows a user to turn off pregnancy loss resources */
  resetPregnancyLoss?: Maybe<MaternityProfile>;
  /** This mutation enables the master notification preference for a user */
  enableNotifications?: Maybe<NotificationPreference>;
  /** This mutation disables the master notification preference for a user */
  disableNotifications?: Maybe<NotificationPreference>;
  /** This mutation enables the class reminder notification preference for a user */
  enableClassReminderNotifications?: Maybe<NotificationPreference>;
  /** This mutation disables the class reminder notification preference for a user */
  disableClassReminderNotifications?: Maybe<NotificationPreference>;
  /** This mutation enables the maternity notification preference for a user */
  enableMaternityNotifications?: Maybe<NotificationPreference>;
  /** This mutation disables the maternity notification preference for a user */
  disableMaternityNotifications?: Maybe<NotificationPreference>;
  /**
   * this route might be deleted, but it is for creating a set of tokens that will be
   * able to be used by hoag.
   */
  useToken?: Maybe<Token>;
  /** This mutation stores users's note */
  createMyNote?: Maybe<Note>;
  /** This mutation delete user's note */
  deleteMyNote?: Maybe<DeleteNoteResult>;
  /** This mutation edit user's note */
  editMyNote?: Maybe<EditNoteResult>;
  /** This mutation stores the delivery time todo list for a user */
  updateDeliveryTimeTodoList?: Maybe<Array<Maybe<TodoItem>>>;
  /** This mutation stores the baby items todo list for a user */
  updateBabyItemsTodoList?: Maybe<Array<Maybe<TodoItem>>>;
  /** This mutation updates the value of a given todo item based on selection (true/false) for delivery time todo list */
  recordDeliveryTimeTodoListItem?: Maybe<TodoItem>;
  /** This mutation updates the value of a given todo item based on selection (true/false) for baby item todo list */
  recordBabyItemsTodoListItem?: Maybe<TodoItem>;
  /** This mutation stores users's kick counter */
  createMyKickCounter?: Maybe<KickCounter>;
  /** This mutation delete user's kick counter */
  deleteMyKickCounter?: Maybe<DeleteKickCounterResult>;
  /** This mutation edit user's kick counter */
  editMyKickCounter?: Maybe<EditKickCounterResult>;
};

export type MutationLoginArgs = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

export type MutationSignupArgs = {
  signupPayload?: Maybe<SignupPayload>;
};

export type MutationUpdateBirthplanPreferencesArgs = {
  category?: Maybe<CategoryInput>;
  preferences?: Maybe<Array<Maybe<BirthplanInput>>>;
};

export type MutationRecordBirthplanPreferenceArgs = {
  category?: Maybe<CategoryInput>;
  preference?: Maybe<BirthplanInput>;
};

export type MutationCreateDeviceArgs = {
  FCMToken?: Maybe<Scalars['String']>;
};

export type MutationEventRegistrationArgs = {
  postId?: Maybe<Scalars['String']>;
};

export type MutationEventCancelRegistrationArgs = {
  postId?: Maybe<Scalars['String']>;
};

export type MutationEventNotifyTestArgs = {
  fcmToken?: Maybe<Scalars['String']>;
  eventName?: Maybe<Scalars['String']>;
};

export type MutationCreateMaternityProfileArgs = {
  lastMenstrualPeriod?: Maybe<Scalars['String']>;
  estimatedDueDate?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
};

export type MutationUpdateMaternityProfileArgs = {
  lastMenstrualPeriod?: Maybe<Scalars['String']>;
  estimatedDueDate?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
};

export type MutationUseTokenArgs = {
  token?: Maybe<Scalars['String']>;
};

export type MutationCreateMyNoteArgs = {
  contents: Scalars['String'];
};

export type MutationDeleteMyNoteArgs = {
  id: Scalars['ID'];
};

export type MutationEditMyNoteArgs = {
  id: Scalars['ID'];
  contents: Scalars['String'];
};

export type MutationUpdateDeliveryTimeTodoListArgs = {
  todoItems?: Maybe<Array<Maybe<TodoInput>>>;
};

export type MutationUpdateBabyItemsTodoListArgs = {
  todoItems?: Maybe<Array<Maybe<TodoInput>>>;
};

export type MutationRecordDeliveryTimeTodoListItemArgs = {
  item?: Maybe<TodoInput>;
};

export type MutationRecordBabyItemsTodoListItemArgs = {
  item?: Maybe<TodoInput>;
};

export type MutationCreateMyKickCounterArgs = {
  startAt: Scalars['String'];
  kicks: Scalars['Int'];
  duration: Scalars['Int'];
  isActive: Scalars['Boolean'];
};

export type MutationDeleteMyKickCounterArgs = {
  id: Scalars['ID'];
};

export type MutationEditMyKickCounterArgs = {
  id: Scalars['ID'];
  startAt?: Maybe<Scalars['String']>;
  kicks?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  isActive?: Maybe<Scalars['Boolean']>;
};

/** Note is a part of maternity  module and stores note create by user */
export type Note = {
  __typename?: 'Note';
  /** Id of note */
  id: Scalars['ID'];
  /** Timestamp for note creation */
  createdAt?: Maybe<Scalars['String']>;
  /** user's id */
  userId?: Maybe<Scalars['Int']>;
  /** Content of note */
  contents?: Maybe<Scalars['String']>;
};

/** A notification preference stores the various push notification settings for a user */
export type NotificationPreference = {
  __typename?: 'NotificationPreference';
  id: Scalars['ID'];
  /** The id of the user in wordpress */
  userId?: Maybe<Scalars['Int']>;
  /** Whether push notifications are globally enabled or disabled for the user */
  notificationsEnabled?: Maybe<Scalars['Boolean']>;
  /** Whether class reminder push notifications are enabled or disabled for the user */
  classReminderNotificationsEnabled?: Maybe<Scalars['Boolean']>;
  /** Whether maternity push notifications are enabled or disabled for the user */
  maternityNotificationsEnabled?: Maybe<Scalars['Boolean']>;
};

/** A Post is a Wordpress Post entity. Usually consists of a news article. */
export type Post = {
  __typename?: 'Post';
  /** Unique identifier of the post */
  id: Scalars['ID'];
  /** The timestamp of the post */
  date?: Maybe<Scalars['String']>;
  /** The title of the post */
  title?: Maybe<Scalars['String']>;
  /** An expanded description of the post */
  description?: Maybe<Scalars['String']>;
  /** Rich raw HTML provided for the post */
  rawHtml?: Maybe<Scalars['String']>;
  /** Link to the post hosted */
  link?: Maybe<Scalars['String']>;
  /** The related meium-sized media asset source location */
  mediumImage?: Maybe<Scalars['String']>;
  /** The related thumbnail media asset source location */
  thumbnailImage?: Maybe<Scalars['String']>;
  /** Is this a featured post */
  isFeatured?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  me?: Maybe<User>;
  /** Allows a user to query for the info of their device (specifically the birthplan_preferences) */
  birthplanPreferences?: Maybe<Array<Maybe<BirthplanPreference>>>;
  /** Returns a list of users birthplan preferences */
  myBirthplan?: Maybe<Array<Maybe<BirthplanPreference>>>;
  /** Query for a specific content post by id */
  contentById?: Maybe<Content>;
  /** Query for a list of content by contentType and contentProgram */
  content?: Maybe<Array<Maybe<Content>>>;
  /** Query to retrieve content posts for given content category */
  contentByCategory?: Maybe<Array<Maybe<Content>>>;
  /** Query to retrieve content posts for given content category */
  babySizes?: Maybe<Array<Maybe<Content>>>;
  /** Query for birthplan categories */
  birthplanCategories?: Maybe<Array<Maybe<ContentCategory>>>;
  /** Query to retrieve content posts for given category within birth-plan content type */
  birthplanContentByCategory?: Maybe<Array<Maybe<Content>>>;
  /** Query for "milestones" categories */
  milestoneCategories?: Maybe<Array<Maybe<ContentCategory>>>;
  /** Query to retrieve content posts for given category within milestone content type */
  milestoneContentByCategory?: Maybe<Array<Maybe<Content>>>;
  /** Query for "potential lab" categories */
  potentialLabCategories?: Maybe<Array<Maybe<ContentCategory>>>;
  /** Query to retrieve content posts for given category within potential-lab content type */
  potentialLabContentByCategory?: Maybe<Array<Maybe<Content>>>;
  /** Query to retrieve content posts for given trimester category within maternity overview content */
  maternityOverviewArticleContent?: Maybe<Array<Maybe<Content>>>;
  /** Query for todo categories */
  todoCategories?: Maybe<Array<Maybe<ContentCategory>>>;
  /** Query for predefined delivery time todos */
  predefinedDeliveryTimeTodosList?: Maybe<Array<Maybe<Content>>>;
  /** Query for predefined delivery time todos */
  predefinedBabyItemsTodosList?: Maybe<Array<Maybe<Content>>>;
  /** Query for todoSection */
  todoSection?: Maybe<Array<Maybe<TodoSection>>>;
  /** Allows a user to query for the info of their device (specifically the FCMtoken) */
  device?: Maybe<Device>;
  myEvents?: Maybe<Array<Maybe<Event>>>;
  events?: Maybe<Array<Maybe<Event>>>;
  event?: Maybe<Event>;
  featuredEventGroups?: Maybe<Array<Maybe<EventGroup>>>;
  onDemandEvents?: Maybe<Array<Maybe<Event>>>;
  featuredOnDemandEvents?: Maybe<Array<Maybe<Event>>>;
  eventGroups?: Maybe<Array<Maybe<EventGroup>>>;
  eventGroup?: Maybe<EventGroup>;
  eventCategories?: Maybe<Array<Maybe<EventCategory>>>;
  uniqueEvents?: Maybe<Array<Maybe<Event>>>;
  maternityEvents?: Maybe<Array<Maybe<Event>>>;
  /** List all Hospitals */
  hospitalGroups: Array<HospitalGroup>;
  /** Fetch information for single Hospital */
  hospital: Hospital;
  /** Fetch all wait times for a group */
  waitTimes: Array<WaitData>;
  /** Allows a user to query for the info of their device (specifically the maternity_profiles) */
  maternityProfile?: Maybe<MaternityProfile>;
  /** Allows a user to query for the info of their device (specifically the notification_preferences) */
  notificationPreferences?: Maybe<NotificationPreference>;
  /** Query for a specific post */
  post?: Maybe<Post>;
  /** Query for a list of posts */
  posts?: Maybe<Array<Maybe<Post>>>;
  /** Query for a list of featured posts */
  featuredPosts?: Maybe<Array<Maybe<Post>>>;
  /** Query to get posts specific to Maternity Module */
  maternityPosts?: Maybe<Array<Maybe<Post>>>;
  /** Query to get featured posts specific to Maternity Module */
  featuredMaternityPosts?: Maybe<Array<Maybe<Post>>>;
  /** Query for web urls */
  webUrls?: Maybe<WebUrlType>;
  /** Query for app version information */
  minimumAppVersion?: Maybe<DeviceType>;
  /** Allows a user to query for the their notes */
  myNotes?: Maybe<Array<Maybe<Note>>>;
  /** To get detail of user note */
  myNoteContentById?: Maybe<Note>;
  /** Returns a list of users todo items for delivery time */
  deliveryTimeTodoList?: Maybe<Array<Maybe<TodoItem>>>;
  /** Returns a list of users todo items for baby items */
  babyItemsTodoList?: Maybe<Array<Maybe<TodoItem>>>;
  /** Allows a user to query for the their kick counters */
  myKickTracker?: Maybe<Array<Maybe<KickCounter>>>;
};

export type QueryBirthplanPreferencesArgs = {
  categoryId?: Maybe<Scalars['Int']>;
};

export type QueryContentByIdArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryContentArgs = {
  contentType?: Maybe<Scalars['String']>;
  contentProgram?: Maybe<Scalars['String']>;
  appModule?: Maybe<Scalars['String']>;
};

export type QueryContentByCategoryArgs = {
  contentCategoryId?: Maybe<Scalars['ID']>;
};

export type QueryBirthplanContentByCategoryArgs = {
  contentCategoryId?: Maybe<Scalars['ID']>;
};

export type QueryMilestoneContentByCategoryArgs = {
  contentCategoryId?: Maybe<Scalars['ID']>;
};

export type QueryPotentialLabContentByCategoryArgs = {
  contentCategoryId?: Maybe<Scalars['ID']>;
};

export type QueryMaternityOverviewArticleContentArgs = {
  contentTrimester?: Maybe<Scalars['Int']>;
};

export type QueryEventArgs = {
  eventId?: Maybe<Scalars['Int']>;
};

export type QueryEventGroupsArgs = {
  categoryId?: Maybe<Scalars['Int']>;
};

export type QueryEventGroupArgs = {
  eventGroupId?: Maybe<Scalars['String']>;
};

export type QueryMaternityEventsArgs = {
  module?: Maybe<Scalars['String']>;
};

export type QueryWaitTimesArgs = {
  groupId?: Maybe<Scalars['ID']>;
};

export type QueryPostArgs = {
  id?: Maybe<Scalars['ID']>;
};

export type QueryPostsArgs = {
  includeFeatured?: Maybe<Scalars['Boolean']>;
  limit?: Maybe<Scalars['Int']>;
};

export type QueryMaternityPostsArgs = {
  trimester?: Maybe<Scalars['Int']>;
};

export type QueryFeaturedMaternityPostsArgs = {
  trimester?: Maybe<Scalars['Int']>;
};

export type QueryMyNotesArgs = {
  limit?: Maybe<Scalars['Int']>;
};

export type QueryMyNoteContentByIdArgs = {
  id: Scalars['ID'];
};

/** this payload is what we expect when signing up. */
export type SignupPayload = {
  /** the email address used to sign up. */
  email?: Maybe<Scalars['String']>;
  /** first name of the user */
  firstName?: Maybe<Scalars['String']>;
  /** Last name of the user */
  lastName?: Maybe<Scalars['String']>;
  /** the users password */
  password?: Maybe<Scalars['String']>;
  /** the users the password again to verify they typed it correctly */
  passwordConfirmation?: Maybe<Scalars['String']>;
};

export type TodoInput = {
  postId?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  selected?: Maybe<Scalars['Boolean']>;
};

/** A todo item is a part of the maternity module and stores a reference to a "post" from wordpress */
export type TodoItem = {
  __typename?: 'TodoItem';
  id: Scalars['ID'];
  /** The id of the user in wordpress */
  userId?: Maybe<Scalars['Int']>;
  /** id of the post in wordpress */
  postId?: Maybe<Scalars['Int']>;
  /** The title of the post in wordpress */
  title?: Maybe<Scalars['String']>;
  /** The category of the post in wordpress */
  category?: Maybe<Scalars['String']>;
  /** The category tag (slug) of the post in wordpress */
  categoryTag?: Maybe<Scalars['String']>;
};

export type TodoPost = {
  __typename?: 'TodoPost';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
};

export type TodoSection = {
  __typename?: 'TodoSection';
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  todoSubSection?: Maybe<Array<Maybe<TodoSubSection>>>;
};

export type TodoSubSection = {
  __typename?: 'TodoSubSection';
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  data?: Maybe<Array<Maybe<Content>>>;
};

/**
 * Tokens are used to unlock classes that need to be approved by doctors.
 * when a user types in the correct token, we will attach a user id to the token
 * this will let us check the logged in user for a token. tokens have expiration
 * so they need to be validated by the date that they were used.
 */
export type Token = {
  __typename?: 'Token';
  id: Scalars['ID'];
  token: Scalars['String'];
  userId?: Maybe<Scalars['Int']>;
  dateUsed?: Maybe<Scalars['String']>;
  daysValid?: Maybe<Scalars['Int']>;
};

export type User = {
  __typename?: 'User';
  /** the primary Key of the user */
  id: Scalars['ID'];
  /** the email address used to login. */
  email?: Maybe<Scalars['String']>;
  /** first name of the user */
  firstName?: Maybe<Scalars['String']>;
  /** last name of the user */
  lastName?: Maybe<Scalars['String']>;
  /**
   * this is the last day the token can be used to sign up for classes.
   * if it is null they dont have a valid token
   */
  tokenExpirationDate?: Maybe<Scalars['String']>;
  /** the device for a user */
  device?: Maybe<Device>;
  /** The users profile for maternity module if it exists */
  maternityProfile?: Maybe<MaternityProfile>;
  /** The users notificationPreferences if they exist */
  notificationPreferences?: Maybe<NotificationPreference>;
};

/** Represents a Hospitals wait information from Clockwise MD Data. */
export type WaitData = {
  __typename?: 'WaitData';
  /** The hospital this wait data is associated with */
  hospital?: Maybe<Hospital>;
  /** The wait information for this hospital */
  waits?: Maybe<WaitInformation>;
  /** Appointment Queues */
  appointmentQueues: Array<AppointmentQueue>;
};

/** Wait information */
export type WaitInformation = {
  __typename?: 'WaitInformation';
  /** The next available visit (26). If the facility is closed, this will return null */
  nextAvailableVisit?: Maybe<Scalars['Int']>;
  /** The current wait time (26 - 46). If the facility is closed, this will return null */
  currentWait?: Maybe<Scalars['String']>;
  /** The current length of queue */
  queueLength?: Maybe<Scalars['Int']>;
  /** The total people in queue (9) */
  queueTotal?: Maybe<Scalars['Int']>;
};

/** A list of web urls */
export type WebUrlType = {
  __typename?: 'WebUrlType';
  /** Maternity pre admission form web url */
  maternityPreAdmission?: Maybe<Scalars['String']>;
  /** Password Reset url */
  passwordReset?: Maybe<Scalars['String']>;
  /** Foothill Appointment url */
  foothillAppointment?: Maybe<Scalars['String']>;
  /** Womens url */
  womens?: Maybe<Scalars['String']>;
  /** Telehealth url */
  telehealth?: Maybe<Scalars['String']>;
  /** Self Assessment url */
  selfAssessment?: Maybe<Scalars['String']>;
};

export type RedeemTokenMutationVariables = {
  token?: Maybe<Scalars['String']>;
};

export type RedeemTokenMutation = { __typename?: 'Mutation' } & {
  useToken: Maybe<
    { __typename?: 'Token' } & Pick<Token, 'id' | 'token' | 'userId' | 'dateUsed' | 'daysValid'>
  >;
};

export type BiocircuitEventsQueryVariables = {};

export type BiocircuitEventsQuery = { __typename?: 'Query' } & {
  eventGroup: Maybe<
    { __typename?: 'EventGroup' } & Pick<EventGroup, 'name' | 'postContent'> & {
        events: Maybe<
          Array<
            Maybe<
              { __typename?: 'Event' } & Pick<
                Event,
                | 'date'
                | 'endTime'
                | 'id'
                | 'name'
                | 'postDate'
                | 'postId'
                | 'spacesRemaining'
                | 'startTime'
                | 'totalSpaces'
              >
            >
          >
        >;
      }
  >;
};

export type WaitTimesQueryVariables = {};

export type WaitTimesQuery = { __typename?: 'Query' } & {
  waitTimes: Array<
    { __typename?: 'WaitData' } & {
      hospital: Maybe<
        { __typename?: 'Hospital' } & Pick<
          Hospital,
          'id' | 'name' | 'todaysBusinessHours' | 'latitude' | 'longitude' | 'fullAddress'
        >
      >;
      waits: Maybe<
        { __typename?: 'WaitInformation' } & Pick<
          WaitInformation,
          'nextAvailableVisit' | 'currentWait' | 'queueLength' | 'queueTotal'
        >
      >;
    }
  >;
};

export type PostsForContentCategoryQueryVariables = {
  categoryId?: Maybe<Scalars['ID']>;
};

export type PostsForContentCategoryQuery = { __typename?: 'Query' } & {
  contentByCategory: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'date'
          | 'title'
          | 'description'
          | 'rawHtml'
          | 'link'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
          | 'selected'
        >
      >
    >
  >;
};

export type ContentByIdQueryVariables = {
  postId?: Maybe<Scalars['ID']>;
};

export type ContentByIdQuery = { __typename?: 'Query' } & {
  contentById: Maybe<
    { __typename?: 'Content' } & Pick<
      Content,
      | 'id'
      | 'title'
      | 'date'
      | 'description'
      | 'mediumImage'
      | 'isFeatured'
      | 'rawHtml'
      | 'featuredVideo'
    >
  >;
};

export type EventCancelRegistrationMutationVariables = {
  postId?: Maybe<Scalars['String']>;
};

export type EventCancelRegistrationMutation = { __typename?: 'Mutation' } & Pick<
  Mutation,
  'eventCancelRegistration'
>;

export type EventCategoriesQueryVariables = {};

export type EventCategoriesQuery = { __typename?: 'Query' } & {
  eventCategories: Maybe<
    Array<
      Maybe<
        { __typename?: 'EventCategory' } & Pick<
          EventCategory,
          'id' | 'name' | 'description' | 'count' | 'mediumImage'
        >
      >
    >
  >;
};

export type EventGroupsForEventCategoryQueryVariables = {
  eventCategoryId?: Maybe<Scalars['Int']>;
};

export type EventGroupsForEventCategoryQuery = { __typename?: 'Query' } & {
  eventGroups: Maybe<
    Array<
      Maybe<
        { __typename?: 'EventGroup' } & Pick<EventGroup, 'name' | 'description' | 'mediumImage'>
      >
    >
  >;
};

export type EventQueryVariables = {
  eventId?: Maybe<Scalars['Int']>;
};

export type EventQuery = { __typename?: 'Query' } & {
  event: Maybe<
    { __typename?: 'Event' } & Pick<
      Event,
      | 'description'
      | 'duration'
      | 'endTime'
      | 'eventEnd'
      | 'eventEndDate'
      | 'eventEndTime'
      | 'eventStart'
      | 'eventStartDate'
      | 'eventStartTime'
      | 'id'
      | 'name'
      | 'postDate'
      | 'postId'
      | 'startTime'
      | 'mediumImage'
      | 'youtubeLiveLink'
      | 'rawHtml'
    >
  >;
};

export type HoagEventGroupsQueryVariables = {
  eventGroupName?: Maybe<Scalars['String']>;
};

export type HoagEventGroupsQuery = { __typename?: 'Query' } & {
  eventGroup: Maybe<
    { __typename?: 'EventGroup' } & Pick<
      EventGroup,
      'name' | 'description' | 'mediumImage' | 'rawHtml'
    > & {
        events: Maybe<
          Array<
            Maybe<
              { __typename?: 'Event' } & Pick<
                Event,
                | 'date'
                | 'endTime'
                | 'eventStartDate'
                | 'id'
                | 'isVirtual'
                | 'name'
                | 'postId'
                | 'spacesRemaining'
                | 'startTime'
                | 'totalSpaces'
                | 'youtubeLiveLink'
                | 'isOnDemand'
                | 'isFeatured'
              >
            >
          >
        >;
      }
  >;
};

export type MyScheduledEventsQueryVariables = {};

export type MyScheduledEventsQuery = { __typename?: 'Query' } & {
  myEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'postId'
          | 'date'
          | 'eventId'
          | 'name'
          | 'eventStartDate'
          | 'eventStart'
          | 'startTime'
          | 'endTime'
        >
      >
    >
  >;
};

export type OnDemandEventsQueryVariables = {};

export type OnDemandEventsQuery = { __typename?: 'Query' } & {
  onDemandEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'description'
          | 'duration'
          | 'endTime'
          | 'eventCategoryId'
          | 'eventId'
          | 'id'
          | 'name'
          | 'postDate'
          | 'postId'
          | 'startTime'
          | 'mediumImage'
          | 'youtubeLiveLink'
        >
      >
    >
  >;
};

export type EventRegistrationMutationVariables = {
  postId?: Maybe<Scalars['String']>;
};

export type EventRegistrationMutation = { __typename?: 'Mutation' } & Pick<
  Mutation,
  'eventRegistration'
>;

export type UniqueEventsQueryVariables = {};

export type UniqueEventsQuery = { __typename?: 'Query' } & {
  uniqueEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'description'
          | 'duration'
          | 'endTime'
          | 'eventCategoryId'
          | 'eventId'
          | 'id'
          | 'name'
          | 'postDate'
          | 'postId'
          | 'startTime'
          | 'mediumImage'
          | 'isOnDemand'
        >
      >
    >
  >;
};

export type CreateMaternityProfileMutationVariables = {
  lastMenstrualPeriod?: Maybe<Scalars['String']>;
  estimatedDueDate?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
};

export type CreateMaternityProfileMutation = { __typename?: 'Mutation' } & {
  createMaternityProfile: Maybe<
    { __typename?: 'MaternityProfile' } & Pick<
      MaternityProfile,
      'id' | 'lastMenstrualPeriod' | 'estimatedDueDate'
    > & {
        gestationalAge: Maybe<
          { __typename?: 'GestationalAge' } & Pick<GestationalAge, 'weeks' | 'days' | 'trimester'>
        >;
      }
  >;
};

export type UpdateMaternityProfileMutationVariables = {
  lastMenstrualPeriod?: Maybe<Scalars['String']>;
  estimatedDueDate?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
};

export type UpdateMaternityProfileMutation = { __typename?: 'Mutation' } & {
  updateMaternityProfile: Maybe<
    { __typename?: 'MaternityProfile' } & Pick<
      MaternityProfile,
      'id' | 'lastMenstrualPeriod' | 'estimatedDueDate' | 'role'
    > & {
        gestationalAge: Maybe<
          { __typename?: 'GestationalAge' } & Pick<GestationalAge, 'weeks' | 'days' | 'trimester'>
        >;
      }
  >;
};

export type ReportPregnancyLossMutationVariables = {};

export type ReportPregnancyLossMutation = { __typename?: 'Mutation' } & {
  reportPregnancyLoss: Maybe<
    { __typename?: 'MaternityProfile' } & Pick<
      MaternityProfile,
      'id' | 'userId' | 'role' | 'lastMenstrualPeriod' | 'estimatedDueDate' | 'hasPregnancyLoss'
    > & {
        gestationalAge: Maybe<
          { __typename?: 'GestationalAge' } & Pick<GestationalAge, 'weeks' | 'days' | 'trimester'>
        >;
      }
  >;
};

export type ResetPregnancyLossMutationVariables = {};

export type ResetPregnancyLossMutation = { __typename?: 'Mutation' } & {
  resetPregnancyLoss: Maybe<
    { __typename?: 'MaternityProfile' } & Pick<
      MaternityProfile,
      'id' | 'userId' | 'role' | 'lastMenstrualPeriod' | 'estimatedDueDate' | 'hasPregnancyLoss'
    > & {
        gestationalAge: Maybe<
          { __typename?: 'GestationalAge' } & Pick<GestationalAge, 'weeks' | 'days' | 'trimester'>
        >;
      }
  >;
};

export type NicuFaqQueryVariables = {};

export type NicuFaqQuery = { __typename?: 'Query' } & {
  irvine: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'title' | 'link' | 'description' | 'date' | 'mediumImage' | 'isFeatured'
        >
      >
    >
  >;
  newportBeach: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'title' | 'link' | 'date' | 'mediumImage' | 'isFeatured'
        >
      >
    >
  >;
};

export type MaternityPostsQueryVariables = {
  trimester?: Maybe<Scalars['Int']>;
};

export type MaternityPostsQuery = { __typename?: 'Query' } & {
  maternityPosts: Maybe<
    Array<
      Maybe<
        { __typename?: 'Post' } & Pick<
          Post,
          | 'id'
          | 'title'
          | 'date'
          | 'description'
          | 'thumbnailImage'
          | 'mediumImage'
          | 'isFeatured'
          | 'rawHtml'
        >
      >
    >
  >;
};

export type FeaturedMaternityPostsQueryVariables = {};

export type FeaturedMaternityPostsQuery = { __typename?: 'Query' } & {
  featuredMaternityPosts: Maybe<
    Array<
      Maybe<
        { __typename?: 'Post' } & Pick<
          Post,
          | 'id'
          | 'title'
          | 'date'
          | 'description'
          | 'thumbnailImage'
          | 'mediumImage'
          | 'isFeatured'
          | 'rawHtml'
        >
      >
    >
  >;
};

export type MaternityPromoContentQueryVariables = {};

export type MaternityPromoContentQuery = { __typename?: 'Query' } & {
  content: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'title'
          | 'link'
          | 'description'
          | 'date'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
        >
      >
    >
  >;
};

export type BirthplanCategoriesQueryVariables = {};

export type BirthplanCategoriesQuery = { __typename?: 'Query' } & {
  birthplanCategories: Maybe<
    Array<
      Maybe<
        { __typename?: 'ContentCategory' } & Pick<ContentCategory, 'id' | 'name' | 'numberSelected'>
      >
    >
  >;
};

export type MyBirthplanQueryVariables = {};

export type MyBirthplanQuery = { __typename?: 'Query' } & {
  myBirthplan: Maybe<
    Array<
      Maybe<
        { __typename?: 'BirthplanPreference' } & Pick<
          BirthplanPreference,
          'id' | 'title' | 'category'
        >
      >
    >
  >;
};

export type BirthplanContentCategoryQueryVariables = {
  categoryId?: Maybe<Scalars['ID']>;
};

export type BirthplanContentCategoryQuery = { __typename?: 'Query' } & {
  birthplanContentByCategory: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'contentCategoryId' | 'selected' | 'title'
        >
      >
    >
  >;
};

export type RecordBirthplanPreferenceMutationVariables = {
  category?: Maybe<CategoryInput>;
  preference?: Maybe<BirthplanInput>;
};

export type RecordBirthplanPreferenceMutation = { __typename?: 'Mutation' } & {
  recordBirthplanPreference: Maybe<
    { __typename?: 'BirthplanPreference' } & Pick<BirthplanPreference, 'id' | 'userId' | 'title'>
  >;
};

export type MaternityEventsQueryVariables = {};

export type MaternityEventsQuery = { __typename?: 'Query' } & {
  maternityEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'description'
          | 'duration'
          | 'endTime'
          | 'eventCategoryId'
          | 'eventId'
          | 'id'
          | 'name'
          | 'postDate'
          | 'postId'
          | 'startTime'
          | 'mediumImage'
          | 'isOnDemand'
        >
      >
    >
  >;
};

export type PotentialLabCategoriesQueryVariables = {};

export type PotentialLabCategoriesQuery = { __typename?: 'Query' } & {
  potentialLabCategories: Maybe<
    Array<
      Maybe<
        { __typename?: 'ContentCategory' } & Pick<ContentCategory, 'id' | 'name' | 'numberSelected'>
      >
    >
  >;
};

export type PotentialLabContentByCategoryQueryVariables = {
  categoryId?: Maybe<Scalars['ID']>;
};

export type PotentialLabContentByCategoryQuery = { __typename?: 'Query' } & {
  potentialLabContentByCategory: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'date'
          | 'title'
          | 'description'
          | 'rawHtml'
          | 'link'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
          | 'selected'
        >
      >
    >
  >;
};

export type MilestoneCategoriesQueryVariables = {};

export type MilestoneCategoriesQuery = { __typename?: 'Query' } & {
  milestoneCategories: Maybe<
    Array<
      Maybe<
        { __typename?: 'ContentCategory' } & Pick<ContentCategory, 'id' | 'name' | 'numberSelected'>
      >
    >
  >;
};

export type MilestoneContentByCategoryQueryVariables = {
  categoryId?: Maybe<Scalars['ID']>;
};

export type MilestoneContentByCategoryQuery = { __typename?: 'Query' } & {
  milestoneContentByCategory: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'date'
          | 'title'
          | 'description'
          | 'rawHtml'
          | 'link'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
          | 'selected'
        >
      >
    >
  >;
};

export type MyNotesQueryVariables = {
  limit?: Maybe<Scalars['Int']>;
};

export type MyNotesQuery = { __typename?: 'Query' } & {
  myNotes: Maybe<
    Array<Maybe<{ __typename?: 'Note' } & Pick<Note, 'id' | 'createdAt' | 'userId' | 'contents'>>>
  >;
};

export type MyNoteContentByIdQueryVariables = {
  id: Scalars['ID'];
};

export type MyNoteContentByIdQuery = { __typename?: 'Query' } & {
  myNoteContentById: Maybe<
    { __typename?: 'Note' } & Pick<Note, 'id' | 'createdAt' | 'userId' | 'contents'>
  >;
};

export type CreateMyNoteMutationVariables = {
  contents: Scalars['String'];
};

export type CreateMyNoteMutation = { __typename?: 'Mutation' } & {
  createMyNote: Maybe<
    { __typename?: 'Note' } & Pick<Note, 'id' | 'createdAt' | 'userId' | 'contents'>
  >;
};

export type DeleteMyNoteMutationVariables = {
  id: Scalars['ID'];
};

export type DeleteMyNoteMutation = { __typename?: 'Mutation' } & {
  deleteMyNote: Maybe<
    { __typename?: 'DeleteNoteResult' } & Pick<DeleteNoteResult, 'id' | 'status'>
  >;
};

export type EditMyNoteMutationVariables = {
  id: Scalars['ID'];
  contents: Scalars['String'];
};

export type EditMyNoteMutation = { __typename?: 'Mutation' } & {
  editMyNote: Maybe<{ __typename?: 'EditNoteResult' } & Pick<EditNoteResult, 'id' | 'status'>>;
};

export type MaternityResourceContentQueryVariables = {};

export type MaternityResourceContentQuery = { __typename?: 'Query' } & {
  pregnancyLoss: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'title' | 'link' | 'description' | 'date' | 'mediumImage' | 'isFeatured'
        >
      >
    >
  >;
};

export type MyKickTrackerQueryVariables = {};

export type MyKickTrackerQuery = { __typename?: 'Query' } & {
  myKickTracker: Maybe<
    Array<
      Maybe<
        { __typename?: 'KickCounter' } & Pick<
          KickCounter,
          'id' | 'startAt' | 'kicks' | 'duration' | 'isActive'
        >
      >
    >
  >;
};

export type CreateMyKickCounterMutationVariables = {
  startAt: Scalars['String'];
  kicks: Scalars['Int'];
  duration: Scalars['Int'];
  isActive: Scalars['Boolean'];
};

export type CreateMyKickCounterMutation = { __typename?: 'Mutation' } & {
  createMyKickCounter: Maybe<
    { __typename?: 'KickCounter' } & Pick<
      KickCounter,
      'id' | 'startAt' | 'kicks' | 'duration' | 'isActive'
    >
  >;
};

export type DeleteMyKickCounterMutationVariables = {
  id: Scalars['ID'];
};

export type DeleteMyKickCounterMutation = { __typename?: 'Mutation' } & {
  deleteMyKickCounter: Maybe<
    { __typename?: 'DeleteKickCounterResult' } & Pick<DeleteKickCounterResult, 'id' | 'status'>
  >;
};

export type EditMyKickCounterMutationVariables = {
  id: Scalars['ID'];
  startAt?: Maybe<Scalars['String']>;
  kicks?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  isActive?: Maybe<Scalars['Boolean']>;
};

export type EditMyKickCounterMutation = { __typename?: 'Mutation' } & {
  editMyKickCounter: Maybe<
    { __typename?: 'EditKickCounterResult' } & Pick<EditKickCounterResult, 'id' | 'status'>
  >;
};

export type MaternityOverviewContentQueryVariables = {
  trimester?: Maybe<Scalars['Int']>;
};

export type MaternityOverviewContentQuery = { __typename?: 'Query' } & {
  maternityOverviewArticleContent: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'date'
          | 'title'
          | 'description'
          | 'rawHtml'
          | 'link'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
          | 'selected'
        >
      >
    >
  >;
  maternityPromoContent: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          | 'id'
          | 'title'
          | 'link'
          | 'description'
          | 'date'
          | 'mediumImage'
          | 'thumbnailImage'
          | 'isFeatured'
        >
      >
    >
  >;
  maternityEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'description'
          | 'duration'
          | 'endTime'
          | 'eventCategoryId'
          | 'eventId'
          | 'id'
          | 'name'
          | 'postDate'
          | 'postId'
          | 'startTime'
          | 'mediumImage'
          | 'isOnDemand'
        >
      >
    >
  >;
};

export type BabySizesQueryVariables = {};

export type BabySizesQuery = { __typename?: 'Query' } & {
  babySizes: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'title' | 'mediumImage' | 'babySize' | 'babyWeight'
        >
      >
    >
  >;
};

export type PredefinedBabyItemsTodosListQueryVariables = {};

export type PredefinedBabyItemsTodosListQuery = { __typename?: 'Query' } & {
  predefinedBabyItemsTodosList: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'contentCategoryId' | 'selected' | 'title'
        >
      >
    >
  >;
};

export type PredefinedDeliveryTimeTodosListQueryVariables = {};

export type PredefinedDeliveryTimeTodosListQuery = { __typename?: 'Query' } & {
  predefinedDeliveryTimeTodosList: Maybe<
    Array<
      Maybe<
        { __typename?: 'Content' } & Pick<
          Content,
          'id' | 'contentCategoryId' | 'selected' | 'title'
        >
      >
    >
  >;
};

export type TodoSectionQueryVariables = {};

export type TodoSectionQuery = { __typename?: 'Query' } & {
  todoSection: Maybe<
    Array<
      Maybe<
        { __typename?: 'TodoSection' } & Pick<TodoSection, 'title' | 'description'> & {
            todoSubSection: Maybe<
              Array<
                Maybe<
                  { __typename?: 'TodoSubSection' } & Pick<
                    TodoSubSection,
                    'title' | 'description'
                  > & {
                      data: Maybe<
                        Array<
                          Maybe<
                            { __typename?: 'Content' } & Pick<Content, 'id' | 'title' | 'selected'>
                          >
                        >
                      >;
                    }
                >
              >
            >;
          }
      >
    >
  >;
};

export type MyDeliveryTimeTodoListQueryVariables = {};

export type MyDeliveryTimeTodoListQuery = { __typename?: 'Query' } & {
  deliveryTimeTodoList: Maybe<
    Array<Maybe<{ __typename?: 'TodoItem' } & Pick<TodoItem, 'id' | 'title'>>>
  >;
};

export type MyBabyItemsTodoListQueryVariables = {};

export type MyBabyItemsTodoListQuery = { __typename?: 'Query' } & {
  babyItemsTodoList: Maybe<
    Array<Maybe<{ __typename?: 'TodoItem' } & Pick<TodoItem, 'id' | 'title'>>>
  >;
};

export type RecordDeliveryTimeTodoListItemMutationVariables = {
  item?: Maybe<TodoInput>;
};

export type RecordDeliveryTimeTodoListItemMutation = { __typename?: 'Mutation' } & {
  recordDeliveryTimeTodoListItem: Maybe<
    { __typename?: 'TodoItem' } & Pick<TodoItem, 'id' | 'title'>
  >;
};

export type RecordBabyItemsTodoListItemMutationVariables = {
  item?: Maybe<TodoInput>;
};

export type RecordBabyItemsTodoListItemMutation = { __typename?: 'Mutation' } & {
  recordBabyItemsTodoListItem: Maybe<{ __typename?: 'TodoItem' } & Pick<TodoItem, 'id' | 'title'>>;
};

export type MinimumAppVersionQueryVariables = {};

export type MinimumAppVersionQuery = { __typename?: 'Query' } & {
  minimumAppVersion: Maybe<{ __typename?: 'DeviceType' } & Pick<DeviceType, 'ios' | 'android'>>;
};

export type PostQueryVariables = {
  postId?: Maybe<Scalars['ID']>;
};

export type PostQuery = { __typename?: 'Query' } & {
  post: Maybe<
    { __typename?: 'Post' } & Pick<
      Post,
      'id' | 'title' | 'date' | 'description' | 'mediumImage' | 'isFeatured' | 'rawHtml'
    >
  >;
};

export type HomeScreenContentQueryVariables = {};

export type HomeScreenContentQuery = { __typename?: 'Query' } & {
  posts: Maybe<
    Array<
      Maybe<
        { __typename?: 'Post' } & Pick<
          Post,
          'id' | 'title' | 'date' | 'description' | 'mediumImage' | 'isFeatured' | 'rawHtml'
        >
      >
    >
  >;
  featuredPosts: Maybe<
    Array<
      Maybe<
        { __typename?: 'Post' } & Pick<
          Post,
          'id' | 'title' | 'date' | 'description' | 'mediumImage' | 'isFeatured' | 'rawHtml'
        >
      >
    >
  >;
  featuredEventGroups: Maybe<
    Array<
      Maybe<
        { __typename?: 'EventGroup' } & Pick<
          EventGroup,
          'name' | 'description' | 'mediumImage' | 'rawHtml'
        >
      >
    >
  >;
  featuredOnDemandEvents: Maybe<
    Array<
      Maybe<
        { __typename?: 'Event' } & Pick<
          Event,
          | 'description'
          | 'duration'
          | 'endTime'
          | 'eventCategoryId'
          | 'eventId'
          | 'id'
          | 'name'
          | 'postDate'
          | 'postId'
          | 'startTime'
          | 'mediumImage'
          | 'youtubeLiveLink'
          | 'rawHtml'
        >
      >
    >
  >;
};

export type LoginMutationVariables = {
  email?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

export type LoginMutation = { __typename?: 'Mutation' } & {
  login: Maybe<
    { __typename?: 'AuthPayload' } & Pick<AuthPayload, 'token'> & {
        user: Maybe<{ __typename?: 'User' } & Pick<User, 'id' | 'email'>>;
      }
  >;
};

export type MeQueryVariables = {};

export type MeQuery = { __typename?: 'Query' } & {
  me: Maybe<
    { __typename?: 'User' } & Pick<
      User,
      'id' | 'email' | 'firstName' | 'lastName' | 'tokenExpirationDate'
    > & {
        device: Maybe<{ __typename?: 'Device' } & Pick<Device, 'FCMToken'>>;
        maternityProfile: Maybe<
          { __typename?: 'MaternityProfile' } & Pick<
            MaternityProfile,
            'userId' | 'role' | 'lastMenstrualPeriod' | 'estimatedDueDate' | 'hasPregnancyLoss'
          > & {
              gestationalAge: Maybe<
                { __typename?: 'GestationalAge' } & Pick<
                  GestationalAge,
                  'weeks' | 'days' | 'trimester'
                > & {
                    currentBabySize: Maybe<
                      { __typename?: 'CurrentBabySize' } & Pick<
                        CurrentBabySize,
                        'description' | 'image'
                      >
                    >;
                  }
              >;
            }
        >;
      }
  >;
};

export type SaveDeviceMutationVariables = {
  fcmToken?: Maybe<Scalars['String']>;
};

export type SaveDeviceMutation = { __typename?: 'Mutation' } & {
  createDevice: Maybe<{ __typename?: 'Device' } & Pick<Device, 'id' | 'userId' | 'FCMToken'>>;
};

export type SignupMutationVariables = {
  signupPayload?: Maybe<SignupPayload>;
};

export type SignupMutation = { __typename?: 'Mutation' } & {
  signup: Maybe<
    { __typename?: 'AuthPayload' } & Pick<AuthPayload, 'token'> & {
        user: Maybe<{ __typename?: 'User' } & Pick<User, 'id' | 'email'>>;
      }
  >;
};

export type NotificationPreferencesQueryVariables = {};

export type NotificationPreferencesQuery = { __typename?: 'Query' } & {
  notificationPreferences: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type EnableNotificationsMutationVariables = {};

export type EnableNotificationsMutation = { __typename?: 'Mutation' } & {
  enableNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type DisableNotificationsMutationVariables = {};

export type DisableNotificationsMutation = { __typename?: 'Mutation' } & {
  disableNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type EnableClassReminderNotificationsMutationVariables = {};

export type EnableClassReminderNotificationsMutation = { __typename?: 'Mutation' } & {
  enableClassReminderNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type DisableClassReminderNotificationsMutationVariables = {};

export type DisableClassReminderNotificationsMutation = { __typename?: 'Mutation' } & {
  disableClassReminderNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type EnableMaternityNotificationsMutationVariables = {};

export type EnableMaternityNotificationsMutation = { __typename?: 'Mutation' } & {
  enableMaternityNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type DisableMaternityNotificationsMutationVariables = {};

export type DisableMaternityNotificationsMutation = { __typename?: 'Mutation' } & {
  disableMaternityNotifications: Maybe<
    { __typename?: 'NotificationPreference' } & Pick<
      NotificationPreference,
      | 'userId'
      | 'notificationsEnabled'
      | 'classReminderNotificationsEnabled'
      | 'maternityNotificationsEnabled'
    >
  >;
};

export type WebUrlsQueryVariables = {};

export type WebUrlsQuery = { __typename?: 'Query' } & {
  webUrls: Maybe<
    { __typename?: 'WebUrlType' } & Pick<
      WebUrlType,
      | 'foothillAppointment'
      | 'maternityPreAdmission'
      | 'passwordReset'
      | 'selfAssessment'
      | 'telehealth'
    >
  >;
};

export const RedeemTokenDocument = gql`
  mutation redeemToken($token: String) {
    useToken(token: $token) {
      id
      token
      userId
      dateUsed
      daysValid
    }
  }
`;
export type RedeemTokenMutationFn = ApolloReactCommon.MutationFunction<
  RedeemTokenMutation,
  RedeemTokenMutationVariables
>;
export type RedeemTokenComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<RedeemTokenMutation, RedeemTokenMutationVariables>,
  'mutation'
>;

export const RedeemTokenComponent = (props: RedeemTokenComponentProps) => (
  <ApolloReactComponents.Mutation<RedeemTokenMutation, RedeemTokenMutationVariables>
    mutation={RedeemTokenDocument}
    {...props}
  />
);

/**
 * __useRedeemTokenMutation__
 *
 * To run a mutation, you first call `useRedeemTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRedeemTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [redeemTokenMutation, { data, loading, error }] = useRedeemTokenMutation({
 *   variables: {
 *      token: // value for 'token'
 *   },
 * });
 */
export function useRedeemTokenMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    RedeemTokenMutation,
    RedeemTokenMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<RedeemTokenMutation, RedeemTokenMutationVariables>(
    RedeemTokenDocument,
    baseOptions
  );
}
export type RedeemTokenMutationHookResult = ReturnType<typeof useRedeemTokenMutation>;
export type RedeemTokenMutationResult = ApolloReactCommon.MutationResult<RedeemTokenMutation>;
export type RedeemTokenMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RedeemTokenMutation,
  RedeemTokenMutationVariables
>;
export const BiocircuitEventsDocument = gql`
  query biocircuitEvents {
    eventGroup(eventGroupId: "Biocircuit Studio") {
      name
      postContent
      events {
        date
        endTime
        id
        name
        postDate
        postId
        spacesRemaining
        startTime
        totalSpaces
      }
    }
  }
`;
export type BiocircuitEventsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    BiocircuitEventsQuery,
    BiocircuitEventsQueryVariables
  >,
  'query'
>;

export const BiocircuitEventsComponent = (props: BiocircuitEventsComponentProps) => (
  <ApolloReactComponents.Query<BiocircuitEventsQuery, BiocircuitEventsQueryVariables>
    query={BiocircuitEventsDocument}
    {...props}
  />
);

/**
 * __useBiocircuitEventsQuery__
 *
 * To run a query within a React component, call `useBiocircuitEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useBiocircuitEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useBiocircuitEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useBiocircuitEventsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    BiocircuitEventsQuery,
    BiocircuitEventsQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<BiocircuitEventsQuery, BiocircuitEventsQueryVariables>(
    BiocircuitEventsDocument,
    baseOptions
  );
}
export function useBiocircuitEventsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    BiocircuitEventsQuery,
    BiocircuitEventsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<BiocircuitEventsQuery, BiocircuitEventsQueryVariables>(
    BiocircuitEventsDocument,
    baseOptions
  );
}
export type BiocircuitEventsQueryHookResult = ReturnType<typeof useBiocircuitEventsQuery>;
export type BiocircuitEventsLazyQueryHookResult = ReturnType<typeof useBiocircuitEventsLazyQuery>;
export type BiocircuitEventsQueryResult = ApolloReactCommon.QueryResult<
  BiocircuitEventsQuery,
  BiocircuitEventsQueryVariables
>;
export const WaitTimesDocument = gql`
  query waitTimes {
    waitTimes {
      hospital {
        id
        name
        todaysBusinessHours
        latitude
        longitude
        fullAddress
      }
      waits {
        nextAvailableVisit
        currentWait
        queueLength
        queueTotal
      }
    }
  }
`;
export type WaitTimesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<WaitTimesQuery, WaitTimesQueryVariables>,
  'query'
>;

export const WaitTimesComponent = (props: WaitTimesComponentProps) => (
  <ApolloReactComponents.Query<WaitTimesQuery, WaitTimesQueryVariables>
    query={WaitTimesDocument}
    {...props}
  />
);

/**
 * __useWaitTimesQuery__
 *
 * To run a query within a React component, call `useWaitTimesQuery` and pass it any options that fit your needs.
 * When your component renders, `useWaitTimesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWaitTimesQuery({
 *   variables: {
 *   },
 * });
 */
export function useWaitTimesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<WaitTimesQuery, WaitTimesQueryVariables>
) {
  return ApolloReactHooks.useQuery<WaitTimesQuery, WaitTimesQueryVariables>(
    WaitTimesDocument,
    baseOptions
  );
}
export function useWaitTimesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<WaitTimesQuery, WaitTimesQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<WaitTimesQuery, WaitTimesQueryVariables>(
    WaitTimesDocument,
    baseOptions
  );
}
export type WaitTimesQueryHookResult = ReturnType<typeof useWaitTimesQuery>;
export type WaitTimesLazyQueryHookResult = ReturnType<typeof useWaitTimesLazyQuery>;
export type WaitTimesQueryResult = ApolloReactCommon.QueryResult<
  WaitTimesQuery,
  WaitTimesQueryVariables
>;
export const PostsForContentCategoryDocument = gql`
  query postsForContentCategory($categoryId: ID) {
    contentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      selected
    }
  }
`;
export type PostsForContentCategoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    PostsForContentCategoryQuery,
    PostsForContentCategoryQueryVariables
  >,
  'query'
>;

export const PostsForContentCategoryComponent = (props: PostsForContentCategoryComponentProps) => (
  <ApolloReactComponents.Query<PostsForContentCategoryQuery, PostsForContentCategoryQueryVariables>
    query={PostsForContentCategoryDocument}
    {...props}
  />
);

/**
 * __usePostsForContentCategoryQuery__
 *
 * To run a query within a React component, call `usePostsForContentCategoryQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostsForContentCategoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostsForContentCategoryQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function usePostsForContentCategoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    PostsForContentCategoryQuery,
    PostsForContentCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    PostsForContentCategoryQuery,
    PostsForContentCategoryQueryVariables
  >(PostsForContentCategoryDocument, baseOptions);
}
export function usePostsForContentCategoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    PostsForContentCategoryQuery,
    PostsForContentCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    PostsForContentCategoryQuery,
    PostsForContentCategoryQueryVariables
  >(PostsForContentCategoryDocument, baseOptions);
}
export type PostsForContentCategoryQueryHookResult = ReturnType<
  typeof usePostsForContentCategoryQuery
>;
export type PostsForContentCategoryLazyQueryHookResult = ReturnType<
  typeof usePostsForContentCategoryLazyQuery
>;
export type PostsForContentCategoryQueryResult = ApolloReactCommon.QueryResult<
  PostsForContentCategoryQuery,
  PostsForContentCategoryQueryVariables
>;
export const ContentByIdDocument = gql`
  query contentById($postId: ID) {
    contentById(id: $postId) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
      featuredVideo
    }
  }
`;
export type ContentByIdComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<ContentByIdQuery, ContentByIdQueryVariables>,
  'query'
>;

export const ContentByIdComponent = (props: ContentByIdComponentProps) => (
  <ApolloReactComponents.Query<ContentByIdQuery, ContentByIdQueryVariables>
    query={ContentByIdDocument}
    {...props}
  />
);

/**
 * __useContentByIdQuery__
 *
 * To run a query within a React component, call `useContentByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useContentByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useContentByIdQuery({
 *   variables: {
 *      postId: // value for 'postId'
 *   },
 * });
 */
export function useContentByIdQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<ContentByIdQuery, ContentByIdQueryVariables>
) {
  return ApolloReactHooks.useQuery<ContentByIdQuery, ContentByIdQueryVariables>(
    ContentByIdDocument,
    baseOptions
  );
}
export function useContentByIdLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ContentByIdQuery, ContentByIdQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<ContentByIdQuery, ContentByIdQueryVariables>(
    ContentByIdDocument,
    baseOptions
  );
}
export type ContentByIdQueryHookResult = ReturnType<typeof useContentByIdQuery>;
export type ContentByIdLazyQueryHookResult = ReturnType<typeof useContentByIdLazyQuery>;
export type ContentByIdQueryResult = ApolloReactCommon.QueryResult<
  ContentByIdQuery,
  ContentByIdQueryVariables
>;
export const EventCancelRegistrationDocument = gql`
  mutation eventCancelRegistration($postId: String) {
    eventCancelRegistration(postId: $postId)
  }
`;
export type EventCancelRegistrationMutationFn = ApolloReactCommon.MutationFunction<
  EventCancelRegistrationMutation,
  EventCancelRegistrationMutationVariables
>;
export type EventCancelRegistrationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EventCancelRegistrationMutation,
    EventCancelRegistrationMutationVariables
  >,
  'mutation'
>;

export const EventCancelRegistrationComponent = (props: EventCancelRegistrationComponentProps) => (
  <ApolloReactComponents.Mutation<
    EventCancelRegistrationMutation,
    EventCancelRegistrationMutationVariables
  >
    mutation={EventCancelRegistrationDocument}
    {...props}
  />
);

/**
 * __useEventCancelRegistrationMutation__
 *
 * To run a mutation, you first call `useEventCancelRegistrationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEventCancelRegistrationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [eventCancelRegistrationMutation, { data, loading, error }] = useEventCancelRegistrationMutation({
 *   variables: {
 *      postId: // value for 'postId'
 *   },
 * });
 */
export function useEventCancelRegistrationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EventCancelRegistrationMutation,
    EventCancelRegistrationMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EventCancelRegistrationMutation,
    EventCancelRegistrationMutationVariables
  >(EventCancelRegistrationDocument, baseOptions);
}
export type EventCancelRegistrationMutationHookResult = ReturnType<
  typeof useEventCancelRegistrationMutation
>;
export type EventCancelRegistrationMutationResult = ApolloReactCommon.MutationResult<
  EventCancelRegistrationMutation
>;
export type EventCancelRegistrationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EventCancelRegistrationMutation,
  EventCancelRegistrationMutationVariables
>;
export const EventCategoriesDocument = gql`
  query eventCategories {
    eventCategories {
      id
      name
      description
      count
      mediumImage
    }
  }
`;
export type EventCategoriesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<EventCategoriesQuery, EventCategoriesQueryVariables>,
  'query'
>;

export const EventCategoriesComponent = (props: EventCategoriesComponentProps) => (
  <ApolloReactComponents.Query<EventCategoriesQuery, EventCategoriesQueryVariables>
    query={EventCategoriesDocument}
    {...props}
  />
);

/**
 * __useEventCategoriesQuery__
 *
 * To run a query within a React component, call `useEventCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useEventCategoriesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    EventCategoriesQuery,
    EventCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<EventCategoriesQuery, EventCategoriesQueryVariables>(
    EventCategoriesDocument,
    baseOptions
  );
}
export function useEventCategoriesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    EventCategoriesQuery,
    EventCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<EventCategoriesQuery, EventCategoriesQueryVariables>(
    EventCategoriesDocument,
    baseOptions
  );
}
export type EventCategoriesQueryHookResult = ReturnType<typeof useEventCategoriesQuery>;
export type EventCategoriesLazyQueryHookResult = ReturnType<typeof useEventCategoriesLazyQuery>;
export type EventCategoriesQueryResult = ApolloReactCommon.QueryResult<
  EventCategoriesQuery,
  EventCategoriesQueryVariables
>;
export const EventGroupsForEventCategoryDocument = gql`
  query eventGroupsForEventCategory($eventCategoryId: Int) {
    eventGroups(categoryId: $eventCategoryId) {
      name
      description
      mediumImage
    }
  }
`;
export type EventGroupsForEventCategoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >,
  'query'
>;

export const EventGroupsForEventCategoryComponent = (
  props: EventGroupsForEventCategoryComponentProps
) => (
  <ApolloReactComponents.Query<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >
    query={EventGroupsForEventCategoryDocument}
    {...props}
  />
);

/**
 * __useEventGroupsForEventCategoryQuery__
 *
 * To run a query within a React component, call `useEventGroupsForEventCategoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventGroupsForEventCategoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventGroupsForEventCategoryQuery({
 *   variables: {
 *      eventCategoryId: // value for 'eventCategoryId'
 *   },
 * });
 */
export function useEventGroupsForEventCategoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >(EventGroupsForEventCategoryDocument, baseOptions);
}
export function useEventGroupsForEventCategoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    EventGroupsForEventCategoryQuery,
    EventGroupsForEventCategoryQueryVariables
  >(EventGroupsForEventCategoryDocument, baseOptions);
}
export type EventGroupsForEventCategoryQueryHookResult = ReturnType<
  typeof useEventGroupsForEventCategoryQuery
>;
export type EventGroupsForEventCategoryLazyQueryHookResult = ReturnType<
  typeof useEventGroupsForEventCategoryLazyQuery
>;
export type EventGroupsForEventCategoryQueryResult = ApolloReactCommon.QueryResult<
  EventGroupsForEventCategoryQuery,
  EventGroupsForEventCategoryQueryVariables
>;
export const EventDocument = gql`
  query event($eventId: Int) {
    event(eventId: $eventId) {
      description
      duration
      endTime
      eventEnd
      eventEndDate
      eventEndTime
      eventStart
      eventStartDate
      eventStartTime
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
      rawHtml
    }
  }
`;
export type EventComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<EventQuery, EventQueryVariables>,
  'query'
>;

export const EventComponent = (props: EventComponentProps) => (
  <ApolloReactComponents.Query<EventQuery, EventQueryVariables> query={EventDocument} {...props} />
);

/**
 * __useEventQuery__
 *
 * To run a query within a React component, call `useEventQuery` and pass it any options that fit your needs.
 * When your component renders, `useEventQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEventQuery({
 *   variables: {
 *      eventId: // value for 'eventId'
 *   },
 * });
 */
export function useEventQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<EventQuery, EventQueryVariables>
) {
  return ApolloReactHooks.useQuery<EventQuery, EventQueryVariables>(EventDocument, baseOptions);
}
export function useEventLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<EventQuery, EventQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<EventQuery, EventQueryVariables>(EventDocument, baseOptions);
}
export type EventQueryHookResult = ReturnType<typeof useEventQuery>;
export type EventLazyQueryHookResult = ReturnType<typeof useEventLazyQuery>;
export type EventQueryResult = ApolloReactCommon.QueryResult<EventQuery, EventQueryVariables>;
export const HoagEventGroupsDocument = gql`
  query hoagEventGroups($eventGroupName: String) {
    eventGroup(eventGroupId: $eventGroupName) {
      name
      description
      mediumImage
      rawHtml
      events {
        date
        endTime
        eventStartDate
        id
        isVirtual
        name
        postId
        spacesRemaining
        startTime
        totalSpaces
        youtubeLiveLink
        isOnDemand
        isFeatured
      }
    }
  }
`;
export type HoagEventGroupsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<HoagEventGroupsQuery, HoagEventGroupsQueryVariables>,
  'query'
>;

export const HoagEventGroupsComponent = (props: HoagEventGroupsComponentProps) => (
  <ApolloReactComponents.Query<HoagEventGroupsQuery, HoagEventGroupsQueryVariables>
    query={HoagEventGroupsDocument}
    {...props}
  />
);

/**
 * __useHoagEventGroupsQuery__
 *
 * To run a query within a React component, call `useHoagEventGroupsQuery` and pass it any options that fit your needs.
 * When your component renders, `useHoagEventGroupsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHoagEventGroupsQuery({
 *   variables: {
 *      eventGroupName: // value for 'eventGroupName'
 *   },
 * });
 */
export function useHoagEventGroupsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    HoagEventGroupsQuery,
    HoagEventGroupsQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<HoagEventGroupsQuery, HoagEventGroupsQueryVariables>(
    HoagEventGroupsDocument,
    baseOptions
  );
}
export function useHoagEventGroupsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    HoagEventGroupsQuery,
    HoagEventGroupsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<HoagEventGroupsQuery, HoagEventGroupsQueryVariables>(
    HoagEventGroupsDocument,
    baseOptions
  );
}
export type HoagEventGroupsQueryHookResult = ReturnType<typeof useHoagEventGroupsQuery>;
export type HoagEventGroupsLazyQueryHookResult = ReturnType<typeof useHoagEventGroupsLazyQuery>;
export type HoagEventGroupsQueryResult = ApolloReactCommon.QueryResult<
  HoagEventGroupsQuery,
  HoagEventGroupsQueryVariables
>;
export const MyScheduledEventsDocument = gql`
  query myScheduledEvents {
    myEvents {
      postId
      date
      eventId
      name
      eventStartDate
      eventStart
      startTime
      endTime
    }
  }
`;
export type MyScheduledEventsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MyScheduledEventsQuery,
    MyScheduledEventsQueryVariables
  >,
  'query'
>;

export const MyScheduledEventsComponent = (props: MyScheduledEventsComponentProps) => (
  <ApolloReactComponents.Query<MyScheduledEventsQuery, MyScheduledEventsQueryVariables>
    query={MyScheduledEventsDocument}
    {...props}
  />
);

/**
 * __useMyScheduledEventsQuery__
 *
 * To run a query within a React component, call `useMyScheduledEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyScheduledEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyScheduledEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyScheduledEventsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MyScheduledEventsQuery,
    MyScheduledEventsQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MyScheduledEventsQuery, MyScheduledEventsQueryVariables>(
    MyScheduledEventsDocument,
    baseOptions
  );
}
export function useMyScheduledEventsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MyScheduledEventsQuery,
    MyScheduledEventsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MyScheduledEventsQuery, MyScheduledEventsQueryVariables>(
    MyScheduledEventsDocument,
    baseOptions
  );
}
export type MyScheduledEventsQueryHookResult = ReturnType<typeof useMyScheduledEventsQuery>;
export type MyScheduledEventsLazyQueryHookResult = ReturnType<typeof useMyScheduledEventsLazyQuery>;
export type MyScheduledEventsQueryResult = ApolloReactCommon.QueryResult<
  MyScheduledEventsQuery,
  MyScheduledEventsQueryVariables
>;
export const OnDemandEventsDocument = gql`
  query onDemandEvents {
    onDemandEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
    }
  }
`;
export type OnDemandEventsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<OnDemandEventsQuery, OnDemandEventsQueryVariables>,
  'query'
>;

export const OnDemandEventsComponent = (props: OnDemandEventsComponentProps) => (
  <ApolloReactComponents.Query<OnDemandEventsQuery, OnDemandEventsQueryVariables>
    query={OnDemandEventsDocument}
    {...props}
  />
);

/**
 * __useOnDemandEventsQuery__
 *
 * To run a query within a React component, call `useOnDemandEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useOnDemandEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOnDemandEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useOnDemandEventsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<OnDemandEventsQuery, OnDemandEventsQueryVariables>
) {
  return ApolloReactHooks.useQuery<OnDemandEventsQuery, OnDemandEventsQueryVariables>(
    OnDemandEventsDocument,
    baseOptions
  );
}
export function useOnDemandEventsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    OnDemandEventsQuery,
    OnDemandEventsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<OnDemandEventsQuery, OnDemandEventsQueryVariables>(
    OnDemandEventsDocument,
    baseOptions
  );
}
export type OnDemandEventsQueryHookResult = ReturnType<typeof useOnDemandEventsQuery>;
export type OnDemandEventsLazyQueryHookResult = ReturnType<typeof useOnDemandEventsLazyQuery>;
export type OnDemandEventsQueryResult = ApolloReactCommon.QueryResult<
  OnDemandEventsQuery,
  OnDemandEventsQueryVariables
>;
export const EventRegistrationDocument = gql`
  mutation EventRegistration($postId: String) {
    eventRegistration(postId: $postId)
  }
`;
export type EventRegistrationMutationFn = ApolloReactCommon.MutationFunction<
  EventRegistrationMutation,
  EventRegistrationMutationVariables
>;
export type EventRegistrationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EventRegistrationMutation,
    EventRegistrationMutationVariables
  >,
  'mutation'
>;

export const EventRegistrationComponent = (props: EventRegistrationComponentProps) => (
  <ApolloReactComponents.Mutation<EventRegistrationMutation, EventRegistrationMutationVariables>
    mutation={EventRegistrationDocument}
    {...props}
  />
);

/**
 * __useEventRegistrationMutation__
 *
 * To run a mutation, you first call `useEventRegistrationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEventRegistrationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [eventRegistrationMutation, { data, loading, error }] = useEventRegistrationMutation({
 *   variables: {
 *      postId: // value for 'postId'
 *   },
 * });
 */
export function useEventRegistrationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EventRegistrationMutation,
    EventRegistrationMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EventRegistrationMutation,
    EventRegistrationMutationVariables
  >(EventRegistrationDocument, baseOptions);
}
export type EventRegistrationMutationHookResult = ReturnType<typeof useEventRegistrationMutation>;
export type EventRegistrationMutationResult = ApolloReactCommon.MutationResult<
  EventRegistrationMutation
>;
export type EventRegistrationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EventRegistrationMutation,
  EventRegistrationMutationVariables
>;
export const UniqueEventsDocument = gql`
  query uniqueEvents {
    uniqueEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;
export type UniqueEventsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<UniqueEventsQuery, UniqueEventsQueryVariables>,
  'query'
>;

export const UniqueEventsComponent = (props: UniqueEventsComponentProps) => (
  <ApolloReactComponents.Query<UniqueEventsQuery, UniqueEventsQueryVariables>
    query={UniqueEventsDocument}
    {...props}
  />
);

/**
 * __useUniqueEventsQuery__
 *
 * To run a query within a React component, call `useUniqueEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUniqueEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUniqueEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useUniqueEventsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<UniqueEventsQuery, UniqueEventsQueryVariables>
) {
  return ApolloReactHooks.useQuery<UniqueEventsQuery, UniqueEventsQueryVariables>(
    UniqueEventsDocument,
    baseOptions
  );
}
export function useUniqueEventsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<UniqueEventsQuery, UniqueEventsQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<UniqueEventsQuery, UniqueEventsQueryVariables>(
    UniqueEventsDocument,
    baseOptions
  );
}
export type UniqueEventsQueryHookResult = ReturnType<typeof useUniqueEventsQuery>;
export type UniqueEventsLazyQueryHookResult = ReturnType<typeof useUniqueEventsLazyQuery>;
export type UniqueEventsQueryResult = ApolloReactCommon.QueryResult<
  UniqueEventsQuery,
  UniqueEventsQueryVariables
>;
export const CreateMaternityProfileDocument = gql`
  mutation createMaternityProfile(
    $lastMenstrualPeriod: String
    $estimatedDueDate: String
    $role: String
  ) {
    createMaternityProfile(
      lastMenstrualPeriod: $lastMenstrualPeriod
      estimatedDueDate: $estimatedDueDate
      role: $role
    ) {
      id
      lastMenstrualPeriod
      estimatedDueDate
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;
export type CreateMaternityProfileMutationFn = ApolloReactCommon.MutationFunction<
  CreateMaternityProfileMutation,
  CreateMaternityProfileMutationVariables
>;
export type CreateMaternityProfileComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    CreateMaternityProfileMutation,
    CreateMaternityProfileMutationVariables
  >,
  'mutation'
>;

export const CreateMaternityProfileComponent = (props: CreateMaternityProfileComponentProps) => (
  <ApolloReactComponents.Mutation<
    CreateMaternityProfileMutation,
    CreateMaternityProfileMutationVariables
  >
    mutation={CreateMaternityProfileDocument}
    {...props}
  />
);

/**
 * __useCreateMaternityProfileMutation__
 *
 * To run a mutation, you first call `useCreateMaternityProfileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMaternityProfileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMaternityProfileMutation, { data, loading, error }] = useCreateMaternityProfileMutation({
 *   variables: {
 *      lastMenstrualPeriod: // value for 'lastMenstrualPeriod'
 *      estimatedDueDate: // value for 'estimatedDueDate'
 *      role: // value for 'role'
 *   },
 * });
 */
export function useCreateMaternityProfileMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateMaternityProfileMutation,
    CreateMaternityProfileMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    CreateMaternityProfileMutation,
    CreateMaternityProfileMutationVariables
  >(CreateMaternityProfileDocument, baseOptions);
}
export type CreateMaternityProfileMutationHookResult = ReturnType<
  typeof useCreateMaternityProfileMutation
>;
export type CreateMaternityProfileMutationResult = ApolloReactCommon.MutationResult<
  CreateMaternityProfileMutation
>;
export type CreateMaternityProfileMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateMaternityProfileMutation,
  CreateMaternityProfileMutationVariables
>;
export const UpdateMaternityProfileDocument = gql`
  mutation updateMaternityProfile(
    $lastMenstrualPeriod: String
    $estimatedDueDate: String
    $role: String
  ) {
    updateMaternityProfile(
      lastMenstrualPeriod: $lastMenstrualPeriod
      estimatedDueDate: $estimatedDueDate
      role: $role
    ) {
      id
      lastMenstrualPeriod
      estimatedDueDate
      role
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;
export type UpdateMaternityProfileMutationFn = ApolloReactCommon.MutationFunction<
  UpdateMaternityProfileMutation,
  UpdateMaternityProfileMutationVariables
>;
export type UpdateMaternityProfileComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    UpdateMaternityProfileMutation,
    UpdateMaternityProfileMutationVariables
  >,
  'mutation'
>;

export const UpdateMaternityProfileComponent = (props: UpdateMaternityProfileComponentProps) => (
  <ApolloReactComponents.Mutation<
    UpdateMaternityProfileMutation,
    UpdateMaternityProfileMutationVariables
  >
    mutation={UpdateMaternityProfileDocument}
    {...props}
  />
);

/**
 * __useUpdateMaternityProfileMutation__
 *
 * To run a mutation, you first call `useUpdateMaternityProfileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateMaternityProfileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateMaternityProfileMutation, { data, loading, error }] = useUpdateMaternityProfileMutation({
 *   variables: {
 *      lastMenstrualPeriod: // value for 'lastMenstrualPeriod'
 *      estimatedDueDate: // value for 'estimatedDueDate'
 *      role: // value for 'role'
 *   },
 * });
 */
export function useUpdateMaternityProfileMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateMaternityProfileMutation,
    UpdateMaternityProfileMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    UpdateMaternityProfileMutation,
    UpdateMaternityProfileMutationVariables
  >(UpdateMaternityProfileDocument, baseOptions);
}
export type UpdateMaternityProfileMutationHookResult = ReturnType<
  typeof useUpdateMaternityProfileMutation
>;
export type UpdateMaternityProfileMutationResult = ApolloReactCommon.MutationResult<
  UpdateMaternityProfileMutation
>;
export type UpdateMaternityProfileMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateMaternityProfileMutation,
  UpdateMaternityProfileMutationVariables
>;
export const ReportPregnancyLossDocument = gql`
  mutation reportPregnancyLoss {
    reportPregnancyLoss {
      id
      userId
      role
      lastMenstrualPeriod
      estimatedDueDate
      hasPregnancyLoss
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;
export type ReportPregnancyLossMutationFn = ApolloReactCommon.MutationFunction<
  ReportPregnancyLossMutation,
  ReportPregnancyLossMutationVariables
>;
export type ReportPregnancyLossComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    ReportPregnancyLossMutation,
    ReportPregnancyLossMutationVariables
  >,
  'mutation'
>;

export const ReportPregnancyLossComponent = (props: ReportPregnancyLossComponentProps) => (
  <ApolloReactComponents.Mutation<ReportPregnancyLossMutation, ReportPregnancyLossMutationVariables>
    mutation={ReportPregnancyLossDocument}
    {...props}
  />
);

/**
 * __useReportPregnancyLossMutation__
 *
 * To run a mutation, you first call `useReportPregnancyLossMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useReportPregnancyLossMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [reportPregnancyLossMutation, { data, loading, error }] = useReportPregnancyLossMutation({
 *   variables: {
 *   },
 * });
 */
export function useReportPregnancyLossMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    ReportPregnancyLossMutation,
    ReportPregnancyLossMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    ReportPregnancyLossMutation,
    ReportPregnancyLossMutationVariables
  >(ReportPregnancyLossDocument, baseOptions);
}
export type ReportPregnancyLossMutationHookResult = ReturnType<
  typeof useReportPregnancyLossMutation
>;
export type ReportPregnancyLossMutationResult = ApolloReactCommon.MutationResult<
  ReportPregnancyLossMutation
>;
export type ReportPregnancyLossMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ReportPregnancyLossMutation,
  ReportPregnancyLossMutationVariables
>;
export const ResetPregnancyLossDocument = gql`
  mutation resetPregnancyLoss {
    resetPregnancyLoss {
      id
      userId
      role
      lastMenstrualPeriod
      estimatedDueDate
      hasPregnancyLoss
      gestationalAge {
        weeks
        days
        trimester
      }
    }
  }
`;
export type ResetPregnancyLossMutationFn = ApolloReactCommon.MutationFunction<
  ResetPregnancyLossMutation,
  ResetPregnancyLossMutationVariables
>;
export type ResetPregnancyLossComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    ResetPregnancyLossMutation,
    ResetPregnancyLossMutationVariables
  >,
  'mutation'
>;

export const ResetPregnancyLossComponent = (props: ResetPregnancyLossComponentProps) => (
  <ApolloReactComponents.Mutation<ResetPregnancyLossMutation, ResetPregnancyLossMutationVariables>
    mutation={ResetPregnancyLossDocument}
    {...props}
  />
);

/**
 * __useResetPregnancyLossMutation__
 *
 * To run a mutation, you first call `useResetPregnancyLossMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useResetPregnancyLossMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [resetPregnancyLossMutation, { data, loading, error }] = useResetPregnancyLossMutation({
 *   variables: {
 *   },
 * });
 */
export function useResetPregnancyLossMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    ResetPregnancyLossMutation,
    ResetPregnancyLossMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    ResetPregnancyLossMutation,
    ResetPregnancyLossMutationVariables
  >(ResetPregnancyLossDocument, baseOptions);
}
export type ResetPregnancyLossMutationHookResult = ReturnType<typeof useResetPregnancyLossMutation>;
export type ResetPregnancyLossMutationResult = ApolloReactCommon.MutationResult<
  ResetPregnancyLossMutation
>;
export type ResetPregnancyLossMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ResetPregnancyLossMutation,
  ResetPregnancyLossMutationVariables
>;
export const NicuFaqDocument = gql`
  query nicuFAQ {
    irvine: content(contentType: "faq", contentProgram: "nicu-irvine", appModule: "Maternity") {
      id
      title
      link
      description
      date
      mediumImage
      isFeatured
    }
    newportBeach: content(
      contentType: "faq"
      contentProgram: "nicu-newport-beach"
      appModule: "Maternity"
    ) {
      id
      title
      link
      date
      mediumImage
      isFeatured
    }
  }
`;
export type NicuFaqComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<NicuFaqQuery, NicuFaqQueryVariables>,
  'query'
>;

export const NicuFaqComponent = (props: NicuFaqComponentProps) => (
  <ApolloReactComponents.Query<NicuFaqQuery, NicuFaqQueryVariables>
    query={NicuFaqDocument}
    {...props}
  />
);

/**
 * __useNicuFaqQuery__
 *
 * To run a query within a React component, call `useNicuFaqQuery` and pass it any options that fit your needs.
 * When your component renders, `useNicuFaqQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useNicuFaqQuery({
 *   variables: {
 *   },
 * });
 */
export function useNicuFaqQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<NicuFaqQuery, NicuFaqQueryVariables>
) {
  return ApolloReactHooks.useQuery<NicuFaqQuery, NicuFaqQueryVariables>(
    NicuFaqDocument,
    baseOptions
  );
}
export function useNicuFaqLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<NicuFaqQuery, NicuFaqQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<NicuFaqQuery, NicuFaqQueryVariables>(
    NicuFaqDocument,
    baseOptions
  );
}
export type NicuFaqQueryHookResult = ReturnType<typeof useNicuFaqQuery>;
export type NicuFaqLazyQueryHookResult = ReturnType<typeof useNicuFaqLazyQuery>;
export type NicuFaqQueryResult = ApolloReactCommon.QueryResult<NicuFaqQuery, NicuFaqQueryVariables>;
export const MaternityPostsDocument = gql`
  query maternityPosts($trimester: Int) {
    maternityPosts(trimester: $trimester) {
      id
      title
      date
      description
      thumbnailImage
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;
export type MaternityPostsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MaternityPostsQuery, MaternityPostsQueryVariables>,
  'query'
>;

export const MaternityPostsComponent = (props: MaternityPostsComponentProps) => (
  <ApolloReactComponents.Query<MaternityPostsQuery, MaternityPostsQueryVariables>
    query={MaternityPostsDocument}
    {...props}
  />
);

/**
 * __useMaternityPostsQuery__
 *
 * To run a query within a React component, call `useMaternityPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaternityPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaternityPostsQuery({
 *   variables: {
 *      trimester: // value for 'trimester'
 *   },
 * });
 */
export function useMaternityPostsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<MaternityPostsQuery, MaternityPostsQueryVariables>
) {
  return ApolloReactHooks.useQuery<MaternityPostsQuery, MaternityPostsQueryVariables>(
    MaternityPostsDocument,
    baseOptions
  );
}
export function useMaternityPostsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MaternityPostsQuery,
    MaternityPostsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MaternityPostsQuery, MaternityPostsQueryVariables>(
    MaternityPostsDocument,
    baseOptions
  );
}
export type MaternityPostsQueryHookResult = ReturnType<typeof useMaternityPostsQuery>;
export type MaternityPostsLazyQueryHookResult = ReturnType<typeof useMaternityPostsLazyQuery>;
export type MaternityPostsQueryResult = ApolloReactCommon.QueryResult<
  MaternityPostsQuery,
  MaternityPostsQueryVariables
>;
export const FeaturedMaternityPostsDocument = gql`
  query featuredMaternityPosts {
    featuredMaternityPosts {
      id
      title
      date
      description
      thumbnailImage
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;
export type FeaturedMaternityPostsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    FeaturedMaternityPostsQuery,
    FeaturedMaternityPostsQueryVariables
  >,
  'query'
>;

export const FeaturedMaternityPostsComponent = (props: FeaturedMaternityPostsComponentProps) => (
  <ApolloReactComponents.Query<FeaturedMaternityPostsQuery, FeaturedMaternityPostsQueryVariables>
    query={FeaturedMaternityPostsDocument}
    {...props}
  />
);

/**
 * __useFeaturedMaternityPostsQuery__
 *
 * To run a query within a React component, call `useFeaturedMaternityPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFeaturedMaternityPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFeaturedMaternityPostsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFeaturedMaternityPostsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    FeaturedMaternityPostsQuery,
    FeaturedMaternityPostsQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    FeaturedMaternityPostsQuery,
    FeaturedMaternityPostsQueryVariables
  >(FeaturedMaternityPostsDocument, baseOptions);
}
export function useFeaturedMaternityPostsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    FeaturedMaternityPostsQuery,
    FeaturedMaternityPostsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    FeaturedMaternityPostsQuery,
    FeaturedMaternityPostsQueryVariables
  >(FeaturedMaternityPostsDocument, baseOptions);
}
export type FeaturedMaternityPostsQueryHookResult = ReturnType<
  typeof useFeaturedMaternityPostsQuery
>;
export type FeaturedMaternityPostsLazyQueryHookResult = ReturnType<
  typeof useFeaturedMaternityPostsLazyQuery
>;
export type FeaturedMaternityPostsQueryResult = ApolloReactCommon.QueryResult<
  FeaturedMaternityPostsQuery,
  FeaturedMaternityPostsQueryVariables
>;
export const MaternityPromoContentDocument = gql`
  query maternityPromoContent {
    content(contentType: "Promo", appModule: "Maternity") {
      id
      title
      link
      description
      date
      mediumImage
      thumbnailImage
      isFeatured
    }
  }
`;
export type MaternityPromoContentComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MaternityPromoContentQuery,
    MaternityPromoContentQueryVariables
  >,
  'query'
>;

export const MaternityPromoContentComponent = (props: MaternityPromoContentComponentProps) => (
  <ApolloReactComponents.Query<MaternityPromoContentQuery, MaternityPromoContentQueryVariables>
    query={MaternityPromoContentDocument}
    {...props}
  />
);

/**
 * __useMaternityPromoContentQuery__
 *
 * To run a query within a React component, call `useMaternityPromoContentQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaternityPromoContentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaternityPromoContentQuery({
 *   variables: {
 *   },
 * });
 */
export function useMaternityPromoContentQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MaternityPromoContentQuery,
    MaternityPromoContentQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MaternityPromoContentQuery, MaternityPromoContentQueryVariables>(
    MaternityPromoContentDocument,
    baseOptions
  );
}
export function useMaternityPromoContentLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MaternityPromoContentQuery,
    MaternityPromoContentQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    MaternityPromoContentQuery,
    MaternityPromoContentQueryVariables
  >(MaternityPromoContentDocument, baseOptions);
}
export type MaternityPromoContentQueryHookResult = ReturnType<typeof useMaternityPromoContentQuery>;
export type MaternityPromoContentLazyQueryHookResult = ReturnType<
  typeof useMaternityPromoContentLazyQuery
>;
export type MaternityPromoContentQueryResult = ApolloReactCommon.QueryResult<
  MaternityPromoContentQuery,
  MaternityPromoContentQueryVariables
>;
export const BirthplanCategoriesDocument = gql`
  query birthplanCategories {
    birthplanCategories {
      id
      name
      numberSelected
    }
  }
`;
export type BirthplanCategoriesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    BirthplanCategoriesQuery,
    BirthplanCategoriesQueryVariables
  >,
  'query'
>;

export const BirthplanCategoriesComponent = (props: BirthplanCategoriesComponentProps) => (
  <ApolloReactComponents.Query<BirthplanCategoriesQuery, BirthplanCategoriesQueryVariables>
    query={BirthplanCategoriesDocument}
    {...props}
  />
);

/**
 * __useBirthplanCategoriesQuery__
 *
 * To run a query within a React component, call `useBirthplanCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useBirthplanCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useBirthplanCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useBirthplanCategoriesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    BirthplanCategoriesQuery,
    BirthplanCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<BirthplanCategoriesQuery, BirthplanCategoriesQueryVariables>(
    BirthplanCategoriesDocument,
    baseOptions
  );
}
export function useBirthplanCategoriesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    BirthplanCategoriesQuery,
    BirthplanCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<BirthplanCategoriesQuery, BirthplanCategoriesQueryVariables>(
    BirthplanCategoriesDocument,
    baseOptions
  );
}
export type BirthplanCategoriesQueryHookResult = ReturnType<typeof useBirthplanCategoriesQuery>;
export type BirthplanCategoriesLazyQueryHookResult = ReturnType<
  typeof useBirthplanCategoriesLazyQuery
>;
export type BirthplanCategoriesQueryResult = ApolloReactCommon.QueryResult<
  BirthplanCategoriesQuery,
  BirthplanCategoriesQueryVariables
>;
export const MyBirthplanDocument = gql`
  query myBirthplan {
    myBirthplan {
      id
      title
      category
    }
  }
`;
export type MyBirthplanComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MyBirthplanQuery, MyBirthplanQueryVariables>,
  'query'
>;

export const MyBirthplanComponent = (props: MyBirthplanComponentProps) => (
  <ApolloReactComponents.Query<MyBirthplanQuery, MyBirthplanQueryVariables>
    query={MyBirthplanDocument}
    {...props}
  />
);

/**
 * __useMyBirthplanQuery__
 *
 * To run a query within a React component, call `useMyBirthplanQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyBirthplanQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyBirthplanQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyBirthplanQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<MyBirthplanQuery, MyBirthplanQueryVariables>
) {
  return ApolloReactHooks.useQuery<MyBirthplanQuery, MyBirthplanQueryVariables>(
    MyBirthplanDocument,
    baseOptions
  );
}
export function useMyBirthplanLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MyBirthplanQuery, MyBirthplanQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<MyBirthplanQuery, MyBirthplanQueryVariables>(
    MyBirthplanDocument,
    baseOptions
  );
}
export type MyBirthplanQueryHookResult = ReturnType<typeof useMyBirthplanQuery>;
export type MyBirthplanLazyQueryHookResult = ReturnType<typeof useMyBirthplanLazyQuery>;
export type MyBirthplanQueryResult = ApolloReactCommon.QueryResult<
  MyBirthplanQuery,
  MyBirthplanQueryVariables
>;
export const BirthplanContentCategoryDocument = gql`
  query birthplanContentCategory($categoryId: ID) {
    birthplanContentByCategory(contentCategoryId: $categoryId) {
      id
      contentCategoryId
      selected
      title
    }
  }
`;
export type BirthplanContentCategoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >,
  'query'
>;

export const BirthplanContentCategoryComponent = (
  props: BirthplanContentCategoryComponentProps
) => (
  <ApolloReactComponents.Query<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >
    query={BirthplanContentCategoryDocument}
    {...props}
  />
);

/**
 * __useBirthplanContentCategoryQuery__
 *
 * To run a query within a React component, call `useBirthplanContentCategoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useBirthplanContentCategoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useBirthplanContentCategoryQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useBirthplanContentCategoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >(BirthplanContentCategoryDocument, baseOptions);
}
export function useBirthplanContentCategoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    BirthplanContentCategoryQuery,
    BirthplanContentCategoryQueryVariables
  >(BirthplanContentCategoryDocument, baseOptions);
}
export type BirthplanContentCategoryQueryHookResult = ReturnType<
  typeof useBirthplanContentCategoryQuery
>;
export type BirthplanContentCategoryLazyQueryHookResult = ReturnType<
  typeof useBirthplanContentCategoryLazyQuery
>;
export type BirthplanContentCategoryQueryResult = ApolloReactCommon.QueryResult<
  BirthplanContentCategoryQuery,
  BirthplanContentCategoryQueryVariables
>;
export const RecordBirthplanPreferenceDocument = gql`
  mutation recordBirthplanPreference($category: CategoryInput, $preference: BirthplanInput) {
    recordBirthplanPreference(category: $category, preference: $preference) {
      id
      userId
      title
    }
  }
`;
export type RecordBirthplanPreferenceMutationFn = ApolloReactCommon.MutationFunction<
  RecordBirthplanPreferenceMutation,
  RecordBirthplanPreferenceMutationVariables
>;
export type RecordBirthplanPreferenceComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    RecordBirthplanPreferenceMutation,
    RecordBirthplanPreferenceMutationVariables
  >,
  'mutation'
>;

export const RecordBirthplanPreferenceComponent = (
  props: RecordBirthplanPreferenceComponentProps
) => (
  <ApolloReactComponents.Mutation<
    RecordBirthplanPreferenceMutation,
    RecordBirthplanPreferenceMutationVariables
  >
    mutation={RecordBirthplanPreferenceDocument}
    {...props}
  />
);

/**
 * __useRecordBirthplanPreferenceMutation__
 *
 * To run a mutation, you first call `useRecordBirthplanPreferenceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRecordBirthplanPreferenceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [recordBirthplanPreferenceMutation, { data, loading, error }] = useRecordBirthplanPreferenceMutation({
 *   variables: {
 *      category: // value for 'category'
 *      preference: // value for 'preference'
 *   },
 * });
 */
export function useRecordBirthplanPreferenceMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    RecordBirthplanPreferenceMutation,
    RecordBirthplanPreferenceMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    RecordBirthplanPreferenceMutation,
    RecordBirthplanPreferenceMutationVariables
  >(RecordBirthplanPreferenceDocument, baseOptions);
}
export type RecordBirthplanPreferenceMutationHookResult = ReturnType<
  typeof useRecordBirthplanPreferenceMutation
>;
export type RecordBirthplanPreferenceMutationResult = ApolloReactCommon.MutationResult<
  RecordBirthplanPreferenceMutation
>;
export type RecordBirthplanPreferenceMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RecordBirthplanPreferenceMutation,
  RecordBirthplanPreferenceMutationVariables
>;
export const MaternityEventsDocument = gql`
  query maternityEvents {
    maternityEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;
export type MaternityEventsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MaternityEventsQuery, MaternityEventsQueryVariables>,
  'query'
>;

export const MaternityEventsComponent = (props: MaternityEventsComponentProps) => (
  <ApolloReactComponents.Query<MaternityEventsQuery, MaternityEventsQueryVariables>
    query={MaternityEventsDocument}
    {...props}
  />
);

/**
 * __useMaternityEventsQuery__
 *
 * To run a query within a React component, call `useMaternityEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaternityEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaternityEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useMaternityEventsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MaternityEventsQuery,
    MaternityEventsQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MaternityEventsQuery, MaternityEventsQueryVariables>(
    MaternityEventsDocument,
    baseOptions
  );
}
export function useMaternityEventsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MaternityEventsQuery,
    MaternityEventsQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MaternityEventsQuery, MaternityEventsQueryVariables>(
    MaternityEventsDocument,
    baseOptions
  );
}
export type MaternityEventsQueryHookResult = ReturnType<typeof useMaternityEventsQuery>;
export type MaternityEventsLazyQueryHookResult = ReturnType<typeof useMaternityEventsLazyQuery>;
export type MaternityEventsQueryResult = ApolloReactCommon.QueryResult<
  MaternityEventsQuery,
  MaternityEventsQueryVariables
>;
export const PotentialLabCategoriesDocument = gql`
  query potentialLabCategories {
    potentialLabCategories {
      id
      name
      numberSelected
    }
  }
`;
export type PotentialLabCategoriesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    PotentialLabCategoriesQuery,
    PotentialLabCategoriesQueryVariables
  >,
  'query'
>;

export const PotentialLabCategoriesComponent = (props: PotentialLabCategoriesComponentProps) => (
  <ApolloReactComponents.Query<PotentialLabCategoriesQuery, PotentialLabCategoriesQueryVariables>
    query={PotentialLabCategoriesDocument}
    {...props}
  />
);

/**
 * __usePotentialLabCategoriesQuery__
 *
 * To run a query within a React component, call `usePotentialLabCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `usePotentialLabCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePotentialLabCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function usePotentialLabCategoriesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    PotentialLabCategoriesQuery,
    PotentialLabCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    PotentialLabCategoriesQuery,
    PotentialLabCategoriesQueryVariables
  >(PotentialLabCategoriesDocument, baseOptions);
}
export function usePotentialLabCategoriesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    PotentialLabCategoriesQuery,
    PotentialLabCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    PotentialLabCategoriesQuery,
    PotentialLabCategoriesQueryVariables
  >(PotentialLabCategoriesDocument, baseOptions);
}
export type PotentialLabCategoriesQueryHookResult = ReturnType<
  typeof usePotentialLabCategoriesQuery
>;
export type PotentialLabCategoriesLazyQueryHookResult = ReturnType<
  typeof usePotentialLabCategoriesLazyQuery
>;
export type PotentialLabCategoriesQueryResult = ApolloReactCommon.QueryResult<
  PotentialLabCategoriesQuery,
  PotentialLabCategoriesQueryVariables
>;
export const PotentialLabContentByCategoryDocument = gql`
  query potentialLabContentByCategory($categoryId: ID) {
    potentialLabContentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      selected
    }
  }
`;
export type PotentialLabContentByCategoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >,
  'query'
>;

export const PotentialLabContentByCategoryComponent = (
  props: PotentialLabContentByCategoryComponentProps
) => (
  <ApolloReactComponents.Query<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >
    query={PotentialLabContentByCategoryDocument}
    {...props}
  />
);

/**
 * __usePotentialLabContentByCategoryQuery__
 *
 * To run a query within a React component, call `usePotentialLabContentByCategoryQuery` and pass it any options that fit your needs.
 * When your component renders, `usePotentialLabContentByCategoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePotentialLabContentByCategoryQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function usePotentialLabContentByCategoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >(PotentialLabContentByCategoryDocument, baseOptions);
}
export function usePotentialLabContentByCategoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    PotentialLabContentByCategoryQuery,
    PotentialLabContentByCategoryQueryVariables
  >(PotentialLabContentByCategoryDocument, baseOptions);
}
export type PotentialLabContentByCategoryQueryHookResult = ReturnType<
  typeof usePotentialLabContentByCategoryQuery
>;
export type PotentialLabContentByCategoryLazyQueryHookResult = ReturnType<
  typeof usePotentialLabContentByCategoryLazyQuery
>;
export type PotentialLabContentByCategoryQueryResult = ApolloReactCommon.QueryResult<
  PotentialLabContentByCategoryQuery,
  PotentialLabContentByCategoryQueryVariables
>;
export const MilestoneCategoriesDocument = gql`
  query milestoneCategories {
    milestoneCategories {
      id
      name
      numberSelected
    }
  }
`;
export type MilestoneCategoriesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MilestoneCategoriesQuery,
    MilestoneCategoriesQueryVariables
  >,
  'query'
>;

export const MilestoneCategoriesComponent = (props: MilestoneCategoriesComponentProps) => (
  <ApolloReactComponents.Query<MilestoneCategoriesQuery, MilestoneCategoriesQueryVariables>
    query={MilestoneCategoriesDocument}
    {...props}
  />
);

/**
 * __useMilestoneCategoriesQuery__
 *
 * To run a query within a React component, call `useMilestoneCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMilestoneCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMilestoneCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useMilestoneCategoriesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MilestoneCategoriesQuery,
    MilestoneCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MilestoneCategoriesQuery, MilestoneCategoriesQueryVariables>(
    MilestoneCategoriesDocument,
    baseOptions
  );
}
export function useMilestoneCategoriesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MilestoneCategoriesQuery,
    MilestoneCategoriesQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MilestoneCategoriesQuery, MilestoneCategoriesQueryVariables>(
    MilestoneCategoriesDocument,
    baseOptions
  );
}
export type MilestoneCategoriesQueryHookResult = ReturnType<typeof useMilestoneCategoriesQuery>;
export type MilestoneCategoriesLazyQueryHookResult = ReturnType<
  typeof useMilestoneCategoriesLazyQuery
>;
export type MilestoneCategoriesQueryResult = ApolloReactCommon.QueryResult<
  MilestoneCategoriesQuery,
  MilestoneCategoriesQueryVariables
>;
export const MilestoneContentByCategoryDocument = gql`
  query milestoneContentByCategory($categoryId: ID) {
    milestoneContentByCategory(contentCategoryId: $categoryId) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      selected
    }
  }
`;
export type MilestoneContentByCategoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >,
  'query'
>;

export const MilestoneContentByCategoryComponent = (
  props: MilestoneContentByCategoryComponentProps
) => (
  <ApolloReactComponents.Query<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >
    query={MilestoneContentByCategoryDocument}
    {...props}
  />
);

/**
 * __useMilestoneContentByCategoryQuery__
 *
 * To run a query within a React component, call `useMilestoneContentByCategoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useMilestoneContentByCategoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMilestoneContentByCategoryQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useMilestoneContentByCategoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >(MilestoneContentByCategoryDocument, baseOptions);
}
export function useMilestoneContentByCategoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    MilestoneContentByCategoryQuery,
    MilestoneContentByCategoryQueryVariables
  >(MilestoneContentByCategoryDocument, baseOptions);
}
export type MilestoneContentByCategoryQueryHookResult = ReturnType<
  typeof useMilestoneContentByCategoryQuery
>;
export type MilestoneContentByCategoryLazyQueryHookResult = ReturnType<
  typeof useMilestoneContentByCategoryLazyQuery
>;
export type MilestoneContentByCategoryQueryResult = ApolloReactCommon.QueryResult<
  MilestoneContentByCategoryQuery,
  MilestoneContentByCategoryQueryVariables
>;
export const MyNotesDocument = gql`
  query myNotes($limit: Int) {
    myNotes(limit: $limit) {
      id
      createdAt
      userId
      contents
    }
  }
`;
export type MyNotesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MyNotesQuery, MyNotesQueryVariables>,
  'query'
>;

export const MyNotesComponent = (props: MyNotesComponentProps) => (
  <ApolloReactComponents.Query<MyNotesQuery, MyNotesQueryVariables>
    query={MyNotesDocument}
    {...props}
  />
);

/**
 * __useMyNotesQuery__
 *
 * To run a query within a React component, call `useMyNotesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyNotesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyNotesQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useMyNotesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<MyNotesQuery, MyNotesQueryVariables>
) {
  return ApolloReactHooks.useQuery<MyNotesQuery, MyNotesQueryVariables>(
    MyNotesDocument,
    baseOptions
  );
}
export function useMyNotesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MyNotesQuery, MyNotesQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<MyNotesQuery, MyNotesQueryVariables>(
    MyNotesDocument,
    baseOptions
  );
}
export type MyNotesQueryHookResult = ReturnType<typeof useMyNotesQuery>;
export type MyNotesLazyQueryHookResult = ReturnType<typeof useMyNotesLazyQuery>;
export type MyNotesQueryResult = ApolloReactCommon.QueryResult<MyNotesQuery, MyNotesQueryVariables>;
export const MyNoteContentByIdDocument = gql`
  query myNoteContentById($id: ID!) {
    myNoteContentById(id: $id) {
      id
      createdAt
      userId
      contents
    }
  }
`;
export type MyNoteContentByIdComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MyNoteContentByIdQuery,
    MyNoteContentByIdQueryVariables
  >,
  'query'
> &
  ({ variables: MyNoteContentByIdQueryVariables; skip?: boolean } | { skip: boolean });

export const MyNoteContentByIdComponent = (props: MyNoteContentByIdComponentProps) => (
  <ApolloReactComponents.Query<MyNoteContentByIdQuery, MyNoteContentByIdQueryVariables>
    query={MyNoteContentByIdDocument}
    {...props}
  />
);

/**
 * __useMyNoteContentByIdQuery__
 *
 * To run a query within a React component, call `useMyNoteContentByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyNoteContentByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyNoteContentByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMyNoteContentByIdQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MyNoteContentByIdQuery,
    MyNoteContentByIdQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MyNoteContentByIdQuery, MyNoteContentByIdQueryVariables>(
    MyNoteContentByIdDocument,
    baseOptions
  );
}
export function useMyNoteContentByIdLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MyNoteContentByIdQuery,
    MyNoteContentByIdQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MyNoteContentByIdQuery, MyNoteContentByIdQueryVariables>(
    MyNoteContentByIdDocument,
    baseOptions
  );
}
export type MyNoteContentByIdQueryHookResult = ReturnType<typeof useMyNoteContentByIdQuery>;
export type MyNoteContentByIdLazyQueryHookResult = ReturnType<typeof useMyNoteContentByIdLazyQuery>;
export type MyNoteContentByIdQueryResult = ApolloReactCommon.QueryResult<
  MyNoteContentByIdQuery,
  MyNoteContentByIdQueryVariables
>;
export const CreateMyNoteDocument = gql`
  mutation createMyNote($contents: String!) {
    createMyNote(contents: $contents) {
      id
      createdAt
      userId
      contents
    }
  }
`;
export type CreateMyNoteMutationFn = ApolloReactCommon.MutationFunction<
  CreateMyNoteMutation,
  CreateMyNoteMutationVariables
>;
export type CreateMyNoteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    CreateMyNoteMutation,
    CreateMyNoteMutationVariables
  >,
  'mutation'
>;

export const CreateMyNoteComponent = (props: CreateMyNoteComponentProps) => (
  <ApolloReactComponents.Mutation<CreateMyNoteMutation, CreateMyNoteMutationVariables>
    mutation={CreateMyNoteDocument}
    {...props}
  />
);

/**
 * __useCreateMyNoteMutation__
 *
 * To run a mutation, you first call `useCreateMyNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMyNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMyNoteMutation, { data, loading, error }] = useCreateMyNoteMutation({
 *   variables: {
 *      contents: // value for 'contents'
 *   },
 * });
 */
export function useCreateMyNoteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateMyNoteMutation,
    CreateMyNoteMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<CreateMyNoteMutation, CreateMyNoteMutationVariables>(
    CreateMyNoteDocument,
    baseOptions
  );
}
export type CreateMyNoteMutationHookResult = ReturnType<typeof useCreateMyNoteMutation>;
export type CreateMyNoteMutationResult = ApolloReactCommon.MutationResult<CreateMyNoteMutation>;
export type CreateMyNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateMyNoteMutation,
  CreateMyNoteMutationVariables
>;
export const DeleteMyNoteDocument = gql`
  mutation deleteMyNote($id: ID!) {
    deleteMyNote(id: $id) {
      id
      status
    }
  }
`;
export type DeleteMyNoteMutationFn = ApolloReactCommon.MutationFunction<
  DeleteMyNoteMutation,
  DeleteMyNoteMutationVariables
>;
export type DeleteMyNoteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DeleteMyNoteMutation,
    DeleteMyNoteMutationVariables
  >,
  'mutation'
>;

export const DeleteMyNoteComponent = (props: DeleteMyNoteComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteMyNoteMutation, DeleteMyNoteMutationVariables>
    mutation={DeleteMyNoteDocument}
    {...props}
  />
);

/**
 * __useDeleteMyNoteMutation__
 *
 * To run a mutation, you first call `useDeleteMyNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMyNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMyNoteMutation, { data, loading, error }] = useDeleteMyNoteMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMyNoteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DeleteMyNoteMutation,
    DeleteMyNoteMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<DeleteMyNoteMutation, DeleteMyNoteMutationVariables>(
    DeleteMyNoteDocument,
    baseOptions
  );
}
export type DeleteMyNoteMutationHookResult = ReturnType<typeof useDeleteMyNoteMutation>;
export type DeleteMyNoteMutationResult = ApolloReactCommon.MutationResult<DeleteMyNoteMutation>;
export type DeleteMyNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteMyNoteMutation,
  DeleteMyNoteMutationVariables
>;
export const EditMyNoteDocument = gql`
  mutation editMyNote($id: ID!, $contents: String!) {
    editMyNote(id: $id, contents: $contents) {
      id
      status
    }
  }
`;
export type EditMyNoteMutationFn = ApolloReactCommon.MutationFunction<
  EditMyNoteMutation,
  EditMyNoteMutationVariables
>;
export type EditMyNoteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<EditMyNoteMutation, EditMyNoteMutationVariables>,
  'mutation'
>;

export const EditMyNoteComponent = (props: EditMyNoteComponentProps) => (
  <ApolloReactComponents.Mutation<EditMyNoteMutation, EditMyNoteMutationVariables>
    mutation={EditMyNoteDocument}
    {...props}
  />
);

/**
 * __useEditMyNoteMutation__
 *
 * To run a mutation, you first call `useEditMyNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditMyNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editMyNoteMutation, { data, loading, error }] = useEditMyNoteMutation({
 *   variables: {
 *      id: // value for 'id'
 *      contents: // value for 'contents'
 *   },
 * });
 */
export function useEditMyNoteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EditMyNoteMutation,
    EditMyNoteMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<EditMyNoteMutation, EditMyNoteMutationVariables>(
    EditMyNoteDocument,
    baseOptions
  );
}
export type EditMyNoteMutationHookResult = ReturnType<typeof useEditMyNoteMutation>;
export type EditMyNoteMutationResult = ApolloReactCommon.MutationResult<EditMyNoteMutation>;
export type EditMyNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EditMyNoteMutation,
  EditMyNoteMutationVariables
>;
export const MaternityResourceContentDocument = gql`
  query maternityResourceContent {
    pregnancyLoss: content(contentType: "resources", contentProgram: "pregnancy-loss") {
      id
      title
      link
      description
      date
      mediumImage
      isFeatured
    }
  }
`;
export type MaternityResourceContentComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >,
  'query'
>;

export const MaternityResourceContentComponent = (
  props: MaternityResourceContentComponentProps
) => (
  <ApolloReactComponents.Query<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >
    query={MaternityResourceContentDocument}
    {...props}
  />
);

/**
 * __useMaternityResourceContentQuery__
 *
 * To run a query within a React component, call `useMaternityResourceContentQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaternityResourceContentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaternityResourceContentQuery({
 *   variables: {
 *   },
 * });
 */
export function useMaternityResourceContentQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >(MaternityResourceContentDocument, baseOptions);
}
export function useMaternityResourceContentLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    MaternityResourceContentQuery,
    MaternityResourceContentQueryVariables
  >(MaternityResourceContentDocument, baseOptions);
}
export type MaternityResourceContentQueryHookResult = ReturnType<
  typeof useMaternityResourceContentQuery
>;
export type MaternityResourceContentLazyQueryHookResult = ReturnType<
  typeof useMaternityResourceContentLazyQuery
>;
export type MaternityResourceContentQueryResult = ApolloReactCommon.QueryResult<
  MaternityResourceContentQuery,
  MaternityResourceContentQueryVariables
>;
export const MyKickTrackerDocument = gql`
  query myKickTracker {
    myKickTracker {
      id
      startAt
      kicks
      duration
      isActive
    }
  }
`;
export type MyKickTrackerComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MyKickTrackerQuery, MyKickTrackerQueryVariables>,
  'query'
>;

export const MyKickTrackerComponent = (props: MyKickTrackerComponentProps) => (
  <ApolloReactComponents.Query<MyKickTrackerQuery, MyKickTrackerQueryVariables>
    query={MyKickTrackerDocument}
    {...props}
  />
);

/**
 * __useMyKickTrackerQuery__
 *
 * To run a query within a React component, call `useMyKickTrackerQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyKickTrackerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyKickTrackerQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyKickTrackerQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<MyKickTrackerQuery, MyKickTrackerQueryVariables>
) {
  return ApolloReactHooks.useQuery<MyKickTrackerQuery, MyKickTrackerQueryVariables>(
    MyKickTrackerDocument,
    baseOptions
  );
}
export function useMyKickTrackerLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MyKickTrackerQuery,
    MyKickTrackerQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MyKickTrackerQuery, MyKickTrackerQueryVariables>(
    MyKickTrackerDocument,
    baseOptions
  );
}
export type MyKickTrackerQueryHookResult = ReturnType<typeof useMyKickTrackerQuery>;
export type MyKickTrackerLazyQueryHookResult = ReturnType<typeof useMyKickTrackerLazyQuery>;
export type MyKickTrackerQueryResult = ApolloReactCommon.QueryResult<
  MyKickTrackerQuery,
  MyKickTrackerQueryVariables
>;
export const CreateMyKickCounterDocument = gql`
  mutation createMyKickCounter(
    $startAt: String!
    $kicks: Int!
    $duration: Int!
    $isActive: Boolean!
  ) {
    createMyKickCounter(
      startAt: $startAt
      kicks: $kicks
      duration: $duration
      isActive: $isActive
    ) {
      id
      startAt
      kicks
      duration
      isActive
    }
  }
`;
export type CreateMyKickCounterMutationFn = ApolloReactCommon.MutationFunction<
  CreateMyKickCounterMutation,
  CreateMyKickCounterMutationVariables
>;
export type CreateMyKickCounterComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    CreateMyKickCounterMutation,
    CreateMyKickCounterMutationVariables
  >,
  'mutation'
>;

export const CreateMyKickCounterComponent = (props: CreateMyKickCounterComponentProps) => (
  <ApolloReactComponents.Mutation<CreateMyKickCounterMutation, CreateMyKickCounterMutationVariables>
    mutation={CreateMyKickCounterDocument}
    {...props}
  />
);

/**
 * __useCreateMyKickCounterMutation__
 *
 * To run a mutation, you first call `useCreateMyKickCounterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMyKickCounterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMyKickCounterMutation, { data, loading, error }] = useCreateMyKickCounterMutation({
 *   variables: {
 *      startAt: // value for 'startAt'
 *      kicks: // value for 'kicks'
 *      duration: // value for 'duration'
 *      isActive: // value for 'isActive'
 *   },
 * });
 */
export function useCreateMyKickCounterMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateMyKickCounterMutation,
    CreateMyKickCounterMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    CreateMyKickCounterMutation,
    CreateMyKickCounterMutationVariables
  >(CreateMyKickCounterDocument, baseOptions);
}
export type CreateMyKickCounterMutationHookResult = ReturnType<
  typeof useCreateMyKickCounterMutation
>;
export type CreateMyKickCounterMutationResult = ApolloReactCommon.MutationResult<
  CreateMyKickCounterMutation
>;
export type CreateMyKickCounterMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateMyKickCounterMutation,
  CreateMyKickCounterMutationVariables
>;
export const DeleteMyKickCounterDocument = gql`
  mutation deleteMyKickCounter($id: ID!) {
    deleteMyKickCounter(id: $id) {
      id
      status
    }
  }
`;
export type DeleteMyKickCounterMutationFn = ApolloReactCommon.MutationFunction<
  DeleteMyKickCounterMutation,
  DeleteMyKickCounterMutationVariables
>;
export type DeleteMyKickCounterComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DeleteMyKickCounterMutation,
    DeleteMyKickCounterMutationVariables
  >,
  'mutation'
>;

export const DeleteMyKickCounterComponent = (props: DeleteMyKickCounterComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteMyKickCounterMutation, DeleteMyKickCounterMutationVariables>
    mutation={DeleteMyKickCounterDocument}
    {...props}
  />
);

/**
 * __useDeleteMyKickCounterMutation__
 *
 * To run a mutation, you first call `useDeleteMyKickCounterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMyKickCounterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMyKickCounterMutation, { data, loading, error }] = useDeleteMyKickCounterMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMyKickCounterMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DeleteMyKickCounterMutation,
    DeleteMyKickCounterMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    DeleteMyKickCounterMutation,
    DeleteMyKickCounterMutationVariables
  >(DeleteMyKickCounterDocument, baseOptions);
}
export type DeleteMyKickCounterMutationHookResult = ReturnType<
  typeof useDeleteMyKickCounterMutation
>;
export type DeleteMyKickCounterMutationResult = ApolloReactCommon.MutationResult<
  DeleteMyKickCounterMutation
>;
export type DeleteMyKickCounterMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteMyKickCounterMutation,
  DeleteMyKickCounterMutationVariables
>;
export const EditMyKickCounterDocument = gql`
  mutation editMyKickCounter(
    $id: ID!
    $startAt: String
    $kicks: Int
    $duration: Int
    $isActive: Boolean
  ) {
    editMyKickCounter(
      id: $id
      startAt: $startAt
      kicks: $kicks
      duration: $duration
      isActive: $isActive
    ) {
      id
      status
    }
  }
`;
export type EditMyKickCounterMutationFn = ApolloReactCommon.MutationFunction<
  EditMyKickCounterMutation,
  EditMyKickCounterMutationVariables
>;
export type EditMyKickCounterComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EditMyKickCounterMutation,
    EditMyKickCounterMutationVariables
  >,
  'mutation'
>;

export const EditMyKickCounterComponent = (props: EditMyKickCounterComponentProps) => (
  <ApolloReactComponents.Mutation<EditMyKickCounterMutation, EditMyKickCounterMutationVariables>
    mutation={EditMyKickCounterDocument}
    {...props}
  />
);

/**
 * __useEditMyKickCounterMutation__
 *
 * To run a mutation, you first call `useEditMyKickCounterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditMyKickCounterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editMyKickCounterMutation, { data, loading, error }] = useEditMyKickCounterMutation({
 *   variables: {
 *      id: // value for 'id'
 *      startAt: // value for 'startAt'
 *      kicks: // value for 'kicks'
 *      duration: // value for 'duration'
 *      isActive: // value for 'isActive'
 *   },
 * });
 */
export function useEditMyKickCounterMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EditMyKickCounterMutation,
    EditMyKickCounterMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EditMyKickCounterMutation,
    EditMyKickCounterMutationVariables
  >(EditMyKickCounterDocument, baseOptions);
}
export type EditMyKickCounterMutationHookResult = ReturnType<typeof useEditMyKickCounterMutation>;
export type EditMyKickCounterMutationResult = ApolloReactCommon.MutationResult<
  EditMyKickCounterMutation
>;
export type EditMyKickCounterMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EditMyKickCounterMutation,
  EditMyKickCounterMutationVariables
>;
export const MaternityOverviewContentDocument = gql`
  query maternityOverviewContent($trimester: Int) {
    maternityOverviewArticleContent(contentTrimester: $trimester) {
      id
      date
      title
      description
      rawHtml
      link
      mediumImage
      thumbnailImage
      isFeatured
      selected
    }
    maternityPromoContent: content(contentType: "Promo", appModule: "Maternity") {
      id
      title
      link
      description
      date
      mediumImage
      thumbnailImage
      isFeatured
    }
    maternityEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      isOnDemand
    }
  }
`;
export type MaternityOverviewContentComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >,
  'query'
>;

export const MaternityOverviewContentComponent = (
  props: MaternityOverviewContentComponentProps
) => (
  <ApolloReactComponents.Query<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >
    query={MaternityOverviewContentDocument}
    {...props}
  />
);

/**
 * __useMaternityOverviewContentQuery__
 *
 * To run a query within a React component, call `useMaternityOverviewContentQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaternityOverviewContentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaternityOverviewContentQuery({
 *   variables: {
 *      trimester: // value for 'trimester'
 *   },
 * });
 */
export function useMaternityOverviewContentQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >(MaternityOverviewContentDocument, baseOptions);
}
export function useMaternityOverviewContentLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    MaternityOverviewContentQuery,
    MaternityOverviewContentQueryVariables
  >(MaternityOverviewContentDocument, baseOptions);
}
export type MaternityOverviewContentQueryHookResult = ReturnType<
  typeof useMaternityOverviewContentQuery
>;
export type MaternityOverviewContentLazyQueryHookResult = ReturnType<
  typeof useMaternityOverviewContentLazyQuery
>;
export type MaternityOverviewContentQueryResult = ApolloReactCommon.QueryResult<
  MaternityOverviewContentQuery,
  MaternityOverviewContentQueryVariables
>;
export const BabySizesDocument = gql`
  query babySizes {
    babySizes {
      id
      title
      mediumImage
      babySize
      babyWeight
    }
  }
`;
export type BabySizesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<BabySizesQuery, BabySizesQueryVariables>,
  'query'
>;

export const BabySizesComponent = (props: BabySizesComponentProps) => (
  <ApolloReactComponents.Query<BabySizesQuery, BabySizesQueryVariables>
    query={BabySizesDocument}
    {...props}
  />
);

/**
 * __useBabySizesQuery__
 *
 * To run a query within a React component, call `useBabySizesQuery` and pass it any options that fit your needs.
 * When your component renders, `useBabySizesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useBabySizesQuery({
 *   variables: {
 *   },
 * });
 */
export function useBabySizesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<BabySizesQuery, BabySizesQueryVariables>
) {
  return ApolloReactHooks.useQuery<BabySizesQuery, BabySizesQueryVariables>(
    BabySizesDocument,
    baseOptions
  );
}
export function useBabySizesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<BabySizesQuery, BabySizesQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<BabySizesQuery, BabySizesQueryVariables>(
    BabySizesDocument,
    baseOptions
  );
}
export type BabySizesQueryHookResult = ReturnType<typeof useBabySizesQuery>;
export type BabySizesLazyQueryHookResult = ReturnType<typeof useBabySizesLazyQuery>;
export type BabySizesQueryResult = ApolloReactCommon.QueryResult<
  BabySizesQuery,
  BabySizesQueryVariables
>;
export const PredefinedBabyItemsTodosListDocument = gql`
  query predefinedBabyItemsTodosList {
    predefinedBabyItemsTodosList {
      id
      contentCategoryId
      selected
      title
    }
  }
`;
export type PredefinedBabyItemsTodosListComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >,
  'query'
>;

export const PredefinedBabyItemsTodosListComponent = (
  props: PredefinedBabyItemsTodosListComponentProps
) => (
  <ApolloReactComponents.Query<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >
    query={PredefinedBabyItemsTodosListDocument}
    {...props}
  />
);

/**
 * __usePredefinedBabyItemsTodosListQuery__
 *
 * To run a query within a React component, call `usePredefinedBabyItemsTodosListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePredefinedBabyItemsTodosListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePredefinedBabyItemsTodosListQuery({
 *   variables: {
 *   },
 * });
 */
export function usePredefinedBabyItemsTodosListQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >(PredefinedBabyItemsTodosListDocument, baseOptions);
}
export function usePredefinedBabyItemsTodosListLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    PredefinedBabyItemsTodosListQuery,
    PredefinedBabyItemsTodosListQueryVariables
  >(PredefinedBabyItemsTodosListDocument, baseOptions);
}
export type PredefinedBabyItemsTodosListQueryHookResult = ReturnType<
  typeof usePredefinedBabyItemsTodosListQuery
>;
export type PredefinedBabyItemsTodosListLazyQueryHookResult = ReturnType<
  typeof usePredefinedBabyItemsTodosListLazyQuery
>;
export type PredefinedBabyItemsTodosListQueryResult = ApolloReactCommon.QueryResult<
  PredefinedBabyItemsTodosListQuery,
  PredefinedBabyItemsTodosListQueryVariables
>;
export const PredefinedDeliveryTimeTodosListDocument = gql`
  query predefinedDeliveryTimeTodosList {
    predefinedDeliveryTimeTodosList {
      id
      contentCategoryId
      selected
      title
    }
  }
`;
export type PredefinedDeliveryTimeTodosListComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >,
  'query'
>;

export const PredefinedDeliveryTimeTodosListComponent = (
  props: PredefinedDeliveryTimeTodosListComponentProps
) => (
  <ApolloReactComponents.Query<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >
    query={PredefinedDeliveryTimeTodosListDocument}
    {...props}
  />
);

/**
 * __usePredefinedDeliveryTimeTodosListQuery__
 *
 * To run a query within a React component, call `usePredefinedDeliveryTimeTodosListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePredefinedDeliveryTimeTodosListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePredefinedDeliveryTimeTodosListQuery({
 *   variables: {
 *   },
 * });
 */
export function usePredefinedDeliveryTimeTodosListQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >(PredefinedDeliveryTimeTodosListDocument, baseOptions);
}
export function usePredefinedDeliveryTimeTodosListLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    PredefinedDeliveryTimeTodosListQuery,
    PredefinedDeliveryTimeTodosListQueryVariables
  >(PredefinedDeliveryTimeTodosListDocument, baseOptions);
}
export type PredefinedDeliveryTimeTodosListQueryHookResult = ReturnType<
  typeof usePredefinedDeliveryTimeTodosListQuery
>;
export type PredefinedDeliveryTimeTodosListLazyQueryHookResult = ReturnType<
  typeof usePredefinedDeliveryTimeTodosListLazyQuery
>;
export type PredefinedDeliveryTimeTodosListQueryResult = ApolloReactCommon.QueryResult<
  PredefinedDeliveryTimeTodosListQuery,
  PredefinedDeliveryTimeTodosListQueryVariables
>;
export const TodoSectionDocument = gql`
  query todoSection {
    todoSection {
      title
      description
      todoSubSection {
        title
        description
        data {
          id
          title
          selected
        }
      }
    }
  }
`;
export type TodoSectionComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<TodoSectionQuery, TodoSectionQueryVariables>,
  'query'
>;

export const TodoSectionComponent = (props: TodoSectionComponentProps) => (
  <ApolloReactComponents.Query<TodoSectionQuery, TodoSectionQueryVariables>
    query={TodoSectionDocument}
    {...props}
  />
);

/**
 * __useTodoSectionQuery__
 *
 * To run a query within a React component, call `useTodoSectionQuery` and pass it any options that fit your needs.
 * When your component renders, `useTodoSectionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTodoSectionQuery({
 *   variables: {
 *   },
 * });
 */
export function useTodoSectionQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<TodoSectionQuery, TodoSectionQueryVariables>
) {
  return ApolloReactHooks.useQuery<TodoSectionQuery, TodoSectionQueryVariables>(
    TodoSectionDocument,
    baseOptions
  );
}
export function useTodoSectionLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<TodoSectionQuery, TodoSectionQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<TodoSectionQuery, TodoSectionQueryVariables>(
    TodoSectionDocument,
    baseOptions
  );
}
export type TodoSectionQueryHookResult = ReturnType<typeof useTodoSectionQuery>;
export type TodoSectionLazyQueryHookResult = ReturnType<typeof useTodoSectionLazyQuery>;
export type TodoSectionQueryResult = ApolloReactCommon.QueryResult<
  TodoSectionQuery,
  TodoSectionQueryVariables
>;
export const MyDeliveryTimeTodoListDocument = gql`
  query myDeliveryTimeTodoList {
    deliveryTimeTodoList {
      id
      title
    }
  }
`;
export type MyDeliveryTimeTodoListComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MyDeliveryTimeTodoListQuery,
    MyDeliveryTimeTodoListQueryVariables
  >,
  'query'
>;

export const MyDeliveryTimeTodoListComponent = (props: MyDeliveryTimeTodoListComponentProps) => (
  <ApolloReactComponents.Query<MyDeliveryTimeTodoListQuery, MyDeliveryTimeTodoListQueryVariables>
    query={MyDeliveryTimeTodoListDocument}
    {...props}
  />
);

/**
 * __useMyDeliveryTimeTodoListQuery__
 *
 * To run a query within a React component, call `useMyDeliveryTimeTodoListQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyDeliveryTimeTodoListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyDeliveryTimeTodoListQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyDeliveryTimeTodoListQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MyDeliveryTimeTodoListQuery,
    MyDeliveryTimeTodoListQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    MyDeliveryTimeTodoListQuery,
    MyDeliveryTimeTodoListQueryVariables
  >(MyDeliveryTimeTodoListDocument, baseOptions);
}
export function useMyDeliveryTimeTodoListLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MyDeliveryTimeTodoListQuery,
    MyDeliveryTimeTodoListQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    MyDeliveryTimeTodoListQuery,
    MyDeliveryTimeTodoListQueryVariables
  >(MyDeliveryTimeTodoListDocument, baseOptions);
}
export type MyDeliveryTimeTodoListQueryHookResult = ReturnType<
  typeof useMyDeliveryTimeTodoListQuery
>;
export type MyDeliveryTimeTodoListLazyQueryHookResult = ReturnType<
  typeof useMyDeliveryTimeTodoListLazyQuery
>;
export type MyDeliveryTimeTodoListQueryResult = ApolloReactCommon.QueryResult<
  MyDeliveryTimeTodoListQuery,
  MyDeliveryTimeTodoListQueryVariables
>;
export const MyBabyItemsTodoListDocument = gql`
  query myBabyItemsTodoList {
    babyItemsTodoList {
      id
      title
    }
  }
`;
export type MyBabyItemsTodoListComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MyBabyItemsTodoListQuery,
    MyBabyItemsTodoListQueryVariables
  >,
  'query'
>;

export const MyBabyItemsTodoListComponent = (props: MyBabyItemsTodoListComponentProps) => (
  <ApolloReactComponents.Query<MyBabyItemsTodoListQuery, MyBabyItemsTodoListQueryVariables>
    query={MyBabyItemsTodoListDocument}
    {...props}
  />
);

/**
 * __useMyBabyItemsTodoListQuery__
 *
 * To run a query within a React component, call `useMyBabyItemsTodoListQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyBabyItemsTodoListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyBabyItemsTodoListQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyBabyItemsTodoListQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MyBabyItemsTodoListQuery,
    MyBabyItemsTodoListQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MyBabyItemsTodoListQuery, MyBabyItemsTodoListQueryVariables>(
    MyBabyItemsTodoListDocument,
    baseOptions
  );
}
export function useMyBabyItemsTodoListLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MyBabyItemsTodoListQuery,
    MyBabyItemsTodoListQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MyBabyItemsTodoListQuery, MyBabyItemsTodoListQueryVariables>(
    MyBabyItemsTodoListDocument,
    baseOptions
  );
}
export type MyBabyItemsTodoListQueryHookResult = ReturnType<typeof useMyBabyItemsTodoListQuery>;
export type MyBabyItemsTodoListLazyQueryHookResult = ReturnType<
  typeof useMyBabyItemsTodoListLazyQuery
>;
export type MyBabyItemsTodoListQueryResult = ApolloReactCommon.QueryResult<
  MyBabyItemsTodoListQuery,
  MyBabyItemsTodoListQueryVariables
>;
export const RecordDeliveryTimeTodoListItemDocument = gql`
  mutation recordDeliveryTimeTodoListItem($item: TodoInput) {
    recordDeliveryTimeTodoListItem(item: $item) {
      id
      title
    }
  }
`;
export type RecordDeliveryTimeTodoListItemMutationFn = ApolloReactCommon.MutationFunction<
  RecordDeliveryTimeTodoListItemMutation,
  RecordDeliveryTimeTodoListItemMutationVariables
>;
export type RecordDeliveryTimeTodoListItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    RecordDeliveryTimeTodoListItemMutation,
    RecordDeliveryTimeTodoListItemMutationVariables
  >,
  'mutation'
>;

export const RecordDeliveryTimeTodoListItemComponent = (
  props: RecordDeliveryTimeTodoListItemComponentProps
) => (
  <ApolloReactComponents.Mutation<
    RecordDeliveryTimeTodoListItemMutation,
    RecordDeliveryTimeTodoListItemMutationVariables
  >
    mutation={RecordDeliveryTimeTodoListItemDocument}
    {...props}
  />
);

/**
 * __useRecordDeliveryTimeTodoListItemMutation__
 *
 * To run a mutation, you first call `useRecordDeliveryTimeTodoListItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRecordDeliveryTimeTodoListItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [recordDeliveryTimeTodoListItemMutation, { data, loading, error }] = useRecordDeliveryTimeTodoListItemMutation({
 *   variables: {
 *      item: // value for 'item'
 *   },
 * });
 */
export function useRecordDeliveryTimeTodoListItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    RecordDeliveryTimeTodoListItemMutation,
    RecordDeliveryTimeTodoListItemMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    RecordDeliveryTimeTodoListItemMutation,
    RecordDeliveryTimeTodoListItemMutationVariables
  >(RecordDeliveryTimeTodoListItemDocument, baseOptions);
}
export type RecordDeliveryTimeTodoListItemMutationHookResult = ReturnType<
  typeof useRecordDeliveryTimeTodoListItemMutation
>;
export type RecordDeliveryTimeTodoListItemMutationResult = ApolloReactCommon.MutationResult<
  RecordDeliveryTimeTodoListItemMutation
>;
export type RecordDeliveryTimeTodoListItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RecordDeliveryTimeTodoListItemMutation,
  RecordDeliveryTimeTodoListItemMutationVariables
>;
export const RecordBabyItemsTodoListItemDocument = gql`
  mutation recordBabyItemsTodoListItem($item: TodoInput) {
    recordBabyItemsTodoListItem(item: $item) {
      id
      title
    }
  }
`;
export type RecordBabyItemsTodoListItemMutationFn = ApolloReactCommon.MutationFunction<
  RecordBabyItemsTodoListItemMutation,
  RecordBabyItemsTodoListItemMutationVariables
>;
export type RecordBabyItemsTodoListItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    RecordBabyItemsTodoListItemMutation,
    RecordBabyItemsTodoListItemMutationVariables
  >,
  'mutation'
>;

export const RecordBabyItemsTodoListItemComponent = (
  props: RecordBabyItemsTodoListItemComponentProps
) => (
  <ApolloReactComponents.Mutation<
    RecordBabyItemsTodoListItemMutation,
    RecordBabyItemsTodoListItemMutationVariables
  >
    mutation={RecordBabyItemsTodoListItemDocument}
    {...props}
  />
);

/**
 * __useRecordBabyItemsTodoListItemMutation__
 *
 * To run a mutation, you first call `useRecordBabyItemsTodoListItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRecordBabyItemsTodoListItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [recordBabyItemsTodoListItemMutation, { data, loading, error }] = useRecordBabyItemsTodoListItemMutation({
 *   variables: {
 *      item: // value for 'item'
 *   },
 * });
 */
export function useRecordBabyItemsTodoListItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    RecordBabyItemsTodoListItemMutation,
    RecordBabyItemsTodoListItemMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    RecordBabyItemsTodoListItemMutation,
    RecordBabyItemsTodoListItemMutationVariables
  >(RecordBabyItemsTodoListItemDocument, baseOptions);
}
export type RecordBabyItemsTodoListItemMutationHookResult = ReturnType<
  typeof useRecordBabyItemsTodoListItemMutation
>;
export type RecordBabyItemsTodoListItemMutationResult = ApolloReactCommon.MutationResult<
  RecordBabyItemsTodoListItemMutation
>;
export type RecordBabyItemsTodoListItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RecordBabyItemsTodoListItemMutation,
  RecordBabyItemsTodoListItemMutationVariables
>;
export const MinimumAppVersionDocument = gql`
  query minimumAppVersion {
    minimumAppVersion {
      ios
      android
    }
  }
`;
export type MinimumAppVersionComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    MinimumAppVersionQuery,
    MinimumAppVersionQueryVariables
  >,
  'query'
>;

export const MinimumAppVersionComponent = (props: MinimumAppVersionComponentProps) => (
  <ApolloReactComponents.Query<MinimumAppVersionQuery, MinimumAppVersionQueryVariables>
    query={MinimumAppVersionDocument}
    {...props}
  />
);

/**
 * __useMinimumAppVersionQuery__
 *
 * To run a query within a React component, call `useMinimumAppVersionQuery` and pass it any options that fit your needs.
 * When your component renders, `useMinimumAppVersionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMinimumAppVersionQuery({
 *   variables: {
 *   },
 * });
 */
export function useMinimumAppVersionQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    MinimumAppVersionQuery,
    MinimumAppVersionQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<MinimumAppVersionQuery, MinimumAppVersionQueryVariables>(
    MinimumAppVersionDocument,
    baseOptions
  );
}
export function useMinimumAppVersionLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    MinimumAppVersionQuery,
    MinimumAppVersionQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<MinimumAppVersionQuery, MinimumAppVersionQueryVariables>(
    MinimumAppVersionDocument,
    baseOptions
  );
}
export type MinimumAppVersionQueryHookResult = ReturnType<typeof useMinimumAppVersionQuery>;
export type MinimumAppVersionLazyQueryHookResult = ReturnType<typeof useMinimumAppVersionLazyQuery>;
export type MinimumAppVersionQueryResult = ApolloReactCommon.QueryResult<
  MinimumAppVersionQuery,
  MinimumAppVersionQueryVariables
>;
export const PostDocument = gql`
  query post($postId: ID) {
    post(id: $postId) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
  }
`;
export type PostComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<PostQuery, PostQueryVariables>,
  'query'
>;

export const PostComponent = (props: PostComponentProps) => (
  <ApolloReactComponents.Query<PostQuery, PostQueryVariables> query={PostDocument} {...props} />
);

/**
 * __usePostQuery__
 *
 * To run a query within a React component, call `usePostQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostQuery({
 *   variables: {
 *      postId: // value for 'postId'
 *   },
 * });
 */
export function usePostQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<PostQuery, PostQueryVariables>
) {
  return ApolloReactHooks.useQuery<PostQuery, PostQueryVariables>(PostDocument, baseOptions);
}
export function usePostLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PostQuery, PostQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<PostQuery, PostQueryVariables>(PostDocument, baseOptions);
}
export type PostQueryHookResult = ReturnType<typeof usePostQuery>;
export type PostLazyQueryHookResult = ReturnType<typeof usePostLazyQuery>;
export type PostQueryResult = ApolloReactCommon.QueryResult<PostQuery, PostQueryVariables>;
export const HomeScreenContentDocument = gql`
  query homeScreenContent {
    posts(includeFeatured: false, limit: 15) {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
    featuredPosts {
      id
      title
      date
      description
      mediumImage
      isFeatured
      rawHtml
    }
    featuredEventGroups {
      name
      description
      mediumImage
      rawHtml
    }
    featuredOnDemandEvents {
      description
      duration
      endTime
      eventCategoryId
      eventId
      id
      name
      postDate
      postId
      startTime
      mediumImage
      youtubeLiveLink
      rawHtml
    }
  }
`;
export type HomeScreenContentComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    HomeScreenContentQuery,
    HomeScreenContentQueryVariables
  >,
  'query'
>;

export const HomeScreenContentComponent = (props: HomeScreenContentComponentProps) => (
  <ApolloReactComponents.Query<HomeScreenContentQuery, HomeScreenContentQueryVariables>
    query={HomeScreenContentDocument}
    {...props}
  />
);

/**
 * __useHomeScreenContentQuery__
 *
 * To run a query within a React component, call `useHomeScreenContentQuery` and pass it any options that fit your needs.
 * When your component renders, `useHomeScreenContentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHomeScreenContentQuery({
 *   variables: {
 *   },
 * });
 */
export function useHomeScreenContentQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    HomeScreenContentQuery,
    HomeScreenContentQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<HomeScreenContentQuery, HomeScreenContentQueryVariables>(
    HomeScreenContentDocument,
    baseOptions
  );
}
export function useHomeScreenContentLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    HomeScreenContentQuery,
    HomeScreenContentQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<HomeScreenContentQuery, HomeScreenContentQueryVariables>(
    HomeScreenContentDocument,
    baseOptions
  );
}
export type HomeScreenContentQueryHookResult = ReturnType<typeof useHomeScreenContentQuery>;
export type HomeScreenContentLazyQueryHookResult = ReturnType<typeof useHomeScreenContentLazyQuery>;
export type HomeScreenContentQueryResult = ApolloReactCommon.QueryResult<
  HomeScreenContentQuery,
  HomeScreenContentQueryVariables
>;
export const LoginDocument = gql`
  mutation login($email: String, $password: String) {
    login(email: $email, password: $password) {
      token
      user {
        id
        email
      }
    }
  }
`;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<
  LoginMutation,
  LoginMutationVariables
>;
export type LoginComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<LoginMutation, LoginMutationVariables>,
  'mutation'
>;

export const LoginComponent = (props: LoginComponentProps) => (
  <ApolloReactComponents.Mutation<LoginMutation, LoginMutationVariables>
    mutation={LoginDocument}
    {...props}
  />
);

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>
) {
  return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(
    LoginDocument,
    baseOptions
  );
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const MeDocument = gql`
  query ME {
    me {
      id
      email
      firstName
      lastName
      tokenExpirationDate
      device {
        FCMToken
      }
      maternityProfile {
        userId
        role
        lastMenstrualPeriod
        estimatedDueDate
        hasPregnancyLoss
        gestationalAge {
          weeks
          days
          trimester
          currentBabySize {
            description
            image
          }
        }
      }
    }
  }
`;
export type MeComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<MeQuery, MeQueryVariables>,
  'query'
>;

export const MeComponent = (props: MeComponentProps) => (
  <ApolloReactComponents.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
);

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<MeQuery, MeQueryVariables>
) {
  return ApolloReactHooks.useQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
}
export function useMeLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MeQuery, MeQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = ApolloReactCommon.QueryResult<MeQuery, MeQueryVariables>;
export const SaveDeviceDocument = gql`
  mutation saveDevice($fcmToken: String) {
    createDevice(FCMToken: $fcmToken) {
      id
      userId
      FCMToken
    }
  }
`;
export type SaveDeviceMutationFn = ApolloReactCommon.MutationFunction<
  SaveDeviceMutation,
  SaveDeviceMutationVariables
>;
export type SaveDeviceComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<SaveDeviceMutation, SaveDeviceMutationVariables>,
  'mutation'
>;

export const SaveDeviceComponent = (props: SaveDeviceComponentProps) => (
  <ApolloReactComponents.Mutation<SaveDeviceMutation, SaveDeviceMutationVariables>
    mutation={SaveDeviceDocument}
    {...props}
  />
);

/**
 * __useSaveDeviceMutation__
 *
 * To run a mutation, you first call `useSaveDeviceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSaveDeviceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [saveDeviceMutation, { data, loading, error }] = useSaveDeviceMutation({
 *   variables: {
 *      fcmToken: // value for 'fcmToken'
 *   },
 * });
 */
export function useSaveDeviceMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    SaveDeviceMutation,
    SaveDeviceMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<SaveDeviceMutation, SaveDeviceMutationVariables>(
    SaveDeviceDocument,
    baseOptions
  );
}
export type SaveDeviceMutationHookResult = ReturnType<typeof useSaveDeviceMutation>;
export type SaveDeviceMutationResult = ApolloReactCommon.MutationResult<SaveDeviceMutation>;
export type SaveDeviceMutationOptions = ApolloReactCommon.BaseMutationOptions<
  SaveDeviceMutation,
  SaveDeviceMutationVariables
>;
export const SignupDocument = gql`
  mutation signup($signupPayload: SignupPayload) {
    signup(signupPayload: $signupPayload) {
      token
      user {
        id
        email
      }
    }
  }
`;
export type SignupMutationFn = ApolloReactCommon.MutationFunction<
  SignupMutation,
  SignupMutationVariables
>;
export type SignupComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<SignupMutation, SignupMutationVariables>,
  'mutation'
>;

export const SignupComponent = (props: SignupComponentProps) => (
  <ApolloReactComponents.Mutation<SignupMutation, SignupMutationVariables>
    mutation={SignupDocument}
    {...props}
  />
);

/**
 * __useSignupMutation__
 *
 * To run a mutation, you first call `useSignupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signupMutation, { data, loading, error }] = useSignupMutation({
 *   variables: {
 *      signupPayload: // value for 'signupPayload'
 *   },
 * });
 */
export function useSignupMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<SignupMutation, SignupMutationVariables>
) {
  return ApolloReactHooks.useMutation<SignupMutation, SignupMutationVariables>(
    SignupDocument,
    baseOptions
  );
}
export type SignupMutationHookResult = ReturnType<typeof useSignupMutation>;
export type SignupMutationResult = ApolloReactCommon.MutationResult<SignupMutation>;
export type SignupMutationOptions = ApolloReactCommon.BaseMutationOptions<
  SignupMutation,
  SignupMutationVariables
>;
export const NotificationPreferencesDocument = gql`
  query notificationPreferences {
    notificationPreferences {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type NotificationPreferencesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    NotificationPreferencesQuery,
    NotificationPreferencesQueryVariables
  >,
  'query'
>;

export const NotificationPreferencesComponent = (props: NotificationPreferencesComponentProps) => (
  <ApolloReactComponents.Query<NotificationPreferencesQuery, NotificationPreferencesQueryVariables>
    query={NotificationPreferencesDocument}
    {...props}
  />
);

/**
 * __useNotificationPreferencesQuery__
 *
 * To run a query within a React component, call `useNotificationPreferencesQuery` and pass it any options that fit your needs.
 * When your component renders, `useNotificationPreferencesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useNotificationPreferencesQuery({
 *   variables: {
 *   },
 * });
 */
export function useNotificationPreferencesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    NotificationPreferencesQuery,
    NotificationPreferencesQueryVariables
  >
) {
  return ApolloReactHooks.useQuery<
    NotificationPreferencesQuery,
    NotificationPreferencesQueryVariables
  >(NotificationPreferencesDocument, baseOptions);
}
export function useNotificationPreferencesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    NotificationPreferencesQuery,
    NotificationPreferencesQueryVariables
  >
) {
  return ApolloReactHooks.useLazyQuery<
    NotificationPreferencesQuery,
    NotificationPreferencesQueryVariables
  >(NotificationPreferencesDocument, baseOptions);
}
export type NotificationPreferencesQueryHookResult = ReturnType<
  typeof useNotificationPreferencesQuery
>;
export type NotificationPreferencesLazyQueryHookResult = ReturnType<
  typeof useNotificationPreferencesLazyQuery
>;
export type NotificationPreferencesQueryResult = ApolloReactCommon.QueryResult<
  NotificationPreferencesQuery,
  NotificationPreferencesQueryVariables
>;
export const EnableNotificationsDocument = gql`
  mutation enableNotifications {
    enableNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type EnableNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  EnableNotificationsMutation,
  EnableNotificationsMutationVariables
>;
export type EnableNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EnableNotificationsMutation,
    EnableNotificationsMutationVariables
  >,
  'mutation'
>;

export const EnableNotificationsComponent = (props: EnableNotificationsComponentProps) => (
  <ApolloReactComponents.Mutation<EnableNotificationsMutation, EnableNotificationsMutationVariables>
    mutation={EnableNotificationsDocument}
    {...props}
  />
);

/**
 * __useEnableNotificationsMutation__
 *
 * To run a mutation, you first call `useEnableNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEnableNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [enableNotificationsMutation, { data, loading, error }] = useEnableNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useEnableNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EnableNotificationsMutation,
    EnableNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EnableNotificationsMutation,
    EnableNotificationsMutationVariables
  >(EnableNotificationsDocument, baseOptions);
}
export type EnableNotificationsMutationHookResult = ReturnType<
  typeof useEnableNotificationsMutation
>;
export type EnableNotificationsMutationResult = ApolloReactCommon.MutationResult<
  EnableNotificationsMutation
>;
export type EnableNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EnableNotificationsMutation,
  EnableNotificationsMutationVariables
>;
export const DisableNotificationsDocument = gql`
  mutation disableNotifications {
    disableNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type DisableNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  DisableNotificationsMutation,
  DisableNotificationsMutationVariables
>;
export type DisableNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DisableNotificationsMutation,
    DisableNotificationsMutationVariables
  >,
  'mutation'
>;

export const DisableNotificationsComponent = (props: DisableNotificationsComponentProps) => (
  <ApolloReactComponents.Mutation<
    DisableNotificationsMutation,
    DisableNotificationsMutationVariables
  >
    mutation={DisableNotificationsDocument}
    {...props}
  />
);

/**
 * __useDisableNotificationsMutation__
 *
 * To run a mutation, you first call `useDisableNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisableNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disableNotificationsMutation, { data, loading, error }] = useDisableNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useDisableNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DisableNotificationsMutation,
    DisableNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    DisableNotificationsMutation,
    DisableNotificationsMutationVariables
  >(DisableNotificationsDocument, baseOptions);
}
export type DisableNotificationsMutationHookResult = ReturnType<
  typeof useDisableNotificationsMutation
>;
export type DisableNotificationsMutationResult = ApolloReactCommon.MutationResult<
  DisableNotificationsMutation
>;
export type DisableNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DisableNotificationsMutation,
  DisableNotificationsMutationVariables
>;
export const EnableClassReminderNotificationsDocument = gql`
  mutation enableClassReminderNotifications {
    enableClassReminderNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type EnableClassReminderNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  EnableClassReminderNotificationsMutation,
  EnableClassReminderNotificationsMutationVariables
>;
export type EnableClassReminderNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EnableClassReminderNotificationsMutation,
    EnableClassReminderNotificationsMutationVariables
  >,
  'mutation'
>;

export const EnableClassReminderNotificationsComponent = (
  props: EnableClassReminderNotificationsComponentProps
) => (
  <ApolloReactComponents.Mutation<
    EnableClassReminderNotificationsMutation,
    EnableClassReminderNotificationsMutationVariables
  >
    mutation={EnableClassReminderNotificationsDocument}
    {...props}
  />
);

/**
 * __useEnableClassReminderNotificationsMutation__
 *
 * To run a mutation, you first call `useEnableClassReminderNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEnableClassReminderNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [enableClassReminderNotificationsMutation, { data, loading, error }] = useEnableClassReminderNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useEnableClassReminderNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EnableClassReminderNotificationsMutation,
    EnableClassReminderNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EnableClassReminderNotificationsMutation,
    EnableClassReminderNotificationsMutationVariables
  >(EnableClassReminderNotificationsDocument, baseOptions);
}
export type EnableClassReminderNotificationsMutationHookResult = ReturnType<
  typeof useEnableClassReminderNotificationsMutation
>;
export type EnableClassReminderNotificationsMutationResult = ApolloReactCommon.MutationResult<
  EnableClassReminderNotificationsMutation
>;
export type EnableClassReminderNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EnableClassReminderNotificationsMutation,
  EnableClassReminderNotificationsMutationVariables
>;
export const DisableClassReminderNotificationsDocument = gql`
  mutation disableClassReminderNotifications {
    disableClassReminderNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type DisableClassReminderNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  DisableClassReminderNotificationsMutation,
  DisableClassReminderNotificationsMutationVariables
>;
export type DisableClassReminderNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DisableClassReminderNotificationsMutation,
    DisableClassReminderNotificationsMutationVariables
  >,
  'mutation'
>;

export const DisableClassReminderNotificationsComponent = (
  props: DisableClassReminderNotificationsComponentProps
) => (
  <ApolloReactComponents.Mutation<
    DisableClassReminderNotificationsMutation,
    DisableClassReminderNotificationsMutationVariables
  >
    mutation={DisableClassReminderNotificationsDocument}
    {...props}
  />
);

/**
 * __useDisableClassReminderNotificationsMutation__
 *
 * To run a mutation, you first call `useDisableClassReminderNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisableClassReminderNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disableClassReminderNotificationsMutation, { data, loading, error }] = useDisableClassReminderNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useDisableClassReminderNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DisableClassReminderNotificationsMutation,
    DisableClassReminderNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    DisableClassReminderNotificationsMutation,
    DisableClassReminderNotificationsMutationVariables
  >(DisableClassReminderNotificationsDocument, baseOptions);
}
export type DisableClassReminderNotificationsMutationHookResult = ReturnType<
  typeof useDisableClassReminderNotificationsMutation
>;
export type DisableClassReminderNotificationsMutationResult = ApolloReactCommon.MutationResult<
  DisableClassReminderNotificationsMutation
>;
export type DisableClassReminderNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DisableClassReminderNotificationsMutation,
  DisableClassReminderNotificationsMutationVariables
>;
export const EnableMaternityNotificationsDocument = gql`
  mutation enableMaternityNotifications {
    enableMaternityNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type EnableMaternityNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  EnableMaternityNotificationsMutation,
  EnableMaternityNotificationsMutationVariables
>;
export type EnableMaternityNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    EnableMaternityNotificationsMutation,
    EnableMaternityNotificationsMutationVariables
  >,
  'mutation'
>;

export const EnableMaternityNotificationsComponent = (
  props: EnableMaternityNotificationsComponentProps
) => (
  <ApolloReactComponents.Mutation<
    EnableMaternityNotificationsMutation,
    EnableMaternityNotificationsMutationVariables
  >
    mutation={EnableMaternityNotificationsDocument}
    {...props}
  />
);

/**
 * __useEnableMaternityNotificationsMutation__
 *
 * To run a mutation, you first call `useEnableMaternityNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEnableMaternityNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [enableMaternityNotificationsMutation, { data, loading, error }] = useEnableMaternityNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useEnableMaternityNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    EnableMaternityNotificationsMutation,
    EnableMaternityNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    EnableMaternityNotificationsMutation,
    EnableMaternityNotificationsMutationVariables
  >(EnableMaternityNotificationsDocument, baseOptions);
}
export type EnableMaternityNotificationsMutationHookResult = ReturnType<
  typeof useEnableMaternityNotificationsMutation
>;
export type EnableMaternityNotificationsMutationResult = ApolloReactCommon.MutationResult<
  EnableMaternityNotificationsMutation
>;
export type EnableMaternityNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EnableMaternityNotificationsMutation,
  EnableMaternityNotificationsMutationVariables
>;
export const DisableMaternityNotificationsDocument = gql`
  mutation disableMaternityNotifications {
    disableMaternityNotifications {
      userId
      notificationsEnabled
      classReminderNotificationsEnabled
      maternityNotificationsEnabled
    }
  }
`;
export type DisableMaternityNotificationsMutationFn = ApolloReactCommon.MutationFunction<
  DisableMaternityNotificationsMutation,
  DisableMaternityNotificationsMutationVariables
>;
export type DisableMaternityNotificationsComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DisableMaternityNotificationsMutation,
    DisableMaternityNotificationsMutationVariables
  >,
  'mutation'
>;

export const DisableMaternityNotificationsComponent = (
  props: DisableMaternityNotificationsComponentProps
) => (
  <ApolloReactComponents.Mutation<
    DisableMaternityNotificationsMutation,
    DisableMaternityNotificationsMutationVariables
  >
    mutation={DisableMaternityNotificationsDocument}
    {...props}
  />
);

/**
 * __useDisableMaternityNotificationsMutation__
 *
 * To run a mutation, you first call `useDisableMaternityNotificationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisableMaternityNotificationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disableMaternityNotificationsMutation, { data, loading, error }] = useDisableMaternityNotificationsMutation({
 *   variables: {
 *   },
 * });
 */
export function useDisableMaternityNotificationsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DisableMaternityNotificationsMutation,
    DisableMaternityNotificationsMutationVariables
  >
) {
  return ApolloReactHooks.useMutation<
    DisableMaternityNotificationsMutation,
    DisableMaternityNotificationsMutationVariables
  >(DisableMaternityNotificationsDocument, baseOptions);
}
export type DisableMaternityNotificationsMutationHookResult = ReturnType<
  typeof useDisableMaternityNotificationsMutation
>;
export type DisableMaternityNotificationsMutationResult = ApolloReactCommon.MutationResult<
  DisableMaternityNotificationsMutation
>;
export type DisableMaternityNotificationsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DisableMaternityNotificationsMutation,
  DisableMaternityNotificationsMutationVariables
>;
export const WebUrlsDocument = gql`
  query webUrls {
    webUrls {
      foothillAppointment
      maternityPreAdmission
      passwordReset
      selfAssessment
      telehealth
    }
  }
`;
export type WebUrlsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<WebUrlsQuery, WebUrlsQueryVariables>,
  'query'
>;

export const WebUrlsComponent = (props: WebUrlsComponentProps) => (
  <ApolloReactComponents.Query<WebUrlsQuery, WebUrlsQueryVariables>
    query={WebUrlsDocument}
    {...props}
  />
);

/**
 * __useWebUrlsQuery__
 *
 * To run a query within a React component, call `useWebUrlsQuery` and pass it any options that fit your needs.
 * When your component renders, `useWebUrlsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWebUrlsQuery({
 *   variables: {
 *   },
 * });
 */
export function useWebUrlsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<WebUrlsQuery, WebUrlsQueryVariables>
) {
  return ApolloReactHooks.useQuery<WebUrlsQuery, WebUrlsQueryVariables>(
    WebUrlsDocument,
    baseOptions
  );
}
export function useWebUrlsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<WebUrlsQuery, WebUrlsQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<WebUrlsQuery, WebUrlsQueryVariables>(
    WebUrlsDocument,
    baseOptions
  );
}
export type WebUrlsQueryHookResult = ReturnType<typeof useWebUrlsQuery>;
export type WebUrlsLazyQueryHookResult = ReturnType<typeof useWebUrlsLazyQuery>;
export type WebUrlsQueryResult = ApolloReactCommon.QueryResult<WebUrlsQuery, WebUrlsQueryVariables>;

// color names from https://www.htmlcsscolor.com/hex/[HEXVALUE]
const aqua = '#5C8ADB';
const balticSea = '#343A40';
const balticSea50 = 'rgba(52, 58, 64, 0.5)';
const blackTransparent = 'rgba(18, 18, 18, 0.9)';
const blueTransparent = 'rgba(77,142,206, 0.10)';
const bouquet = '#A77E96';
const buttermilk = '#F4E3A6';
const cabaret = '#D54B68';
const cerulean = '#0066A6';
const chino = '#AFA78A';
const cinnabar = '#E63B3A';
const circleReview = '#083B79';
const cornflowerBlue = '#50A0FF';
const cornflowerBlue2 = '#66A4EE';
const cornflowerBlue3 = '#7394EE';
const crusta = '#F98549';
const deepBlush = '#EC708A';
const denim = '#2172D5';
const eclipse = '#3f3f3f';
const gainsboro = '#E2E2E2';
const gold = '#EED104';
const gray = '#8E908F';
const grayTransparent = 'rgba(142, 144, 143, 0.7)';
const green = '#1E8449';
const lightGreen = '#97EF8E';
const lightGray = '#ebebeb';
const lineGray = '#dbdbdb';
const lightSalmon = '#e896a2';
const malibu = '#61B2F5';
const mediumOrchid = '#B248FF';
const myChartReview = '#882338';
const offYellow = '#F9F3E2';
const oxley = '#64A47F';
const pearlLusta = '#E8DFC6';
const pictonBlue = '#4891CE';
const royalBlue = '#5072D2';
const salmon = '#EE9E74';
const scooter = '#267FA1';
const silver = '#C4C4C4';
const sky = '#E9F1FB';
const skyBlue = '#63B1F5';
const snow = '#FAFAFA';
const solitude = '#DFE4ED';
const solitudeLight = '#ECEFF4';
const strikemaster = '#926980';
const summerSky = '#1FC2E0';
const sunsetOrange = '#F04B4B';
const tangerineYellow = '#F0CE07';
const terra = '#E0601F';
const transparent = 'rgba(0, 0, 0, 0)';
const turquoiseBlue = '#5CCCDB';
const viking = '#41ADCA';
const violet = '#E87DDD';
const vistaBlue = '#9ED0B0';
const wellDReview = '#0F4250';
const wellDScooter = '#2D8AA3';
const wewak = '#E896A2';
const whisper = '#E4E4E4';
const white = '#fff';
const whiteSmoke = '#F0F0F0';
const whiteSmoke2 = '#FBFBFB';
const whiteSmokeLight = '#F8F8F8';
const whiteTransparent = 'rgba(255,255,255, 0.97)';
const wisteria = '#C883DC';
const youtubeRed = '#B71719';

export const colors = {
  aqua,
  balticSea,
  balticSea50,
  black: balticSea,
  blackTransparent,
  blueTransparent,
  bouquet,
  buttermilk,
  cerulean,
  chino,
  cinnabar,
  circleReview,
  cornflowerBlue,
  cornflowerBlue2,
  cornflowerBlue3,
  crusta,
  denim,
  eclipse,
  gainsboro,
  gold,
  gray,
  grayTransparent,
  green,
  lightGreen,
  malibu,
  mediumOrchid,
  myChartReview,
  offYellow,
  oxley,
  pearlLusta,
  pictonBlue,
  royalBlue,
  salmon,
  scooter,
  silver,
  sky,
  skyBlue,
  snow,
  solitude,
  solitudeLight,
  strikemaster,
  summerSky,
  sunsetOrange,
  tangerineYellow,
  terra,
  transparent,
  turquoiseBlue,
  violet,
  vistaBlue,
  wellDReview,
  wewak,
  whisper,
  white,
  whiteSmoke,
  whiteSmoke2,
  whiteSmokeLight,
  whiteTransparent,
  wisteria,
  youtubeRed,
  classes: mediumOrchid,
  disabled: silver,
  grayText: eclipse,
  news: malibu,
  primary: scooter,
  lightGray,
  toDoActive: lightSalmon,
  trimesterActive: lightSalmon,
  lineGray,
  deepBlush,
  //status colors
  error: sunsetOrange,
  success: lightGreen,
  warning: crusta,
};

export const gradients = {
  circle: [cornflowerBlue, denim],
  mychart: [deepBlush, cabaret],
  wellD: [viking, wellDScooter],
  solitude: [solitudeLight, solitude],
  whiteSolitudeLight: [white, solitudeLight],
  blackTransparentClear: [blackTransparent, transparent],
};

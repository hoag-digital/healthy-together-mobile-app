const bold = {
  fontFamily: 'Helvetica',
};

const regular = {
  fontFamily: 'Helvetica',
};

const semibold = {
  fontFamily: 'Helvetica',
};

const medium = {
  fontFamily: 'Helvetica',
};

const light = {
  fontFamily: 'HelveticaNeue-Light',
};

export const fonts = {
  regular,
  semibold,
  bold,
  medium,
  light,
};

export const fontSizes = [8, 12, 14, 16, 20, 24, 32, 48, 64, 72, 96];
export const lineHeights = fontSizes.map(fs => fs * 1.2);
export const weights = [100, 200, 300, 400, 500, 600, 700, 800, 900];

import account from './account';
import biocircuit from './biocircuit';
import care from './care';
import classes from './classes';
import errors from './errors';
import filter from './filter';
import home from './home';
import login from './login';
import maternity from './maternity';
import navigation from './navigation';
import news from './news';
import programs from './programs';
import registration from './registration';
import schedule from './schedule';
import sort from './sort';

export default {
  account,
  biocircuit,
  care,
  classes,
  errors,
  filter,
  home,
  login,
  maternity,
  navigation,
  news,
  programs,
  registration,
  schedule,
  sort,
};

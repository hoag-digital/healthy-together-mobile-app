export default {
  welcome: 'Welcome, please\nsign in.',
  login: 'Login',
  password: 'Password',
  passwordReset: 'Password Reset',
  email: 'Email',
  buttons: {
    login: 'Login',
    cancel: 'Cancel',
    forgotPassword: 'Forgot Password?',
    createAccount: 'Create Account',
  },
  error: 'Error Logging In, please try again',
};

export default {
  home: 'Home',
  care: 'Care',
  classes: 'Classes',
  myHoag: 'My Hoag',
  programs: 'Programs',
  schedule: 'Schedule',
};

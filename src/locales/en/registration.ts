export default {
  welcome: "Let's create your account!",
  fullName: 'Full name',
  firstName: 'First Name',
  lastName: 'Last Name',
  password: 'Password',
  passwordConfirmation: 'Password Confirmation',
  email: 'Email',
  errors: {
    email: 'Account already exists',
  },
  buttons: {
    createAccount: 'Create Account',
  },
  validation: {
    match: 'Passwords do not match',
    rules: `Password must contain the following:
    *  Uppercase
    *  Lowercase
    *  Number
    *  8 character minimum`,
  },
};

export default {
  buttons: {
    ok: 'OK',
  },
  classCapacity: 'This class is at capacity',
  classTokenExpired: 'This class requires a valid token.',
  general: 'There was an error processing your request, please try again',
};

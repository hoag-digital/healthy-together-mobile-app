export default {
  name: 'Name',
  notifications: 'Notifications',
  notificationsEnabledStatement: 'Notifications are enabled',
  notificationsDisabledStatement: 'Notifications are disabled.',
  notificationModuleSettingsStatement: 'Configure your individual notifications:',
  classReminderNotifications: 'Class reminder notifications',
  maternityNotifications: 'Maternity notifications',
  reportPregnancyLoss: 'Report a Loss',
};

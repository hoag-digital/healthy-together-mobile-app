export default {
  welcome: 'Welcome to Hoag.',
  name: 'Healthy Together',
  mantra: 'Hoag Lifestyle Program',
};

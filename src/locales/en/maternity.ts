export default {
  name: 'Maternity',
  birthplan: {
    title: 'My Birthplan',
    share: {
      title: 'My Birthplan',
      message: 'Here is a copy of my birthplan.',
    },
    explanation:
      'Every birth is unique and unpredictable. A good birth plan written in advance will likely have minor and possibly major adjustments come delivery day. That’s why the best plan should include preferences in case things don’t go according to plan. It’s important to understand Hoag’s policies before delivery and talk through your plan with your healthcare provider. Start selecting your birth plan preferences here.',
  },
  onboarding: {
    welcome: {
      title: 'WELCOME TO',
      subtitle: 'HOAG MATERNITY',
    },
    buttons: {
      next: 'Next',
      back: 'Back',
      cancel: 'Cancel',
    },
    firstStep: {
      title: 'Are you...',
      pregnantOption: 'already pregnant',
      newParentOption: 'a new parent',
    },
    secondStep: {
      title: 'How far along\nare you?',
      lastMenstrual: 'Last menstrual period',
      or: 'or',
      estimatedDueDate: 'Estimated due date',
    },
    thirdStep: {
      title: 'Pre-Admission\nRegistration',
      subtitle: 'Would you like to sign up\nfor the pre-admission\nregistration form?',
      buttons: {
        no: 'No',
        signup: 'Sign up',
      },
    },
    customization: {
      line1: `Customizing\nyour experience.`,
      line2: 'Enjoy the app!',
    },
  },
  overview: {
    babyline: 'Call Baby Line',
    preRegistration: 'Pre-Registration',
  },
  unknownRole: {
    title: 'Oh no!',
    line1: `We're missing some of your maternity information.`,
    line2: `Visit your account settings\nto complete your profile.`,
  },
  notes: {
    createNote: {
      placeholder: 'Write your notes here...',
      saveLabel: 'Save',
      savingLabel: 'Saving...',
    },
    listView: {
      screenTitle: 'Notes',
      contentTitle: 'Recent Entries:',
      viewAll: 'View all',
    },
    editNote: {
      screenTitle: 'Edit Note',
      updateLabel: 'Update',
      updatingLabel: 'Updating',
    },
  },
  pregnancyLoss: {
    letter: `Dear Parents and Family,\n
We know this is a painful time and one you were not prepared for, and we extend our heartfelt sympathy. During this time you may be experiencing a variety of emotions and reactions that may be difficult to understand or control. It may also be difficult to think clearly and make decisions. We hope this site provides some understanding about the emotions you will experience as you grieve for the loss of your baby. We hope this information is helpful as you think through the decisions you will be faced with in the days ahead.\n
You may be presented with a number of decisions to make regarding your baby, depending upon the stage of your pregnancy at the time of loss. These may include naming your baby, finding out the gender of your baby, viewing and holding your baby, and receiving a photo or other memento. You may have specific cultural or religious practices we can help with if possible. We encourage you to ask questions and take time making these decisions and if you change your mind, just let us know. In addition, we have included some information to assist you in making funeral or memorial service arrangements.\n
As you go through the grieving and healing process, you will find that friends and family will want to help, however, they may not know the best way to lend their support. This site includes helpful information for them as well.\n
If you are reading this information after leaving the hospital and have questions or need additional support, please contact Hoag's Baby Line at 949-764-2229.\n
Hoag has resources to assist you, including support groups, social services and pastoral care services. Please do not hesitate to approach our staff with questions or concerns about your feelings, what to expect, and what decisions need to be made.\n
In deepest sympathy,
Hoag Memorial Hospital Presbyterian
    `,
    resetButton: 'I no longer wish to see this content',
  },
  todo: {
    title: `Action Items`,
    description: `Please have the items below checked off for the best delivery experience possible.`,
  },
  tracking: {
    title: "Track your baby's kicks",
    buttons: {
      cancel: 'Cancel',
      delete: 'Delete',
      confirm: 'Confirm',
      save: 'Save',
      saving: 'Saving',
      update: 'Update',
      updating: 'Updating',
      addOutsideEntry: 'Add outside entry',
      kickCounterStart: 'Tap to start',
      kickCounterAdd: 'Tap to add',
      kickCounterStop: 'Tap to stop',
    },
    field: {
      placeholder: {
        date: 'Date & Time',
        duration: 'Duration(in seconds)',
        kicks: 'Kicks',
      },
    },
    edit: {
      screenTitle: 'Edit Kick Tracking',
    },
    addManual: {
      screenTitle: 'Add Outside Entry',
    },
    table: {
      header: {
        start: 'Start',
        kicks: 'Kicks',
        duration: 'Duration',
        dateTime: 'Date & Time',
      },
    },
    error: {
      startCounter: 'An error has occured, please tap again to start the counter!!',
    },
    alert: {
      delete: {
        notAllowedOnCurrentSession: "You are tracking this counter, so can't delete it!!",
        confirmation: {
          title: 'Delete Kick Tracking Record',
          description: 'Are you sure you want to delete?',
        },
      },
      edit: {
        notAllowedOnCurrentSession: "You are tracking this counter, so can't edit it!!",
      },
      manualAdd: {
        alreadyActiveSession: 'You are already in tracking mode!!',
      },
      allFieldsRequired: 'All fields are required!!',
    },
  },
  tabDescription: {
    testDescription:
      'Ensuring the health of you and your baby will include testing specific to each stage of pregnancy. Speak to your healthcare provider if you have any questions or concerns.',
    milestoneDescription: 'Learn more about how you and your baby are developing each month.',
  },
};

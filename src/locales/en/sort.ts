export default {
  label: 'Sort',
  greatestDuration: 'Greatest Duration',
  leastDuration: 'Least Duration',
  mostRecent: 'Most Recent',
  oldestFirst: 'Oldest First',
};

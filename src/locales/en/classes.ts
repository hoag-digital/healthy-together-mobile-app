export default {
  categories: {
    header: 'Class Categories',
  },
  classes: {
    header: 'Classes',
    noneForFilters: 'There are no classes matching the active filters.',
    noneAvailable: 'There are no classes available.',
  },
  livestream: {
    header: 'Livestream Classes',
  },
  tabs: {
    livestream: 'Live Stream',
    onDemand: 'On-Demand',
  },
  waiver: {
    header: 'Waiver',
    clear: 'Clear',
    agree: 'Agree',
    date: 'Date',
  },
  introduction: {
    skip: 'Skip',
  },
  confirmation: {
    title: 'Your class has been scheduled.',
    subtitle: 'Thank you!',
    primaryActionLabel: 'My Schedule',
    secondaryActionLabel: 'Back to my classes',
  },
  title: 'Class',
  details: {
    label: 'Classes',
    liveStreamLabel: 'Live Stream',
  },
  onDemand: {
    header: 'On-Demand',
  },
  onDemandClass: {
    watchVideo: 'Watch Video',
    label: 'On-Demand',
  },
};

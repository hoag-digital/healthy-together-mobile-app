export default {
  categories: {
    header: 'Programs',
  },
  waiver: {
    header: 'Waiver',
    clear: 'Clear',
    agree: 'Agree',
    date: 'Date',
  },
  introduction: {
    skip: 'View Program',
    personalizedExperience: {
      title: 'Personalized Experience',
      copy:
        'Hoag Health Center Foothill Ranch will be equipped with a state-of-the-art BIOCIRCUIT ™ studio, which will be the first-of-its-kind on the West Coast.',
    },
    optimizeYourHealth: {
      title: 'Optimize Your Health',
      copy:
        'With a personal trainer on-hand to encourage and refine your excercise prescription, you can achieve your health goals with a plan designed every step of the way just for you.',
    },
  },
  token: {
    modal: {
      text: `Please enter your code to gain access\nto the BIOCIRCUIT program.`,
      button: 'Validate',
      error: 'Error Validating Token, Please Try Again',
    },
  },
  biocircuit: {
    title: 'Biocircuit',
    description: 'LOREM IPSUM',
  },
  title: 'Program',
  confirmation: {
    message: 'Your program has been scheduled.',
    backlabel: 'Back to program',
  },
};

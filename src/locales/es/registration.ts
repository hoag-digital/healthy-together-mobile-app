export default {
  welcome: '¡Creemos su cuenta!',
  fullName: 'Nombre completo',
  firstName: 'Nombre de pila',
  lastName: 'Apellido',
  password: 'Contraseña',
  passwordConfirmation: 'Confirmación de contraseña',
  email: 'correo electrónico',
  buttons: {
    createAccount: 'Crear una cuenta',
  },
  validation: {
    match: 'Las contraseñas no coinciden',
    rules: `La contraseña debe contener lo siguiente:
              * Mayúsculas
              * Minúsculas
              * Número
              * 8 caracteres mínimo`,
  },
};

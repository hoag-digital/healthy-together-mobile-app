export default {
  home: 'Casa',
  care: 'Cuidado',
  classes: 'Clases',
  media: 'Medios de comunicación',
  programs: 'Programas',
  schedule: 'Programar',
};

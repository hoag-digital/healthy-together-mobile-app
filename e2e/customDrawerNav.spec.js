describe('Healthy Together', () => {
  beforeAll(() => {
    device.reloadReactNative();
  });

  it('should have screen header with drawer icon', () => {
    expect(element(by.id('drawer-icon'))).toBeVisible();
  });

  describe('drawer', () => {
    it('should have drawer items', () => {
      element(by.id('drawer-icon')).tap();
      expect(element(by.id('drawer-items'))).toBeVisible();
    });

    it('should have Hoag Contact Info', () => {
      expect(element(by.id('hoag-contact'))).toBeVisible();
      expect(element(by.id('hoag-address'))).toHaveText(
        'One Hoag Drive\nP.O. Box 6100\nNewport Beach, CA\n92658-6100'
      );

      expect(element(by.id('hoag-phone'))).toHaveText('949-764-HOAG(4624)');
      expect(element(by.id('hoag-website'))).toHaveText('hoagfoothillranch.com');
    });
  });
});

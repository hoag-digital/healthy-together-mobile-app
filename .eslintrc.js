module.exports = {
  root: true,
  extends: ['plugin:echobind/react-native'],
  settings:{
    'import/ignore':[
      //prevent ESLint error that does not like the node_modules index.js file interface imports
      'react-native'
    ]
  },
  rules: {
    'react/jsx-curly-brace-presence': [2, { props: 'never', children: 'never' }],
    'import/no-unresolved': [
      'error',
      {
        // Prevents ESLint from thinking it cant resolve an svg
        ignore: ['.svg'],
      },
    ],
  },
};
